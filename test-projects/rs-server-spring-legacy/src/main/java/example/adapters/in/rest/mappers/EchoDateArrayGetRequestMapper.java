package example.adapters.in.rest.mappers;

import example.adapters.in.rest.dtos.EchoDateArrayGetRrequest;
import example.commons.SerializationUtils;
import example.ports.in.rest.dtos.EchoDateArrayGetRequest;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoDateArrayGetRequestMapper {

    public EchoDateArrayGetRrequest map2EchoDateArrayGetRrequest(List<String> dateArray) {
        return new EchoDateArrayGetRrequest().setDateArray(SerializationUtils.transformList(dateArray, v -> v));
    }

    public EchoDateArrayGetRequest map2EchoDateArrayGetRequest(EchoDateArrayGetRrequest value) {
        return value == null
                ? null
                : new EchoDateArrayGetRequest().setDateArray(
                        SerializationUtils.transformList(value.getDateArray(), SerializationUtils::toLocalDate));
    }

}
