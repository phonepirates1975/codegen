package example.adapters.in.rest;

import example.adapters.in.rest.dtos.EchoArraysBodyRdto;
import example.adapters.in.rest.dtos.EchoBodyRdto;
import example.adapters.in.rest.dtos.EchoDefaultsBodyRdto;
import example.adapters.in.rest.mappers.DeleteFileRequestMapper;
import example.adapters.in.rest.mappers.DeleteFileResponseMapper;
import example.adapters.in.rest.mappers.DownloadEchoFileRequestMapper;
import example.adapters.in.rest.mappers.DownloadEchoFileResponseMapper;
import example.adapters.in.rest.mappers.EchoArraysPostRequestMapper;
import example.adapters.in.rest.mappers.EchoArraysPostResponseMapper;
import example.adapters.in.rest.mappers.EchoDateArrayGetRequestMapper;
import example.adapters.in.rest.mappers.EchoDateArrayGetResponseMapper;
import example.adapters.in.rest.mappers.EchoDefaultsPostRequestMapper;
import example.adapters.in.rest.mappers.EchoDefaultsPostResponseMapper;
import example.adapters.in.rest.mappers.EchoEmptyRequestMapper;
import example.adapters.in.rest.mappers.EchoEmptyResponseMapper;
import example.adapters.in.rest.mappers.EchoErrorRequestMapper;
import example.adapters.in.rest.mappers.EchoErrorResponseMapper;
import example.adapters.in.rest.mappers.EchoGetRequestMapper;
import example.adapters.in.rest.mappers.EchoGetResponseMapper;
import example.adapters.in.rest.mappers.EchoPostRequestMapper;
import example.adapters.in.rest.mappers.EchoPostResponseMapper;
import example.adapters.in.rest.mappers.UploadEchoFileRequestMapper;
import example.adapters.in.rest.mappers.UploadEchoFileResponseMapper;
import example.adapters.in.rest.mappers.UploadEchoFileWithFormRequestMapper;
import example.adapters.in.rest.mappers.UploadEchoFileWithFormResponseMapper;
import example.adapters.in.rest.validators.DeleteFileValidator;
import example.adapters.in.rest.validators.DownloadEchoFileValidator;
import example.adapters.in.rest.validators.EchoArraysPostValidator;
import example.adapters.in.rest.validators.EchoDateArrayGetValidator;
import example.adapters.in.rest.validators.EchoDefaultsPostValidator;
import example.adapters.in.rest.validators.EchoEmptyValidator;
import example.adapters.in.rest.validators.EchoErrorValidator;
import example.adapters.in.rest.validators.EchoGetValidator;
import example.adapters.in.rest.validators.EchoPostValidator;
import example.adapters.in.rest.validators.UploadEchoFileValidator;
import example.adapters.in.rest.validators.UploadEchoFileWithFormValidator;
import example.commons.RestRequestHandler;
import example.commons.validator.ValidationResultMapper;
import example.ports.in.rest.EchoFacade;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@Generated("pl.metaprogramming.codegen")
public class EchoController {

    @Autowired
    private EchoPostRequestMapper echoPostRequestMapper;
    @Autowired
    private EchoPostValidator echoPostValidator;
    @Autowired
    private ValidationResultMapper validationResultMapper;
    @Autowired
    private EchoFacade echoFacade;
    @Autowired
    private EchoPostResponseMapper echoPostResponseMapper;
    @Autowired
    private EchoGetRequestMapper echoGetRequestMapper;
    @Autowired
    private EchoGetValidator echoGetValidator;
    @Autowired
    private EchoGetResponseMapper echoGetResponseMapper;
    @Autowired
    private EchoDefaultsPostRequestMapper echoDefaultsPostRequestMapper;
    @Autowired
    private EchoDefaultsPostValidator echoDefaultsPostValidator;
    @Autowired
    private EchoDefaultsPostResponseMapper echoDefaultsPostResponseMapper;
    @Autowired
    private EchoArraysPostRequestMapper echoArraysPostRequestMapper;
    @Autowired
    private EchoArraysPostValidator echoArraysPostValidator;
    @Autowired
    private EchoArraysPostResponseMapper echoArraysPostResponseMapper;
    @Autowired
    private EchoDateArrayGetRequestMapper echoDateArrayGetRequestMapper;
    @Autowired
    private EchoDateArrayGetValidator echoDateArrayGetValidator;
    @Autowired
    private EchoDateArrayGetResponseMapper echoDateArrayGetResponseMapper;
    @Autowired
    private UploadEchoFileWithFormRequestMapper uploadEchoFileWithFormRequestMapper;
    @Autowired
    private UploadEchoFileWithFormValidator uploadEchoFileWithFormValidator;
    @Autowired
    private UploadEchoFileWithFormResponseMapper uploadEchoFileWithFormResponseMapper;
    @Autowired
    private UploadEchoFileRequestMapper uploadEchoFileRequestMapper;
    @Autowired
    private UploadEchoFileValidator uploadEchoFileValidator;
    @Autowired
    private UploadEchoFileResponseMapper uploadEchoFileResponseMapper;
    @Autowired
    private DownloadEchoFileRequestMapper downloadEchoFileRequestMapper;
    @Autowired
    private DownloadEchoFileValidator downloadEchoFileValidator;
    @Autowired
    private DownloadEchoFileResponseMapper downloadEchoFileResponseMapper;
    @Autowired
    private DeleteFileRequestMapper deleteFileRequestMapper;
    @Autowired
    private DeleteFileValidator deleteFileValidator;
    @Autowired
    private DeleteFileResponseMapper deleteFileResponseMapper;
    @Autowired
    private EchoEmptyRequestMapper echoEmptyRequestMapper;
    @Autowired
    private EchoEmptyValidator echoEmptyValidator;
    @Autowired
    private EchoEmptyResponseMapper echoEmptyResponseMapper;
    @Autowired
    private EchoErrorRequestMapper echoErrorRequestMapper;
    @Autowired
    private EchoErrorValidator echoErrorValidator;
    @Autowired
    private EchoErrorResponseMapper echoErrorResponseMapper;

    @PostMapping(value = "/api/v1/echo", produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity echoPost(@RequestHeader(value = "Authorization", required = false) String authorizationParam,
            @RequestHeader(value = "X-Correlation-ID", required = false) String correlationIdParam,
            @RequestHeader(value = "timestamp", required = false) String timestampParam,
            @RequestHeader(value = "Inline-Header-Param", required = false) String inlineHeaderParam,
            @RequestBody EchoBodyRdto requestBody) {
        return RestRequestHandler.handle(
                echoPostRequestMapper.map2EchoPostRrequest(authorizationParam, correlationIdParam, timestampParam,
                        inlineHeaderParam, requestBody),
                echoPostValidator::validate, validationResultMapper::map, echoPostRequestMapper::map2EchoPostRequest,
                echoFacade::echoPost, echoPostResponseMapper::map);
    }

    @GetMapping(value = "/api/v1/echo", produces = {"application/json"})
    public ResponseEntity echoGet(
            @RequestHeader(value = "X-Correlation-ID", required = false) String correlationIdParam,
            @RequestParam(value = "prop_int_required", required = false) String propIntRequired) {
        return RestRequestHandler.handle(echoGetRequestMapper.map2EchoGetRrequest(correlationIdParam, propIntRequired),
                echoGetValidator::validate, validationResultMapper::map, echoGetRequestMapper::map2EchoGetRequest,
                echoFacade::echoGet, echoGetResponseMapper::map);
    }

    @PostMapping(value = "/api/v1/echo-defaults", produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity echoDefaultsPost(
            @RequestHeader(value = "Default-Header-Param", required = false) String defaultHeaderParam,
            @RequestBody EchoDefaultsBodyRdto requestBody) {
        return RestRequestHandler.handle(
                echoDefaultsPostRequestMapper.map2EchoDefaultsPostRrequest(defaultHeaderParam, requestBody),
                echoDefaultsPostValidator::validate, validationResultMapper::map,
                echoDefaultsPostRequestMapper::map2EchoDefaultsPostRequest, echoFacade::echoDefaultsPost,
                echoDefaultsPostResponseMapper::map);
    }

    @PostMapping(value = "/api/v1/echo-arrays", produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity echoArraysPost(
            @RequestHeader(value = "Authorization", required = false) String authorizationParam,
            @RequestHeader(value = "Inline-Header-Param", required = false) String inlineHeaderParam,
            @RequestBody List<EchoArraysBodyRdto> requestBody) {
        return RestRequestHandler.handle(
                echoArraysPostRequestMapper.map2EchoArraysPostRrequest(authorizationParam, inlineHeaderParam,
                        requestBody),
                echoArraysPostValidator::validate, validationResultMapper::map,
                echoArraysPostRequestMapper::map2EchoArraysPostRequest, echoFacade::echoArraysPost,
                echoArraysPostResponseMapper::map);
    }

    @GetMapping(value = "/api/v1/echo-date-array", produces = {"application/json"})
    public ResponseEntity echoDateArrayGet(
            @RequestParam(value = "date_array", required = false) List<String> dateArray) {
        return RestRequestHandler.handle(echoDateArrayGetRequestMapper.map2EchoDateArrayGetRrequest(dateArray),
                echoDateArrayGetValidator::validate, validationResultMapper::map,
                echoDateArrayGetRequestMapper::map2EchoDateArrayGetRequest, echoFacade::echoDateArrayGet,
                echoDateArrayGetResponseMapper::map);
    }

    /**
     * upload file using multipart/form-data
     */
    @PostMapping(value = "/api/v1/echo-file/{id}/form", produces = {"application/json"}, consumes = {
            "multipart/form-data"})
    public ResponseEntity uploadEchoFileWithForm(@PathVariable(required = false) String id,
            @RequestPart(required = false) MultipartFile file) {
        return RestRequestHandler.handle(
                uploadEchoFileWithFormRequestMapper.map2UploadEchoFileWithFormRrequest(id, file),
                uploadEchoFileWithFormValidator::validate, validationResultMapper::map,
                uploadEchoFileWithFormRequestMapper::map2UploadEchoFileWithFormRequest,
                echoFacade::uploadEchoFileWithForm, uploadEchoFileWithFormResponseMapper::map);
    }

    /**
     * upload file - not supported by OAS2
     */
    @PostMapping(value = "/api/v1/echo-file/{id}", produces = {"application/json"}, consumes = {
            "application/octet-stream"})
    public ResponseEntity uploadEchoFile(@PathVariable(required = false) String id, @RequestBody Resource requestBody) {
        return RestRequestHandler.handle(uploadEchoFileRequestMapper.map2UploadEchoFileRrequest(id, requestBody),
                uploadEchoFileValidator::validate, validationResultMapper::map,
                uploadEchoFileRequestMapper::map2UploadEchoFileRequest, echoFacade::uploadEchoFile,
                uploadEchoFileResponseMapper::map);
    }

    /**
     * Returns a file
     */
    @GetMapping(value = "/api/v1/echo-file/{id}", produces = {"image/jpeg", "application/json"})
    public ResponseEntity downloadEchoFile(@PathVariable(required = false) String id) {
        return RestRequestHandler.handle(downloadEchoFileRequestMapper.map2DownloadEchoFileRrequest(id),
                downloadEchoFileValidator::validate, validationResultMapper::map,
                downloadEchoFileRequestMapper::map2DownloadEchoFileRequest, echoFacade::downloadEchoFile,
                downloadEchoFileResponseMapper::map);
    }

    /**
     * deletes a file based on the id supplied
     */
    @DeleteMapping(value = "/api/v1/echo-file/{id}", produces = {"application/json"})
    public ResponseEntity deleteFile(@PathVariable(required = false) String id) {
        return RestRequestHandler.handle(deleteFileRequestMapper.map2DeleteFileRrequest(id),
                deleteFileValidator::validate, validationResultMapper::map,
                deleteFileRequestMapper::map2DeleteFileRequest, echoFacade::deleteFile, deleteFileResponseMapper::map);
    }

    @GetMapping(value = "/api/v1/echo-empty", produces = {"application/json"})
    public ResponseEntity echoEmpty() {
        return RestRequestHandler.handle(echoEmptyRequestMapper.map2EchoEmptyRrequest(), echoEmptyValidator::validate,
                validationResultMapper::map, echoEmptyRequestMapper::map2EchoEmptyRequest, echoFacade::echoEmpty,
                echoEmptyResponseMapper::map);
    }

    @GetMapping(value = "/api/v1/echo-error", produces = {"application/json"})
    public ResponseEntity echoError(@RequestParam(required = false) String errorMessage) {
        return RestRequestHandler.handle(echoErrorRequestMapper.map2EchoErrorRrequest(errorMessage),
                echoErrorValidator::validate, validationResultMapper::map, echoErrorRequestMapper::map2EchoErrorRequest,
                echoFacade::echoError, echoErrorResponseMapper::map);
    }

}
