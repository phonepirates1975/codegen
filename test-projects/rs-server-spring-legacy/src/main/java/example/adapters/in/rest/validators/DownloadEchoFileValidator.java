package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.DownloadEchoFileRrequest;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.dtos.DownloadEchoFileRrequest.*;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class DownloadEchoFileValidator extends Validator<DownloadEchoFileRrequest> {

    public void check(ValidationContext<DownloadEchoFileRrequest> ctx) {
        ctx.check(FIELD_ID, required(), INT64);
    }

}
