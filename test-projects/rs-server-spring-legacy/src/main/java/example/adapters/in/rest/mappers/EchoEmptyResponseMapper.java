package example.adapters.in.rest.mappers;

import example.ports.in.rest.dtos.EchoEmptyResponse;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoEmptyResponseMapper {

    public ResponseEntity map(@Nonnull EchoEmptyResponse response) {
        ResponseEntity.BodyBuilder responseBuilder = ResponseEntity.status(response.getStatus());
        response.getHeaders().forEach(responseBuilder::header);
        return responseBuilder.build();
    }

}
