/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.in.rest.validators;

import example.commons.validator.Checker;
import example.commons.validator.ValidationContext;
import example.commons.validator.ValidationError;
import org.springframework.stereotype.Component;

@Component
public class AuthorizationChecker implements Checker<String> {

    @Override
    public void check(ValidationContext<String> context) {
        if (context.getValue() == null || !context.getValue().startsWith("Bearer ")) {
            context.addError(ValidationError.builder()
                    .code("invalid_token")
                    .message("invalid_token")
                    .status(403)
                    .stopValidation(true)
                    .build());
        }
    }

    @Override
    public boolean checkNull() {
        return true;
    }
}
