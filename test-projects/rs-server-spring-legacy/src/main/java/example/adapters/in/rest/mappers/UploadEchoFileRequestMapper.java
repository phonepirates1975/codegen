package example.adapters.in.rest.mappers;

import example.adapters.in.rest.dtos.UploadEchoFileRrequest;
import example.commons.SerializationUtils;
import example.ports.in.rest.dtos.UploadEchoFileRequest;
import javax.annotation.Generated;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileRequestMapper {

    public UploadEchoFileRrequest map2UploadEchoFileRrequest(String id, Resource requestBody) {
        return new UploadEchoFileRrequest().setId(id).setRequestBody(requestBody);
    }

    public UploadEchoFileRequest map2UploadEchoFileRequest(UploadEchoFileRrequest value) {
        return value == null
                ? null
                : new UploadEchoFileRequest().setId(SerializationUtils.toLong(value.getId()))
                        .setRequestBody(SerializationUtils.toBytes(value.getRequestBody()));
    }

}
