/*
 * Copyright (c) 2020 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoBodyRdto;
import example.adapters.in.rest.dtos.EchoPostRrequest;
import example.commons.validator.ValidationContext;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class ValidationBeanFactory {

    @AllArgsConstructor
    static class UserData {
        BigDecimal minAmount;
        BigDecimal maxAmount;
    }

    public UserDataValidationBean createUserDataValidationBean(ValidationContext<EchoPostRrequest> context) {
        return new UserDataValidationBean(context, this::fetchUserData);
    }

    public ExtendedObjectEnumChecker createExtendedObjectEnumChecker(ValidationContext<EchoBodyRdto> context) {
        return new ExtendedObjectEnumChecker(context);
    }


    private UserData fetchUserData(String authorizationParam) {
        if (authorizationParam.equals("Bearer 123")) {
            return new UserData(BigDecimal.valueOf(5), BigDecimal.valueOf(1000));
        } else {
            return new UserData(BigDecimal.valueOf(1), BigDecimal.valueOf(200));
        }
    }
}
