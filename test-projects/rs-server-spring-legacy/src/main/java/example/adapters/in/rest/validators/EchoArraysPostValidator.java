package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoArraysPostRrequest;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.dtos.EchoArraysPostRrequest.*;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostValidator extends Validator<EchoArraysPostRrequest> {

    @Autowired
    private AuthorizationChecker authorizationChecker;
    @Autowired
    private EchoArraysBodyValidator echoArraysBodyValidator;

    public void check(ValidationContext<EchoArraysPostRrequest> ctx) {
        ctx.check(FIELD_AUTHORIZATION_PARAM, authorizationChecker, required());
        ctx.check(FIELD_REQUEST_BODY, required(), list(echoArraysBodyValidator));
    }

}
