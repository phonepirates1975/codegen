package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoArraysBodyRdto;
import example.commons.adapters.in.rest.dtos.SimpleObjectRdto;
import example.commons.adapters.in.rest.validators.SimpleObjectCustomValidator;
import example.commons.adapters.in.rest.validators.SimpleObjectValidator;
import example.commons.ports.in.rest.dtos.ReusableEnumEnum;
import example.commons.validator.Checker;
import example.commons.validator.DictionaryChecker;
import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.EnumTypeEnum;
import java.util.List;
import java.util.regex.Pattern;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.dtos.EchoArraysBodyRdto.*;
import static example.commons.validator.CommonCheckers.*;
import static example.commons.validator.DictionaryCodes.*;
import static example.commons.validator.ErrorCodes.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoArraysBodyValidator implements Checker<EchoArraysBodyRdto> {

    private static final Checker<List<String>> FIELD_PROP_INT_LIST_SIZE = size(1, null);
    private static final Checker<String> FIELD_PROP_INT_LIST_MIN_MAX = range(Integer::new, "-1", null);
    private static final Checker<String> FIELD_PROP_FLOAT_LIST_MIN_MAX = range(Float::new, null, "101");
    private static final Checker<List<String>> FIELD_PROP_DOUBLE_LIST_SIZE = size(2, 4);
    private static final Checker<String> FIELD_PROP_DOUBLE_LIST_MIN_MAX = range(Double::new, "0.01",
            "1000000000000000");
    private static final Checker<String> FIELD_PROP_AMOUNT_LIST_PATTERN = matches(
            Pattern.compile("^(0|([1-9][0-9]{0,}))\\.\\d{2}$"), "invalid_amount");
    private static final Checker<String> FIELD_PROP_STRING_LIST_LENGTH = length(5, 10);
    private static final Checker<List<List<String>>> FIELD_PROP_DATE_TIME_LIST_OF_LIST_SIZE = size(1, null);
    private static final Checker<List<String>> FIELD_PROP_DATE_TIME_LIST_OF_LIST_2_SIZE = size(2, null);
    private static final Checker<String> REUSABLE_ENUM_ENUM = allow(ReusableEnumEnum.values());
    private static final Checker<String> ENUM_TYPE_ENUM = allow(EnumTypeEnum.values());
    private static final Checker<String> FIELD_PROP_MAP_OF_INT_MIN_MAX = range(Integer::new, null, "100");

    @Autowired
    private DictionaryChecker dictionaryChecker;
    @Autowired
    private SimpleObjectValidator simpleObjectValidator;
    @Qualifier("SIMPLE_OBJECT_CUSTOM_CONSTRAINT")
    @Autowired
    private Checker<SimpleObjectRdto> simpleObjectCustomConstraint;
    @Autowired
    private SimpleObjectCustomValidator simpleObjectCustomValidator;

    public void check(ValidationContext<EchoArraysBodyRdto> ctx) {
        ctx.check(FIELD_PROP_INT_LIST, FIELD_PROP_INT_LIST_SIZE, list(INT32, FIELD_PROP_INT_LIST_MIN_MAX));
        ctx.check(FIELD_PROP_FLOAT_LIST, list(FLOAT, FIELD_PROP_FLOAT_LIST_MIN_MAX));
        ctx.check(FIELD_PROP_DOUBLE_LIST, required(), unique(), FIELD_PROP_DOUBLE_LIST_SIZE,
                list(DOUBLE, FIELD_PROP_DOUBLE_LIST_MIN_MAX));
        ctx.check(FIELD_PROP_AMOUNT_LIST, list(FIELD_PROP_AMOUNT_LIST_PATTERN));
        ctx.check(FIELD_PROP_STRING_LIST,
                list(dictionaryChecker.check(ANIMALS).withError(INVALID_ANIMAL), FIELD_PROP_STRING_LIST_LENGTH));
        ctx.check(FIELD_PROP_DATE_LIST, list(ISO_DATE));
        ctx.check(FIELD_PROP_DATE_TIME_LIST_OF_LIST, FIELD_PROP_DATE_TIME_LIST_OF_LIST_SIZE,
                list(FIELD_PROP_DATE_TIME_LIST_OF_LIST_2_SIZE, list(ISO_DATE_TIME)));
        ctx.check(FIELD_PROP_BOOLEAN_LIST, list(BOOLEAN));
        ctx.check(FIELD_PROP_OBJECT_LIST,
                list(simpleObjectValidator, simpleObjectCustomConstraint.withError(CUSTOM_FAILED_CODE)));
        ctx.check(FIELD_PROP_OBJECT_LIST_OF_LIST,
                list(list(simpleObjectValidator, simpleObjectCustomValidator::check)));
        ctx.check(FIELD_PROP_ENUM_REUSABLE_LIST, list(REUSABLE_ENUM_ENUM));
        ctx.check(FIELD_PROP_ENUM_LIST, list(ENUM_TYPE_ENUM));
        ctx.check(FIELD_PROP_MAP_OF_INT, mapValues(INT32, FIELD_PROP_MAP_OF_INT_MIN_MAX));
        ctx.check(FIELD_PROP_MAP_OF_OBJECT, mapValues(simpleObjectValidator));
        ctx.check(FIELD_PROP_MAP_OF_LIST_OF_OBJECT, mapValues(list(simpleObjectValidator)));
    }

}
