package example.adapters.in.rest.dtos;

import example.commons.validator.Field;
import javax.annotation.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoPostRrequest {

    public static final Field<EchoPostRrequest, String> FIELD_AUTHORIZATION_PARAM = new Field<>(
            "Authorization (HEADER parameter)", EchoPostRrequest::getAuthorizationParam);
    public static final Field<EchoPostRrequest, String> FIELD_CORRELATION_ID_PARAM = new Field<>(
            "X-Correlation-ID (HEADER parameter)", EchoPostRrequest::getCorrelationIdParam);
    public static final Field<EchoPostRrequest, String> FIELD_TIMESTAMP_PARAM = new Field<>(
            "timestamp (HEADER parameter)", EchoPostRrequest::getTimestampParam);
    public static final Field<EchoPostRrequest, String> FIELD_INLINE_HEADER_PARAM = new Field<>(
            "Inline-Header-Param (HEADER parameter)", EchoPostRrequest::getInlineHeaderParam);
    public static final Field<EchoPostRrequest, EchoBodyRdto> FIELD_REQUEST_BODY = new Field<>("requestBody",
            EchoPostRrequest::getRequestBody);

    private String authorizationParam;
    private String correlationIdParam;
    private String timestampParam;
    private String inlineHeaderParam;
    private EchoBodyRdto requestBody;

}
