package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoGetRrequest;
import example.commons.validator.Checker;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.dtos.EchoGetRrequest.*;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoGetValidator extends Validator<EchoGetRrequest> {

    private static final Checker<String> FIELD_PROP_INT_REQUIRED_MIN_MAX = range(Long::new, "5", null);

    public void check(ValidationContext<EchoGetRrequest> ctx) {
        ctx.check(FIELD_CORRELATION_ID_PARAM, required());
        ctx.check(FIELD_PROP_INT_REQUIRED, required(), INT64, FIELD_PROP_INT_REQUIRED_MIN_MAX);
    }

}
