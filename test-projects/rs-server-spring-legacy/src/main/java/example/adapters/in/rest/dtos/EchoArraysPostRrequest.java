package example.adapters.in.rest.dtos;

import example.commons.validator.Field;
import java.util.List;
import javax.annotation.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostRrequest {

    public static final Field<EchoArraysPostRrequest, String> FIELD_AUTHORIZATION_PARAM = new Field<>(
            "Authorization (HEADER parameter)", EchoArraysPostRrequest::getAuthorizationParam);
    public static final Field<EchoArraysPostRrequest, String> FIELD_INLINE_HEADER_PARAM = new Field<>(
            "Inline-Header-Param (HEADER parameter)", EchoArraysPostRrequest::getInlineHeaderParam);
    public static final Field<EchoArraysPostRrequest, List<EchoArraysBodyRdto>> FIELD_REQUEST_BODY = new Field<>(
            "requestBody", EchoArraysPostRrequest::getRequestBody);

    private String authorizationParam;
    private String inlineHeaderParam;
    private List<EchoArraysBodyRdto> requestBody;

}
