package example.adapters.in.rest.mappers;

import example.commons.adapters.in.rest.mappers.ErrorDescriptionMapper;
import example.ports.in.rest.dtos.UploadEchoFileResponse;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileResponseMapper {

    @Autowired
    private ErrorDescriptionMapper errorDescriptionMapper;

    public ResponseEntity map(@Nonnull UploadEchoFileResponse response) {
        ResponseEntity.BodyBuilder responseBuilder = ResponseEntity.status(response.getStatus());
        response.getHeaders().forEach(responseBuilder::header);
        if (response.is204()) {
            return responseBuilder.build();
        }
        return responseBuilder.body(errorDescriptionMapper.map2ErrorDescriptionRdto(response.getOther()));
    }

}
