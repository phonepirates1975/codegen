package example.adapters.in.rest.mappers;

import example.adapters.in.rest.dtos.EchoBodyRdto;
import example.adapters.in.rest.dtos.EchoPostRrequest;
import example.commons.SerializationUtils;
import example.ports.in.rest.dtos.EchoPostRequest;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoPostRequestMapper {

    @Autowired
    private EchoBodyMapper echoBodyMapper;

    public EchoPostRrequest map2EchoPostRrequest(String authorizationParam, String correlationIdParam,
            String timestampParam, String inlineHeaderParam, EchoBodyRdto requestBody) {
        return new EchoPostRrequest().setAuthorizationParam(authorizationParam)
                .setCorrelationIdParam(correlationIdParam).setTimestampParam(timestampParam)
                .setInlineHeaderParam(inlineHeaderParam).setRequestBody(requestBody);
    }

    public EchoPostRequest map2EchoPostRequest(EchoPostRrequest value) {
        return value == null
                ? null
                : new EchoPostRequest().setAuthorizationParam(value.getAuthorizationParam())
                        .setCorrelationIdParam(value.getCorrelationIdParam())
                        .setTimestampParam(SerializationUtils.toLocalDateTime(value.getTimestampParam()))
                        .setInlineHeaderParam(value.getInlineHeaderParam())
                        .setRequestBody(echoBodyMapper.map2EchoBodyDto(value.getRequestBody()));
    }

}
