package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoErrorRrequest;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.dtos.EchoErrorRrequest.*;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoErrorValidator extends Validator<EchoErrorRrequest> {

    public void check(ValidationContext<EchoErrorRrequest> ctx) {
        ctx.check(FIELD_ERROR_MESSAGE, required());
    }

}
