package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoPostRrequest;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.dtos.EchoPostRrequest.*;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoPostValidator extends Validator<EchoPostRrequest> {

    @Autowired
    private ValidationBeanFactory validationBeanFactory;
    @Autowired
    private AuthorizationChecker authorizationChecker;
    @Autowired
    private EchoBodyValidator echoBodyValidator;

    public void check(ValidationContext<EchoPostRrequest> ctx) {
        ctx.setBean(UserDataValidationBean.class, validationBeanFactory::createUserDataValidationBean);
        ctx.check(FIELD_AUTHORIZATION_PARAM, authorizationChecker, required());
        ctx.check(FIELD_CORRELATION_ID_PARAM, required());
        ctx.check(FIELD_TIMESTAMP_PARAM, ISO_DATE_TIME);
        ctx.check(FIELD_REQUEST_BODY, required(), echoBodyValidator);
    }

}
