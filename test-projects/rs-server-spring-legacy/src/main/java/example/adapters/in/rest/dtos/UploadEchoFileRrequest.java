package example.adapters.in.rest.dtos;

import example.commons.validator.Field;
import javax.annotation.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.core.io.Resource;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileRrequest {

    public static final Field<UploadEchoFileRrequest, String> FIELD_ID = new Field<>("id (PATH parameter)",
            UploadEchoFileRrequest::getId);
    public static final Field<UploadEchoFileRrequest, Resource> FIELD_REQUEST_BODY = new Field<>("requestBody",
            UploadEchoFileRrequest::getRequestBody);

    private String id;
    private Resource requestBody;

}
