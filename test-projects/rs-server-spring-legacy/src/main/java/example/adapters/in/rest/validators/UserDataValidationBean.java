/*
 * Copyright (c) 2020 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoPostRrequest;
import example.commons.validator.ValidationContext;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.util.function.Function;

import static example.commons.validator.CommonCheckers.*;

@RequiredArgsConstructor
public class UserDataValidationBean {

    private final ValidationContext<EchoPostRrequest> request;
    private final Function<String, ValidationBeanFactory.UserData> userDataSupplier;
    @Getter(lazy=true) private final ValidationBeanFactory.UserData userData = userDataSupplier.apply(request.getValue().getAuthorizationParam());

    public void checkAmountByUser(ValidationContext<String> context) {
        BigDecimal amount = new BigDecimal(context.getValue());
        if (getUserData().maxAmount.compareTo(amount) < 0) {
            writeError(context, context.getName() + " should be <= " + getUserData().maxAmount, context.getName(), ERR_CODE_IS_TOO_BIG);
        } else if (getUserData().minAmount.compareTo(amount) > 0) {
            writeError(context, context.getName() + " should be >= " + getUserData().minAmount, context.getName(), ERR_CODE_IS_TOO_SMALL);
        }
    }
}
