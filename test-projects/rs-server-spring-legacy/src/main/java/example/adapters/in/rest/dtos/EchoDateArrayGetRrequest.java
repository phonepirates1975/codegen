package example.adapters.in.rest.dtos;

import example.commons.validator.Field;
import java.util.List;
import javax.annotation.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoDateArrayGetRrequest {

    public static final Field<EchoDateArrayGetRrequest, List<String>> FIELD_DATE_ARRAY = new Field<>(
            "date_array (QUERY parameter)", EchoDateArrayGetRrequest::getDateArray);

    private List<String> dateArray;

}
