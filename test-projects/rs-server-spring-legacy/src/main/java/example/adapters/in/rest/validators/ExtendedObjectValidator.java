package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.ExtendedObjectRdto;
import example.commons.adapters.in.rest.validators.SimpleObjectValidator;
import example.commons.ports.in.rest.dtos.ReusableEnumEnum;
import example.commons.validator.Checker;
import example.commons.validator.ValidationContext;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.dtos.ExtendedObjectRdto.*;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class ExtendedObjectValidator implements Checker<ExtendedObjectRdto> {

    private static final Checker<String> REUSABLE_ENUM_ENUM = allow(ReusableEnumEnum.values());

    @Autowired
    private SimpleObjectValidator simpleObjectValidator;
    @Autowired
    private ExtendedObjectChecker extendedObjectChecker;

    public void check(ValidationContext<ExtendedObjectRdto> ctx) {
        simpleObjectValidator.checkWithParent(ctx);
        ctx.check(FIELD_EO_ENUM_REUSABLE, REUSABLE_ENUM_ENUM, ctx.getBean(ExtendedObjectEnumChecker.class));
        ctx.check(FIELD_SELF_PROPERTY, this);
        ctx.check(extendedObjectChecker);
    }

}
