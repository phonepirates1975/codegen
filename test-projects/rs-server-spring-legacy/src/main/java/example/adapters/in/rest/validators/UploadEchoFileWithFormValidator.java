package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.UploadEchoFileWithFormRrequest;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.dtos.UploadEchoFileWithFormRrequest.*;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileWithFormValidator extends Validator<UploadEchoFileWithFormRrequest> {

    public void check(ValidationContext<UploadEchoFileWithFormRrequest> ctx) {
        ctx.check(FIELD_ID, required(), INT64);
        ctx.check(FIELD_FILE, required());
    }

}
