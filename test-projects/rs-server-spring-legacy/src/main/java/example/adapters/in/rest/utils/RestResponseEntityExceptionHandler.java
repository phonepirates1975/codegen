package example.adapters.in.rest.utils;

import example.commons.adapters.in.rest.dtos.ErrorDescriptionRdto;
import example.commons.adapters.in.rest.dtos.ErrorDetailRdto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Collections;

@Slf4j
@ControllerAdvice
public class RestResponseEntityExceptionHandler {

    @ExceptionHandler({Exception.class})
    public ResponseEntity<ErrorDescriptionRdto> handleAll(Exception ex, WebRequest request) {
        if (ex instanceof HttpMessageNotReadableException) {
            return new ResponseEntity<>(new ErrorDescriptionRdto()
                    .setCode("400")
                    .setMessage("requestBody is invalid")
                    .setErrors(Collections.singletonList(new ErrorDetailRdto()
                            .setCode("is_required")
                            .setField("requestBody")
                            .setMessage("requestBody is required")
                    )),
                    HttpStatus.BAD_REQUEST);
        }
        log.error("Exception", ex);
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
