package example.adapters.in.rest.mappers;

import example.adapters.in.rest.dtos.EchoErrorRrequest;
import example.ports.in.rest.dtos.EchoErrorRequest;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoErrorRequestMapper {

    public EchoErrorRrequest map2EchoErrorRrequest(String errorMessage) {
        return new EchoErrorRrequest().setErrorMessage(errorMessage);
    }

    public EchoErrorRequest map2EchoErrorRequest(EchoErrorRrequest value) {
        return value == null ? null : new EchoErrorRequest().setErrorMessage(value.getErrorMessage());
    }

}
