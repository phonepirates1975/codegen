package example.adapters.in.rest.mappers;

import example.commons.adapters.in.rest.mappers.ErrorDescriptionMapper;
import example.ports.in.rest.dtos.EchoErrorResponse;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoErrorResponseMapper {

    @Autowired
    private ErrorDescriptionMapper errorDescriptionMapper;

    public ResponseEntity map(@Nonnull EchoErrorResponse response) {
        ResponseEntity.BodyBuilder responseBuilder = ResponseEntity.status(response.getStatus());
        response.getHeaders().forEach(responseBuilder::header);
        return responseBuilder.body(errorDescriptionMapper.map2ErrorDescriptionRdto(response.getOther()));
    }

}
