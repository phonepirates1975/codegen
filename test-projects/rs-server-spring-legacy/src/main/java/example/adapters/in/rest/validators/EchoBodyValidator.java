package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoBodyRdto;
import example.commons.SerializationUtils;
import example.commons.adapters.in.rest.validators.SimpleObjectCustomValidator;
import example.commons.adapters.in.rest.validators.SimpleObjectValidator;
import example.commons.ports.in.rest.dtos.ReusableEnumEnum;
import example.commons.validator.AmountChecker;
import example.commons.validator.Checker;
import example.commons.validator.Checkers;
import example.commons.validator.DictionaryChecker;
import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.EnumTypeEnum;
import java.math.BigDecimal;
import java.util.regex.Pattern;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.dtos.EchoBodyRdto.*;
import static example.commons.validator.CommonCheckers.*;
import static example.commons.validator.DictionaryCodes.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoBodyValidator implements Checker<EchoBodyRdto> {

    private static final Checker<String> FIELD_PROP_INT_REQUIRED_MIN_MAX = range(Long::new, "-1", null);
    private static final Checker<String> FIELD_PROP_FLOAT_MIN_MAX = range(Float::new, null, "101");
    private static final Checker<String> FIELD_PROP_DOUBLE_MIN_MAX = range(Double::new, "0.01", "9999.9999");
    private static final Checker<String> FIELD_PROP_AMOUNT_PATTERN = matches(
            Pattern.compile("^(0|([1-9][0-9]{0,}))\\.\\d{2}$"));
    private static final Checker<String> FIELD_PROP_AMOUNT_NUMBER_MIN_MAX = range(BigDecimal::new, "0.02",
            "9999999999.99");
    private static final Checker<String> FIELD_PROP_STRING_LENGTH = length(5, 10);
    private static final Checker<String> FIELD_PROP_STRING_PATTERN_PATTERN = matches(
            Pattern.compile("^[\\-\\\".',:;/\\\\!@#$%^&*()+_?|><=]{2,19}$"), "custom_error_code");
    private static final Checker<String> FIELD_PROP_DEFAULT_LENGTH = length(5, null);
    private static final Checker<String> REUSABLE_ENUM_ENUM = allow(ReusableEnumEnum.values());
    private static final Checker<String> ENUM_TYPE_ENUM = allow(EnumTypeEnum.values());

    @Autowired
    private ValidationBeanFactory validationBeanFactory;
    @Autowired
    private AmountChecker amountChecker;
    @Autowired
    private DictionaryChecker dictionaryChecker;
    @Autowired
    private SimpleObjectValidator simpleObjectValidator;
    @Autowired
    private SimpleObjectCustomValidator simpleObjectCustomValidator;
    @Autowired
    private ExtendedObjectValidator extendedObjectValidator;

    public void check(ValidationContext<EchoBodyRdto> ctx) {
        ctx.setBean(ExtendedObjectEnumChecker.class, validationBeanFactory::createExtendedObjectEnumChecker);
        ctx.check(FIELD_PROP_INT, INT32);
        ctx.check(FIELD_PROP_INT_SECOND, INT32);
        ctx.check(FIELD_PROP_INT_REQUIRED, required(), INT64, FIELD_PROP_INT_REQUIRED_MIN_MAX);
        ctx.check(FIELD_PROP_FLOAT, FLOAT, FIELD_PROP_FLOAT_MIN_MAX);
        ctx.check(FIELD_PROP_DOUBLE, DOUBLE, FIELD_PROP_DOUBLE_MIN_MAX);
        ctx.check(FIELD_PROP_AMOUNT, FIELD_PROP_AMOUNT_PATTERN,
                ctx.getBean(UserDataValidationBean.class)::checkAmountByUser, Checkers.AMOUNT_SCALE_CHECKER);
        ctx.check(FIELD_PROP_AMOUNT_NUMBER, amountChecker, FIELD_PROP_AMOUNT_NUMBER_MIN_MAX);
        ctx.check(FIELD_PROP_STRING, FIELD_PROP_STRING_LENGTH, dictionaryChecker.check(COLORS));
        ctx.check(FIELD_PROP_STRING_PATTERN, FIELD_PROP_STRING_PATTERN_PATTERN);
        ctx.check(FIELD_PROP_DEFAULT, FIELD_PROP_DEFAULT_LENGTH);
        ctx.check(FIELD_PROP_DATE, ISO_DATE);
        ctx.check(FIELD_PROP_DATE_SECOND, ISO_DATE);
        ctx.check(FIELD_PROP_DATE_TIME, ISO_DATE_TIME);
        ctx.check(FIELD_PROP_BASE64, BASE64);
        ctx.check(FIELD_PROP_BOOLEAN, BOOLEAN);
        ctx.check(FIELD_PROP_OBJECT, simpleObjectValidator, simpleObjectCustomValidator);
        ctx.check(FIELD_PROP_OBJECT_EXTENDED, extendedObjectValidator);
        ctx.check(FIELD_PROP_ENUM_REUSABLE, REUSABLE_ENUM_ENUM);
        ctx.check(FIELD_PROP_ENUM, ENUM_TYPE_ENUM);
        ctx.check(lt(FIELD_PROP_INT, FIELD_PROP_INT_SECOND, SerializationUtils::toInteger));
        ctx.check(ge(FIELD_PROP_DATE, FIELD_PROP_DATE_SECOND, SerializationUtils::toLocalDate));
    }

}
