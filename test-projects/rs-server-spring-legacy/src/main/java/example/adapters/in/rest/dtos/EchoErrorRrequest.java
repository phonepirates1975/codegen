package example.adapters.in.rest.dtos;

import example.commons.validator.Field;
import javax.annotation.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoErrorRrequest {

    public static final Field<EchoErrorRrequest, String> FIELD_ERROR_MESSAGE = new Field<>(
            "errorMessage (QUERY parameter)", EchoErrorRrequest::getErrorMessage);

    private String errorMessage;

}
