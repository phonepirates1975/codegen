package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoDefaultsBodyRdto;
import example.commons.ports.in.rest.dtos.ReusableEnumEnum;
import example.commons.validator.AmountChecker;
import example.commons.validator.Checker;
import example.commons.validator.DictionaryChecker;
import example.commons.validator.ValidationContext;
import java.math.BigDecimal;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.dtos.EchoDefaultsBodyRdto.*;
import static example.commons.validator.CommonCheckers.*;
import static example.commons.validator.DictionaryCodes.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsBodyValidator implements Checker<EchoDefaultsBodyRdto> {

    private static final Checker<String> FIELD_PROP_INT_MIN_MAX = range(Integer::new, "1", null);
    private static final Checker<String> FIELD_PROP_FLOAT_MIN_MAX = range(Float::new, "1", null);
    private static final Checker<String> FIELD_PROP_DOUBLE_MIN_MAX = range(Double::new, "1", null);
    private static final Checker<String> FIELD_PROP_NUMBER_MIN_MAX = range(BigDecimal::new, "1", null);
    private static final Checker<String> FIELD_PROP_STRING_LENGTH = length(5, null);
    private static final Checker<String> REUSABLE_ENUM_ENUM = allow(ReusableEnumEnum.values());

    @Autowired
    private AmountChecker amountChecker;
    @Autowired
    private DictionaryChecker dictionaryChecker;

    public void check(ValidationContext<EchoDefaultsBodyRdto> ctx) {
        ctx.check(FIELD_PROP_INT, INT32, FIELD_PROP_INT_MIN_MAX);
        ctx.check(FIELD_PROP_FLOAT, FLOAT, FIELD_PROP_FLOAT_MIN_MAX);
        ctx.check(FIELD_PROP_DOUBLE, DOUBLE, FIELD_PROP_DOUBLE_MIN_MAX);
        ctx.check(FIELD_PROP_NUMBER, amountChecker, FIELD_PROP_NUMBER_MIN_MAX);
        ctx.check(FIELD_PROP_STRING, FIELD_PROP_STRING_LENGTH, dictionaryChecker.check(COLORS));
        ctx.check(FIELD_PROP_ENUM, REUSABLE_ENUM_ENUM);
        ctx.check(FIELD_PROP_DATE, ISO_DATE);
        ctx.check(FIELD_PROP_DATE_TIME, ISO_DATE_TIME);
    }

}
