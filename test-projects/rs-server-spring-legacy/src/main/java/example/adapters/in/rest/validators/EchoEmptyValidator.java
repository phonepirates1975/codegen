package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoEmptyRrequest;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoEmptyValidator extends Validator<EchoEmptyRrequest> {

    public void check(ValidationContext<EchoEmptyRrequest> ctx) {
    }

}
