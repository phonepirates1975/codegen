package example.adapters.in.rest.dtos;

import example.commons.validator.Field;
import javax.annotation.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class DownloadEchoFileRrequest {

    public static final Field<DownloadEchoFileRrequest, String> FIELD_ID = new Field<>("id (PATH parameter)",
            DownloadEchoFileRrequest::getId);

    private String id;

}
