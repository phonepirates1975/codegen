package example.commons;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Generated;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
@Generated("pl.metaprogramming.codegen")
public interface RestResponse<R> {

    Object getBody();

    Integer getStatus();

    R self();

    Collection<Integer> getDeclaredStatuses();

    Map<String, String> getHeaders();

    default R setHeader(String name, String value) {
        getHeaders().put(name, value);
        return self();
    }

    default boolean isStatus(Integer status) {
        return Objects.equals(status, getStatus());
    }

    R set(Integer status, Object body);

}
