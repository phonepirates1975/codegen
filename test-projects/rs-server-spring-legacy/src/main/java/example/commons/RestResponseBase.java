package example.commons;

import java.util.Map;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.Getter;

@Generated("pl.metaprogramming.codegen")
public abstract class RestResponseBase<R> implements RestResponse<R> {

    @Getter
    private final Map<String, String> headers = new java.util.TreeMap<>();

    @Getter
    private Integer status;
    @Getter
    private Object body;

    @Override
    public R set(@Nonnull Integer status, Object body) {
        if (this.status != null) {
            throw new IllegalStateException(
                    String.format("Response already initialized with %s - %s", this.status, this.body));
        }
        this.status = status;
        this.body = body;
        return self();
    }

}
