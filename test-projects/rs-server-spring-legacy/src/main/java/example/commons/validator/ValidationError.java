package example.commons.validator;

import java.io.Serializable;
import javax.annotation.Generated;
import lombok.Builder;
import lombok.Value;

@Builder
@Value
@Generated("pl.metaprogramming.codegen")
public class ValidationError implements Serializable {

    private String field;
    private String code;
    private String message;
    private Object[] messageArgs;
    private boolean stopValidation;
    private Integer status;

}
