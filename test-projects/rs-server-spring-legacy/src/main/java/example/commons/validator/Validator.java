package example.commons.validator;

import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public abstract class Validator<V> implements Checker<V> {

    public ValidationResult validate(V input) {
        ValidationContext<V> context = new ValidationContext<>(input);
        check(context);
        return context.getResult();
    }

}
