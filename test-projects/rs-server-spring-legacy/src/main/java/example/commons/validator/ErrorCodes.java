package example.commons.validator;

import javax.annotation.Generated;
import lombok.Getter;

@Generated("pl.metaprogramming.codegen")
public enum ErrorCodes {

    CUSTOM_FAILED_CODE("custom.failed.code"), INVALID_ANIMAL("invalid-animal");

    @Getter
    private final String value;

    ErrorCodes(String value) {
        this.value = value;
    }

}
