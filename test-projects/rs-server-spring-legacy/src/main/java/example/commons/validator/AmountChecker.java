package example.commons.validator;

import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
public class AmountChecker extends SimpleChecker<String> {

    private static final Pattern PATTERN = Pattern.compile("^(0|([1-9][0-9]{0,}))\\.\\d{2}$");

    public AmountChecker() {
        super(
                ctx -> PATTERN.matcher(ctx.getValue()).matches(),
                ctx -> ctx.addError("is_not_amount", ctx.getName() + " is not amount"));
    }
}
