package example.commons;

import javax.annotation.Generated;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
@Generated("pl.metaprogramming.codegen")
public interface RestResponseOther<R, T> extends RestResponse<R> {

    default boolean isOther() {
        return !getDeclaredStatuses().contains(getStatus());
    }

    @SuppressWarnings("unchecked")
    default T getOther() {
        if (isOther()) {
            return (T) getBody();
        }
        throw new IllegalStateException(String.format("Response other than %s is not set", getDeclaredStatuses()));
    }

    default R setOther(Integer status, T body) {
        if (getDeclaredStatuses().contains(status)) {
            throw new IllegalArgumentException(
                    String.format("Status %s is declared. Use dedicated setter for it.", status));
        }
        return set(status, body);
    }

}
