package example.commons;

import javax.annotation.Generated;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
@Generated("pl.metaprogramming.codegen")
public interface RestResponse200<R, T> extends RestResponse<R> {

    default boolean is200() {
        return isStatus(200);
    }

    @SuppressWarnings("unchecked")
    default T get200() {
        if (is200()) {
            return (T) getBody();
        }
        throw new IllegalStateException("Response 200 is not set");
    }

    default R set200(T body) {
        return set(200, body);
    }

}
