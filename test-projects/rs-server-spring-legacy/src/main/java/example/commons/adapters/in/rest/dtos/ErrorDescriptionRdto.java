package example.commons.adapters.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import example.commons.validator.Field;
import java.util.List;
import javax.annotation.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE)
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class ErrorDescriptionRdto {

    public static final Field<ErrorDescriptionRdto, String> FIELD_CODE = new Field<>("code",
            ErrorDescriptionRdto::getCode);
    public static final Field<ErrorDescriptionRdto, String> FIELD_MESSAGE = new Field<>("message",
            ErrorDescriptionRdto::getMessage);
    public static final Field<ErrorDescriptionRdto, List<ErrorDetailRdto>> FIELD_ERRORS = new Field<>("errors",
            ErrorDescriptionRdto::getErrors);

    @JsonProperty("code")
    @JsonRawValue
    private String code;
    @JsonProperty("message")
    private String message;
    @JsonProperty("errors")
    private List<ErrorDetailRdto> errors;

}
