package example.commons.adapters.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import example.commons.validator.Field;
import javax.annotation.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE)
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class ErrorDetailRdto {

    public static final Field<ErrorDetailRdto, String> FIELD_FIELD = new Field<>("field", ErrorDetailRdto::getField);
    public static final Field<ErrorDetailRdto, String> FIELD_CODE = new Field<>("code", ErrorDetailRdto::getCode);
    public static final Field<ErrorDetailRdto, String> FIELD_MESSAGE = new Field<>("message",
            ErrorDetailRdto::getMessage);

    @JsonProperty("field")
    private String field;
    @JsonProperty("code")
    private String code;
    @JsonProperty("message")
    private String message;

}
