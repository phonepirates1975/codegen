package example.commons.adapters.in.rest.validators;

import example.commons.adapters.in.rest.dtos.SimpleObjectRdto;
import example.commons.validator.Checker;
import example.commons.validator.ValidationContext;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import static example.commons.adapters.in.rest.dtos.SimpleObjectRdto.*;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class SimpleObjectValidator implements Checker<SimpleObjectRdto> {

    public void check(ValidationContext<SimpleObjectRdto> ctx) {
        ctx.check(FIELD_SO_PROP_STRING, required());
        ctx.check(FIELD_SO_PROP_DATE, ISO_DATE);
    }

}
