package example.commons;

import org.springframework.http.ResponseEntity;
import example.commons.RestResponse;
import example.commons.validator.ValidationResult;

import javax.annotation.Generated;
import java.util.function.Function;

@Generated("pl.metaprogramming.codegen")
public class RestRequestHandler<REQ, N, RES_N> {

    private REQ request;
    private RestResponse result;

    private RestRequestHandler(REQ request) {
        this.request = request;
    }

    public static <REQ, REQ_N, RES_N> ResponseEntity handle(REQ request, Function<REQ, ValidationResult> validator,
            Function<ValidationResult, ResponseEntity> validationResultMapper, Function<REQ, REQ_N> dtoMapper,
            Function<REQ_N, RES_N> serviceCaller, Function<RES_N, ResponseEntity> resultMapper) {
        return new RestRequestHandler<REQ, REQ_N, RES_N>(request).handle(validator, validationResultMapper, dtoMapper,
                serviceCaller, resultMapper);
    }

    private ResponseEntity handle(Function<REQ, ValidationResult> validator,
            Function<ValidationResult, ResponseEntity> validationResultMapper, Function<REQ, N> dtoMapper,
            Function<N, RES_N> serviceCaller, Function<RES_N, ResponseEntity> resultMapper) {
        ValidationResult validationResult = validator.apply(request);
        if (!validationResult.isValid()) {
            return validationResultMapper.apply(validationResult);
        }
        return dtoMapper.andThen(serviceCaller).andThen(resultMapper).apply(request);
    }
}