package example.ports.in.rest.dtos;

import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileRequest {

    /**
     * id of file
     */
    @Nonnull
    private Long id;

    /**
     * file data to upload
     */
    @Nonnull
    private byte[] requestBody;

}
