package example.ports.in.rest.dtos;

import example.commons.RestResponse200NoContent;
import example.commons.RestResponseBase;
import java.util.Collection;
import java.util.Collections;
import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public class EchoEmptyResponse extends RestResponseBase<EchoEmptyResponse>
        implements
            RestResponse200NoContent<EchoEmptyResponse> {

    private static final Collection<Integer> DECLARED_STATUSES = Collections.singletonList(200);

    public Collection<Integer> getDeclaredStatuses() {
        return DECLARED_STATUSES;
    }

    public EchoEmptyResponse self() {
        return this;
    }

}
