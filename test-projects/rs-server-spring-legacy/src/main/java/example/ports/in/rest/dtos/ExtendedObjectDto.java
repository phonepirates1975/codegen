package example.ports.in.rest.dtos;

import example.commons.ports.in.rest.dtos.ReusableEnumEnum;
import example.commons.ports.in.rest.dtos.SimpleObjectDto;
import javax.annotation.Generated;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Simple object for testing
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Generated("pl.metaprogramming.codegen")
public class ExtendedObjectDto extends SimpleObjectDto {

    /**
     * enum property with external definition
     */
    @Nullable
    private ReusableEnumEnum eoEnumReusable;

    /**
     * Simple object for testing
     */
    @Nullable
    private ExtendedObjectDto selfProperty;

}
