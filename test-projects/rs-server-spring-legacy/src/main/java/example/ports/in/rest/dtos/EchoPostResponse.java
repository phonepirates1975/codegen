package example.ports.in.rest.dtos;

import example.commons.RestResponse200;
import example.commons.RestResponseBase;
import example.commons.RestResponseOther;
import example.commons.ports.in.rest.dtos.ErrorDescriptionDto;
import java.util.Collection;
import java.util.Collections;
import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public class EchoPostResponse extends RestResponseBase<EchoPostResponse>
        implements
            RestResponse200<EchoPostResponse, EchoBodyDto>,
            RestResponseOther<EchoPostResponse, ErrorDescriptionDto> {

    private static final Collection<Integer> DECLARED_STATUSES = Collections.singletonList(200);

    public Collection<Integer> getDeclaredStatuses() {
        return DECLARED_STATUSES;
    }

    public EchoPostResponse self() {
        return this;
    }

}
