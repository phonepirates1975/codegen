package example.ports.in.rest.dtos;

import example.commons.RestResponse200;
import example.commons.RestResponseBase;
import example.commons.RestResponseOther;
import example.commons.ports.in.rest.dtos.ErrorDescriptionDto;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public class EchoDateArrayGetResponse extends RestResponseBase<EchoDateArrayGetResponse>
        implements
            RestResponse200<EchoDateArrayGetResponse, List<LocalDate>>,
            RestResponseOther<EchoDateArrayGetResponse, ErrorDescriptionDto> {

    private static final Collection<Integer> DECLARED_STATUSES = Collections.singletonList(200);

    public Collection<Integer> getDeclaredStatuses() {
        return DECLARED_STATUSES;
    }

    public EchoDateArrayGetResponse self() {
        return this;
    }

}
