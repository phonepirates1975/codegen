package example.adapters.in.rest.validators;

import example.adapters.in.rest.validators.custom.DictionaryChecker;
import example.commons.validator.AmountChecker;
import example.commons.validator.Checker;
import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.EchoDefaultsBodyDto;
import java.math.BigDecimal;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.validators.custom.DictionaryCodes.*;
import static example.commons.validator.CommonCheckers.*;
import static example.ports.in.rest.dtos.EchoDefaultsBodyDto.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsBodyValidator implements Checker<EchoDefaultsBodyDto> {
    
    private static final Checker<Integer> FIELD_PROP_INT_MIN_MAX = range(1, null);
    private static final Checker<Float> FIELD_PROP_FLOAT_MIN_MAX = range(1f, null);
    private static final Checker<Double> FIELD_PROP_DOUBLE_MIN_MAX = range(1d, null);
    private static final Checker<BigDecimal> FIELD_PROP_NUMBER_MIN_MAX = range(new BigDecimal("1"), null);
    private static final Checker<String> FIELD_PROP_STRING_LENGTH = length(5, null);
    
    private final AmountChecker amountChecker;
    private final DictionaryChecker dictionaryChecker;


    public void check(ValidationContext<EchoDefaultsBodyDto> ctx) {
        ctx.check(FIELD_PROP_INT, FIELD_PROP_INT_MIN_MAX);
        ctx.check(FIELD_PROP_FLOAT, FIELD_PROP_FLOAT_MIN_MAX);
        ctx.check(FIELD_PROP_DOUBLE, FIELD_PROP_DOUBLE_MIN_MAX);
        ctx.check(FIELD_PROP_NUMBER, amountChecker, FIELD_PROP_NUMBER_MIN_MAX);
        ctx.check(FIELD_PROP_STRING, FIELD_PROP_STRING_LENGTH, dictionaryChecker.check(COLORS));
    }

}
