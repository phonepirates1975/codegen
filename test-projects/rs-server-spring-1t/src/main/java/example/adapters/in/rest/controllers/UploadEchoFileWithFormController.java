package example.adapters.in.rest.controllers;

import example.adapters.in.rest.validators.UploadEchoFileWithFormValidator;
import example.commons.SerializationUtils;
import example.ports.in.rest.IUploadEchoFileWithFormCommand;
import example.ports.in.rest.dtos.UploadEchoFileWithFormRequest;
import example.process.Context;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileWithFormController {
    
    private final Context context;
    private final UploadEchoFileWithFormValidator uploadEchoFileWithFormValidator;
    private final IUploadEchoFileWithFormCommand uploadEchoFileWithFormCommand;


    /**
     * upload file using multipart/form-data
     */
    @PostMapping(value = "/api/v1/echo-file/{id}/form", produces = {"application/json"}, consumes = {"multipart/form-data"})
    public ResponseEntity<Void> uploadEchoFileWithForm(@PathVariable Long id, @RequestPart MultipartFile file) {
        UploadEchoFileWithFormRequest request = new UploadEchoFileWithFormRequest()
                .setId(id)
                .setFile(SerializationUtils.toBytes(file))
                .setContext(context);
        uploadEchoFileWithFormValidator.validate(request);
        uploadEchoFileWithFormCommand.execute(request);
        return ResponseEntity.noContent().build();
    }

}
