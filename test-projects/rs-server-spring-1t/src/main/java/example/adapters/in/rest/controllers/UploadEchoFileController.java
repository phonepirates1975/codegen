package example.adapters.in.rest.controllers;

import example.adapters.in.rest.validators.UploadEchoFileValidator;
import example.commons.SerializationUtils;
import example.ports.in.rest.IUploadEchoFileCommand;
import example.ports.in.rest.dtos.UploadEchoFileRequest;
import example.process.Context;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileController {
    
    private final Context context;
    private final UploadEchoFileValidator uploadEchoFileValidator;
    private final IUploadEchoFileCommand uploadEchoFileCommand;


    /**
     * upload file
     */
    @PostMapping(value = "/api/v1/echo-file/{id}", produces = {"application/json"}, consumes = {"application/octet-stream"})
    public ResponseEntity<Void> uploadEchoFile(@PathVariable Long id, @RequestBody Resource requestBody) {
        UploadEchoFileRequest request = new UploadEchoFileRequest()
                .setId(id)
                .setRequestBody(SerializationUtils.toBytes(requestBody))
                .setContext(context);
        uploadEchoFileValidator.validate(request);
        uploadEchoFileCommand.execute(request);
        return ResponseEntity.noContent().build();
    }

}
