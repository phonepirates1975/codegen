package example.adapters.in.rest.validators;

import example.commons.validator.Checker;
import example.commons.validator.ValidationContext;
import example.commons.validator.ValidationError;
import example.process.IRequest;
import example.process.UserData;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class AuthorizationChecker implements Checker<String> {

    @Override
    public void check(ValidationContext<String> context) {
        if (context.getValue() == null || !context.getValue().startsWith("Bearer ")) {
            context.addError(ValidationError.builder()
                    .code("invalid_token")
                    .message("invalid_token")
                    .status(403)
                    .stopValidation(true)
                    .build());
        } else {
            context.getRequest(IRequest.class).getContext().initUser(
                    loadUserData(context.getValue().substring(7)));
        }
    }

    @Override
    public boolean checkNull() {
        return true;
    }

    private UserData loadUserData(String token) {
        return "123".equals(token)
                ? new UserData(BigDecimal.valueOf(5), BigDecimal.valueOf(1000))
                : new UserData(BigDecimal.valueOf(1), BigDecimal.valueOf(200));
    }
}
