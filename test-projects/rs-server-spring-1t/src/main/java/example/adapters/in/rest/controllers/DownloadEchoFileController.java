package example.adapters.in.rest.controllers;

import example.adapters.in.rest.validators.DownloadEchoFileValidator;
import example.ports.in.rest.IDownloadEchoFileQuery;
import example.ports.in.rest.dtos.DownloadEchoFileRequest;
import example.process.Context;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class DownloadEchoFileController {
    
    private final Context context;
    private final DownloadEchoFileValidator downloadEchoFileValidator;
    private final IDownloadEchoFileQuery downloadEchoFileQuery;


    /**
     * Returns a file
     */
    @GetMapping(value = "/api/v1/echo-file/{id}", produces = {"image/jpeg", "application/json"})
    public ResponseEntity<byte[]> downloadEchoFile(@PathVariable Long id) {
        DownloadEchoFileRequest request = new DownloadEchoFileRequest()
                .setId(id)
                .setContext(context);
        downloadEchoFileValidator.validate(request);
        return downloadEchoFileQuery.execute(request);
    }

}
