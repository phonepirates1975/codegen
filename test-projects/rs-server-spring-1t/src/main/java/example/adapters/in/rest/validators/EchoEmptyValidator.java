package example.adapters.in.rest.validators;

import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.EchoEmptyRequest;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoEmptyValidator extends Validator<EchoEmptyRequest> {

    public void check(ValidationContext<EchoEmptyRequest> ctx) {
    }

}
