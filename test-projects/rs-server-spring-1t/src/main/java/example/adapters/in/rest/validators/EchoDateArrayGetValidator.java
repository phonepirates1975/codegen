package example.adapters.in.rest.validators;

import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.EchoDateArrayGetRequest;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoDateArrayGetValidator extends Validator<EchoDateArrayGetRequest> {

    public void check(ValidationContext<EchoDateArrayGetRequest> ctx) {
    }

}
