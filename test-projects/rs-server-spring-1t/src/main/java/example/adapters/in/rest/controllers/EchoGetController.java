package example.adapters.in.rest.controllers;

import example.adapters.in.rest.validators.EchoGetValidator;
import example.ports.in.rest.IEchoGetQuery;
import example.ports.in.rest.dtos.DefaultEnum;
import example.ports.in.rest.dtos.EchoGetBodyDto;
import example.ports.in.rest.dtos.EchoGetRequest;
import example.process.Context;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoGetController {
    
    private final Context context;
    private final EchoGetValidator echoGetValidator;
    private final IEchoGetQuery echoGetQuery;


    @GetMapping(value = "/api/v1/echo", produces = {"application/json"})
    public ResponseEntity<EchoGetBodyDto> echoGet(@RequestHeader("X-Correlation-ID") String correlationIdParam, @RequestParam("prop_int_required") Long propIntRequired, @RequestParam(value = "prop_float", required = false) Float propFloat, @RequestParam(value = "prop_enum", required = false) String propEnum) {
        EchoGetRequest request = new EchoGetRequest()
                .setCorrelationIdParam(correlationIdParam)
                .setPropIntRequired(propIntRequired)
                .setPropFloat(propFloat)
                .setPropEnum(DefaultEnum.fromValue(propEnum))
                .setContext(context);
        echoGetValidator.validate(request);
        return ResponseEntity.ok(echoGetQuery.execute(request));
    }

}
