package example.adapters.in.rest.validators;

import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.UploadEchoFileWithFormRequest;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;
import static example.ports.in.rest.dtos.UploadEchoFileWithFormRequest.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileWithFormValidator extends Validator<UploadEchoFileWithFormRequest> {

    public void check(ValidationContext<UploadEchoFileWithFormRequest> ctx) {
        ctx.check(FIELD_ID, required());
        ctx.check(FIELD_FILE, required());
    }

}
