package example.adapters.in.rest.validators;

import example.adapters.in.rest.validators.custom.DictionaryChecker;
import example.commons.validator.Checker;
import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.EchoArraysBodyDto;
import example.ports.in.rest.dtos.SimpleObjectDto;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.regex.Pattern;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.validators.custom.DictionaryCodes.*;
import static example.adapters.in.rest.validators.custom.ErrorCodes.*;
import static example.commons.validator.CommonCheckers.*;
import static example.ports.in.rest.dtos.EchoArraysBodyDto.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoArraysBodyValidator implements Checker<EchoArraysBodyDto> {
    
    private static final Checker<List<Integer>> FIELD_PROP_INT_LIST_SIZE = size(1, null);
    private static final Checker<Integer> FIELD_PROP_INT_LIST_MIN_MAX = range(-1, null);
    private static final Checker<Float> FIELD_PROP_FLOAT_LIST_MIN_MAX = range(null, 101f);
    private static final Checker<List<Double>> FIELD_PROP_DOUBLE_LIST_SIZE = size(2, 4);
    private static final Checker<Double> FIELD_PROP_DOUBLE_LIST_MIN_MAX = range(0.01, 1000000000000000d);
    private static final Checker<String> FIELD_PROP_AMOUNT_LIST_PATTERN = matches(Pattern.compile("^(0|([1-9][0-9]{0,}))\\.\\d{2}$"), "invalid_amount");
    private static final Checker<String> FIELD_PROP_STRING_LIST_LENGTH = length(5, 10);
    private static final Checker<List<List<ZonedDateTime>>> FIELD_PROP_DATE_TIME_LIST_OF_LIST_SIZE = size(1, null);
    private static final Checker<List<ZonedDateTime>> FIELD_PROP_DATE_TIME_LIST_OF_LIST_2_SIZE = size(2, null);
    private static final Checker<Integer> FIELD_PROP_MAP_OF_INT_MIN_MAX = range(null, 100);
    
    private final DictionaryChecker dictionaryChecker;
    private final SimpleObjectValidator simpleObjectValidator;
    @Qualifier("SIMPLE_OBJECT_CUSTOM_CONSTRAINT") private final Checker<SimpleObjectDto> simpleObjectCustomConstraint;


    public void check(ValidationContext<EchoArraysBodyDto> ctx) {
        ctx.check(FIELD_PROP_INT_LIST, FIELD_PROP_INT_LIST_SIZE, list(FIELD_PROP_INT_LIST_MIN_MAX));
        ctx.check(FIELD_PROP_FLOAT_LIST, list(FIELD_PROP_FLOAT_LIST_MIN_MAX));
        ctx.check(FIELD_PROP_DOUBLE_LIST, required(), unique(), FIELD_PROP_DOUBLE_LIST_SIZE, list(FIELD_PROP_DOUBLE_LIST_MIN_MAX));
        ctx.check(FIELD_PROP_AMOUNT_LIST, list(FIELD_PROP_AMOUNT_LIST_PATTERN));
        ctx.check(FIELD_PROP_STRING_LIST, list(dictionaryChecker.check(ANIMALS).withError(INVALID_ANIMAL), FIELD_PROP_STRING_LIST_LENGTH));
        ctx.check(FIELD_PROP_DATE_TIME_LIST_OF_LIST, FIELD_PROP_DATE_TIME_LIST_OF_LIST_SIZE, list(FIELD_PROP_DATE_TIME_LIST_OF_LIST_2_SIZE));
        ctx.check(FIELD_PROP_OBJECT_LIST, list(simpleObjectValidator, simpleObjectCustomConstraint.withError(CUSTOM_FAILED_CODE)));
        ctx.check(FIELD_PROP_OBJECT_LIST_OF_LIST, list(list(simpleObjectValidator, simpleObjectCustomConstraint)));
        ctx.check(FIELD_PROP_MAP_OF_INT, mapValues(FIELD_PROP_MAP_OF_INT_MIN_MAX));
        ctx.check(FIELD_PROP_MAP_OF_OBJECT, mapValues(simpleObjectValidator));
        ctx.check(FIELD_PROP_MAP_OF_LIST_OF_OBJECT, mapValues(list(simpleObjectValidator)));
    }

}
