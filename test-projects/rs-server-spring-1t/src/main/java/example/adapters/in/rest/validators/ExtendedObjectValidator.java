package example.adapters.in.rest.validators;

import example.commons.validator.Checker;
import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.ExtendedObjectDto;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;
import static example.ports.in.rest.dtos.ExtendedObjectDto.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class ExtendedObjectValidator implements Checker<ExtendedObjectDto> {
    
    private final ExtendedObjectChecker extendedObjectChecker;
    private final SimpleObjectValidator simpleObjectValidator;
    private final ExtendedObjectEnumChecker extendedObjectEnumChecker;


    public void check(ValidationContext<ExtendedObjectDto> ctx) {
        ctx.check(extendedObjectChecker);
        simpleObjectValidator.checkWithParent(ctx);
        ctx.check(FIELD_EO_ENUM_REUSABLE, extendedObjectEnumChecker);
        ctx.check(FIELD_SELF_PROPERTY, this);
    }

}
