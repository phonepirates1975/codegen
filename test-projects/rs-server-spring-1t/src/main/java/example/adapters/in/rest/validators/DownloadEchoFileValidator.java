package example.adapters.in.rest.validators;

import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.DownloadEchoFileRequest;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;
import static example.ports.in.rest.dtos.DownloadEchoFileRequest.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class DownloadEchoFileValidator extends Validator<DownloadEchoFileRequest> {

    public void check(ValidationContext<DownloadEchoFileRequest> ctx) {
        ctx.check(FIELD_ID, required());
    }

}
