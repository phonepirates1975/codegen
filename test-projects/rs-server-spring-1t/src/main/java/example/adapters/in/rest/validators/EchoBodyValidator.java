package example.adapters.in.rest.validators;

import example.adapters.in.rest.validators.custom.DictionaryChecker;
import example.commons.validator.AmountChecker;
import example.commons.validator.Checker;
import example.commons.validator.Checkers;
import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.EchoBodyDto;
import example.ports.in.rest.dtos.SimpleObjectDto;
import java.math.BigDecimal;
import java.util.regex.Pattern;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.validators.custom.DictionaryCodes.*;
import static example.adapters.in.rest.validators.custom.ErrorCodes.*;
import static example.commons.validator.CommonCheckers.*;
import static example.ports.in.rest.dtos.EchoBodyDto.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoBodyValidator implements Checker<EchoBodyDto> {
    
    private static final Checker<Long> FIELD_PROP_INT_REQUIRED_MIN_MAX = range(-1l, null);
    private static final Checker<Float> FIELD_PROP_FLOAT_MIN_MAX = range(null, 101f);
    private static final Checker<Double> FIELD_PROP_DOUBLE_MIN_MAX = range(0.01, 9999.9999);
    private static final Checker<String> FIELD_PROP_AMOUNT_PATTERN = matches(Pattern.compile("^(0|([1-9][0-9]{0,}))\\.\\d{2}$"));
    private static final Checker<BigDecimal> FIELD_PROP_AMOUNT_NUMBER_MIN_MAX = range(new BigDecimal("0.02"), new BigDecimal("9999999999.99"));
    private static final Checker<String> FIELD_PROP_STRING_LENGTH = length(5, 10);
    private static final Checker<String> FIELD_PROP_STRING_PATTERN_PATTERN = matches(Pattern.compile("^[\\-\\\".',:;/\\\\!@#$%^&*()+_?|><=]{2,19}$"), "custom_error_code");
    private static final Checker<String> FIELD_PROP_DEFAULT_LENGTH = length(5, null);
    
    private final UserDataValidationBean userDataValidationBean;
    private final AmountChecker amountChecker;
    private final DictionaryChecker dictionaryChecker;
    private final SimpleObjectValidator simpleObjectValidator;
    @Qualifier("SIMPLE_OBJECT_CUSTOM_CONSTRAINT") private final Checker<SimpleObjectDto> simpleObjectCustomConstraint;
    private final ExtendedObjectValidator extendedObjectValidator;


    public void check(ValidationContext<EchoBodyDto> ctx) {
        ctx.check(FIELD_PROP_INT_REQUIRED, required(), FIELD_PROP_INT_REQUIRED_MIN_MAX);
        ctx.check(FIELD_PROP_FLOAT, FIELD_PROP_FLOAT_MIN_MAX);
        ctx.check(FIELD_PROP_DOUBLE, FIELD_PROP_DOUBLE_MIN_MAX);
        ctx.check(FIELD_PROP_AMOUNT, FIELD_PROP_AMOUNT_PATTERN, userDataValidationBean::checkAmountByUser, Checkers.AMOUNT_SCALE_CHECKER);
        ctx.check(FIELD_PROP_AMOUNT_NUMBER, amountChecker, FIELD_PROP_AMOUNT_NUMBER_MIN_MAX);
        ctx.check(FIELD_PROP_STRING, FIELD_PROP_STRING_LENGTH, dictionaryChecker.check(COLORS));
        ctx.check(FIELD_PROP_STRING_PATTERN, FIELD_PROP_STRING_PATTERN_PATTERN);
        ctx.check(FIELD_PROP_DEFAULT, FIELD_PROP_DEFAULT_LENGTH);
        ctx.check(FIELD_PROP_OBJECT, simpleObjectValidator, simpleObjectCustomConstraint);
        ctx.check(FIELD_PROP_OBJECT_EXTENDED, extendedObjectValidator);
        ctx.check(lt(FIELD_PROP_INT, FIELD_PROP_INT_SECOND).withError(COMPARE_PROP_INT_FAILED));
        ctx.check(ge(FIELD_PROP_DATE, FIELD_PROP_DATE_SECOND));
    }

}
