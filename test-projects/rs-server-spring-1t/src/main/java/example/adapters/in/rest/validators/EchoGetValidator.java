package example.adapters.in.rest.validators;

import example.commons.validator.Checker;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.EchoGetRequest;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;
import static example.ports.in.rest.dtos.EchoGetRequest.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoGetValidator extends Validator<EchoGetRequest> {
    
    private static final Checker<Long> FIELD_PROP_INT_REQUIRED_MIN_MAX = range(5l, null);
    private static final Checker<Float> FIELD_PROP_FLOAT_MIN_MAX = range(null, 5.02f);
    


    public void check(ValidationContext<EchoGetRequest> ctx) {
        ctx.check(FIELD_CORRELATION_ID_PARAM, required());
        ctx.check(FIELD_PROP_INT_REQUIRED, required(), FIELD_PROP_INT_REQUIRED_MIN_MAX);
        ctx.check(FIELD_PROP_FLOAT, FIELD_PROP_FLOAT_MIN_MAX);
    }

}
