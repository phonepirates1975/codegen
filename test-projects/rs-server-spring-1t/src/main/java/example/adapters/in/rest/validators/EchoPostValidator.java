package example.adapters.in.rest.validators;

import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.EchoPostRequest;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;
import static example.ports.in.rest.dtos.EchoPostRequest.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoPostValidator extends Validator<EchoPostRequest> {
    
    private final AuthorizationChecker authorizationChecker;
    private final EchoBodyValidator echoBodyValidator;


    public void check(ValidationContext<EchoPostRequest> ctx) {
        ctx.check(FIELD_AUTHORIZATION_PARAM, authorizationChecker, required());
        ctx.check(FIELD_CORRELATION_ID_PARAM, required());
        ctx.check(FIELD_REQUEST_BODY, required(), echoBodyValidator);
    }

}
