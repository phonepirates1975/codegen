package example.adapters.in.rest.controllers;

import example.adapters.in.rest.validators.EchoArraysPostValidator;
import example.ports.in.rest.IEchoArraysPostCommand;
import example.ports.in.rest.dtos.EchoArraysBodyDto;
import example.ports.in.rest.dtos.EchoArraysPostRequest;
import example.process.Context;
import java.util.List;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostController {
    
    private final Context context;
    private final EchoArraysPostValidator echoArraysPostValidator;
    private final IEchoArraysPostCommand echoArraysPostCommand;


    @PostMapping(value = "/api/v1/echo-arrays", produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity<List<EchoArraysBodyDto>> echoArraysPost(@RequestHeader("Authorization") String authorizationParam, @RequestHeader(value = "Inline-Header-Param", required = false) String inlineHeaderParam, @RequestBody List<EchoArraysBodyDto> requestBody) {
        EchoArraysPostRequest request = new EchoArraysPostRequest()
                .setAuthorizationParam(authorizationParam)
                .setInlineHeaderParam(inlineHeaderParam)
                .setRequestBody(requestBody)
                .setContext(context);
        echoArraysPostValidator.validate(request);
        return ResponseEntity.ok(echoArraysPostCommand.execute(request));
    }

}
