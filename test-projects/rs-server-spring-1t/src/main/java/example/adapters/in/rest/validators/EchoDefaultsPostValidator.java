package example.adapters.in.rest.validators;

import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.EchoDefaultsPostRequest;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;
import static example.ports.in.rest.dtos.EchoDefaultsPostRequest.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsPostValidator extends Validator<EchoDefaultsPostRequest> {
    
    private final EchoDefaultsBodyValidator echoDefaultsBodyValidator;


    public void check(ValidationContext<EchoDefaultsPostRequest> ctx) {
        ctx.check(FIELD_REQUEST_BODY, required(), echoDefaultsBodyValidator);
    }

}
