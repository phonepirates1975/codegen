package example.adapters.in.rest.controllers;

import example.adapters.in.rest.validators.EchoEmptyValidator;
import example.ports.in.rest.IEchoEmptyQuery;
import example.ports.in.rest.dtos.EchoEmptyRequest;
import example.process.Context;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoEmptyController {
    
    private final Context context;
    private final EchoEmptyValidator echoEmptyValidator;
    private final IEchoEmptyQuery echoEmptyQuery;


    @GetMapping(value = "/api/v1/echo-empty", produces = {})
    public ResponseEntity<Void> echoEmpty() {
        EchoEmptyRequest request = new EchoEmptyRequest()
                .setContext(context);
        echoEmptyValidator.validate(request);
        echoEmptyQuery.execute(request);
        return ResponseEntity.ok().build();
    }

}
