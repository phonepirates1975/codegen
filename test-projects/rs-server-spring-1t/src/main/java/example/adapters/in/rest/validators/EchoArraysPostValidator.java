package example.adapters.in.rest.validators;

import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.EchoArraysPostRequest;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;
import static example.ports.in.rest.dtos.EchoArraysPostRequest.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostValidator extends Validator<EchoArraysPostRequest> {
    
    private final AuthorizationChecker authorizationChecker;
    private final EchoArraysBodyValidator echoArraysBodyValidator;


    public void check(ValidationContext<EchoArraysPostRequest> ctx) {
        ctx.check(FIELD_AUTHORIZATION_PARAM, authorizationChecker, required());
        ctx.check(FIELD_REQUEST_BODY, required(), list(echoArraysBodyValidator));
    }

}
