package example.adapters.in.rest.validators;

import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.EchoErrorRequest;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;
import static example.ports.in.rest.dtos.EchoErrorRequest.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoErrorValidator extends Validator<EchoErrorRequest> {

    public void check(ValidationContext<EchoErrorRequest> ctx) {
        ctx.check(FIELD_ERROR_MESSAGE, required());
    }

}
