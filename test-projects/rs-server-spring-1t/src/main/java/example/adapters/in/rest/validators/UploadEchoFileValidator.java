package example.adapters.in.rest.validators;

import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.UploadEchoFileRequest;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;
import static example.ports.in.rest.dtos.UploadEchoFileRequest.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileValidator extends Validator<UploadEchoFileRequest> {

    public void check(ValidationContext<UploadEchoFileRequest> ctx) {
        ctx.check(FIELD_ID, required());
        ctx.check(FIELD_REQUEST_BODY, required());
    }

}
