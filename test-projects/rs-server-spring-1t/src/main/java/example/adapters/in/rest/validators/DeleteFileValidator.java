package example.adapters.in.rest.validators;

import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.DeleteFileRequest;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;
import static example.ports.in.rest.dtos.DeleteFileRequest.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class DeleteFileValidator extends Validator<DeleteFileRequest> {

    public void check(ValidationContext<DeleteFileRequest> ctx) {
        ctx.check(FIELD_ID, required());
    }

}
