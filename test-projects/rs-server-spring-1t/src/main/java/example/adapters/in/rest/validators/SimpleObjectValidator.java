package example.adapters.in.rest.validators;

import example.commons.validator.Checker;
import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.SimpleObjectDto;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;
import static example.ports.in.rest.dtos.SimpleObjectDto.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class SimpleObjectValidator implements Checker<SimpleObjectDto> {

    public void check(ValidationContext<SimpleObjectDto> ctx) {
        ctx.check(FIELD_SO_PROP_STRING, required());
    }

}
