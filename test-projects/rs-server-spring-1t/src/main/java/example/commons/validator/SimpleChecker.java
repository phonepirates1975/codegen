package example.commons.validator;

import java.util.function.Consumer;
import java.util.function.Function;
import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public class SimpleChecker<I> implements Checker<I> {

    private final Function<ValidationContext<I>, Boolean> checker;
    private final Consumer<ValidationContext<I>> resultWriter;
    private final boolean checkNull;

    public SimpleChecker(
            Function<ValidationContext<I>, Boolean> checker,
            Consumer<ValidationContext<I>> resultWriter) {
        this.checker = checker;
        this.resultWriter = resultWriter;
        this.checkNull = false;
    }

    public SimpleChecker(
            Function<ValidationContext<I>, Boolean> checker,
            Consumer<ValidationContext<I>> resultWriter,
            boolean checkNull
            ) {
        this.checker = checker;
        this.resultWriter = resultWriter;
        this.checkNull = checkNull;
    }

    @Override
    public void check(ValidationContext<I> ctx) {
        if (!checker.apply(ctx)) {
            resultWriter.accept(ctx);
        }
    }

    @Override
    public boolean checkNull() {
        return checkNull;
    }
}