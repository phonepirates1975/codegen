package example.commons.validator;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class AmountChecker extends SimpleChecker<BigDecimal> {

    public AmountChecker() {
        super(
                ctx -> ctx.getValue().scale() == 2,
                ctx -> ctx.addError("is_not_amount", ctx.getName() + " is not amount"));
    }
}
