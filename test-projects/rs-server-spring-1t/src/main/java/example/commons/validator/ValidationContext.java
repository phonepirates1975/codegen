package example.commons.validator;

import lombok.Getter;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.UnaryOperator;

@Generated("pl.metaprogramming.codegen")
public class ValidationContext<I> {

    @Getter private final ValidationContext<?> parent;
    @Getter private final ValidationResult result;
    @Getter private final I value;
    @Getter private final String name;

    private Map<Object, Object> requestScope;

    private final UnaryOperator<ValidationError> errorInterceptor;

    ValidationContext(I value) {
        this.value = value;
        this.result = new ValidationResult(value);
        this.parent = null;
        this.name = null;
        this.errorInterceptor = null;
    }

    ValidationContext(I value, String name, ValidationContext<?> parent) {
        this.parent = parent;
        this.result = parent.result;
        this.errorInterceptor = parent.errorInterceptor;
        this.value = value;
        this.name = name;
    }

    ValidationContext(ValidationContext<I> orignal, UnaryOperator<ValidationError> errorInterceptor) {
        this.parent = orignal.parent;
        this.result = orignal.result;
        this.value = orignal.value;
        this.name = orignal.name;
        this.errorInterceptor = errorInterceptor;
    }

    protected String getChildName(String childName) {
        return name == null || "requestBody".equals(name) ? childName : (name + "." + childName);
    }

    public ValidationContext<I> addError(String code) {
        return addError(code, code);
    }

    public ValidationContext<I> addError(String code, String message, Object...args) {
        return addError(ValidationError.builder().field(name).code(code).message(message).messageArgs(args).build());
    }

    public ValidationContext<I> addError(ValidationError error) {
        result.addError(errorInterceptor == null ? error : errorInterceptor.apply(error));
        return this;
    }

    public ValidationContext<I> setStatus(int status) {
        result.setStatus(status);
        return this;
    }

    public boolean isValid() {
        return result.isValid();
    }

    public boolean isCurrentFieldValid() {
        return result.isFieldValid(name);
    }

    private boolean shouldTest() {
        return !result.isStopped() && isCurrentFieldValid();
    }

    public <T> T getRequest(Class<T> clazz) {
        return clazz.cast(result.getRequest());
    }

    public <T> boolean isRequest(Class<T> clazz) {
        return result.getRequest().getClass().isAssignableFrom(clazz);
    }

    public <T> T getBean(Class<T> beanClass) {
        if (requestScope != null && requestScope.containsKey(beanClass)) {
            return beanClass.cast(requestScope.get(beanClass));
        }
        if (parent != null) {
            return parent.getBean(beanClass);
        }
        throw new IllegalStateException("No validation bean " + beanClass);
    }


    public <T> void setBean(Class<T> beanClass, Function<ValidationContext<I>,T> beanCreator) {
        if (requestScope == null) {
            requestScope = new HashMap<>();
        }
        requestScope.put(beanClass, beanCreator.apply(this));
    }

    @SafeVarargs
    public final void check(Checker<I>... checkers) {
        if (shouldTest()) {
            for (Checker<I> checker : checkers) {
                if (!shouldTest()) {
                    break;
                }
                performCheck(checker);
            }
        }
    }

    private void performCheck(Checker<I> checker) {
        if (value != null || checker.checkNull()) {
            try {
                checker.check(this);
            } catch (Exception e) {
                if (isValid()) {
                    throw new IllegalStateException("Can't perform validation", e);
                }
            }
        }
    }

    @SafeVarargs
    public final <T> void check(Field<I,T> field, Checker<T>...checkers) {
        getChild(field).check(checkers);
    }

    public <T> ValidationContext<T> getChild(Field<I,T> field) {
        return new ValidationContext<>(field.getValue(getValue()), getChildName(field.getName()), this);
    }

    @SuppressWarnings("unchecked")
    public <T> Optional<ValidationContext<T>> getParent(Class<T> clazz) {
        if (parent == null) {
            return Optional.empty();
        }
        if (parent.getValue().getClass() == clazz) {
            return Optional.of((ValidationContext<T>) parent);
        }
        return parent.getParent(clazz);
    }

    public <T> T getParentValue(Class<T> clazz) {
        return getParent(clazz).map(ValidationContext::getValue).orElse(null);
    }

    @SuppressWarnings("java:S1452")
    public ValidationContext<?> getRoot() {
        return parent != null ? parent.getRoot() : this;
    }

    public  <T> void setLocalBean(T bean) {
        if (requestScope == null) {
            requestScope = new HashMap<>();
        }
        requestScope.put(bean.getClass(), bean);
    }

    public <T> void setBean(T bean) {
        getRoot().setLocalBean(bean);
    }

}