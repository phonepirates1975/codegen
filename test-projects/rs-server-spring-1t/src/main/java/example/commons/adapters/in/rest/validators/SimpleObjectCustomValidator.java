/*
 * Copyright (c) 2020 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.commons.adapters.in.rest.validators;

import example.commons.validator.Checker;
import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.EchoArraysBodyDto;
import example.ports.in.rest.dtos.EchoBodyDto;
import example.ports.in.rest.dtos.EnumType;
import example.ports.in.rest.dtos.SimpleObjectDto;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import static example.commons.validator.CommonCheckers.length;

@Component
@Qualifier("SIMPLE_OBJECT_CUSTOM_CONSTRAINT")
public class SimpleObjectCustomValidator implements Checker<SimpleObjectDto> {

    public void check(ValidationContext<SimpleObjectDto> ctx) {
        EchoArraysBodyDto arrayBody = ctx.getParentValue(EchoArraysBodyDto.class);
        if (arrayBody != null && arrayBody.getPropEnumList() != null && arrayBody.getPropEnumList().contains(EnumType.B)) {
            ctx.check(SimpleObjectDto.FIELD_SO_PROP_STRING, length(6, 6));
        }

        EchoBodyDto parent = ctx.getParentValue(EchoBodyDto.class);
        if (parent != null && EnumType.V_1 == parent.getPropEnum()) {
            ctx.check(SimpleObjectDto.FIELD_SO_PROP_STRING, length(2, 2));
        }
    }
}
