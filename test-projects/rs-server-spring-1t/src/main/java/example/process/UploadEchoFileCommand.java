package example.process;

import example.ports.in.rest.IUploadEchoFileCommand;
import example.ports.in.rest.dtos.UploadEchoFileRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;

@Service
@RequiredArgsConstructor
public class UploadEchoFileCommand implements IUploadEchoFileCommand {

    private final FileStore files;

    @Override
    public void execute(@Nonnull UploadEchoFileRequest request) {
        files.store(request.getId(), request.getRequestBody());
    }

}
