package example.process;

import example.ports.in.rest.IDownloadEchoFileQuery;
import example.ports.in.rest.dtos.DownloadEchoFileRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;

@Service
@Scope("prototype")
@RequiredArgsConstructor
public class DownloadEchoFileQuery implements IDownloadEchoFileQuery {

    private final FileStore files;

    @Override
    public ResponseEntity<byte[]> execute(@Nonnull DownloadEchoFileRequest request) {
        return files.load(request.getId())
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

}
