package example.process;

import example.ports.in.rest.IEchoEmptyQuery;
import example.ports.in.rest.dtos.EchoEmptyRequest;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;

@Component
public class EchoEmptyQuery implements IEchoEmptyQuery {

    @Override
    public void execute(@Nonnull EchoEmptyRequest request) {
        // nothing to do
    }

}
