package example.process;

import example.ports.in.rest.IEchoDateArrayGetQuery;
import example.ports.in.rest.dtos.EchoDateArrayGetRequest;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.time.LocalDate;
import java.util.List;

@Service
@Scope("prototype")
public class EchoDateArrayGetQuery implements IEchoDateArrayGetQuery {

    @Override
    public List<LocalDate> execute(@Nonnull EchoDateArrayGetRequest request) {
        return request.getDateArray();
    }

}
