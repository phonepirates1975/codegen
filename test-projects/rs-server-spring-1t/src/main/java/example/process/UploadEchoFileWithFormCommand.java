package example.process;

import example.ports.in.rest.IUploadEchoFileWithFormCommand;
import example.ports.in.rest.dtos.UploadEchoFileWithFormRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;

@Component
@RequiredArgsConstructor
public class UploadEchoFileWithFormCommand implements IUploadEchoFileWithFormCommand {

    private final FileStore files;

    @Override
    public void execute(@Nonnull UploadEchoFileWithFormRequest request) {
        files.store(request.getId(), request.getFile());
    }

}
