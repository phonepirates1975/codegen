package example.process;

import example.ports.in.rest.IEchoDefaultsPostCommand;
import example.ports.in.rest.dtos.EchoDefaultsBodyDto;
import example.ports.in.rest.dtos.EchoDefaultsPostRequest;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;

@Service
@Scope("prototype")
public class EchoDefaultsPostCommand implements IEchoDefaultsPostCommand {

    @Override
    public ResponseEntity<EchoDefaultsBodyDto> execute(@Nonnull EchoDefaultsPostRequest request) {
        return ResponseEntity
                .ok()
                .header("Default-Header-Param", request.getDefaultHeaderParam().getValue())
                .body(request.getRequestBody());
    }

}
