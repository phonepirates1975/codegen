package example.process;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class UserData {
    BigDecimal minAmount;
    BigDecimal maxAmount;
}
