package example.process;

import example.ports.in.rest.IEchoGetQuery;
import example.ports.in.rest.dtos.EchoGetBodyDto;
import example.ports.in.rest.dtos.EchoGetRequest;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;

@Service
@Scope("prototype")
public class EchoGetQuery implements IEchoGetQuery {

    @Override
    public EchoGetBodyDto execute(@Nonnull EchoGetRequest request) {
        return new EchoGetBodyDto()
                .setPropIntRequired(request.getPropIntRequired())
                .setPropFloat(request.getPropFloat())
                .setPropEnum(request.getPropEnum());
    }

}
