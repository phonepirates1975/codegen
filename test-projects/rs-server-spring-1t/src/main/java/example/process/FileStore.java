package example.process;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class FileStore {

    private final Map<Long, byte[]> files = new ConcurrentHashMap<>();

    public void store(Long id, byte[] file) {
        files.put(id, file);
    }

    public void delete(Long id) {
        files.remove(id);
    }

    public Optional<byte[]> load(Long id) {
        return Optional.ofNullable(files.get(id));
    }

}
