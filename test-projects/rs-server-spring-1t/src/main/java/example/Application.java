package example;

import example.filters.RestLoggingFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;

import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    // useful for debugging binary requests
//    @Bean
    public FilterRegistrationBean restLoggingFilter() {
        FilterRegistrationBean<RestLoggingFilter> registration = new FilterRegistrationBean<>();
        registration.setFilter(new RestLoggingFilter());
        registration.addUrlPatterns("/api/*");
        registration.setOrder(HIGHEST_PRECEDENCE);
        return registration;
    }

}
