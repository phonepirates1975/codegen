/*
 * Copyright (c) 2020 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.filters;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class RestLoggingFilter extends OncePerRequestFilter {
    private static final String CHARSET = "UTF-8";
    private static final Logger apiLog = org.slf4j.LoggerFactory.getLogger("api");


    @Override protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        BufferedRequestWrapper wrappedRequest = new BufferedRequestWrapper(request);
        ContentCachingResponseWrapper wrappedResponse = new ContentCachingResponseWrapper(response);
        traceReq(wrappedRequest);
        try {
            filterChain.doFilter(wrappedRequest, wrappedResponse);
        } finally {
            traceRes(wrappedResponse);
            wrappedResponse.copyBodyToResponse();
        }
    }

    private void traceReq(BufferedRequestWrapper request) {
        HttpServletRequest httpReq = (HttpServletRequest) request.getRequest();
        String headers = Collections.list(httpReq.getHeaderNames()).stream()
                .map(h -> h + ": '" + httpReq.getHeader(h) + "'")
                .collect(Collectors.joining(", "));
        apiLog.info(formatLogParams(Arrays.asList(
                request.getMethod(),
                request.getRequestURI() + (request.getQueryString() != null ? "?" + request.getQueryString() : "")))
                + " " + request.getRequestBody()
                + " HEADERS[" + headers + "]")
        ;
    }

    private void traceRes(ContentCachingResponseWrapper response) {
        HttpServletResponse httpRes = (HttpServletResponse) response.getResponse();
        String headers = httpRes.getHeaderNames().stream()
                .map(h -> h + ": '" + httpRes.getHeader(h) + "'")
                .collect(Collectors.joining(", "));
        String body = "";
        try {
            body = IOUtils.toString(response.getContentAsByteArray(), CHARSET);
        } catch (Exception e) {
            log.warn("Can't get response body", e);
        }
        apiLog.info("[" + response.getStatus() + "] " + body + " HEADERS[" + headers + "]");
    }

    private String formatLogParams(List<Object> params) {
        StringBuilder buf = new StringBuilder();
        params.forEach(s -> {if (s != null) buf.append('[').append(s).append(']');});
        return buf.toString();
    }
}
