package example.ports.in.rest.dtos;

import example.commons.validator.Field;
import example.process.Context;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileWithFormRequest {
    
    public static final Field<UploadEchoFileWithFormRequest,Long> FIELD_ID = new Field<>("id (PATH parameter)", UploadEchoFileWithFormRequest::getId);
    public static final Field<UploadEchoFileWithFormRequest,byte[]> FIELD_FILE = new Field<>("file (FORMDATA parameter)", UploadEchoFileWithFormRequest::getFile);
    
    private Context context;
    
    /**
     * id PATH parameter.
     * <br/>id of file
     */
    @Nonnull private Long id;
    
    /**
     * file FORMDATA parameter.
     * <br/>file to upload
     */
    @Nonnull private byte[] file;

}
