package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import example.commons.validator.Field;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoArraysBodyDto {
    
    public static final Field<EchoArraysBodyDto,List<Integer>> FIELD_PROP_INT_LIST = new Field<>("prop_int_list", EchoArraysBodyDto::getPropIntList);
    public static final Field<EchoArraysBodyDto,List<Float>> FIELD_PROP_FLOAT_LIST = new Field<>("prop_float_list", EchoArraysBodyDto::getPropFloatList);
    public static final Field<EchoArraysBodyDto,List<Double>> FIELD_PROP_DOUBLE_LIST = new Field<>("prop_double_list", EchoArraysBodyDto::getPropDoubleList);
    public static final Field<EchoArraysBodyDto,List<String>> FIELD_PROP_AMOUNT_LIST = new Field<>("prop_amount_list", EchoArraysBodyDto::getPropAmountList);
    public static final Field<EchoArraysBodyDto,List<String>> FIELD_PROP_STRING_LIST = new Field<>("prop_string_list", EchoArraysBodyDto::getPropStringList);
    public static final Field<EchoArraysBodyDto,List<LocalDate>> FIELD_PROP_DATE_LIST = new Field<>("prop_date_list", EchoArraysBodyDto::getPropDateList);
    public static final Field<EchoArraysBodyDto,List<List<ZonedDateTime>>> FIELD_PROP_DATE_TIME_LIST_OF_LIST = new Field<>("prop_date_time_list_of_list", EchoArraysBodyDto::getPropDateTimeListOfList);
    public static final Field<EchoArraysBodyDto,List<Boolean>> FIELD_PROP_BOOLEAN_LIST = new Field<>("prop_boolean_list", EchoArraysBodyDto::getPropBooleanList);
    public static final Field<EchoArraysBodyDto,List<SimpleObjectDto>> FIELD_PROP_OBJECT_LIST = new Field<>("prop_object_list", EchoArraysBodyDto::getPropObjectList);
    public static final Field<EchoArraysBodyDto,List<List<SimpleObjectDto>>> FIELD_PROP_OBJECT_LIST_OF_LIST = new Field<>("prop_object_list_of_list", EchoArraysBodyDto::getPropObjectListOfList);
    public static final Field<EchoArraysBodyDto,List<ReusableEnum>> FIELD_PROP_ENUM_REUSABLE_LIST = new Field<>("prop_enum_reusable_list", EchoArraysBodyDto::getPropEnumReusableList);
    public static final Field<EchoArraysBodyDto,List<EnumType>> FIELD_PROP_ENUM_LIST = new Field<>("prop_enum_list", EchoArraysBodyDto::getPropEnumList);
    public static final Field<EchoArraysBodyDto,Map<String,Integer>> FIELD_PROP_MAP_OF_INT = new Field<>("prop_map_of_int", EchoArraysBodyDto::getPropMapOfInt);
    public static final Field<EchoArraysBodyDto,Map<String,SimpleObjectDto>> FIELD_PROP_MAP_OF_OBJECT = new Field<>("prop_map_of_object", EchoArraysBodyDto::getPropMapOfObject);
    public static final Field<EchoArraysBodyDto,Map<String,List<SimpleObjectDto>>> FIELD_PROP_MAP_OF_LIST_OF_OBJECT = new Field<>("prop_map_of_list_of_object", EchoArraysBodyDto::getPropMapOfListOfObject);
    
    @JsonProperty("prop_int_list") @Nullable private List<Integer> propIntList;
    @JsonProperty("prop_float_list") @Nullable private List<Float> propFloatList;
    @JsonProperty("prop_double_list") @Nonnull private List<Double> propDoubleList;
    @JsonProperty("prop_amount_list") @Nullable private List<String> propAmountList;
    @JsonProperty("prop_string_list") @Nullable private List<String> propStringList;
    @JsonProperty("prop_date_list") @Nullable private List<LocalDate> propDateList;
    @JsonProperty("prop_date_time_list_of_list") @Nullable private List<List<ZonedDateTime>> propDateTimeListOfList;
    @JsonProperty("prop_boolean_list") @Nullable private List<Boolean> propBooleanList;
    @JsonProperty("prop_object_list") @Nullable private List<SimpleObjectDto> propObjectList;
    @JsonProperty("prop_object_list_of_list") @Nullable private List<List<SimpleObjectDto>> propObjectListOfList;
    
    /**
     * enum property with external definition
     */
    @JsonProperty("prop_enum_reusable_list") @Nullable private List<ReusableEnum> propEnumReusableList;
    @JsonProperty("prop_enum_list") @Nullable private List<EnumType> propEnumList;
    
    /**
     * associative array with integer value
     */
    @JsonProperty("prop_map_of_int") @Nullable private Map<String,Integer> propMapOfInt;
    
    /**
     * associative array with object as value
     */
    @JsonProperty("prop_map_of_object") @Nullable private Map<String,SimpleObjectDto> propMapOfObject;
    
    /**
     * associative array with list of objects as value
     */
    @JsonProperty("prop_map_of_list_of_object") @Nullable private Map<String,List<SimpleObjectDto>> propMapOfListOfObject;

}
