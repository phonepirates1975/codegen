package example.ports.in.rest;

import example.ports.in.rest.dtos.EchoErrorRequest;
import javax.annotation.Generated;
import javax.annotation.Nonnull;

@Generated("pl.metaprogramming.codegen")
public interface IEchoErrorQuery {
    

    void execute(@Nonnull EchoErrorRequest request);
    
}
