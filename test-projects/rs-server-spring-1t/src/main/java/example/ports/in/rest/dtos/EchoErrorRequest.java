package example.ports.in.rest.dtos;

import example.commons.validator.Field;
import example.process.Context;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoErrorRequest {
    
    public static final Field<EchoErrorRequest,String> FIELD_ERROR_MESSAGE = new Field<>("errorMessage (QUERY parameter)", EchoErrorRequest::getErrorMessage);
    
    private Context context;
    
    /**
     * errorMessage QUERY parameter.
     * <br/>Example header param
     */
    @Nonnull private String errorMessage;

}
