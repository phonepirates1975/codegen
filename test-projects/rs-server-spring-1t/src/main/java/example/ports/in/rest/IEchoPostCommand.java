package example.ports.in.rest;

import example.ports.in.rest.dtos.EchoBodyDto;
import example.ports.in.rest.dtos.EchoPostRequest;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import org.springframework.http.ResponseEntity;

@Generated("pl.metaprogramming.codegen")
public interface IEchoPostCommand {
    

    ResponseEntity<EchoBodyDto> execute(@Nonnull EchoPostRequest request);
    
}
