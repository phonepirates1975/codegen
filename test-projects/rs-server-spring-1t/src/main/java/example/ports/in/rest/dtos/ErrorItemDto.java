package example.ports.in.rest.dtos;

import example.commons.validator.Field;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class ErrorItemDto {
    
    public static final Field<ErrorItemDto,String> FIELD_CODE = new Field<>("code", ErrorItemDto::getCode);
    
    @Nonnull private String code;

}
