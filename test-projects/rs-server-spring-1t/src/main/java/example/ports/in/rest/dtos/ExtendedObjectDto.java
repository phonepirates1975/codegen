package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import example.commons.validator.Field;
import javax.annotation.Generated;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Generated("pl.metaprogramming.codegen")
public class ExtendedObjectDto extends SimpleObjectDto {
    
    public static final Field<ExtendedObjectDto,ReusableEnum> FIELD_EO_ENUM_REUSABLE = new Field<>("eo_enum_reusable", ExtendedObjectDto::getEoEnumReusable);
    public static final Field<ExtendedObjectDto,ExtendedObjectDto> FIELD_SELF_PROPERTY = new Field<>("self_property", ExtendedObjectDto::getSelfProperty);
    
    
    /**
     * enum property with external definition
     */
    @JsonProperty("eo_enum_reusable") @Nullable private ReusableEnum eoEnumReusable;
    
    /**
     * Simple object for testing
     */
    @JsonProperty("self_property") @Nullable private ExtendedObjectDto selfProperty;

}
