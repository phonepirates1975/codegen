package example.ports.in.rest;

import example.ports.in.rest.dtos.EchoEmptyRequest;
import javax.annotation.Generated;
import javax.annotation.Nonnull;

@Generated("pl.metaprogramming.codegen")
public interface IEchoEmptyQuery {
    

    void execute(@Nonnull EchoEmptyRequest request);
    
}
