package example.ports.in.rest;

import example.ports.in.rest.dtos.UploadEchoFileRequest;
import javax.annotation.Generated;
import javax.annotation.Nonnull;

@Generated("pl.metaprogramming.codegen")
public interface IUploadEchoFileCommand {
    

    /**
     * upload file
     */
    void execute(@Nonnull UploadEchoFileRequest request);
    
}
