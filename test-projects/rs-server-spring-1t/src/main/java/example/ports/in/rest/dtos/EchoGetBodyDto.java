package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import example.commons.validator.Field;
import javax.annotation.Generated;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoGetBodyDto {
    
    public static final Field<EchoGetBodyDto,Long> FIELD_PROP_INT_REQUIRED = new Field<>("prop_int_required", EchoGetBodyDto::getPropIntRequired);
    public static final Field<EchoGetBodyDto,Float> FIELD_PROP_FLOAT = new Field<>("prop_float", EchoGetBodyDto::getPropFloat);
    public static final Field<EchoGetBodyDto,DefaultEnum> FIELD_PROP_ENUM = new Field<>("prop_enum", EchoGetBodyDto::getPropEnum);
    
    @JsonProperty("prop_int_required") @Nullable private Long propIntRequired;
    @JsonProperty("prop_float") @Nullable private Float propFloat;
    @JsonProperty("prop_enum") @Nullable private DefaultEnum propEnum;

}
