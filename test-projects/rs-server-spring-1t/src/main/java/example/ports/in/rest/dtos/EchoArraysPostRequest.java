package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import example.commons.validator.Field;
import example.process.Context;
import example.process.IRequest;
import java.util.List;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostRequest implements IRequest {
    
    public static final Field<EchoArraysPostRequest,String> FIELD_AUTHORIZATION_PARAM = new Field<>("Authorization (HEADER parameter)", EchoArraysPostRequest::getAuthorizationParam);
    public static final Field<EchoArraysPostRequest,String> FIELD_INLINE_HEADER_PARAM = new Field<>("Inline-Header-Param (HEADER parameter)", EchoArraysPostRequest::getInlineHeaderParam);
    public static final Field<EchoArraysPostRequest,List<EchoArraysBodyDto>> FIELD_REQUEST_BODY = new Field<>("requestBody", EchoArraysPostRequest::getRequestBody);
    
    private Context context;
    
    /**
     * Authorization HEADER parameter.
     * <br/>The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
     */
    @Nonnull private String authorizationParam;
    
    /**
     * Inline-Header-Param HEADER parameter.
     * <br/>Example header param
     */
    @JsonProperty("Inline-Header-Param") @Nullable private String inlineHeaderParam;
    
    /**
     * body param
     */
    @Nonnull private List<EchoArraysBodyDto> requestBody;

}
