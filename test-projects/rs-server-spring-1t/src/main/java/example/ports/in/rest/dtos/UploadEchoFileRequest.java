package example.ports.in.rest.dtos;

import example.commons.validator.Field;
import example.process.Context;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileRequest {
    
    public static final Field<UploadEchoFileRequest,Long> FIELD_ID = new Field<>("id (PATH parameter)", UploadEchoFileRequest::getId);
    public static final Field<UploadEchoFileRequest,byte[]> FIELD_REQUEST_BODY = new Field<>("requestBody", UploadEchoFileRequest::getRequestBody);
    
    private Context context;
    
    /**
     * id PATH parameter.
     * <br/>id of file
     */
    @Nonnull private Long id;
    
    /**
     * file data to upload
     */
    @Nonnull private byte[] requestBody;

}
