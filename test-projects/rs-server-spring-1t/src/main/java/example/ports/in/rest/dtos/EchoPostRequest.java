package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import example.commons.validator.Field;
import example.process.Context;
import example.process.IRequest;
import java.time.ZonedDateTime;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoPostRequest implements IRequest {
    
    public static final Field<EchoPostRequest,String> FIELD_AUTHORIZATION_PARAM = new Field<>("Authorization (HEADER parameter)", EchoPostRequest::getAuthorizationParam);
    public static final Field<EchoPostRequest,String> FIELD_CORRELATION_ID_PARAM = new Field<>("X-Correlation-ID (HEADER parameter)", EchoPostRequest::getCorrelationIdParam);
    public static final Field<EchoPostRequest,ZonedDateTime> FIELD_TIMESTAMP_PARAM = new Field<>("timestamp (HEADER parameter)", EchoPostRequest::getTimestampParam);
    public static final Field<EchoPostRequest,String> FIELD_INLINE_HEADER_PARAM = new Field<>("Inline-Header-Param (HEADER parameter)", EchoPostRequest::getInlineHeaderParam);
    public static final Field<EchoPostRequest,EchoBodyDto> FIELD_REQUEST_BODY = new Field<>("requestBody", EchoPostRequest::getRequestBody);
    
    private Context context;
    
    /**
     * Authorization HEADER parameter.
     * <br/>The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
     */
    @Nonnull private String authorizationParam;
    
    /**
     * X-Correlation-ID HEADER parameter.
     * <br/>Correlates HTTP requests between a client and server. 
     */
    @Nonnull private String correlationIdParam;
    
    /**
     * timestamp HEADER parameter.
     */
    @Nullable private ZonedDateTime timestampParam;
    
    /**
     * Inline-Header-Param HEADER parameter.
     * <br/>Example header param
     */
    @JsonProperty("Inline-Header-Param") @Nullable private String inlineHeaderParam;
    
    /**
     * body param
     */
    @Nonnull private EchoBodyDto requestBody;

}
