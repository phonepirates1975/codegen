package example.ports.in.rest;

import example.ports.in.rest.dtos.EchoGetBodyDto;
import example.ports.in.rest.dtos.EchoGetRequest;
import javax.annotation.Generated;
import javax.annotation.Nonnull;

@Generated("pl.metaprogramming.codegen")
public interface IEchoGetQuery {
    

    EchoGetBodyDto execute(@Nonnull EchoGetRequest request);
    
}
