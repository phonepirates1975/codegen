package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import example.commons.validator.Field;
import java.time.LocalDate;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class SimpleObjectDto {
    
    public static final Field<SimpleObjectDto,String> FIELD_SO_PROP_STRING = new Field<>("so_prop_string", SimpleObjectDto::getSoPropString);
    public static final Field<SimpleObjectDto,LocalDate> FIELD_SO_PROP_DATE = new Field<>("so_prop_date", SimpleObjectDto::getSoPropDate);
    
    @JsonProperty("so_prop_string") @Nonnull private String soPropString;
    @JsonProperty("so_prop_date") @Nullable private LocalDate soPropDate;

}
