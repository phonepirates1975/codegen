package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import example.commons.validator.Field;
import example.process.Context;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsPostRequest {
    
    public static final Field<EchoDefaultsPostRequest,DefaultEnum> FIELD_DEFAULT_HEADER_PARAM = new Field<>("Default-Header-Param (HEADER parameter)", EchoDefaultsPostRequest::getDefaultHeaderParam);
    public static final Field<EchoDefaultsPostRequest,EchoDefaultsBodyDto> FIELD_REQUEST_BODY = new Field<>("requestBody", EchoDefaultsPostRequest::getRequestBody);
    
    private Context context;
    
    /**
     * Default-Header-Param HEADER parameter.
     * <br/>Example header param with default value
     */
    @JsonProperty("Default-Header-Param") @Nullable private DefaultEnum defaultHeaderParam = DefaultEnum.A2;
    
    /**
     * body param
     */
    @Nonnull private EchoDefaultsBodyDto requestBody;

}
