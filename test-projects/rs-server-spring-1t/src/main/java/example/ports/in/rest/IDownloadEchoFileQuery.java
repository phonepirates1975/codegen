package example.ports.in.rest;

import example.ports.in.rest.dtos.DownloadEchoFileRequest;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import org.springframework.http.ResponseEntity;

@Generated("pl.metaprogramming.codegen")
public interface IDownloadEchoFileQuery {
    

    /**
     * Returns a file
     */
    ResponseEntity<byte[]> execute(@Nonnull DownloadEchoFileRequest request);
    
}
