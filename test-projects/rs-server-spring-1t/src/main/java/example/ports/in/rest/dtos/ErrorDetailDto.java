package example.ports.in.rest.dtos;

import example.commons.validator.Field;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class ErrorDetailDto {
    
    public static final Field<ErrorDetailDto,String> FIELD_FIELD = new Field<>("field", ErrorDetailDto::getField);
    public static final Field<ErrorDetailDto,String> FIELD_CODE = new Field<>("code", ErrorDetailDto::getCode);
    public static final Field<ErrorDetailDto,String> FIELD_MESSAGE = new Field<>("message", ErrorDetailDto::getMessage);
    
    @Nullable private String field;
    @Nonnull private String code;
    @Nonnull private String message;

}
