package example.ports.in.rest;

import example.ports.in.rest.dtos.DeleteFileRequest;
import javax.annotation.Generated;
import javax.annotation.Nonnull;

@Generated("pl.metaprogramming.codegen")
public interface IDeleteFileCommand {
    

    /**
     * deletes a file based on the id supplied
     */
    void execute(@Nonnull DeleteFileRequest request);
    
}
