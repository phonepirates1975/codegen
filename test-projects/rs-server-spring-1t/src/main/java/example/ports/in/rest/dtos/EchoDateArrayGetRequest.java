package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import example.commons.validator.Field;
import example.process.Context;
import java.time.LocalDate;
import java.util.List;
import javax.annotation.Generated;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoDateArrayGetRequest {
    
    public static final Field<EchoDateArrayGetRequest,List<LocalDate>> FIELD_DATE_ARRAY = new Field<>("date_array (QUERY parameter)", EchoDateArrayGetRequest::getDateArray);
    
    private Context context;
    
    /**
     * date_array QUERY parameter.
     */
    @JsonProperty("date_array") @Nullable private List<LocalDate> dateArray;

}
