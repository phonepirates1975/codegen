package example.ports.in.rest.dtos;

import example.commons.SerializationUtils;
import example.commons.validator.Field;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import javax.annotation.Generated;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsBodyDto {
    
    public static final Field<EchoDefaultsBodyDto,Integer> FIELD_PROP_INT = new Field<>("propInt", EchoDefaultsBodyDto::getPropInt);
    public static final Field<EchoDefaultsBodyDto,Float> FIELD_PROP_FLOAT = new Field<>("propFloat", EchoDefaultsBodyDto::getPropFloat);
    public static final Field<EchoDefaultsBodyDto,Double> FIELD_PROP_DOUBLE = new Field<>("propDouble", EchoDefaultsBodyDto::getPropDouble);
    public static final Field<EchoDefaultsBodyDto,BigDecimal> FIELD_PROP_NUMBER = new Field<>("propNumber", EchoDefaultsBodyDto::getPropNumber);
    public static final Field<EchoDefaultsBodyDto,String> FIELD_PROP_STRING = new Field<>("propString", EchoDefaultsBodyDto::getPropString);
    public static final Field<EchoDefaultsBodyDto,ReusableEnum> FIELD_PROP_ENUM = new Field<>("propEnum", EchoDefaultsBodyDto::getPropEnum);
    public static final Field<EchoDefaultsBodyDto,LocalDate> FIELD_PROP_DATE = new Field<>("propDate", EchoDefaultsBodyDto::getPropDate);
    public static final Field<EchoDefaultsBodyDto,ZonedDateTime> FIELD_PROP_DATE_TIME = new Field<>("propDateTime", EchoDefaultsBodyDto::getPropDateTime);
    
    @Nullable private Integer propInt = 101;
    @Nullable private Float propFloat = 101.101f;
    @Nullable private Double propDouble = 202.202;
    @Nullable private BigDecimal propNumber = new BigDecimal("3.33");
    @Nullable private String propString = "WHITE";
    @Nullable private ReusableEnum propEnum = ReusableEnum.V_3;
    @Nullable private LocalDate propDate = SerializationUtils.toLocalDate("2021-09-01");
    @Nullable private ZonedDateTime propDateTime = ZonedDateTime.parse("2021-09-01T09:09:09Z");

}
