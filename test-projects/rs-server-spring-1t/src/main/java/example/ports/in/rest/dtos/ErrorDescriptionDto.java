package example.ports.in.rest.dtos;

import example.commons.validator.Field;
import java.util.List;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class ErrorDescriptionDto {
    
    public static final Field<ErrorDescriptionDto,Integer> FIELD_CODE = new Field<>("code", ErrorDescriptionDto::getCode);
    public static final Field<ErrorDescriptionDto,String> FIELD_MESSAGE = new Field<>("message", ErrorDescriptionDto::getMessage);
    public static final Field<ErrorDescriptionDto,List<ErrorDetailDto>> FIELD_ERRORS = new Field<>("errors", ErrorDescriptionDto::getErrors);
    
    @Nonnull private Integer code;
    @Nonnull private String message;
    @Nullable private List<ErrorDetailDto> errors;

}
