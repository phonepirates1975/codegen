package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import example.commons.validator.Field;
import example.process.Context;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoGetRequest {
    
    public static final Field<EchoGetRequest,String> FIELD_CORRELATION_ID_PARAM = new Field<>("X-Correlation-ID (HEADER parameter)", EchoGetRequest::getCorrelationIdParam);
    public static final Field<EchoGetRequest,Long> FIELD_PROP_INT_REQUIRED = new Field<>("prop_int_required (QUERY parameter)", EchoGetRequest::getPropIntRequired);
    public static final Field<EchoGetRequest,Float> FIELD_PROP_FLOAT = new Field<>("prop_float (QUERY parameter)", EchoGetRequest::getPropFloat);
    public static final Field<EchoGetRequest,DefaultEnum> FIELD_PROP_ENUM = new Field<>("prop_enum (QUERY parameter)", EchoGetRequest::getPropEnum);
    
    private Context context;
    
    /**
     * X-Correlation-ID HEADER parameter.
     * <br/>Correlates HTTP requests between a client and server. 
     */
    @Nonnull private String correlationIdParam;
    
    /**
     * prop_int_required QUERY parameter.
     */
    @JsonProperty("prop_int_required") @Nonnull private Long propIntRequired;
    
    /**
     * prop_float QUERY parameter.
     */
    @JsonProperty("prop_float") @Nullable private Float propFloat;
    
    /**
     * prop_enum QUERY parameter.
     */
    @JsonProperty("prop_enum") @Nullable private DefaultEnum propEnum;

}
