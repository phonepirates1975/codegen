package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import example.commons.validator.Field;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Map;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoBodyDto {
    
    public static final Field<EchoBodyDto,Integer> FIELD_PROP_INT = new Field<>("prop_int", EchoBodyDto::getPropInt);
    public static final Field<EchoBodyDto,Integer> FIELD_PROP_INT_SECOND = new Field<>("prop_int_second", EchoBodyDto::getPropIntSecond);
    public static final Field<EchoBodyDto,Long> FIELD_PROP_INT_REQUIRED = new Field<>("prop_int_required", EchoBodyDto::getPropIntRequired);
    public static final Field<EchoBodyDto,Float> FIELD_PROP_FLOAT = new Field<>("prop_float", EchoBodyDto::getPropFloat);
    public static final Field<EchoBodyDto,Double> FIELD_PROP_DOUBLE = new Field<>("prop_double", EchoBodyDto::getPropDouble);
    public static final Field<EchoBodyDto,String> FIELD_PROP_AMOUNT = new Field<>("prop_amount", EchoBodyDto::getPropAmount);
    public static final Field<EchoBodyDto,BigDecimal> FIELD_PROP_AMOUNT_NUMBER = new Field<>("prop_amount_number", EchoBodyDto::getPropAmountNumber);
    public static final Field<EchoBodyDto,String> FIELD_PROP_STRING = new Field<>("prop_string", EchoBodyDto::getPropString);
    public static final Field<EchoBodyDto,String> FIELD_PROP_STRING_PATTERN = new Field<>("prop_string_pattern", EchoBodyDto::getPropStringPattern);
    public static final Field<EchoBodyDto,String> FIELD_PROP_DEFAULT = new Field<>("prop_default", EchoBodyDto::getPropDefault);
    public static final Field<EchoBodyDto,LocalDate> FIELD_PROP_DATE = new Field<>("prop_date", EchoBodyDto::getPropDate);
    public static final Field<EchoBodyDto,LocalDate> FIELD_PROP_DATE_SECOND = new Field<>("prop_date_second", EchoBodyDto::getPropDateSecond);
    public static final Field<EchoBodyDto,ZonedDateTime> FIELD_PROP_DATE_TIME = new Field<>("prop_date_time", EchoBodyDto::getPropDateTime);
    public static final Field<EchoBodyDto,byte[]> FIELD_PROP_BASE64 = new Field<>("prop_base64", EchoBodyDto::getPropBase64);
    public static final Field<EchoBodyDto,Boolean> FIELD_PROP_BOOLEAN = new Field<>("prop_boolean", EchoBodyDto::getPropBoolean);
    public static final Field<EchoBodyDto,SimpleObjectDto> FIELD_PROP_OBJECT = new Field<>("prop_object", EchoBodyDto::getPropObject);
    public static final Field<EchoBodyDto,Map<String,Object>> FIELD_PROP_OBJECT_ANY = new Field<>("prop_object_any", EchoBodyDto::getPropObjectAny);
    public static final Field<EchoBodyDto,ExtendedObjectDto> FIELD_PROP_OBJECT_EXTENDED = new Field<>("prop_object_extended", EchoBodyDto::getPropObjectExtended);
    public static final Field<EchoBodyDto,ReusableEnum> FIELD_PROP_ENUM_REUSABLE = new Field<>("prop_enum_reusable", EchoBodyDto::getPropEnumReusable);
    public static final Field<EchoBodyDto,EnumType> FIELD_PROP_ENUM = new Field<>("prop_enum", EchoBodyDto::getPropEnum);
    
    @JsonProperty("prop_int") @Nullable private Integer propInt;
    @JsonProperty("prop_int_second") @Nullable private Integer propIntSecond;
    @JsonProperty("prop_int_required") @Nonnull private Long propIntRequired;
    @JsonProperty("prop_float") @Nullable private Float propFloat;
    @JsonProperty("prop_double") @Nullable private Double propDouble;
    @JsonProperty("prop_amount") @Nullable private String propAmount;
    @JsonProperty("prop_amount_number") @Nullable private BigDecimal propAmountNumber;
    @JsonProperty("prop_string") @Nullable private String propString;
    @JsonProperty("prop_string_pattern") @Nullable private String propStringPattern;
    @JsonProperty("prop_default") @Nullable private String propDefault = "value";
    @JsonProperty("prop_date") @Nullable private LocalDate propDate;
    @JsonProperty("prop_date_second") @Nullable private LocalDate propDateSecond;
    @JsonProperty("prop_date_time") @Nullable private ZonedDateTime propDateTime;
    @JsonProperty("prop_base64") @Nullable private byte[] propBase64;
    @JsonProperty("prop_boolean") @Nullable private Boolean propBoolean;
    
    /**
     * object property
     */
    @JsonProperty("prop_object") @Nullable private SimpleObjectDto propObject;
    @JsonProperty("prop_object_any") @Nullable private Map<String,Object> propObjectAny;
    
    /**
     * Simple object for testing
     */
    @JsonProperty("prop_object_extended") @Nullable private ExtendedObjectDto propObjectExtended;
    
    /**
     * enum property with external definition
     */
    @JsonProperty("prop_enum_reusable") @Nullable private ReusableEnum propEnumReusable = ReusableEnum.A;
    
    /**
     * enum property
     */
    @JsonProperty("prop_enum") @Nullable private EnumType propEnum;

}
