package example.ports.in.rest;

import example.ports.in.rest.dtos.UploadEchoFileWithFormRequest;
import javax.annotation.Generated;
import javax.annotation.Nonnull;

@Generated("pl.metaprogramming.codegen")
public interface IUploadEchoFileWithFormCommand {
    

    /**
     * upload file using multipart/form-data
     */
    void execute(@Nonnull UploadEchoFileWithFormRequest request);
    
}
