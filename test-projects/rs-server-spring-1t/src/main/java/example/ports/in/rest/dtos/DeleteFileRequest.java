package example.ports.in.rest.dtos;

import example.commons.validator.Field;
import example.process.Context;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class DeleteFileRequest {
    
    public static final Field<DeleteFileRequest,Long> FIELD_ID = new Field<>("id (PATH parameter)", DeleteFileRequest::getId);
    
    private Context context;
    
    /**
     * id PATH parameter.
     * <br/>id of book to delete
     */
    @Nonnull private Long id;

}
