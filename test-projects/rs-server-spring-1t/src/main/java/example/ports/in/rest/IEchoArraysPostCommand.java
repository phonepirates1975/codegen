package example.ports.in.rest;

import example.ports.in.rest.dtos.EchoArraysBodyDto;
import example.ports.in.rest.dtos.EchoArraysPostRequest;
import java.util.List;
import javax.annotation.Generated;
import javax.annotation.Nonnull;

@Generated("pl.metaprogramming.codegen")
public interface IEchoArraysPostCommand {
    

    List<EchoArraysBodyDto> execute(@Nonnull EchoArraysPostRequest request);
    
}
