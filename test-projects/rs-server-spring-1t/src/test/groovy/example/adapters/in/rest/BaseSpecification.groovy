/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.in.rest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import rest.RestClient
import spock.lang.Specification

import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
abstract class BaseSpecification extends Specification {

    static final String HEADER_CORRELATION_ID = 'X-Correlation-ID'
    static final Object DO_NOT_SEND = 'DO_NOT_SEND'

    @LocalServerPort
    private int port

    @Autowired
    TestRestTemplate restTemplate

    abstract String getOperationPath()

    String getEndpoint(String operationPath = getOperationPath()) {
        "http://localhost:${port}/${operationPath}"
    }

    RestClient call(HttpMethod method, String path = getEndpoint()) {
        new RestClient(restTemplate.restTemplate, method, path)
    }

    protected <O> ResponseEntity<O> post(def request, Class<O> outputClass) {
        post(operationPath, request, outputClass)
    }

    protected <O> ResponseEntity<O> post(String operationPath, def request, Class<O> outputClass) {
        restTemplate.postForEntity(getEndpoint(operationPath), request, outputClass)
    }

    boolean checkCorrelationIdHeader(def response, def request) {
        response.getHeaders().getFirst(HEADER_CORRELATION_ID) == request.getHeaders().getFirst(HEADER_CORRELATION_ID)
    }

    def setHeader(String header, String defaultValue, HttpHeaders headers, Map params) {
        String key = 'header.' + header
        if (params.containsKey(key)) {
            if (params[key]) {
                headers.set(header, params[key] as String)
            }
        } else {
            if (defaultValue) {
                headers.set(header, defaultValue)
            }
        }
    }

    Map cleanMap(Map value, Object toRemove = null) {
        Map result = value.findAll { it.value != toRemove }
        result.each {
            if (it.value instanceof Map) {
                result[it.key] = cleanMap(it.value as Map, toRemove)
            }
        }
        result
    }

    static boolean equals(Map given, Map expected) {
        def notExpected = given.keySet() - expected.keySet()
        assert notExpected.empty
        def notPresent = expected.keySet() - given.keySet()
        assert notPresent.empty
        given.each { key, value ->
            assert expected[key] == value
        }
        true
    }

    def toDateTime(String value) {
        value ? ZonedDateTime.parse(value, DateTimeFormatter.ISO_OFFSET_DATE_TIME).withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime() : null
    }

}
