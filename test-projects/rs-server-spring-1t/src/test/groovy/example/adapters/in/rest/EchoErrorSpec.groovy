/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.in.rest


import example.ports.in.rest.dtos.ErrorDescriptionDto

class EchoErrorSpec extends BaseSpecification {

    String getOperationPath() {
        '/api/v1/echo-error'
    }

    def "should echo-error return error"() {
        given:
        def errorMessage = 'some error'

        when:
        def response = restTemplate.getForEntity(
                "${getEndpoint()}?errorMessage={message}",
                ErrorDescriptionDto,
                errorMessage)

        then:
        response.statusCodeValue == 500
        response.body.message == errorMessage
    }

}
