/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.in.rest


import static org.springframework.http.HttpMethod.GET

class EchoGetSpec extends BaseSpecification {

    static final String PROP_INT_REQUIRED = 'prop_int_required'
    static final String PROP_FLOAT = 'prop_float'
    static final String PROP_ENUM = 'prop_enum'

    @Override
    String getOperationPath() {
        'api/v1/echo'
    }

    def "should success"() {
        when:
        def response = call(GET)
                .headerParam(HEADER_CORRELATION_ID, '1234567890')
                .queryParam(PROP_INT_REQUIRED, propIntRequired)
                .queryParam(PROP_FLOAT, propFloat)
                .queryParam(PROP_ENUM, propEnum)
                .exchange(Map)

        then:
        response.statusCodeValue == 200
        response.body[PROP_INT_REQUIRED] == propIntRequired
        response.body[PROP_FLOAT] == propFloat
        response.body[PROP_ENUM] == propEnum

        where:
        propIntRequired | propFloat | propEnum
        10              | null      | null
        10              | 5.01      | 'a1'
    }

    // different error result type is not supported in 2t solution
    def "should fail"() {
        when:
        def response = call(GET)
                .headerParam(HEADER_CORRELATION_ID, '1234567890')
                .queryParam(PROP_INT_REQUIRED, propIntRequired)
                .queryParam(PROP_FLOAT, propFloat)
                .exchange(List)

        then:
        response.statusCodeValue == 400
        response.body.size() == 1

        where:
        propIntRequired | propFloat
        1               | null
        10              | 5.03
    }

    def "should fail - without right message"() {
        when:
        def response = call(GET)
                .headerParam(HEADER_CORRELATION_ID, '1234567890')
                .queryParam(PROP_INT_REQUIRED, propIntRequired)
                .queryParam(PROP_FLOAT, propFloat)
                .queryParam(PROP_ENUM, propEnum)
                .exchange(String)

        then:
        response.statusCodeValue == 400

        where:
        propIntRequired | propFloat | propEnum
        'ad'            | null      | null
        10              | 'da'      | null
        10              | 5.01      | 'bad'
    }
}
