PROJECTS=('rs-client-spring' 'rs-server-spring' 'rs-server-spring-legacy' 'ws-client-spring')
for project in "${PROJECTS[@]}"
do
  echo "Going to processing project $project ..."
  cd $project
  ./gradlew sonarqube
  cd ..
done