package example.ports.out.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Generated("pl.metaprogramming.codegen")
public class ExtendedObjectDto extends SimpleObjectDto {
    
    
    /**
     * enum property with external definition
     */
    @JsonProperty("eo_enum_reusable") @Nullable private ReusableEnum eoEnumReusable;
    
    /**
     * Simple object for testing
     */
    @JsonProperty("self_property") @Nullable private ExtendedObjectDto selfProperty;

}
