package example.ports.out.rest.dtos;

import java.time.ZonedDateTime;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoPostRequest {
    
    
    /**
     * Authorization HEADER parameter.
     * <br/>The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
     */
    @Nonnull private String authorizationParam;
    
    /**
     * X-Correlation-ID HEADER parameter.
     * <br/>Correlates HTTP requests between a client and server. 
     */
    @Nonnull private String correlationIdParam;
    
    /**
     * timestamp HEADER parameter.
     */
    @Nullable private ZonedDateTime timestampParam;
    
    /**
     * Inline-Header-Param HEADER parameter.
     * <br/>Example header param
     */
    @Nullable private String inlineHeaderParam;
    
    /**
     * body param
     */
    @Nonnull private EchoBodyDto requestBody;

}
