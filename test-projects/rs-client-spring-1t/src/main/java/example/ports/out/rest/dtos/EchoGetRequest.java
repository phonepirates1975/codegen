package example.ports.out.rest.dtos;

import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoGetRequest {
    
    
    /**
     * X-Correlation-ID HEADER parameter.
     * <br/>Correlates HTTP requests between a client and server. 
     */
    @Nonnull private String correlationIdParam;
    
    /**
     * prop_int_required QUERY parameter.
     */
    @Nonnull private Long propIntRequired;
    
    /**
     * prop_float QUERY parameter.
     */
    @Nullable private Float propFloat;
    
    /**
     * prop_enum QUERY parameter.
     */
    @Nullable private DefaultEnum propEnum;

}
