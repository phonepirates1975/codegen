package example.ports.out.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Map;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoBodyDto {
    
    @JsonProperty("prop_int") @Nullable private Integer propInt;
    @JsonProperty("prop_int_second") @Nullable private Integer propIntSecond;
    @JsonProperty("prop_int_required") @Nonnull private Long propIntRequired;
    @JsonProperty("prop_float") @Nullable private Float propFloat;
    @JsonProperty("prop_double") @Nullable private Double propDouble;
    @JsonProperty("prop_amount") @Nullable private String propAmount;
    @JsonProperty("prop_amount_number") @Nullable private BigDecimal propAmountNumber;
    @JsonProperty("prop_string") @Nullable private String propString;
    @JsonProperty("prop_string_pattern") @Nullable private String propStringPattern;
    @JsonProperty("prop_default") @Nullable private String propDefault = "value";
    @JsonProperty("prop_date") @Nullable private LocalDate propDate;
    @JsonProperty("prop_date_second") @Nullable private LocalDate propDateSecond;
    @JsonProperty("prop_date_time") @Nullable private ZonedDateTime propDateTime;
    @JsonProperty("prop_base64") @Nullable private byte[] propBase64;
    @JsonProperty("prop_boolean") @Nullable private Boolean propBoolean;
    
    /**
     * object property
     */
    @JsonProperty("prop_object") @Nullable private SimpleObjectDto propObject;
    @JsonProperty("prop_object_any") @Nullable private Map<String,Object> propObjectAny;
    
    /**
     * Simple object for testing
     */
    @JsonProperty("prop_object_extended") @Nullable private ExtendedObjectDto propObjectExtended;
    
    /**
     * enum property with external definition
     */
    @JsonProperty("prop_enum_reusable") @Nullable private ReusableEnum propEnumReusable = ReusableEnum.A;
    
    /**
     * enum property
     */
    @JsonProperty("prop_enum") @Nullable private EnumType propEnum;

}
