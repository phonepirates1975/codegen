package example.ports.out.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoArraysBodyDto {
    
    @JsonProperty("prop_int_list") @Nullable private List<Integer> propIntList;
    @JsonProperty("prop_float_list") @Nullable private List<Float> propFloatList;
    @JsonProperty("prop_double_list") @Nonnull private List<Double> propDoubleList;
    @JsonProperty("prop_amount_list") @Nullable private List<String> propAmountList;
    @JsonProperty("prop_string_list") @Nullable private List<String> propStringList;
    @JsonProperty("prop_date_list") @Nullable private List<LocalDate> propDateList;
    @JsonProperty("prop_date_time_list_of_list") @Nullable private List<List<ZonedDateTime>> propDateTimeListOfList;
    @JsonProperty("prop_boolean_list") @Nullable private List<Boolean> propBooleanList;
    @JsonProperty("prop_object_list") @Nullable private List<SimpleObjectDto> propObjectList;
    @JsonProperty("prop_object_list_of_list") @Nullable private List<List<SimpleObjectDto>> propObjectListOfList;
    
    /**
     * enum property with external definition
     */
    @JsonProperty("prop_enum_reusable_list") @Nullable private List<ReusableEnum> propEnumReusableList;
    @JsonProperty("prop_enum_list") @Nullable private List<EnumType> propEnumList;
    
    /**
     * associative array with integer value
     */
    @JsonProperty("prop_map_of_int") @Nullable private Map<String,Integer> propMapOfInt;
    
    /**
     * associative array with object as value
     */
    @JsonProperty("prop_map_of_object") @Nullable private Map<String,SimpleObjectDto> propMapOfObject;
    
    /**
     * associative array with list of objects as value
     */
    @JsonProperty("prop_map_of_list_of_object") @Nullable private Map<String,List<SimpleObjectDto>> propMapOfListOfObject;

}
