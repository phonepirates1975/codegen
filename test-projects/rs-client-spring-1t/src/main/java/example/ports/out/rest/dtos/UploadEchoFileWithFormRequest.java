package example.ports.out.rest.dtos;

import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.core.io.Resource;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileWithFormRequest {
    
    
    /**
     * id PATH parameter.
     * <br/>id of file
     */
    @Nonnull private Long id;
    
    /**
     * file FORMDATA parameter.
     * <br/>file to upload
     */
    @Nonnull private Resource file;

}
