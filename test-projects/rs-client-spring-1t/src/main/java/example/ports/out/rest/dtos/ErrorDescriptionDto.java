package example.ports.out.rest.dtos;

import java.util.List;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class ErrorDescriptionDto {
    
    @Nonnull private Integer code;
    @Nonnull private String message;
    @Nullable private List<ErrorDetailDto> errors;

}
