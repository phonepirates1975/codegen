package example.ports.out.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class SimpleObjectDto {
    
    @JsonProperty("so_prop_string") @Nonnull private String soPropString;
    @JsonProperty("so_prop_date") @Nullable private LocalDate soPropDate;

}
