package example.ports.out.rest.dtos;

import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class DeleteFileRequest {
    
    
    /**
     * id PATH parameter.
     * <br/>id of book to delete
     */
    @Nonnull private Long id;

}
