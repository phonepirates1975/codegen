package example.adapters.out.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import example.commons.SerializationUtils;
import example.ports.out.rest.dtos.DefaultEnum;
import example.ports.out.rest.dtos.DeleteFileRequest;
import example.ports.out.rest.dtos.DownloadEchoFileRequest;
import example.ports.out.rest.dtos.EchoArraysBodyDto;
import example.ports.out.rest.dtos.EchoArraysPostRequest;
import example.ports.out.rest.dtos.EchoBodyDto;
import example.ports.out.rest.dtos.EchoDateArrayGetRequest;
import example.ports.out.rest.dtos.EchoDefaultsBodyDto;
import example.ports.out.rest.dtos.EchoDefaultsPostRequest;
import example.ports.out.rest.dtos.EchoEmptyRequest;
import example.ports.out.rest.dtos.EchoErrorRequest;
import example.ports.out.rest.dtos.EchoGetBodyDto;
import example.ports.out.rest.dtos.EchoGetRequest;
import example.ports.out.rest.dtos.EchoPostRequest;
import example.ports.out.rest.dtos.ErrorItemDto;
import example.ports.out.rest.dtos.UploadEchoFileRequest;
import example.ports.out.rest.dtos.UploadEchoFileWithFormRequest;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoClientImpl implements EchoClient {
    
    private final ExampleApiRestClientProvider clientProvider;


    @Override
    public ResponseEntity<EchoBodyDto> echoPost(EchoPostRequest request) {
        return clientProvider.of(HttpMethod.POST, "/api/v1/echo")
                .headerParam(HttpHeaders.AUTHORIZATION, request.getAuthorizationParam())
                .headerParam("X-Correlation-ID", request.getCorrelationIdParam())
                .headerParamOptional("timestamp", SerializationUtils.toString(request.getTimestampParam()))
                .headerParamOptional("Inline-Header-Param", request.getInlineHeaderParam())
                .body(request.getRequestBody())
                .exchange(EchoBodyDto.class);
    }

    @Override
    public EchoGetBodyDto echoGet(EchoGetRequest request) {
        return clientProvider.of(HttpMethod.GET, "/api/v1/echo")
                .queryParam("prop_int_required", SerializationUtils.toString(request.getPropIntRequired()))
                .queryParamOptional("prop_float", SerializationUtils.toString(request.getPropFloat()))
                .queryParamOptional("prop_enum", Optional.ofNullable(request.getPropEnum()).map(DefaultEnum::getValue).orElse(null))
                .headerParam("X-Correlation-ID", request.getCorrelationIdParam())
                .onError(400, new TypeReference<List<ErrorItemDto>>(){})
                .exchange(EchoGetBodyDto.class)
                .getBody();
    }

    @Override
    public ResponseEntity<EchoDefaultsBodyDto> echoDefaultsPost(EchoDefaultsPostRequest request) {
        return clientProvider.of(HttpMethod.POST, "/api/v1/echo-defaults")
                .headerParamOptional("Default-Header-Param", Optional.ofNullable(request.getDefaultHeaderParam()).map(DefaultEnum::getValue).orElse(null))
                .body(request.getRequestBody())
                .exchange(EchoDefaultsBodyDto.class);
    }

    @Override
    public List<EchoArraysBodyDto> echoArraysPost(EchoArraysPostRequest request) {
        return clientProvider.of(HttpMethod.POST, "/api/v1/echo-arrays")
                .headerParam(HttpHeaders.AUTHORIZATION, request.getAuthorizationParam())
                .headerParamOptional("Inline-Header-Param", request.getInlineHeaderParam())
                .body(request.getRequestBody())
                .exchange(new ParameterizedTypeReference<List<EchoArraysBodyDto>>() {})
                .getBody();
    }

    @Override
    public List<LocalDate> echoDateArrayGet(EchoDateArrayGetRequest request) {
        return clientProvider.of(HttpMethod.GET, "/api/v1/echo-date-array")
                .queryParam("date_array", SerializationUtils.transformList(request.getDateArray(), SerializationUtils::toString))
                .exchange(new ParameterizedTypeReference<List<LocalDate>>() {})
                .getBody();
    }

    @Override
    public void uploadEchoFileWithForm(UploadEchoFileWithFormRequest request) {
        clientProvider.of(HttpMethod.POST, "/api/v1/echo-file/{id}/form")
                .pathParam("id", request.getId())
                .headerParam("content-type", "multipart/form-data")
                .formDataParam("file", request.getFile())
                .exchange(Void.class);
    }

    @Override
    public void uploadEchoFile(UploadEchoFileRequest request) {
        clientProvider.of(HttpMethod.POST, "/api/v1/echo-file/{id}")
                .pathParam("id", request.getId())
                .headerParam("content-type", "application/octet-stream")
                .body(request.getRequestBody())
                .exchange(Void.class);
    }

    @Override
    public ResponseEntity<Resource> downloadEchoFile(DownloadEchoFileRequest request) {
        return clientProvider.of(HttpMethod.GET, "/api/v1/echo-file/{id}")
                .pathParam("id", request.getId())
                .exchange(Resource.class);
    }

    @Override
    public void deleteFile(DeleteFileRequest request) {
        clientProvider.of(HttpMethod.DELETE, "/api/v1/echo-file/{id}")
                .pathParam("id", request.getId())
                .exchange(Void.class);
    }

    @Override
    public void echoEmpty(EchoEmptyRequest request) {
        clientProvider.of(HttpMethod.GET, "/api/v1/echo-empty")
                .exchange(Void.class);
    }

    @Override
    public void echoError(EchoErrorRequest request) {
        clientProvider.of(HttpMethod.GET, "/api/v1/echo-error")
                .queryParam("errorMessage", request.getErrorMessage())
                .exchange(Void.class);
    }

}
