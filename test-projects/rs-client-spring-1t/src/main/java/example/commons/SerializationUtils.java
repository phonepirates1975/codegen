package example.commons;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public class SerializationUtils {
    

    private SerializationUtils() {
    }

    public static <T> T fromString(String value, Function<String,T> transformer) {
        return value == null || value.isEmpty() ? null : transformer.apply(value);
    }

    public static <T> String toString(T value, Function<T,String> transformer) {
        return value == null ? null : transformer.apply(value);
    }

    public static <R,T> List<R> transformList(List<T> value, Function<T,R> transformer) {
        return value == null ? null : value.stream().map(transformer).collect(Collectors.toList());
    }

    public static String toString(Long value) {
        return toString(value, Object::toString);
    }

    public static String toString(Float value) {
        return toString(value, Object::toString);
    }

    public static LocalDate toLocalDate(String value) {
        return fromString(value, LocalDate::parse);
    }

    public static String toString(LocalDate value) {
        return toString(value, DateTimeFormatter.ISO_LOCAL_DATE::format);
    }

    public static String toString(ZonedDateTime value) {
        return toString(value, v -> DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(value));
    }

}
