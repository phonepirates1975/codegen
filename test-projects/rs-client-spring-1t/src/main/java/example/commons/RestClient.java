package example.commons;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class RestClient {
    
    private final Map<Integer,Object> errorClasses = new HashMap<>();
    private final Map<String,Object> pathParams = new HashMap<>();
    private final MultiValueMap<String,String> headerParams = new LinkedMultiValueMap<>();
    private final MultiValueMap<String,Object> formDataParams = new LinkedMultiValueMap<>();
    private final MultiValueMap<String,String> queryParams = new LinkedMultiValueMap<>();
    
    private final HttpMethod method;
    private final String path;
    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;
    private Object defaultErrorType;
    private Object body;


    public RestClient body(@Nonnull Object body) {
        this.body = body;
        return this;
    }

    public RestClient headerParam(@Nonnull String name, @Nonnull String value) {
        headerParams.add(name, required(value, name));
        return this;
    }

    public RestClient headerParamOptional(@Nonnull String name, String value) {
        if (value != null) headerParam(name, value);
        return this;
    }

    public RestClient queryParam(@Nonnull String name, @Nonnull String value) {
        queryParams.add(name, required(value, name));
        return this;
    }

    public RestClient queryParamOptional(@Nonnull String name, String value) {
        if (value != null) queryParam(name, value);
        return this;
    }

    public RestClient queryParam(@Nonnull String name, List<String> value) {
        queryParams.addAll(name, value != null ? value : Collections.emptyList());
        return this;
    }

    public RestClient pathParam(@Nonnull String name, @Nonnull Object value) {
        pathParams.put(name, required(value, name));
        return this;
    }

    public RestClient formDataParam(@Nonnull String name, @Nonnull Object value) {
        formDataParams.add(name, required(value, name));
        return this;
    }

    private <T> T required(T value, String name) {
        return Objects.requireNonNull(value, name + " is required");
    }

    public <T> RestClient onError(TypeReference<T> defaultErrorType) {
        this.defaultErrorType = defaultErrorType;
        return this;
    }

    public <T> RestClient onError(Class<T> defaultErrorClass) {
        this.defaultErrorType = defaultErrorClass;
        return this;
    }

    public <T> RestClient onError(Integer httpStatus, TypeReference<T> errorType) {
        errorClasses.put(httpStatus, errorType);
        return this;
    }

    public <T> RestClient onError(Integer httpStatus, Class<T> errorClass) {
        errorClasses.put(httpStatus, errorClass);
        return this;
    }

    public <T> ResponseEntity<T> exchange(ParameterizedTypeReference<T> responseType) {
        return execute(() -> restTemplate.exchange(makeRequest(), responseType));
    }

    public <T> ResponseEntity<T> exchange(Class<T> responseType) {
        return execute(() -> restTemplate.exchange(makeRequest(), responseType));
    }

    private RequestEntity<?> makeRequest() {
        URI uri = UriComponentsBuilder.fromUriString(path)
                .queryParams(queryParams)
                .build(pathParams);
        Object requestBody = formDataParams.isEmpty() ? this.body : formDataParams;
        return new RequestEntity<>(requestBody, headerParams, method, uri);
    }

    private <T> T execute(Supplier<T> executor) {
        try {
            return executor.get();
        } catch (RestClientResponseException e) {
            throw map(e);
        }
    }

    private RuntimeException map(RestClientResponseException e) {
        try {
            HttpHeaders headers = e.getResponseHeaders();
            if (headers == null || !MediaType.APPLICATION_JSON.equals(headers.getContentType())) return e;
            Object dto = readValue(e.getResponseBodyAsString(), getErrorBodyType(e.getRawStatusCode()));
            return new RestClientException(e.getRawStatusCode(), headers, dto);
        } catch (Exception ex) {
            return e;
        }
    }

    private Object getErrorBodyType(int httpStatus) {
        return errorClasses.containsKey(httpStatus) ? errorClasses.get(httpStatus) : defaultErrorType;
    }

    private Object readValue(String body, Object type) throws JacksonException {
        if (type instanceof Class) {
            return objectMapper.readValue(body, (Class<?>) type);
        }
        if (type instanceof TypeReference) {
            return objectMapper.readValue(body, (TypeReference<?>) type);
        }
        throw new IllegalArgumentException("Unsupported type reference: " + type);
    }

}
