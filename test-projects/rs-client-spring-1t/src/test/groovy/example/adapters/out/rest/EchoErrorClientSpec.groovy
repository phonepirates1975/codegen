/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.out.rest

import example.commons.RestClientException
import example.ports.out.rest.dtos.EchoErrorRequest
import example.ports.out.rest.dtos.ErrorDescriptionDto

class EchoErrorClientSpec extends BaseSpecification {

    def "should fail on echo error"() {
        when:
        client.echoError(new EchoErrorRequest(errorMessage: 'some error'))
        then:
        def e = thrown(RestClientException)
        e.httpStatusCode == 500
        e.response instanceof ErrorDescriptionDto

        when:
        def details = e.getResponse(ErrorDescriptionDto)
        then:
        details.errors.size() == 1
        details.errors[0].field == null
        details.errors[0].code == 'error'
        details.errors[0].message == 'some error'
    }
}
