package example.adapters.out.rest

import example.commons.RestClientException
import example.ports.out.rest.dtos.EchoGetRequest
import example.ports.out.rest.dtos.ErrorItemDto

class EchoGetClientSpec extends BaseSpecification {

    def "should success"() {
        given:
        def request = new EchoGetRequest(
                correlationIdParam: UUID.randomUUID().toString(),
                propIntRequired: 6
        )
        when:
        def result = client.echoGet(request)
        then:
        result.propIntRequired == request.propIntRequired
    }

    def "should fail"() {
        when:
        client.echoGet(new EchoGetRequest(
                correlationIdParam: UUID.randomUUID().toString(),
                propIntRequired: 4
        ))
        then:
        def e = thrown(RestClientException)
        e.response instanceof List

        when:
        def details = e.getResponse(List<ErrorItemDto>) as List<ErrorItemDto>
        then:
        details.size() == 1
        details[0] instanceof ErrorItemDto
        details[0].code == 'is_too_small'
    }
}
