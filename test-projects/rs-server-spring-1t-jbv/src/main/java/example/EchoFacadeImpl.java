package example;

import example.ports.in.rest.EchoFacade;
import example.ports.in.rest.dtos.*;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class EchoFacadeImpl implements EchoFacade {

    private final Map<Long, byte[]> files = new ConcurrentHashMap<>();

    @Override
    public ResponseEntity<EchoBodyDto> echoPost(@Nonnull EchoPostRequest request) {
        return ResponseEntity.ok(request.getRequestBody());
    }

    @Override
    public EchoGetBodyDto echoGet(@Nonnull EchoGetRequest request) {
        return new EchoGetBodyDto()
                .setPropIntRequired(request.getPropIntRequired());
    }

    @Override
    public ResponseEntity<EchoDefaultsBodyDto> echoDefaultsPost(@Nonnull EchoDefaultsPostRequest request) {
        return ResponseEntity.ok(request.getRequestBody());
    }

    @Override
    public List<EchoArraysBodyDto> echoArraysPost(@Nonnull EchoArraysPostRequest request) {
        return request.getRequestBody();
    }

    @Override
    public List<LocalDate> echoDateArrayGet(@Nonnull EchoDateArrayGetRequest request) {
        return request.getDateArray();
    }

    @Override
    public void uploadEchoFile(@Nonnull UploadEchoFileRequest request) {
        files.put(request.getId(), request.getRequestBody());
    }

    @Override
    public void uploadEchoFileWithForm(@Nonnull UploadEchoFileWithFormRequest request) {
        files.put(request.getId(), request.getFile());
    }

    @Override
    public ResponseEntity<byte[]> downloadEchoFile(@Nonnull DownloadEchoFileRequest request) {
        if (files.containsKey(request.getId())) {
            return ResponseEntity.ok(files.get(request.getId()));
        }
        return ResponseEntity.notFound().build();
    }

    @Override
    public void deleteFile(@Nonnull DeleteFileRequest request) {
        files.remove(request.getId());
    }

    @Override
    public void echoEmpty(@Nonnull EchoEmptyRequest request) {
        // nothing to do
    }

    @Override
    public void echoError(@Nonnull EchoErrorRequest request) {
    }

}
