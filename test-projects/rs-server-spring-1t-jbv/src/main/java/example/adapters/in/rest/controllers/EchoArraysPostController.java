package example.adapters.in.rest.controllers;

import example.ports.in.rest.EchoFacade;
import example.ports.in.rest.dtos.EchoArraysBodyDto;
import example.ports.in.rest.dtos.EchoArraysPostRequest;
import java.util.List;
import javax.annotation.Generated;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostController {

    private final EchoFacade echoFacade;

    @PostMapping(value = "/api/v1/echo-arrays", produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity<List<EchoArraysBodyDto>> echoArraysPost(
            @RequestHeader("Authorization") String authorizationParam,
            @RequestHeader(value = "Inline-Header-Param", required = false) String inlineHeaderParam,
            @RequestBody @Valid List<EchoArraysBodyDto> requestBody) {
        EchoArraysPostRequest request = new EchoArraysPostRequest().setAuthorizationParam(authorizationParam)
                .setInlineHeaderParam(inlineHeaderParam).setRequestBody(requestBody);
        return ResponseEntity.ok(echoFacade.echoArraysPost(request));
    }

}
