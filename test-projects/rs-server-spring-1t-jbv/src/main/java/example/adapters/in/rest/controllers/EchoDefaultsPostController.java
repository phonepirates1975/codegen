package example.adapters.in.rest.controllers;

import example.ports.in.rest.EchoFacade;
import example.ports.in.rest.dtos.DefaultEnum;
import example.ports.in.rest.dtos.EchoDefaultsBodyDto;
import example.ports.in.rest.dtos.EchoDefaultsPostRequest;
import java.util.Optional;
import javax.annotation.Generated;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsPostController {

    private final EchoFacade echoFacade;

    @PostMapping(value = "/api/v1/echo-defaults", produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity<EchoDefaultsBodyDto> echoDefaultsPost(
            @RequestHeader(value = "Default-Header-Param", required = false) String defaultHeaderParam,
            @RequestBody @Valid EchoDefaultsBodyDto requestBody) {
        EchoDefaultsPostRequest request = new EchoDefaultsPostRequest()
                .setDefaultHeaderParam(
                        Optional.ofNullable(DefaultEnum.fromValue(defaultHeaderParam)).orElse(DefaultEnum.A2))
                .setRequestBody(requestBody);
        return echoFacade.echoDefaultsPost(request);
    }

}
