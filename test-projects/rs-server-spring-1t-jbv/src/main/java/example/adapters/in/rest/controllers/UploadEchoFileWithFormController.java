package example.adapters.in.rest.controllers;

import example.commons.SerializationUtils;
import example.ports.in.rest.EchoFacade;
import example.ports.in.rest.dtos.UploadEchoFileWithFormRequest;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileWithFormController {

    private final EchoFacade echoFacade;

    /**
     * upload file using multipart/form-data
     */
    @PostMapping(value = "/api/v1/echo-file/{id}/form", produces = {"application/json"}, consumes = {
            "multipart/form-data"})
    public ResponseEntity<Void> uploadEchoFileWithForm(@PathVariable Long id, @RequestPart MultipartFile file) {
        UploadEchoFileWithFormRequest request = new UploadEchoFileWithFormRequest().setId(id)
                .setFile(SerializationUtils.toBytes(file));
        echoFacade.uploadEchoFileWithForm(request);
        return ResponseEntity.noContent().build();
    }

}
