package example.adapters.in.rest.controllers;

import example.ports.in.rest.EchoFacade;
import example.ports.in.rest.dtos.EchoEmptyRequest;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoEmptyController {

    private final EchoFacade echoFacade;

    @GetMapping(value = "/api/v1/echo-empty", produces = {})
    public ResponseEntity<Void> echoEmpty() {
        EchoEmptyRequest request = new EchoEmptyRequest();
        echoFacade.echoEmpty(request);
        return ResponseEntity.ok().build();
    }

}
