package example.ports.in.rest.dtos;

import javax.annotation.Generated;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileRequest {

    /**
     * id PATH parameter. <br/>
     * id of file
     */
    @NotNull
    private Long id;

    /**
     * file data to upload
     */
    @NotNull
    private byte[] requestBody;

}
