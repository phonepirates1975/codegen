package example.ports.in.rest;

import example.ports.in.rest.dtos.DeleteFileRequest;
import example.ports.in.rest.dtos.DownloadEchoFileRequest;
import example.ports.in.rest.dtos.EchoArraysBodyDto;
import example.ports.in.rest.dtos.EchoArraysPostRequest;
import example.ports.in.rest.dtos.EchoBodyDto;
import example.ports.in.rest.dtos.EchoDateArrayGetRequest;
import example.ports.in.rest.dtos.EchoDefaultsBodyDto;
import example.ports.in.rest.dtos.EchoDefaultsPostRequest;
import example.ports.in.rest.dtos.EchoEmptyRequest;
import example.ports.in.rest.dtos.EchoErrorRequest;
import example.ports.in.rest.dtos.EchoGetBodyDto;
import example.ports.in.rest.dtos.EchoGetRequest;
import example.ports.in.rest.dtos.EchoPostRequest;
import example.ports.in.rest.dtos.UploadEchoFileRequest;
import example.ports.in.rest.dtos.UploadEchoFileWithFormRequest;
import java.time.LocalDate;
import java.util.List;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import org.springframework.http.ResponseEntity;

@Generated("pl.metaprogramming.codegen")
public interface EchoFacade {

    ResponseEntity<EchoBodyDto> echoPost(@Nonnull EchoPostRequest request);

    EchoGetBodyDto echoGet(@Nonnull EchoGetRequest request);

    ResponseEntity<EchoDefaultsBodyDto> echoDefaultsPost(@Nonnull EchoDefaultsPostRequest request);

    List<EchoArraysBodyDto> echoArraysPost(@Nonnull EchoArraysPostRequest request);

    List<LocalDate> echoDateArrayGet(@Nonnull EchoDateArrayGetRequest request);

    /**
     * upload file using multipart/form-data
     */
    void uploadEchoFileWithForm(@Nonnull UploadEchoFileWithFormRequest request);

    /**
     * upload file
     */
    void uploadEchoFile(@Nonnull UploadEchoFileRequest request);

    /**
     * Returns a file
     */
    ResponseEntity<byte[]> downloadEchoFile(@Nonnull DownloadEchoFileRequest request);

    /**
     * deletes a file based on the id supplied
     */
    void deleteFile(@Nonnull DeleteFileRequest request);

    void echoEmpty(@Nonnull EchoEmptyRequest request);

    void echoError(@Nonnull EchoErrorRequest request);

}
