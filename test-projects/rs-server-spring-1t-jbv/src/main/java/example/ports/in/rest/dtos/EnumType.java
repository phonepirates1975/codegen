package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonValue;
import example.commons.EnumValue;
import javax.annotation.Generated;
import lombok.Getter;

@Generated("pl.metaprogramming.codegen")
public enum EnumType implements EnumValue {

    A("A"), // A value
    B("b"), // b value
    V_1("1"); // 1 value

    @Getter
    @JsonValue
    private final String value;

    EnumType(String value) {
        this.value = value;
    }

}
