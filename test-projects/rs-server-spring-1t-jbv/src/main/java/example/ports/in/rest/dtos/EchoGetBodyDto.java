package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoGetBodyDto {

    @JsonProperty("prop_int_required")
    private Long propIntRequired;
    @JsonProperty("prop_float")
    private Float propFloat;
    @JsonProperty("prop_enum")
    private DefaultEnum propEnum;

}
