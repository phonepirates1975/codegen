package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoGetRequest {

    /**
     * X-Correlation-ID HEADER parameter. <br/>
     * Correlates HTTP requests between a client and server.
     */
    @NotNull
    private String correlationIdParam;

    /**
     * prop_int_required QUERY parameter.
     */
    @JsonProperty("prop_int_required")
    @NotNull
    @Min(5)
    private Long propIntRequired;

    /**
     * prop_float QUERY parameter.
     */
    @JsonProperty("prop_float")
    @DecimalMax("5.02")
    private Float propFloat;

    /**
     * prop_enum QUERY parameter.
     */
    @JsonProperty("prop_enum")
    private DefaultEnum propEnum;

}
