package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import example.process.IRequest;
import java.util.List;
import javax.annotation.Generated;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostRequest implements IRequest {

    /**
     * Authorization HEADER parameter. <br/>
     * The value of the Authorization header should consist of 'type' +
     * 'credentials', where for the approach using the 'type' token should be
     * 'Bearer'.
     */
    @NotNull
    private String authorizationParam;

    /**
     * Inline-Header-Param HEADER parameter. <br/>
     * Example header param
     */
    @JsonProperty("Inline-Header-Param")
    private String inlineHeaderParam;

    /**
     * body param
     */
    @NotNull
    private List<EchoArraysBodyDto> requestBody;

}
