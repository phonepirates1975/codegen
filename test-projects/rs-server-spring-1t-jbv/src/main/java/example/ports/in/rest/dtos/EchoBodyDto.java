package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Map;
import javax.annotation.Generated;
import javax.validation.Valid;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoBodyDto {

    @JsonProperty("prop_int")
    private Integer propInt;
    @JsonProperty("prop_int_second")
    private Integer propIntSecond;
    @JsonProperty("prop_int_required")
    @NotNull
    @Min(-1)
    private Long propIntRequired;
    @JsonProperty("prop_float")
    @DecimalMax("101")
    private Float propFloat;
    @JsonProperty("prop_double")
    @DecimalMin("0.01")
    @DecimalMax("9999.9999")
    private Double propDouble;
    @JsonProperty("prop_amount")
    private String propAmount;
    @JsonProperty("prop_amount_number")
    @DecimalMin("0.02")
    @DecimalMax("9999999999.99")
    private BigDecimal propAmountNumber;
    @JsonProperty("prop_string")
    private String propString;
    @JsonProperty("prop_string_pattern")
    private String propStringPattern;
    @JsonProperty("prop_default")
    private String propDefault = "value";
    @JsonProperty("prop_date")
    private LocalDate propDate;
    @JsonProperty("prop_date_second")
    private LocalDate propDateSecond;
    @JsonProperty("prop_date_time")
    private ZonedDateTime propDateTime;
    @JsonProperty("prop_base64")
    private byte[] propBase64;
    @JsonProperty("prop_boolean")
    private Boolean propBoolean;

    /**
     * object property
     */
    @JsonProperty("prop_object")
    @Valid
    private SimpleObjectDto propObject;
    @JsonProperty("prop_object_any")
    @Valid
    private Map<String, Object> propObjectAny;

    /**
     * Simple object for testing
     */
    @JsonProperty("prop_object_extended")
    @Valid
    private ExtendedObjectDto propObjectExtended;

    /**
     * enum property with external definition
     */
    @JsonProperty("prop_enum_reusable")
    private ReusableEnum propEnumReusable = ReusableEnum.A;

    /**
     * enum property
     */
    @JsonProperty("prop_enum")
    private EnumType propEnum;

}
