package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonValue;
import example.commons.EnumValue;
import javax.annotation.Generated;
import lombok.Getter;

@Generated("pl.metaprogramming.codegen")
public enum ReusableEnum implements EnumValue {

    A("a"), // a value
    B("B"), // B value
    V_3("3"); // 3 value

    @Getter
    @JsonValue
    private final String value;

    ReusableEnum(String value) {
        this.value = value;
    }

}
