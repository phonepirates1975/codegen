package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoArraysBodyDto {

    @JsonProperty("prop_int_list")
    private List<Integer> propIntList;
    @JsonProperty("prop_float_list")
    private List<Float> propFloatList;
    @JsonProperty("prop_double_list")
    @NotNull
    private List<Double> propDoubleList;
    @JsonProperty("prop_amount_list")
    private List<String> propAmountList;
    @JsonProperty("prop_string_list")
    private List<String> propStringList;
    @JsonProperty("prop_date_list")
    private List<LocalDate> propDateList;
    @JsonProperty("prop_date_time_list_of_list")
    private List<List<ZonedDateTime>> propDateTimeListOfList;
    @JsonProperty("prop_boolean_list")
    private List<Boolean> propBooleanList;
    @JsonProperty("prop_object_list")
    private List<SimpleObjectDto> propObjectList;
    @JsonProperty("prop_object_list_of_list")
    private List<List<SimpleObjectDto>> propObjectListOfList;

    /**
     * enum property with external definition
     */
    @JsonProperty("prop_enum_reusable_list")
    private List<ReusableEnum> propEnumReusableList;
    @JsonProperty("prop_enum_list")
    private List<EnumType> propEnumList;

    /**
     * associative array with integer value
     */
    @JsonProperty("prop_map_of_int")
    private Map<String, Integer> propMapOfInt;

    /**
     * associative array with object as value
     */
    @JsonProperty("prop_map_of_object")
    private Map<String, SimpleObjectDto> propMapOfObject;

    /**
     * associative array with list of objects as value
     */
    @JsonProperty("prop_map_of_list_of_object")
    private Map<String, List<SimpleObjectDto>> propMapOfListOfObject;

}
