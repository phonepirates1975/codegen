package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsPostRequest {

    /**
     * Default-Header-Param HEADER parameter. <br/>
     * Example header param with default value
     */
    @JsonProperty("Default-Header-Param")
    private DefaultEnum defaultHeaderParam = DefaultEnum.A2;

    /**
     * body param
     */
    @NotNull
    @Valid
    private EchoDefaultsBodyDto requestBody;

}
