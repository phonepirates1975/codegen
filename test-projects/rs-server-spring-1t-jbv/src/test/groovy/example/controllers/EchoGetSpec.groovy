/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.controllers

import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.web.util.UriComponentsBuilder

class EchoGetSpec extends BaseSpecification {

    @Override
    String getOperationPath() {
        'api/v1/echo'
    }

    def "should success"() {
        given:
        def params = makeParams()

        when:
        def response = call(params)

        then:
        response.statusCodeValue == 200
        cleanMap(response.body) == params
    }

    def cleanMap(Map value) {
        value.findAll { it.value != null }
    }

    def makeParams(Map params = [:]) {
        ['prop_int_required': params.getOrDefault('prop_int_required', 10)]
    }

    def call(Map<String, Object> params) {
        def builder = UriComponentsBuilder.fromHttpUrl(getEndpoint())
        params.each {
            builder.queryParam(it.key, it.value)
        }
        def headers = new HttpHeaders()
        setHeader(HEADER_CORRELATION_ID, '1234567890', headers, params)
        restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                new HttpEntity<>(headers),
                Map)
    }
}
