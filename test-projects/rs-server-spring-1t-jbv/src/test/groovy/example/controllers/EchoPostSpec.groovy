/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.controllers


import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import spock.lang.Unroll

import java.text.SimpleDateFormat

class EchoPostSpec extends BaseSpecification {

    @Override
    String getOperationPath() {
        'api/v1/echo'
    }

    @Unroll
    def "should success"() {
        given:
        def request = makeRequest(params)

        when:
        def response = call(request)

        then:
        response != null
        response.statusCode.value() == 200

        when:
        request.body.prop_default = 'value'
        request.body.prop_enum_reusable = 'a'
//        response.body.prop_date_time = parseISO8601(response.body.prop_date_time)
        then:
        equals(response.body, request.body)
        checkCorrelationIdHeader(response, request)

        where:
        params << [
                [:],
                ['header.timestamp': '2000-10-31T01:30:00.000-05:00'],
                ['header.timestamp': '2000-10-31T01:30:00.000Z'],
                [prop_object_any: null],
                [prop_object_any: [:]],
        ]
    }

    @Unroll
    def "should validation failed #message"() {
        given:
        def request = makeRequest(params)

        when:
        def response = call(request)

        then:
        response != null
        response.statusCode.value() == 400
        response.body.code == 400
        response.body.message == message
        checkCorrelationIdHeader(response, request)

        where:
        message                                                                    | params
        'prop_date is not yyyy-MM-dd'                                              | [prop_date: '2019-08-32']
        'Authorization (HEADER parameter) is required'                             | ['header.Authorization': null]
        'X-Correlation-ID (HEADER parameter) is required'                          | ['header.X-Correlation-ID': null]
        'timestamp (HEADER parameter) should be valid date time in ISO8601 format' | ['header.timestamp': '123123123']
        'requestBody is required'                                                  | ['body': null]
        'prop_int_required is required'                                            | ['prop_int_required': null]
        'prop_int_required is not 64bit integer'                                   | ['prop_int_required': 'x']
        'prop_int_required should be >= -1'                                        | ['prop_int_required': '-2']
        'prop_float should be <= 101'                                              | ['prop_float': '101.001']
        'prop_date_time should be valid date time in ISO8601 format'               | ['prop_date_time': 'x']
        'prop_enum should have one of values: [A, b, 1]'                           | ['prop_enum': 'x']
        'prop_object.so_prop_string is required'                                   | ['prop_object.so_prop_string': null]
        'prop_object_extended.so_prop_string is required'                          | ['prop_object_extended.so_prop_string': null]
        'prop_object_extended.self_property.so_prop_string is required'            | ['prop_object_extended.self_property.so_prop_string': null]

        // not failing invalid inputs
//        'prop_int_required is not 64bit integer'                                   | ['prop_int_required': 1.1]
    }

    static boolean equals(Map given, Map expected) {
        def notExpected = given.keySet() - expected.keySet()
        assert notExpected.empty
        def notPresent = expected.keySet() - given.keySet()
        assert notPresent.empty
        given.each { key, value ->
            assert expected[key] == value
        }
        true
    }

    static void parseISO8601(String value) {
        new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssXXX").parse(value)
    }

    def makeRequest(Map params) {
        def headers = new HttpHeaders()
        setHeader('Authorization', '123123123', headers, params)
        setHeader(HEADER_CORRELATION_ID, '1234567890', headers, params)
        setHeader('content-type', 'application/json', headers, params)
        setHeader('timestamp', null, headers, params)
        new HttpEntity<>(
                params.getOrDefault('body', makeBody(params)),
                headers)
    }

    Map makeBody(Map params = [:]) {
        [prop_int            : params.getOrDefault('prop_int', 2),
         prop_int_second     : params['prop_int_second'],
         prop_int_required   : params.getOrDefault('prop_int_required', 11),
         prop_float          : params.getOrDefault('prop_float', 1.1),
         prop_double         : 1.1111,
         prop_amount         : '12.10',
         prop_amount_number  : 21.21,
         prop_string         : 'tekst',
         prop_string_pattern : '::',
         prop_date           : params.getOrDefault('prop_date', '2021-08-27'),
         prop_date_second    : params['prop_date_second'],
         prop_date_time      : params.getOrDefault('prop_date_time', '2019-10-22T22:49:55.123Z'),
         prop_boolean        : true,
         prop_base64         : params.getOrDefault('prop_base64', 'dGVzdA=='),
         prop_enum           : params.getOrDefault('prop_enum', 'b'),
         prop_object         : [
                 so_prop_string: params.getOrDefault('prop_object.so_prop_string', 'inny tekst'),
                 so_prop_date  : '2019-10-21'
         ],
         prop_object_any     : params.getOrDefault('prop_object_any', [p1: 'P1', o: [op1: 1, bool: true]]),
         prop_object_extended: [
                 so_prop_string  : params.getOrDefault('prop_object_extended.so_prop_string', 'inny tekst'),
                 so_prop_date    : '2019-10-21',
                 eo_enum_reusable: 'a',
                 self_property   : [
                         so_prop_string  : params.getOrDefault('prop_object_extended.self_property.so_prop_string', 'bla bla'),
                         so_prop_date    : null,
                         eo_enum_reusable: 'a',
                         self_property   : null
                 ]
         ]
        ]
    }
}
