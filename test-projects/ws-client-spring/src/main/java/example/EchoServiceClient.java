package example;

import example.ws.ns1.ExtendedType;
import example.ws.ns1.Message;
import javax.annotation.Generated;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

@Generated("pl.metaprogramming.codegen")
public class EchoServiceClient extends WebServiceGatewaySupport {

    public Message echo(Message request) {
        return (Message) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public ExtendedType echoExtended(ExtendedType request) {
        return (ExtendedType) getWebServiceTemplate().marshalSendAndReceive(request);
    }

}
