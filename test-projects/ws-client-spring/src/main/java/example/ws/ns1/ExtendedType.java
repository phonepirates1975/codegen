package example.ws.ns1;

import example.ws.ns2.BaseRequest;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"someField"})
@XmlRootElement(name = "extendedType")
@Data
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Generated("pl.metaprogramming.codegen")
public class ExtendedType extends BaseRequest {

    @Nonnull
    @XmlElement(required = true)
    private String someField;

}
