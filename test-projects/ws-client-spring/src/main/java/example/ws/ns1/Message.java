package example.ws.ns1;

import example.commons.LocalDateAdapter;
import example.commons.LocalDateTimeAdapter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"stringField", "restrictedField", "nillableField", "objectField", "listStringField",
        "nillableListField", "listObjectField", "enumField", "doubleField", "dateField", "dateTimeField",
        "uPerCaseField"})
@XmlRootElement(name = "message")
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class Message {

    @Nonnull
    @XmlElement(required = true)
    private String stringField;
    @Nonnull
    @XmlElement(required = true)
    private String restrictedField;
    @Nullable
    @XmlElement(required = true, nillable = true)
    private String nillableField;
    @Nonnull
    @XmlElement(required = true)
    private ObjectType objectField;
    @Nullable
    private List<String> listStringField;
    @Nullable
    @XmlElement(nillable = true)
    private List<String> nillableListField;
    @Nullable
    private List<ObjectType> listObjectField;
    @Nullable
    private EnumType enumField;
    @Nullable
    private Double doubleField;
    @Nullable
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate dateField;
    @Nullable
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    private LocalDateTime dateTimeField;
    @Nullable
    @XmlElement(name = "UPerCaseField")
    private String uPerCaseField;

}
