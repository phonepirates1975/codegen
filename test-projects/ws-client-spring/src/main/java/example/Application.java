package example;

import example.ws.ns1.EnumType;
import example.ws.ns1.Message;
import example.ws.ns1.ObjectType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

@Slf4j
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Profile("!test")
    @Bean
    CommandLineRunner start(EchoServiceClient quoteClient) {
        return args -> {
            Message request = createMessage();
            Message response = quoteClient.echo(request);
            if (response == request || !response.equals(request)) {
                log.warn("response == request: " + (response == request));
                log.warn("response.equals(request): " + response.equals(request));
                System.exit(1);
            }
        };
    }

    static Message createMessage() {
        return new Message()
                .setDateField(LocalDate.now())
                .setDateTimeField(LocalDateTime.now())
                .setDoubleField(1.1)
                .setEnumField(EnumType.V_0)
                .setListObjectField(Arrays.asList(
                        new ObjectType().setStringField("o1s"),
                        new ObjectType().setStringField("o2s")
                ))
                .setListStringField(Arrays.asList("s1", "s2"))
                .setUPerCaseField("UPPER_CASE")
                ;
    }
}
