package example;

import example.commons.EndpointProvider;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoServiceClientConfiguration {

    private final EndpointProvider endpointProvider;

    @Bean
    public EchoServiceClient createEchoServiceClient() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setPackagesToScan("example.ws.ns1", "example.ws.ns2");
        EchoServiceClient client = new EchoServiceClient();
        client.setDefaultUri(endpointProvider.getEndpoint("/ws/echo"));
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }

}
