package example

import example.ws.ns1.ExtendedType
import io.undertow.Undertow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification

@ActiveProfiles("test")
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.NONE,
        properties="BASE_URL=http://localhost:18080"
)
class EchoServiceClientSpec extends Specification {

    @Autowired
    EchoServiceClient client

    def setupSpec() {
        startMockWsServer()
    }

    def "the received message should be the same as sent - echo"() {
        given:
        def request = Application.createMessage()

        when:
        def response = client.echo(request)

        then:
        response == request
    }

    def "the received message should be the same as sent - echoExtended"() {
        given:
        def request = new ExtendedType(baseRequestField: 'baseRequestField', someField: 'someField')

        when:
        def response = client.echoExtended(request)

        then:
        response == request
    }

    void startMockWsServer() {
        Undertow server = Undertow.builder()
                .addHttpListener(18080, "localhost")
                .setHandler { it ->
                    it.requestReceiver.receiveFullString { exchange, message ->
                        exchange.getResponseSender().send(message)
                    }
                }.build()
        server.start()
    }
}
