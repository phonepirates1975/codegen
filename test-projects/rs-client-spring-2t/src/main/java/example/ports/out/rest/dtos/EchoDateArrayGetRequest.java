package example.ports.out.rest.dtos;

import java.time.LocalDate;
import java.util.List;
import javax.annotation.Generated;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoDateArrayGetRequest {

    @Nullable
    private List<LocalDate> dateArray;

}
