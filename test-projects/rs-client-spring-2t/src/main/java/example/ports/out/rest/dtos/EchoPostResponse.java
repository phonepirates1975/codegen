package example.ports.out.rest.dtos;

import example.commons.RestResponse200;
import example.commons.RestResponseBase;
import example.commons.RestResponseOther;
import example.commons.ports.out.rest.dtos.ErrorDescriptionDto;
import java.util.Collection;
import java.util.Collections;
import javax.annotation.Generated;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
@Generated("pl.metaprogramming.codegen")
public class EchoPostResponse extends RestResponseBase<EchoPostResponse>
        implements
            RestResponse200<EchoPostResponse, EchoBodyDto>,
            RestResponseOther<EchoPostResponse, ErrorDescriptionDto> {

    private static final Collection<Integer> DECLARED_STATUSES = Collections.singletonList(200);

    private EchoPostResponse(Integer status, Object body) {
        super(status, body);
    }

    public Collection<Integer> getDeclaredStatuses() {
        return DECLARED_STATUSES;
    }

    public EchoPostResponse self() {
        return this;
    }

    public static EchoPostResponse set200(EchoBodyDto body) {
        return new EchoPostResponse(200, body);
    }

    public static EchoPostResponse setOther(Integer status, ErrorDescriptionDto body) {
        if (DECLARED_STATUSES.contains(status)) {
            throw new IllegalArgumentException(
                    String.format("Status %s is declared. Use dedicated factory method for it.", status));
        }
        return new EchoPostResponse(status, body);
    }

}
