package example.ports.out.rest.dtos;

import example.commons.RestResponse200;
import example.commons.RestResponse400;
import example.commons.RestResponseBase;
import example.commons.ports.out.rest.dtos.ErrorDescriptionDto;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.annotation.Generated;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostResponse extends RestResponseBase<EchoArraysPostResponse>
        implements
            RestResponse200<EchoArraysPostResponse, List<EchoArraysBodyDto>>,
            RestResponse400<EchoArraysPostResponse, ErrorDescriptionDto> {

    private static final Collection<Integer> DECLARED_STATUSES = Arrays.asList(200, 400);

    private EchoArraysPostResponse(Integer status, Object body) {
        super(status, body);
    }

    public Collection<Integer> getDeclaredStatuses() {
        return DECLARED_STATUSES;
    }

    public EchoArraysPostResponse self() {
        return this;
    }

    public static EchoArraysPostResponse set200(List<EchoArraysBodyDto> body) {
        return new EchoArraysPostResponse(200, body);
    }

    public static EchoArraysPostResponse set400(ErrorDescriptionDto body) {
        return new EchoArraysPostResponse(400, body);
    }

}
