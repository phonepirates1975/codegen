package example.ports.out.rest.dtos;

import java.util.List;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostRequest {

    /**
     * The value of the Authorization header should consist of 'type' +
     * 'credentials', where for the approach using the 'type' token should be
     * 'Bearer'.
     */
    @Nonnull
    private String authorizationParam;

    /**
     * Example header param
     */
    @Nullable
    private String inlineHeaderParam;

    /**
     * body param
     */
    @Nonnull
    private List<EchoArraysBodyDto> requestBody;

}
