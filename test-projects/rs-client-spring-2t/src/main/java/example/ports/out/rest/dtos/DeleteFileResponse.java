package example.ports.out.rest.dtos;

import example.commons.RestResponse204NoContent;
import example.commons.RestResponseBase;
import example.commons.RestResponseOther;
import example.commons.ports.out.rest.dtos.ErrorDescriptionDto;
import java.util.Collection;
import java.util.Collections;
import javax.annotation.Generated;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
@Generated("pl.metaprogramming.codegen")
public class DeleteFileResponse extends RestResponseBase<DeleteFileResponse>
        implements
            RestResponse204NoContent<DeleteFileResponse>,
            RestResponseOther<DeleteFileResponse, ErrorDescriptionDto> {

    private static final Collection<Integer> DECLARED_STATUSES = Collections.singletonList(204);

    private DeleteFileResponse(Integer status, Object body) {
        super(status, body);
    }

    public Collection<Integer> getDeclaredStatuses() {
        return DECLARED_STATUSES;
    }

    public DeleteFileResponse self() {
        return this;
    }

    public static DeleteFileResponse set204() {
        return new DeleteFileResponse(204, null);
    }

    public static DeleteFileResponse setOther(Integer status, ErrorDescriptionDto body) {
        if (DECLARED_STATUSES.contains(status)) {
            throw new IllegalArgumentException(
                    String.format("Status %s is declared. Use dedicated factory method for it.", status));
        }
        return new DeleteFileResponse(status, body);
    }

}
