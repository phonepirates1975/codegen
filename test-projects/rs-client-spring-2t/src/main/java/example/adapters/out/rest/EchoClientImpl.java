package example.adapters.out.rest;

import example.adapters.out.rest.dtos.EchoArraysBodyRdto;
import example.adapters.out.rest.dtos.EchoBodyRdto;
import example.adapters.out.rest.dtos.EchoDefaultsBodyRdto;
import example.adapters.out.rest.dtos.EchoGetBodyRdto;
import example.adapters.out.rest.mappers.DeleteFileRequestMapper;
import example.adapters.out.rest.mappers.DeleteFileResponseMapper;
import example.adapters.out.rest.mappers.DownloadEchoFileRequestMapper;
import example.adapters.out.rest.mappers.DownloadEchoFileResponseMapper;
import example.adapters.out.rest.mappers.EchoArraysPostRequestMapper;
import example.adapters.out.rest.mappers.EchoArraysPostResponseMapper;
import example.adapters.out.rest.mappers.EchoDateArrayGetRequestMapper;
import example.adapters.out.rest.mappers.EchoDateArrayGetResponseMapper;
import example.adapters.out.rest.mappers.EchoDefaultsPostRequestMapper;
import example.adapters.out.rest.mappers.EchoDefaultsPostResponseMapper;
import example.adapters.out.rest.mappers.EchoEmptyRequestMapper;
import example.adapters.out.rest.mappers.EchoEmptyResponseMapper;
import example.adapters.out.rest.mappers.EchoErrorRequestMapper;
import example.adapters.out.rest.mappers.EchoErrorResponseMapper;
import example.adapters.out.rest.mappers.EchoGetRequestMapper;
import example.adapters.out.rest.mappers.EchoGetResponseMapper;
import example.adapters.out.rest.mappers.EchoPostRequestMapper;
import example.adapters.out.rest.mappers.EchoPostResponseMapper;
import example.adapters.out.rest.mappers.UploadEchoFileRequestMapper;
import example.adapters.out.rest.mappers.UploadEchoFileResponseMapper;
import example.adapters.out.rest.mappers.UploadEchoFileWithFormRequestMapper;
import example.adapters.out.rest.mappers.UploadEchoFileWithFormResponseMapper;
import example.commons.adapters.out.rest.dtos.ErrorDescriptionRdto;
import example.ports.out.rest.EchoClient;
import example.ports.out.rest.dtos.DeleteFileRequest;
import example.ports.out.rest.dtos.DeleteFileResponse;
import example.ports.out.rest.dtos.DownloadEchoFileRequest;
import example.ports.out.rest.dtos.DownloadEchoFileResponse;
import example.ports.out.rest.dtos.EchoArraysPostRequest;
import example.ports.out.rest.dtos.EchoArraysPostResponse;
import example.ports.out.rest.dtos.EchoDateArrayGetRequest;
import example.ports.out.rest.dtos.EchoDateArrayGetResponse;
import example.ports.out.rest.dtos.EchoDefaultsPostRequest;
import example.ports.out.rest.dtos.EchoDefaultsPostResponse;
import example.ports.out.rest.dtos.EchoEmptyRequest;
import example.ports.out.rest.dtos.EchoEmptyResponse;
import example.ports.out.rest.dtos.EchoErrorRequest;
import example.ports.out.rest.dtos.EchoErrorResponse;
import example.ports.out.rest.dtos.EchoGetRequest;
import example.ports.out.rest.dtos.EchoGetResponse;
import example.ports.out.rest.dtos.EchoPostRequest;
import example.ports.out.rest.dtos.EchoPostResponse;
import example.ports.out.rest.dtos.UploadEchoFileRequest;
import example.ports.out.rest.dtos.UploadEchoFileResponse;
import example.ports.out.rest.dtos.UploadEchoFileWithFormRequest;
import example.ports.out.rest.dtos.UploadEchoFileWithFormResponse;
import java.util.List;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoClientImpl implements EchoClient {

    private final RestTemplate restTemplate;
    private final EchoPostRequestMapper echoPostRequestMapper;
    private final EchoPostResponseMapper echoPostResponseMapper;
    private final EchoGetRequestMapper echoGetRequestMapper;
    private final EchoGetResponseMapper echoGetResponseMapper;
    private final EchoDefaultsPostRequestMapper echoDefaultsPostRequestMapper;
    private final EchoDefaultsPostResponseMapper echoDefaultsPostResponseMapper;
    private final EchoArraysPostRequestMapper echoArraysPostRequestMapper;
    private final EchoArraysPostResponseMapper echoArraysPostResponseMapper;
    private final EchoDateArrayGetRequestMapper echoDateArrayGetRequestMapper;
    private final EchoDateArrayGetResponseMapper echoDateArrayGetResponseMapper;
    private final UploadEchoFileWithFormRequestMapper uploadEchoFileWithFormRequestMapper;
    private final UploadEchoFileWithFormResponseMapper uploadEchoFileWithFormResponseMapper;
    private final UploadEchoFileRequestMapper uploadEchoFileRequestMapper;
    private final UploadEchoFileResponseMapper uploadEchoFileResponseMapper;
    private final DownloadEchoFileRequestMapper downloadEchoFileRequestMapper;
    private final DownloadEchoFileResponseMapper downloadEchoFileResponseMapper;
    private final DeleteFileRequestMapper deleteFileRequestMapper;
    private final DeleteFileResponseMapper deleteFileResponseMapper;
    private final EchoEmptyRequestMapper echoEmptyRequestMapper;
    private final EchoEmptyResponseMapper echoEmptyResponseMapper;
    private final EchoErrorRequestMapper echoErrorRequestMapper;
    private final EchoErrorResponseMapper echoErrorResponseMapper;

    @Override
    public EchoPostResponse echoPost(@Nonnull EchoPostRequest request) {
        try {
            return echoPostResponseMapper
                    .map(restTemplate.exchange(echoPostRequestMapper.map(request), EchoBodyRdto.class));
        } catch (HttpStatusCodeException e) {
            return echoPostResponseMapper.map(e);
        }
    }

    @Override
    public EchoGetResponse echoGet(@Nonnull EchoGetRequest request) {
        try {
            return echoGetResponseMapper
                    .map(restTemplate.exchange(echoGetRequestMapper.map(request), EchoGetBodyRdto.class));
        } catch (HttpStatusCodeException e) {
            return echoGetResponseMapper.map(e);
        }
    }

    @Override
    public EchoDefaultsPostResponse echoDefaultsPost(@Nonnull EchoDefaultsPostRequest request) {
        try {
            return echoDefaultsPostResponseMapper
                    .map(restTemplate.exchange(echoDefaultsPostRequestMapper.map(request), EchoDefaultsBodyRdto.class));
        } catch (HttpStatusCodeException e) {
            return echoDefaultsPostResponseMapper.map(e);
        }
    }

    @Override
    public EchoArraysPostResponse echoArraysPost(@Nonnull EchoArraysPostRequest request) {
        try {
            return echoArraysPostResponseMapper.map(restTemplate.exchange(echoArraysPostRequestMapper.map(request),
                    new ParameterizedTypeReference<List<EchoArraysBodyRdto>>() {
                    }));
        } catch (HttpStatusCodeException e) {
            return echoArraysPostResponseMapper.map(e);
        }
    }

    @Override
    public EchoDateArrayGetResponse echoDateArrayGet(@Nonnull EchoDateArrayGetRequest request) {
        try {
            return echoDateArrayGetResponseMapper.map(restTemplate.exchange(echoDateArrayGetRequestMapper.map(request),
                    new ParameterizedTypeReference<List<String>>() {
                    }));
        } catch (HttpStatusCodeException e) {
            return echoDateArrayGetResponseMapper.map(e);
        }
    }

    @Override
    public UploadEchoFileWithFormResponse uploadEchoFileWithForm(@Nonnull UploadEchoFileWithFormRequest request) {
        try {
            return uploadEchoFileWithFormResponseMapper
                    .map(restTemplate.exchange(uploadEchoFileWithFormRequestMapper.map(request), String.class));
        } catch (HttpStatusCodeException e) {
            return uploadEchoFileWithFormResponseMapper.map(e);
        }
    }

    @Override
    public UploadEchoFileResponse uploadEchoFile(@Nonnull UploadEchoFileRequest request) {
        try {
            return uploadEchoFileResponseMapper
                    .map(restTemplate.exchange(uploadEchoFileRequestMapper.map(request), String.class));
        } catch (HttpStatusCodeException e) {
            return uploadEchoFileResponseMapper.map(e);
        }
    }

    @Override
    public DownloadEchoFileResponse downloadEchoFile(@Nonnull DownloadEchoFileRequest request) {
        try {
            return downloadEchoFileResponseMapper
                    .map(restTemplate.exchange(downloadEchoFileRequestMapper.map(request), Resource.class));
        } catch (HttpStatusCodeException e) {
            return downloadEchoFileResponseMapper.map(e);
        }
    }

    @Override
    public DeleteFileResponse deleteFile(@Nonnull DeleteFileRequest request) {
        try {
            return deleteFileResponseMapper
                    .map(restTemplate.exchange(deleteFileRequestMapper.map(request), String.class));
        } catch (HttpStatusCodeException e) {
            return deleteFileResponseMapper.map(e);
        }
    }

    @Override
    public EchoEmptyResponse echoEmpty(@Nonnull EchoEmptyRequest request) {
        try {
            return echoEmptyResponseMapper
                    .map(restTemplate.exchange(echoEmptyRequestMapper.map(request), String.class));
        } catch (HttpStatusCodeException e) {
            return echoEmptyResponseMapper.map(e);
        }
    }

    @Override
    public EchoErrorResponse echoError(@Nonnull EchoErrorRequest request) {
        try {
            return echoErrorResponseMapper
                    .map(restTemplate.exchange(echoErrorRequestMapper.map(request), ErrorDescriptionRdto.class));
        } catch (HttpStatusCodeException e) {
            return echoErrorResponseMapper.map(e);
        }
    }

}
