package example.adapters.out.rest.dtos;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import javax.annotation.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE)
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsBodyRdto {

    @JsonProperty("propInt")
    @JsonRawValue
    private String propInt = "101";
    @JsonProperty("propFloat")
    @JsonRawValue
    private String propFloat = "101.101";
    @JsonProperty("propDouble")
    @JsonRawValue
    private String propDouble = "202.202";
    @JsonProperty("propNumber")
    @JsonRawValue
    private String propNumber = "3.33";
    @JsonProperty("propString")
    private String propString = "WHITE";
    @JsonProperty("propEnum")
    private String propEnum = "3";
    @JsonProperty("propDate")
    private String propDate = "2021-09-01";
    @JsonProperty("propDateTime")
    private String propDateTime = "2021-09-01T09:09:09Z";

}
