package example.adapters.out.rest.mappers;

import example.adapters.out.rest.dtos.EchoDateArrayGetRrequest;
import example.commons.EndpointProvider;
import example.commons.SerializationUtils;
import example.ports.out.rest.dtos.EchoDateArrayGetRequest;
import java.net.URI;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoDateArrayGetRequestMapper {

    private final EndpointProvider endpointProvider;

    public RequestEntity<Void> map(@Nonnull EchoDateArrayGetRequest request) {
        EchoDateArrayGetRrequest rawRequest = map2EchoDateArrayGetRrequest(request);
        URI uri = UriComponentsBuilder.fromUriString(endpointProvider.getEndpoint("/api/v1/echo-date-array"))
                .queryParam("date_array", rawRequest.getDateArray()).build().toUri();
        return RequestEntity.get(uri).build();
    }

    private EchoDateArrayGetRrequest map2EchoDateArrayGetRrequest(EchoDateArrayGetRequest value) {
        return new EchoDateArrayGetRrequest()
                .setDateArray(SerializationUtils.transformList(value.getDateArray(), SerializationUtils::toString));
    }

}
