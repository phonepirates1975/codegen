package example.adapters.out.rest.dtos;

import javax.annotation.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoGetRrequest {

    private String correlationIdParam;
    private String propIntRequired;
    private String propFloat;
    private String propEnum;

}
