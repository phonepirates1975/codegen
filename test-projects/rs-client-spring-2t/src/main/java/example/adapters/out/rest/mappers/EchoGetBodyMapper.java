package example.adapters.out.rest.mappers;

import example.adapters.out.rest.dtos.EchoGetBodyRdto;
import example.commons.SerializationUtils;
import example.ports.out.rest.dtos.DefaultEnumEnum;
import example.ports.out.rest.dtos.EchoGetBodyDto;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoGetBodyMapper {

    public EchoGetBodyDto map2EchoGetBodyDto(EchoGetBodyRdto value) {
        return value == null ? null : map(new EchoGetBodyDto(), value);
    }

    public EchoGetBodyDto map(EchoGetBodyDto result, EchoGetBodyRdto value) {
        return result.setPropIntRequired(SerializationUtils.toLong(value.getPropIntRequired()))
                .setPropFloat(SerializationUtils.toFloat(value.getPropFloat()))
                .setPropEnum(DefaultEnumEnum.fromValue(value.getPropEnum()));
    }

}
