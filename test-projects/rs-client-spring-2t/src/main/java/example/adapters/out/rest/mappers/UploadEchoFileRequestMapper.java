package example.adapters.out.rest.mappers;

import example.adapters.out.rest.dtos.UploadEchoFileRrequest;
import example.commons.EndpointProvider;
import example.commons.SerializationUtils;
import example.ports.out.rest.dtos.UploadEchoFileRequest;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileRequestMapper {

    private final EndpointProvider endpointProvider;

    public RequestEntity<Resource> map(@Nonnull UploadEchoFileRequest request) {
        UploadEchoFileRrequest rawRequest = map2UploadEchoFileRrequest(request);
        Map<String, String> pathParams = new HashMap<>();
        pathParams.put("id", rawRequest.getId());
        URI uri = UriComponentsBuilder.fromUriString(endpointProvider.getEndpoint("/api/v1/echo-file/{id}"))
                .build(pathParams);
        return RequestEntity.post(uri).header("content-type", "application/octet-stream")
                .body(rawRequest.getRequestBody());
    }

    private UploadEchoFileRrequest map2UploadEchoFileRrequest(UploadEchoFileRequest value) {
        return new UploadEchoFileRrequest().setId(SerializationUtils.toString(value.getId()))
                .setRequestBody(value.getRequestBody());
    }

}
