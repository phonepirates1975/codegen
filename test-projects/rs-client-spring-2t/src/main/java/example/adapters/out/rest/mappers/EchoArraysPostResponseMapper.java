package example.adapters.out.rest.mappers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import example.adapters.out.rest.dtos.EchoArraysBodyRdto;
import example.commons.SerializationUtils;
import example.commons.adapters.out.rest.dtos.ErrorDescriptionRdto;
import example.commons.adapters.out.rest.mappers.ErrorDescriptionMapper;
import example.ports.out.rest.dtos.EchoArraysPostResponse;
import java.util.List;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;

@Slf4j
@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostResponseMapper {

    private final EchoArraysBodyMapper echoArraysBodyMapper;
    private final ErrorDescriptionMapper errorDescriptionMapper;

    public EchoArraysPostResponse map(@Nonnull ResponseEntity<List<EchoArraysBodyRdto>> responseEntity) {
        return EchoArraysPostResponse.set200(SerializationUtils.transformList(responseEntity.getBody(),
                echoArraysBodyMapper::map2EchoArraysBodyDto));
    }

    public EchoArraysPostResponse map(HttpStatusCodeException e) {
        try {
            ErrorDescriptionRdto restValue = new ObjectMapper().readValue(e.getResponseBodyAsString(),
                    ErrorDescriptionRdto.class);
            if (e.getRawStatusCode() == 400) {
                return EchoArraysPostResponse.set400(errorDescriptionMapper.map2ErrorDescriptionDto(restValue));
            }
            throw e;
        } catch (JsonProcessingException e2) {
            log.error("Can't deserialize data", e2);
            throw e;
        }
    }

}
