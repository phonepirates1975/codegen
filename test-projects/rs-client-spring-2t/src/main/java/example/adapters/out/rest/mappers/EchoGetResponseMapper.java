package example.adapters.out.rest.mappers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import example.adapters.out.rest.dtos.EchoGetBodyRdto;
import example.commons.SerializationUtils;
import example.commons.adapters.out.rest.dtos.ErrorItemRdto;
import example.commons.adapters.out.rest.mappers.ErrorItemMapper;
import example.ports.out.rest.dtos.EchoGetResponse;
import java.util.List;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;

@Slf4j
@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoGetResponseMapper {

    private final EchoGetBodyMapper echoGetBodyMapper;
    private final ErrorItemMapper errorItemMapper;

    public EchoGetResponse map(@Nonnull ResponseEntity<EchoGetBodyRdto> responseEntity) {
        return EchoGetResponse.set200(echoGetBodyMapper.map2EchoGetBodyDto(responseEntity.getBody()));
    }

    public EchoGetResponse map(HttpStatusCodeException e) {
        try {
            List restValue = new ObjectMapper().readValue(e.getResponseBodyAsString(), List.class);
            if (e.getRawStatusCode() == 400) {
                return EchoGetResponse
                        .set400(SerializationUtils.transformList(restValue, errorItemMapper::map2ErrorItemDto));
            }
            throw e;
        } catch (JsonProcessingException e2) {
            log.error("Can't deserialize data", e2);
            throw e;
        }
    }

}
