package example.adapters.out.rest.mappers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import example.commons.adapters.out.rest.dtos.ErrorDescriptionRdto;
import example.commons.adapters.out.rest.mappers.ErrorDescriptionMapper;
import example.ports.out.rest.dtos.DeleteFileResponse;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;

@Slf4j
@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class DeleteFileResponseMapper {

    private final ErrorDescriptionMapper errorDescriptionMapper;

    public DeleteFileResponse map(@Nonnull ResponseEntity<?> responseEntity) {
        return DeleteFileResponse.set204();
    }

    public DeleteFileResponse map(HttpStatusCodeException e) {
        try {
            ErrorDescriptionRdto restValue = new ObjectMapper().readValue(e.getResponseBodyAsString(),
                    ErrorDescriptionRdto.class);
            return DeleteFileResponse.setOther(e.getRawStatusCode(),
                    errorDescriptionMapper.map2ErrorDescriptionDto(restValue));
        } catch (JsonProcessingException e2) {
            log.error("Can't deserialize data", e2);
            throw e;
        }
    }

}
