package example.adapters.out.rest.mappers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import example.commons.adapters.out.rest.dtos.ErrorDescriptionRdto;
import example.commons.adapters.out.rest.mappers.ErrorDescriptionMapper;
import example.ports.out.rest.dtos.DownloadEchoFileResponse;
import java.util.Objects;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;

@Slf4j
@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class DownloadEchoFileResponseMapper {

    private final ErrorDescriptionMapper errorDescriptionMapper;

    public DownloadEchoFileResponse map(@Nonnull ResponseEntity<Resource> responseEntity) {
        return DownloadEchoFileResponse.set200(Objects.requireNonNull(responseEntity.getBody()));
    }

    public DownloadEchoFileResponse map(HttpStatusCodeException e) {
        try {
            if (e.getRawStatusCode() == 404) {
                return DownloadEchoFileResponse.set404();
            }
            ErrorDescriptionRdto restValue = new ObjectMapper().readValue(e.getResponseBodyAsString(),
                    ErrorDescriptionRdto.class);
            return DownloadEchoFileResponse.setOther(e.getRawStatusCode(),
                    errorDescriptionMapper.map2ErrorDescriptionDto(restValue));
        } catch (JsonProcessingException e2) {
            log.error("Can't deserialize data", e2);
            throw e;
        }
    }

}
