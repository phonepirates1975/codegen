package example.adapters.out.rest.mappers;

import example.adapters.out.rest.dtos.EchoErrorRrequest;
import example.commons.EndpointProvider;
import example.ports.out.rest.dtos.EchoErrorRequest;
import java.net.URI;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoErrorRequestMapper {

    private final EndpointProvider endpointProvider;

    public RequestEntity<Void> map(@Nonnull EchoErrorRequest request) {
        EchoErrorRrequest rawRequest = map2EchoErrorRrequest(request);
        URI uri = UriComponentsBuilder.fromUriString(endpointProvider.getEndpoint("/api/v1/echo-error"))
                .queryParam("errorMessage", rawRequest.getErrorMessage()).build().toUri();
        return RequestEntity.get(uri).build();
    }

    private EchoErrorRrequest map2EchoErrorRrequest(EchoErrorRequest value) {
        return new EchoErrorRrequest().setErrorMessage(value.getErrorMessage());
    }

}
