package example.adapters.out.rest.mappers;

import example.adapters.out.rest.dtos.EchoDefaultsBodyRdto;
import example.commons.SerializationUtils;
import example.commons.ports.out.rest.dtos.ReusableEnumEnum;
import example.ports.out.rest.dtos.EchoDefaultsBodyDto;
import java.util.Optional;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsBodyMapper {

    public EchoDefaultsBodyDto map2EchoDefaultsBodyDto(EchoDefaultsBodyRdto value) {
        return value == null ? null : map(new EchoDefaultsBodyDto(), value);
    }

    public EchoDefaultsBodyRdto map2EchoDefaultsBodyRdto(EchoDefaultsBodyDto value) {
        return value == null ? null : map(new EchoDefaultsBodyRdto(), value);
    }

    public EchoDefaultsBodyDto map(EchoDefaultsBodyDto result, EchoDefaultsBodyRdto value) {
        return result.setPropInt(SerializationUtils.toInteger(value.getPropInt()))
                .setPropFloat(SerializationUtils.toFloat(value.getPropFloat()))
                .setPropDouble(SerializationUtils.toDouble(value.getPropDouble()))
                .setPropNumber(SerializationUtils.toBigDecimal(value.getPropNumber()))
                .setPropString(value.getPropString()).setPropEnum(ReusableEnumEnum.fromValue(value.getPropEnum()))
                .setPropDate(SerializationUtils.toLocalDate(value.getPropDate()))
                .setPropDateTime(SerializationUtils.toLocalDateTime(value.getPropDateTime()));
    }

    public EchoDefaultsBodyRdto map(EchoDefaultsBodyRdto result, EchoDefaultsBodyDto value) {
        return result.setPropInt(SerializationUtils.toString(value.getPropInt()))
                .setPropFloat(SerializationUtils.toString(value.getPropFloat()))
                .setPropDouble(SerializationUtils.toString(value.getPropDouble()))
                .setPropNumber(SerializationUtils.toString(value.getPropNumber())).setPropString(value.getPropString())
                .setPropEnum(Optional.ofNullable(value.getPropEnum()).map(ReusableEnumEnum::getValue).orElse(null))
                .setPropDate(SerializationUtils.toString(value.getPropDate()))
                .setPropDateTime(SerializationUtils.toString(value.getPropDateTime()));
    }

}
