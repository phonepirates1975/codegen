package example.commons;

import java.util.Map;
import javax.annotation.Generated;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public abstract class RestResponseBase<R> implements RestResponse<R> {

    @Getter
    private final Map<String, String> headers = new java.util.TreeMap<>();

    @Getter
    private final Integer status;
    @Getter
    private final Object body;

}
