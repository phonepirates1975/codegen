package example.commons;

import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public interface RestResponseOther<R, T> extends RestResponse<R> {

    default boolean isOther() {
        return !getDeclaredStatuses().contains(getStatus());
    }

    @SuppressWarnings("unchecked")
    default T getOther() {
        if (isOther()) {
            return (T) getBody();
        }
        throw new IllegalStateException(String.format("Response other than %s is not set", getDeclaredStatuses()));
    }

}
