/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.out.rest;

import example.commons.ports.out.rest.dtos.ReusableEnumEnum;
import example.commons.ports.out.rest.dtos.SimpleObjectDto;
import example.ports.out.rest.dtos.*;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EchoPostClientTest extends BaseClientTest {
    private static final String CORRELATION_ID = "correlation-id-123";

    @Test
    void shouldSuccessEchoPostWithMandatoryFieldsOnly() {
        EchoPostRequest request = new EchoPostRequest()
                .setAuthorizationParam(AUTH_TOKEN)
                .setCorrelationIdParam(CORRELATION_ID)
                .setRequestBody(new EchoBodyDto().setPropIntRequired(123L));
        EchoPostResponse response = client.echoPost(request);
        assertTrue(response.is200());
        assertEquals(request.getRequestBody(), response.get200());
    }

    @Test
    void shouldSuccessEchoPostWithAllFields() {
        Map<String, Object> anySubObject = new HashMap<>();
        anySubObject.put("i", 12);
        anySubObject.put("b", true);
        Map<String, Object> anyObject = new HashMap<>();
        anyObject.put("p", "p");
        anyObject.put("o", anySubObject);
        EchoPostRequest request = new EchoPostRequest()
                .setAuthorizationParam(AUTH_TOKEN)
                .setCorrelationIdParam(CORRELATION_ID)
                .setTimestampParam(LocalDateTime.now())
                .setInlineHeaderParam("bla-bla-bla")
                .setRequestBody(new EchoBodyDto()
                        .setPropInt(1)
                        .setPropIntSecond(2)
                        .setPropIntRequired(123L)
                        .setPropFloat(12F)
                        .setPropDouble(23D)
                        .setPropAmount("12.10")
                        .setPropAmountNumber(BigDecimal.valueOf(12.12))
                        .setPropString("WHITE")
                        .setPropStringPattern("-/@?")
                        .setPropDefault("default-text")
                        .setPropDate(LocalDate.now())
                        .setPropDateSecond(LocalDate.now())
                        .setPropDateTime(LocalDateTime.now())
                        .setPropBoolean(true)
                        .setPropBase64("test".getBytes())
                        .setPropObject(new SimpleObjectDto()
                                .setSoPropDate(LocalDate.now().minusDays(1))
                                .setSoPropString("la"))
                        .setPropObjectAny(anyObject)
                        .setPropObjectExtended((ExtendedObjectDto) new ExtendedObjectDto()
                                .setEoEnumReusable(ReusableEnumEnum.B)
                                .setSoPropDate(LocalDate.now().minusDays(2))
                                .setSoPropString("al")
                        )
                        .setPropEnumReusable(ReusableEnumEnum.B)
                        .setPropEnum(EnumTypeEnum.V_1)
                );
        EchoPostResponse response = client.echoPost(request);
        assertTrue(response.is200());
        assertEquals(request.getRequestBody(), response.get200());
    }

    @Test
    void shouldFailEchoPostWithInvalidData() {
        EchoPostRequest echoRequest = new EchoPostRequest()
                .setAuthorizationParam(AUTH_TOKEN)
                .setCorrelationIdParam(CORRELATION_ID)
                .setRequestBody(new EchoBodyDto()
                        .setPropIntRequired(123L)
                        .setPropInt(10)
                        .setPropIntSecond(3)
                );
        EchoPostResponse response = client.echoPost(echoRequest);
        assertTrue(response.isOther());
        assertEquals(400, response.getOther().getCode());
        assertEquals("prop_int should be less than prop_int_second", response.getOther().getMessage());
    }
}
