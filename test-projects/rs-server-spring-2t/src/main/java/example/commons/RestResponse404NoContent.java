package example.commons;

import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public interface RestResponse404NoContent<R> extends RestResponse<R> {

    default boolean is404() {
        return isStatus(404);
    }

}
