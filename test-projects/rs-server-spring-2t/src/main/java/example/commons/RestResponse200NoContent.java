package example.commons;

import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public interface RestResponse200NoContent<R> extends RestResponse<R> {

    default boolean is200() {
        return isStatus(200);
    }

}
