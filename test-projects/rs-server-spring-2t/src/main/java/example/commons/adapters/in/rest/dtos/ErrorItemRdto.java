package example.commons.adapters.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import example.commons.validator.Field;
import javax.annotation.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE)
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class ErrorItemRdto {

    public static final Field<ErrorItemRdto, String> FIELD_CODE = new Field<>("code", ErrorItemRdto::getCode);

    @JsonProperty("code")
    private String code;

}
