package example.commons.adapters.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import example.commons.validator.Field;
import javax.annotation.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE)
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class SimpleObjectRdto {

    public static final Field<SimpleObjectRdto, String> FIELD_SO_PROP_STRING = new Field<>("so_prop_string",
            SimpleObjectRdto::getSoPropString);
    public static final Field<SimpleObjectRdto, String> FIELD_SO_PROP_DATE = new Field<>("so_prop_date",
            SimpleObjectRdto::getSoPropDate);

    @JsonProperty("so_prop_string")
    private String soPropString;
    @JsonProperty("so_prop_date")
    private String soPropDate;

}
