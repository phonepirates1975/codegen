package example.commons;

import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public interface RestResponse204NoContent<R> extends RestResponse<R> {

    default boolean is204() {
        return isStatus(204);
    }

}
