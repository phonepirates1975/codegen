package example.commons.validator;

import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public enum DictionaryCodes {

    ANIMALS, COLORS;

}
