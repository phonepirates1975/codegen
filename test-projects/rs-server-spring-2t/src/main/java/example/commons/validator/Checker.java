package example.commons.validator;

import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public interface Checker<V> {

    void check(ValidationContext<V> context);

    @SuppressWarnings("unchecked")
    default <T extends V> void checkWithParent(ValidationContext<T> context) {
        check((ValidationContext) context);
    }

    default boolean checkNull() {
        return false;
    }

    default Checker<V> withError(ErrorCodes errorCode) {
        return context -> check(new ValidationContext<>(context,
                error -> ValidationError.builder().code(errorCode.getValue()).message(error.getMessage())
                        .field(error.getField()).status(error.getStatus()).stopValidation(error.isStopValidation())
                        .build()));
    }

}
