package example.commons;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.annotation.Generated;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.Resource;

@Generated("pl.metaprogramming.codegen")
public class SerializationUtils {

    private SerializationUtils() {
    }

    public static <T> T fromString(String value, Function<String, T> transformer) {
        return value == null || value.isEmpty() ? null : transformer.apply(value);
    }

    public static <T> String toString(T value, Function<T, String> transformer) {
        return value == null ? null : transformer.apply(value);
    }

    public static <R, T> List<R> transformList(List<T> value, Function<T, R> transformer) {
        return value == null ? null : value.stream().map(transformer).collect(Collectors.toList());
    }

    public static <K, R, T> Map<K, R> transformMap(Map<K, T> value, Function<T, R> transformer) {
        return value == null
                ? null
                : value.entrySet().stream()
                        .collect(Collectors.toMap(Map.Entry::getKey, e -> transformer.apply(e.getValue())));
    }

    public static Long toLong(String value) {
        return fromString(value, Long::valueOf);
    }

    public static String toString(Long value) {
        return toString(value, Object::toString);
    }

    public static Integer toInteger(String value) {
        return fromString(value, Integer::valueOf);
    }

    public static String toString(Integer value) {
        return toString(value, Object::toString);
    }

    public static Float toFloat(String value) {
        return fromString(value, Float::valueOf);
    }

    public static String toString(Float value) {
        return toString(value, Object::toString);
    }

    public static BigDecimal toBigDecimal(String value) {
        return fromString(value, BigDecimal::new);
    }

    public static String toString(BigDecimal value) {
        return toString(value, Object::toString);
    }

    public static LocalDate toLocalDate(String value) {
        return fromString(value, LocalDate::parse);
    }

    public static String toString(LocalDate value) {
        return toString(value, DateTimeFormatter.ISO_LOCAL_DATE::format);
    }

    public static LocalDateTime toLocalDateTime(String value) {
        return fromString(value, v -> ZonedDateTime.parse(v, DateTimeFormatter.ISO_OFFSET_DATE_TIME)
                .withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime());
    }

    public static String toString(LocalDateTime value) {
        return toString(value, v -> DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(value.atZone(ZoneOffset.UTC)));
    }

    public static Boolean toBoolean(String value) {
        return fromString(value, Boolean::valueOf);
    }

    public static String toString(Boolean value) {
        return toString(value, Object::toString);
    }

    public static Double toDouble(String value) {
        return fromString(value, Double::valueOf);
    }

    public static String toString(Double value) {
        return toString(value, Object::toString);
    }

    public static byte[] toBytes(String value) {
        return value != null ? Base64.getDecoder().decode(value) : null;
    }

    public static String toString(byte[] value) {
        return value != null ? Base64.getEncoder().encodeToString(value) : null;
    }

    @SneakyThrows
    public static byte[] toBytes(Resource value) {
        return value != null ? IOUtils.toByteArray(value.getInputStream()) : null;
    }

}
