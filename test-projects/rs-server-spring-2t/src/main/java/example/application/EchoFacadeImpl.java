package example.application;

import example.commons.ports.in.rest.dtos.ErrorDescriptionDto;
import example.commons.ports.in.rest.dtos.ErrorDetailDto;
import example.ports.in.rest.EchoFacade;
import example.ports.in.rest.dtos.*;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class EchoFacadeImpl implements EchoFacade {

    private final Map<Long, byte[]> files = new ConcurrentHashMap<>();

    public EchoPostResponse echoPost(@Nonnull EchoPostRequest request) {
        EchoPostResponse result = EchoPostResponse.set200(request.getRequestBody());
        result.getHeaders().put("X-Correlation-ID", request.getCorrelationIdParam());
        return result;
    }

    public EchoGetResponse echoGet(@Nonnull EchoGetRequest request) {
        return EchoGetResponse.set200(new EchoGetBodyDto()
                .setPropIntRequired(request.getPropIntRequired()));
    }

    public EchoDefaultsPostResponse echoDefaultsPost(@Nonnull EchoDefaultsPostRequest request) {
        return EchoDefaultsPostResponse.set200(request.getRequestBody());
    }

    public EchoArraysPostResponse echoArraysPost(@Nonnull EchoArraysPostRequest request) {
        return EchoArraysPostResponse.set200(request.getRequestBody());
    }

    public EchoDateArrayGetResponse echoDateArrayGet(@Nonnull EchoDateArrayGetRequest request) {
        return EchoDateArrayGetResponse
                .set200(Optional.ofNullable(request.getDateArray()).orElse(Collections.emptyList()));
    }

    @Override
    public UploadEchoFileResponse uploadEchoFile(@Nonnull UploadEchoFileRequest request) {
        files.put(request.getId(), request.getRequestBody());
        return UploadEchoFileResponse.set204();
    }

    @Override
    public UploadEchoFileWithFormResponse uploadEchoFileWithForm(@Nonnull UploadEchoFileWithFormRequest request) {
        files.put(request.getId(), request.getFile());
        return UploadEchoFileWithFormResponse.set204();
    }

    @Override
    public DownloadEchoFileResponse downloadEchoFile(@Nonnull DownloadEchoFileRequest request) {
        if (files.containsKey(request.getId())) {
            return DownloadEchoFileResponse.set200(files.get(request.getId()));
        }
        return DownloadEchoFileResponse.set404();
    }

    @Override
    public DeleteFileResponse deleteFile(@Nonnull DeleteFileRequest request) {
        files.remove(request.getId());
        return DeleteFileResponse.set204();
    }

    @Override
    public EchoErrorResponse echoError(@Nonnull EchoErrorRequest request) {
        return EchoErrorResponse.setOther(500, new ErrorDescriptionDto()
                .setCode(500)
                .setMessage(request.getErrorMessage())
                .setErrors(Collections.singletonList(new ErrorDetailDto()
                        .setCode("error")
                        .setMessage(request.getErrorMessage())
                ))
        );
    }

    @Override
    public EchoEmptyResponse echoEmpty(@Nonnull EchoEmptyRequest request) {
        return EchoEmptyResponse.set200();
    }
}
