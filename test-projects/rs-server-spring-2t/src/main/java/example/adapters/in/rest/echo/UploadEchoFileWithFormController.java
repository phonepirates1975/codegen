package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.UploadEchoFileWithFormRrequest;
import example.adapters.in.rest.mappers.UploadEchoFileWithFormRequestMapper;
import example.commons.validator.ValidationResult;
import example.commons.validator.ValidationResultMapper;
import example.ports.in.rest.EchoFacade;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileWithFormController {

    private final UploadEchoFileWithFormRequestMapper uploadEchoFileWithFormRequestMapper;
    private final UploadEchoFileWithFormValidator uploadEchoFileWithFormValidator;
    private final EchoFacade echoFacade;
    private final UploadEchoFileWithFormResponseMapper uploadEchoFileWithFormResponseMapper;
    private final ValidationResultMapper validationResultMapper;

    /**
     * upload file using multipart/form-data
     */
    @PostMapping(value = "/api/v1/echo-file/{id}/form", produces = {"application/json"}, consumes = {
            "multipart/form-data"})
    public ResponseEntity uploadEchoFileWithForm(@PathVariable(required = false) String id,
            @RequestPart(required = false) MultipartFile file) {
        UploadEchoFileWithFormRrequest request = uploadEchoFileWithFormRequestMapper
                .map2UploadEchoFileWithFormRrequest(id, file);
        ValidationResult validationResult = uploadEchoFileWithFormValidator.validate(request);
        return validationResult.isValid()
                ? uploadEchoFileWithFormResponseMapper.map(echoFacade.uploadEchoFileWithForm(
                        uploadEchoFileWithFormRequestMapper.map2UploadEchoFileWithFormRequest(request)))
                : validationResultMapper.map(validationResult);
    }

}
