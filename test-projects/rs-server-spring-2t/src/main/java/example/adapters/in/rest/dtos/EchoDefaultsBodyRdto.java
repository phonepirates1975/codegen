package example.adapters.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import example.commons.validator.Field;
import javax.annotation.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE)
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsBodyRdto {

    public static final Field<EchoDefaultsBodyRdto, String> FIELD_PROP_INT = new Field<>("propInt",
            EchoDefaultsBodyRdto::getPropInt);
    public static final Field<EchoDefaultsBodyRdto, String> FIELD_PROP_FLOAT = new Field<>("propFloat",
            EchoDefaultsBodyRdto::getPropFloat);
    public static final Field<EchoDefaultsBodyRdto, String> FIELD_PROP_DOUBLE = new Field<>("propDouble",
            EchoDefaultsBodyRdto::getPropDouble);
    public static final Field<EchoDefaultsBodyRdto, String> FIELD_PROP_NUMBER = new Field<>("propNumber",
            EchoDefaultsBodyRdto::getPropNumber);
    public static final Field<EchoDefaultsBodyRdto, String> FIELD_PROP_STRING = new Field<>("propString",
            EchoDefaultsBodyRdto::getPropString);
    public static final Field<EchoDefaultsBodyRdto, String> FIELD_PROP_ENUM = new Field<>("propEnum",
            EchoDefaultsBodyRdto::getPropEnum);
    public static final Field<EchoDefaultsBodyRdto, String> FIELD_PROP_DATE = new Field<>("propDate",
            EchoDefaultsBodyRdto::getPropDate);
    public static final Field<EchoDefaultsBodyRdto, String> FIELD_PROP_DATE_TIME = new Field<>("propDateTime",
            EchoDefaultsBodyRdto::getPropDateTime);

    @JsonProperty("propInt")
    @JsonRawValue
    private String propInt = "101";
    @JsonProperty("propFloat")
    @JsonRawValue
    private String propFloat = "101.101";
    @JsonProperty("propDouble")
    @JsonRawValue
    private String propDouble = "202.202";
    @JsonProperty("propNumber")
    @JsonRawValue
    private String propNumber = "3.33";
    @JsonProperty("propString")
    private String propString = "WHITE";
    @JsonProperty("propEnum")
    private String propEnum = "3";
    @JsonProperty("propDate")
    private String propDate = "2021-09-01";
    @JsonProperty("propDateTime")
    private String propDateTime = "2021-09-01T09:09:09Z";

}
