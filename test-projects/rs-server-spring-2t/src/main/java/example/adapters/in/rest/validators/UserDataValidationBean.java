package example.adapters.in.rest.validators;

import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.EchoPostRequest;
import example.application.UserData;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

import static example.commons.validator.CommonCheckers.*;

@Component
public class UserDataValidationBean {

    public void checkAmountByUser(ValidationContext<String> context) {
        UserData userData = context.getBean(UserData.class);
        BigDecimal amount = new BigDecimal(context.getValue());
        if (userData.getMaxAmount().compareTo(amount) < 0) {
            writeError(context, context.getName() + " should be <= " + userData.getMaxAmount(), context.getName(), ERR_CODE_IS_TOO_BIG);
        } else if (userData.getMinAmount().compareTo(amount) > 0) {
            writeError(context, context.getName() + " should be >= " + userData.getMinAmount(), context.getName(), ERR_CODE_IS_TOO_SMALL);
        }
    }

}
