package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.EchoErrorRrequest;
import example.adapters.in.rest.mappers.EchoErrorRequestMapper;
import example.commons.validator.ValidationResult;
import example.commons.validator.ValidationResultMapper;
import example.ports.in.rest.EchoFacade;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoErrorController {

    private final EchoErrorRequestMapper echoErrorRequestMapper;
    private final EchoErrorValidator echoErrorValidator;
    private final EchoFacade echoFacade;
    private final EchoErrorResponseMapper echoErrorResponseMapper;
    private final ValidationResultMapper validationResultMapper;

    @GetMapping(value = "/api/v1/echo-error", produces = {"application/json"})
    public ResponseEntity echoError(@RequestParam(required = false) String errorMessage) {
        EchoErrorRrequest request = echoErrorRequestMapper.map2EchoErrorRrequest(errorMessage);
        ValidationResult validationResult = echoErrorValidator.validate(request);
        return validationResult.isValid()
                ? echoErrorResponseMapper
                        .map(echoFacade.echoError(echoErrorRequestMapper.map2EchoErrorRequest(request)))
                : validationResultMapper.map(validationResult);
    }

}
