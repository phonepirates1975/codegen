package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.EchoDefaultsBodyRdto;
import example.adapters.in.rest.dtos.EchoDefaultsPostRrequest;
import example.adapters.in.rest.mappers.EchoDefaultsPostRequestMapper;
import example.commons.validator.ValidationResult;
import example.commons.validator.ValidationResultMapper;
import example.ports.in.rest.EchoFacade;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsPostController {

    private final EchoDefaultsPostRequestMapper echoDefaultsPostRequestMapper;
    private final EchoDefaultsPostValidator echoDefaultsPostValidator;
    private final EchoFacade echoFacade;
    private final EchoDefaultsPostResponseMapper echoDefaultsPostResponseMapper;
    private final ValidationResultMapper validationResultMapper;

    @PostMapping(value = "/api/v1/echo-defaults", produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity echoDefaultsPost(
            @RequestHeader(value = "Default-Header-Param", required = false) String defaultHeaderParam,
            @RequestBody EchoDefaultsBodyRdto requestBody) {
        EchoDefaultsPostRrequest request = echoDefaultsPostRequestMapper
                .map2EchoDefaultsPostRrequest(defaultHeaderParam, requestBody);
        ValidationResult validationResult = echoDefaultsPostValidator.validate(request);
        return validationResult.isValid()
                ? echoDefaultsPostResponseMapper.map(
                        echoFacade.echoDefaultsPost(echoDefaultsPostRequestMapper.map2EchoDefaultsPostRequest(request)))
                : validationResultMapper.map(validationResult);
    }

}
