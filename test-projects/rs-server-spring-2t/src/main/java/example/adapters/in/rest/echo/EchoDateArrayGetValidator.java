package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.EchoDateArrayGetRrequest;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.dtos.EchoDateArrayGetRrequest.*;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoDateArrayGetValidator extends Validator<EchoDateArrayGetRrequest> {

    public void check(ValidationContext<EchoDateArrayGetRrequest> ctx) {
        ctx.check(FIELD_DATE_ARRAY, list(ISO_DATE));
    }

}
