package example.adapters.in.rest.echo;

import example.adapters.in.rest.mappers.EchoBodyMapper;
import example.commons.adapters.in.rest.mappers.ErrorDescriptionMapper;
import example.ports.in.rest.dtos.EchoPostResponse;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoPostResponseMapper {

    private final EchoBodyMapper echoBodyMapper;
    private final ErrorDescriptionMapper errorDescriptionMapper;

    public ResponseEntity map(@Nonnull EchoPostResponse response) {
        ResponseEntity.BodyBuilder responseBuilder = ResponseEntity.status(response.getStatus());
        response.getHeaders().forEach(responseBuilder::header);
        if (response.is200()) {
            return responseBuilder.body(echoBodyMapper.map2EchoBodyRdto(response.get200()));
        }
        return responseBuilder.body(errorDescriptionMapper.map2ErrorDescriptionRdto(response.getOther()));
    }

}
