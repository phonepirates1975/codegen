package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.EchoDateArrayGetRrequest;
import example.adapters.in.rest.mappers.EchoDateArrayGetRequestMapper;
import example.commons.validator.ValidationResult;
import example.commons.validator.ValidationResultMapper;
import example.ports.in.rest.EchoFacade;
import java.util.List;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoDateArrayGetController {

    private final EchoDateArrayGetRequestMapper echoDateArrayGetRequestMapper;
    private final EchoDateArrayGetValidator echoDateArrayGetValidator;
    private final EchoFacade echoFacade;
    private final EchoDateArrayGetResponseMapper echoDateArrayGetResponseMapper;
    private final ValidationResultMapper validationResultMapper;

    @GetMapping(value = "/api/v1/echo-date-array", produces = {"application/json"})
    public ResponseEntity echoDateArrayGet(
            @RequestParam(value = "date_array", required = false) List<String> dateArray) {
        EchoDateArrayGetRrequest request = echoDateArrayGetRequestMapper.map2EchoDateArrayGetRrequest(dateArray);
        ValidationResult validationResult = echoDateArrayGetValidator.validate(request);
        return validationResult.isValid()
                ? echoDateArrayGetResponseMapper.map(
                        echoFacade.echoDateArrayGet(echoDateArrayGetRequestMapper.map2EchoDateArrayGetRequest(request)))
                : validationResultMapper.map(validationResult);
    }

}
