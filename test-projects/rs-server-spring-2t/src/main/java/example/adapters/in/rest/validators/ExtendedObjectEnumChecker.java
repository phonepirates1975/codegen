package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoBodyRdto;
import example.commons.validator.Checker;
import example.commons.validator.ValidationContext;
import org.springframework.stereotype.Component;

import static example.commons.validator.CommonCheckers.required;
import static example.commons.validator.CommonCheckers.writeError;

@Component
public class ExtendedObjectEnumChecker implements Checker<String> {

    public void check(ValidationContext<String> context) {
        context.check(required());
        if (context.isCurrentFieldValid()) {
            ValidationContext<String> mainFieldCtx = context.getParent(EchoBodyRdto.class)
                    .orElseThrow(IllegalStateException::new)
                    .getChild(EchoBodyRdto.FIELD_PROP_ENUM_REUSABLE);
            if (!context.getValue().equals(mainFieldCtx.getValue())) {
                writeError(context, context.getName() + " should be equal to " + mainFieldCtx.getName(), context.getName(), "custom_code");
            }
        }
    }

    @Override
    public boolean checkNull() {
        return true;
    }
}
