package example.adapters.in.rest.mappers;

import example.adapters.in.rest.dtos.EchoArraysBodyRdto;
import example.commons.SerializationUtils;
import example.commons.adapters.in.rest.mappers.SimpleObjectMapper;
import example.commons.ports.in.rest.dtos.ReusableEnumEnum;
import example.ports.in.rest.dtos.EchoArraysBodyDto;
import example.ports.in.rest.dtos.EnumTypeEnum;
import java.util.Optional;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoArraysBodyMapper {

    private final SimpleObjectMapper simpleObjectMapper;

    public EchoArraysBodyDto map2EchoArraysBodyDto(EchoArraysBodyRdto value) {
        return value == null ? null : map(new EchoArraysBodyDto(), value);
    }

    public EchoArraysBodyRdto map2EchoArraysBodyRdto(EchoArraysBodyDto value) {
        return value == null ? null : map(new EchoArraysBodyRdto(), value);
    }

    public EchoArraysBodyDto map(EchoArraysBodyDto result, EchoArraysBodyRdto value) {
        return result
                .setPropIntList(SerializationUtils.transformList(value.getPropIntList(), SerializationUtils::toInteger))
                .setPropFloatList(
                        SerializationUtils.transformList(value.getPropFloatList(), SerializationUtils::toFloat))
                .setPropDoubleList(
                        SerializationUtils.transformList(value.getPropDoubleList(), SerializationUtils::toDouble))
                .setPropAmountList(SerializationUtils.transformList(value.getPropAmountList(), v -> v))
                .setPropStringList(SerializationUtils.transformList(value.getPropStringList(), v -> v))
                .setPropDateList(
                        SerializationUtils.transformList(value.getPropDateList(), SerializationUtils::toLocalDate))
                .setPropDateTimeListOfList(SerializationUtils.transformList(value.getPropDateTimeListOfList(),
                        v -> SerializationUtils.transformList(v, SerializationUtils::toLocalDateTime)))
                .setPropBooleanList(
                        SerializationUtils.transformList(value.getPropBooleanList(), SerializationUtils::toBoolean))
                .setPropObjectList(SerializationUtils.transformList(value.getPropObjectList(),
                        simpleObjectMapper::map2SimpleObjectDto))
                .setPropObjectListOfList(SerializationUtils.transformList(value.getPropObjectListOfList(),
                        v -> SerializationUtils.transformList(v, simpleObjectMapper::map2SimpleObjectDto)))
                .setPropEnumReusableList(
                        SerializationUtils.transformList(value.getPropEnumReusableList(), ReusableEnumEnum::fromValue))
                .setPropEnumList(SerializationUtils.transformList(value.getPropEnumList(), EnumTypeEnum::fromValue))
                .setPropMapOfInt(
                        SerializationUtils.transformMap(value.getPropMapOfInt(), SerializationUtils::toInteger))
                .setPropMapOfObject(SerializationUtils.transformMap(value.getPropMapOfObject(),
                        simpleObjectMapper::map2SimpleObjectDto))
                .setPropMapOfListOfObject(SerializationUtils.transformMap(value.getPropMapOfListOfObject(),
                        v -> SerializationUtils.transformList(v, simpleObjectMapper::map2SimpleObjectDto)));
    }

    public EchoArraysBodyRdto map(EchoArraysBodyRdto result, EchoArraysBodyDto value) {
        return result
                .setPropIntList(SerializationUtils.transformList(value.getPropIntList(), SerializationUtils::toString))
                .setPropFloatList(
                        SerializationUtils.transformList(value.getPropFloatList(), SerializationUtils::toString))
                .setPropDoubleList(
                        SerializationUtils.transformList(value.getPropDoubleList(), SerializationUtils::toString))
                .setPropAmountList(SerializationUtils.transformList(value.getPropAmountList(), v -> v))
                .setPropStringList(SerializationUtils.transformList(value.getPropStringList(), v -> v))
                .setPropDateList(
                        SerializationUtils.transformList(value.getPropDateList(), SerializationUtils::toString))
                .setPropDateTimeListOfList(SerializationUtils.transformList(value.getPropDateTimeListOfList(),
                        v -> SerializationUtils.transformList(v, SerializationUtils::toString)))
                .setPropBooleanList(
                        SerializationUtils.transformList(value.getPropBooleanList(), SerializationUtils::toString))
                .setPropObjectList(SerializationUtils.transformList(value.getPropObjectList(),
                        simpleObjectMapper::map2SimpleObjectRdto))
                .setPropObjectListOfList(SerializationUtils.transformList(value.getPropObjectListOfList(),
                        v -> SerializationUtils.transformList(v, simpleObjectMapper::map2SimpleObjectRdto)))
                .setPropEnumReusableList(SerializationUtils.transformList(value.getPropEnumReusableList(),
                        v -> Optional.ofNullable(v).map(ReusableEnumEnum::getValue).orElse(null)))
                .setPropEnumList(SerializationUtils.transformList(value.getPropEnumList(),
                        v -> Optional.ofNullable(v).map(EnumTypeEnum::getValue).orElse(null)))
                .setPropMapOfInt(SerializationUtils.transformMap(value.getPropMapOfInt(), SerializationUtils::toString))
                .setPropMapOfObject(SerializationUtils.transformMap(value.getPropMapOfObject(),
                        simpleObjectMapper::map2SimpleObjectRdto))
                .setPropMapOfListOfObject(SerializationUtils.transformMap(value.getPropMapOfListOfObject(),
                        v -> SerializationUtils.transformList(v, simpleObjectMapper::map2SimpleObjectRdto)));
    }

}
