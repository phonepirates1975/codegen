package example.adapters.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import example.commons.MapRawValueSerializer;
import example.commons.adapters.in.rest.dtos.SimpleObjectRdto;
import example.commons.validator.Field;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE)
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoArraysBodyRdto {

    public static final Field<EchoArraysBodyRdto, List<String>> FIELD_PROP_INT_LIST = new Field<>("prop_int_list",
            EchoArraysBodyRdto::getPropIntList);
    public static final Field<EchoArraysBodyRdto, List<String>> FIELD_PROP_FLOAT_LIST = new Field<>("prop_float_list",
            EchoArraysBodyRdto::getPropFloatList);
    public static final Field<EchoArraysBodyRdto, List<String>> FIELD_PROP_DOUBLE_LIST = new Field<>("prop_double_list",
            EchoArraysBodyRdto::getPropDoubleList);
    public static final Field<EchoArraysBodyRdto, List<String>> FIELD_PROP_AMOUNT_LIST = new Field<>("prop_amount_list",
            EchoArraysBodyRdto::getPropAmountList);
    public static final Field<EchoArraysBodyRdto, List<String>> FIELD_PROP_STRING_LIST = new Field<>("prop_string_list",
            EchoArraysBodyRdto::getPropStringList);
    public static final Field<EchoArraysBodyRdto, List<String>> FIELD_PROP_DATE_LIST = new Field<>("prop_date_list",
            EchoArraysBodyRdto::getPropDateList);
    public static final Field<EchoArraysBodyRdto, List<List<String>>> FIELD_PROP_DATE_TIME_LIST_OF_LIST = new Field<>(
            "prop_date_time_list_of_list", EchoArraysBodyRdto::getPropDateTimeListOfList);
    public static final Field<EchoArraysBodyRdto, List<String>> FIELD_PROP_BOOLEAN_LIST = new Field<>(
            "prop_boolean_list", EchoArraysBodyRdto::getPropBooleanList);
    public static final Field<EchoArraysBodyRdto, List<SimpleObjectRdto>> FIELD_PROP_OBJECT_LIST = new Field<>(
            "prop_object_list", EchoArraysBodyRdto::getPropObjectList);
    public static final Field<EchoArraysBodyRdto, List<List<SimpleObjectRdto>>> FIELD_PROP_OBJECT_LIST_OF_LIST = new Field<>(
            "prop_object_list_of_list", EchoArraysBodyRdto::getPropObjectListOfList);
    public static final Field<EchoArraysBodyRdto, List<String>> FIELD_PROP_ENUM_REUSABLE_LIST = new Field<>(
            "prop_enum_reusable_list", EchoArraysBodyRdto::getPropEnumReusableList);
    public static final Field<EchoArraysBodyRdto, List<String>> FIELD_PROP_ENUM_LIST = new Field<>("prop_enum_list",
            EchoArraysBodyRdto::getPropEnumList);
    public static final Field<EchoArraysBodyRdto, Map<String, String>> FIELD_PROP_MAP_OF_INT = new Field<>(
            "prop_map_of_int", EchoArraysBodyRdto::getPropMapOfInt);
    public static final Field<EchoArraysBodyRdto, Map<String, SimpleObjectRdto>> FIELD_PROP_MAP_OF_OBJECT = new Field<>(
            "prop_map_of_object", EchoArraysBodyRdto::getPropMapOfObject);
    public static final Field<EchoArraysBodyRdto, Map<String, List<SimpleObjectRdto>>> FIELD_PROP_MAP_OF_LIST_OF_OBJECT = new Field<>(
            "prop_map_of_list_of_object", EchoArraysBodyRdto::getPropMapOfListOfObject);

    @JsonProperty("prop_int_list")
    @JsonRawValue
    private List<String> propIntList;
    @JsonProperty("prop_float_list")
    @JsonRawValue
    private List<String> propFloatList;
    @JsonProperty("prop_double_list")
    @JsonRawValue
    private List<String> propDoubleList;
    @JsonProperty("prop_amount_list")
    private List<String> propAmountList;
    @JsonProperty("prop_string_list")
    private List<String> propStringList;
    @JsonProperty("prop_date_list")
    private List<String> propDateList;
    @JsonProperty("prop_date_time_list_of_list")
    private List<List<String>> propDateTimeListOfList;
    @JsonProperty("prop_boolean_list")
    @JsonRawValue
    private List<String> propBooleanList;
    @JsonProperty("prop_object_list")
    private List<SimpleObjectRdto> propObjectList;
    @JsonProperty("prop_object_list_of_list")
    private List<List<SimpleObjectRdto>> propObjectListOfList;
    @JsonProperty("prop_enum_reusable_list")
    private List<String> propEnumReusableList;
    @JsonProperty("prop_enum_list")
    private List<String> propEnumList;
    @JsonProperty("prop_map_of_int")
    @JsonSerialize(using = MapRawValueSerializer.class)
    private Map<String, String> propMapOfInt;
    @JsonProperty("prop_map_of_object")
    private Map<String, SimpleObjectRdto> propMapOfObject;
    @JsonProperty("prop_map_of_list_of_object")
    private Map<String, List<SimpleObjectRdto>> propMapOfListOfObject;

}
