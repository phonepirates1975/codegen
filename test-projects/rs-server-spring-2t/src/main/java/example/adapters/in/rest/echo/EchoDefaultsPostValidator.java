package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.EchoDefaultsPostRrequest;
import example.adapters.in.rest.validators.EchoDefaultsBodyValidator;
import example.commons.validator.Checker;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.DefaultEnumEnum;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.dtos.EchoDefaultsPostRrequest.*;
import static example.commons.validator.CommonCheckers.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsPostValidator extends Validator<EchoDefaultsPostRrequest> {

    private static final Checker<String> DEFAULT_ENUM_ENUM = allow(DefaultEnumEnum.values());

    private final EchoDefaultsBodyValidator echoDefaultsBodyValidator;

    public void check(ValidationContext<EchoDefaultsPostRrequest> ctx) {
        ctx.check(FIELD_DEFAULT_HEADER_PARAM, DEFAULT_ENUM_ENUM);
        ctx.check(FIELD_REQUEST_BODY, required(), echoDefaultsBodyValidator);
    }

}
