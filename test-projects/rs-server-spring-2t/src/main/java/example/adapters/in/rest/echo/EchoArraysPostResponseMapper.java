package example.adapters.in.rest.echo;

import example.adapters.in.rest.mappers.EchoArraysBodyMapper;
import example.commons.SerializationUtils;
import example.commons.adapters.in.rest.mappers.ErrorDescriptionMapper;
import example.ports.in.rest.dtos.EchoArraysPostResponse;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostResponseMapper {

    private final ErrorDescriptionMapper errorDescriptionMapper;
    private final EchoArraysBodyMapper echoArraysBodyMapper;

    public ResponseEntity map(@Nonnull EchoArraysPostResponse response) {
        ResponseEntity.BodyBuilder responseBuilder = ResponseEntity.status(response.getStatus());
        response.getHeaders().forEach(responseBuilder::header);
        if (response.is400()) {
            return responseBuilder.body(errorDescriptionMapper.map2ErrorDescriptionRdto(response.get400()));
        }
        return responseBuilder.body(
                SerializationUtils.transformList(response.get200(), echoArraysBodyMapper::map2EchoArraysBodyRdto));
    }

}
