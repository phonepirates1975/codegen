package example.adapters.in.rest.echo;

import example.adapters.in.rest.mappers.EchoDefaultsBodyMapper;
import example.commons.adapters.in.rest.mappers.ErrorDescriptionMapper;
import example.ports.in.rest.dtos.EchoDefaultsPostResponse;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsPostResponseMapper {

    private final EchoDefaultsBodyMapper echoDefaultsBodyMapper;
    private final ErrorDescriptionMapper errorDescriptionMapper;

    public ResponseEntity map(@Nonnull EchoDefaultsPostResponse response) {
        ResponseEntity.BodyBuilder responseBuilder = ResponseEntity.status(response.getStatus());
        response.getHeaders().forEach(responseBuilder::header);
        if (response.is200()) {
            return responseBuilder.body(echoDefaultsBodyMapper.map2EchoDefaultsBodyRdto(response.get200()));
        }
        return responseBuilder.body(errorDescriptionMapper.map2ErrorDescriptionRdto(response.getOther()));
    }

}
