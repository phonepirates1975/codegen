package example.adapters.in.rest.echo;

import example.commons.adapters.in.rest.mappers.ErrorDescriptionMapper;
import example.ports.in.rest.dtos.DownloadEchoFileResponse;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class DownloadEchoFileResponseMapper {

    private final ErrorDescriptionMapper errorDescriptionMapper;

    public ResponseEntity map(@Nonnull DownloadEchoFileResponse response) {
        ResponseEntity.BodyBuilder responseBuilder = ResponseEntity.status(response.getStatus());
        response.getHeaders().forEach(responseBuilder::header);
        if (response.is404()) {
            return responseBuilder.build();
        }
        if (response.is200()) {
            return responseBuilder.body(response.get200());
        }
        return responseBuilder.body(errorDescriptionMapper.map2ErrorDescriptionRdto(response.getOther()));
    }

}
