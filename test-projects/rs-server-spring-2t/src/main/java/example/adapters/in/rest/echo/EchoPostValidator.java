package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.EchoPostRrequest;
import example.adapters.in.rest.validators.AuthorizationChecker;
import example.adapters.in.rest.validators.EchoBodyValidator;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.dtos.EchoPostRrequest.*;
import static example.commons.validator.CommonCheckers.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoPostValidator extends Validator<EchoPostRrequest> {

    private final AuthorizationChecker authorizationChecker;
    private final EchoBodyValidator echoBodyValidator;

    public void check(ValidationContext<EchoPostRrequest> ctx) {
        ctx.check(FIELD_AUTHORIZATION_PARAM, authorizationChecker, required());
        ctx.check(FIELD_CORRELATION_ID_PARAM, required());
        ctx.check(FIELD_TIMESTAMP_PARAM, ISO_DATE_TIME);
        ctx.check(FIELD_REQUEST_BODY, required(), echoBodyValidator);
    }

}
