package example.adapters.in.rest.dtos;

import example.commons.validator.Field;
import javax.annotation.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoGetRrequest {

    public static final Field<EchoGetRrequest, String> FIELD_CORRELATION_ID_PARAM = new Field<>(
            "X-Correlation-ID (HEADER parameter)", EchoGetRrequest::getCorrelationIdParam);
    public static final Field<EchoGetRrequest, String> FIELD_PROP_INT_REQUIRED = new Field<>(
            "prop_int_required (QUERY parameter)", EchoGetRrequest::getPropIntRequired);
    public static final Field<EchoGetRrequest, String> FIELD_PROP_FLOAT = new Field<>("prop_float (QUERY parameter)",
            EchoGetRrequest::getPropFloat);
    public static final Field<EchoGetRrequest, String> FIELD_PROP_ENUM = new Field<>("prop_enum (QUERY parameter)",
            EchoGetRrequest::getPropEnum);

    private String correlationIdParam;
    private String propIntRequired;
    private String propFloat;
    private String propEnum;

}
