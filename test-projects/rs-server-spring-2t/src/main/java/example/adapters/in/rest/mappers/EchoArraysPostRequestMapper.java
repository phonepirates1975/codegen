package example.adapters.in.rest.mappers;

import example.adapters.in.rest.dtos.EchoArraysBodyRdto;
import example.adapters.in.rest.dtos.EchoArraysPostRrequest;
import example.commons.SerializationUtils;
import example.ports.in.rest.dtos.EchoArraysPostRequest;
import java.util.List;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostRequestMapper {

    private final EchoArraysBodyMapper echoArraysBodyMapper;

    public EchoArraysPostRrequest map2EchoArraysPostRrequest(String authorizationParam, String inlineHeaderParam,
            List<EchoArraysBodyRdto> requestBody) {
        return new EchoArraysPostRrequest().setAuthorizationParam(authorizationParam)
                .setInlineHeaderParam(inlineHeaderParam)
                .setRequestBody(SerializationUtils.transformList(requestBody, v -> v));
    }

    public EchoArraysPostRequest map2EchoArraysPostRequest(EchoArraysPostRrequest value) {
        return value == null
                ? null
                : new EchoArraysPostRequest().setAuthorizationParam(value.getAuthorizationParam())
                        .setInlineHeaderParam(value.getInlineHeaderParam()).setRequestBody(SerializationUtils
                                .transformList(value.getRequestBody(), echoArraysBodyMapper::map2EchoArraysBodyDto));
    }

}
