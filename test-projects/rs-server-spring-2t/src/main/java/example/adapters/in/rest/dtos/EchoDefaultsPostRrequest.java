package example.adapters.in.rest.dtos;

import example.commons.validator.Field;
import javax.annotation.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsPostRrequest {

    public static final Field<EchoDefaultsPostRrequest, String> FIELD_DEFAULT_HEADER_PARAM = new Field<>(
            "Default-Header-Param (HEADER parameter)", EchoDefaultsPostRrequest::getDefaultHeaderParam);
    public static final Field<EchoDefaultsPostRrequest, EchoDefaultsBodyRdto> FIELD_REQUEST_BODY = new Field<>(
            "requestBody", EchoDefaultsPostRrequest::getRequestBody);

    private String defaultHeaderParam;
    private EchoDefaultsBodyRdto requestBody;

}
