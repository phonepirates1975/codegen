package example.adapters.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import example.commons.adapters.in.rest.dtos.SimpleObjectRdto;
import example.commons.validator.Field;
import java.util.Map;
import javax.annotation.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE)
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoBodyRdto {

    public static final Field<EchoBodyRdto, String> FIELD_PROP_INT = new Field<>("prop_int", EchoBodyRdto::getPropInt);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_INT_SECOND = new Field<>("prop_int_second",
            EchoBodyRdto::getPropIntSecond);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_INT_REQUIRED = new Field<>("prop_int_required",
            EchoBodyRdto::getPropIntRequired);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_FLOAT = new Field<>("prop_float",
            EchoBodyRdto::getPropFloat);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_DOUBLE = new Field<>("prop_double",
            EchoBodyRdto::getPropDouble);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_AMOUNT = new Field<>("prop_amount",
            EchoBodyRdto::getPropAmount);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_AMOUNT_NUMBER = new Field<>("prop_amount_number",
            EchoBodyRdto::getPropAmountNumber);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_STRING = new Field<>("prop_string",
            EchoBodyRdto::getPropString);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_STRING_PATTERN = new Field<>("prop_string_pattern",
            EchoBodyRdto::getPropStringPattern);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_DEFAULT = new Field<>("prop_default",
            EchoBodyRdto::getPropDefault);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_DATE = new Field<>("prop_date",
            EchoBodyRdto::getPropDate);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_DATE_SECOND = new Field<>("prop_date_second",
            EchoBodyRdto::getPropDateSecond);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_DATE_TIME = new Field<>("prop_date_time",
            EchoBodyRdto::getPropDateTime);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_BASE64 = new Field<>("prop_base64",
            EchoBodyRdto::getPropBase64);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_BOOLEAN = new Field<>("prop_boolean",
            EchoBodyRdto::getPropBoolean);
    public static final Field<EchoBodyRdto, SimpleObjectRdto> FIELD_PROP_OBJECT = new Field<>("prop_object",
            EchoBodyRdto::getPropObject);
    public static final Field<EchoBodyRdto, Map<String, Object>> FIELD_PROP_OBJECT_ANY = new Field<>("prop_object_any",
            EchoBodyRdto::getPropObjectAny);
    public static final Field<EchoBodyRdto, ExtendedObjectRdto> FIELD_PROP_OBJECT_EXTENDED = new Field<>(
            "prop_object_extended", EchoBodyRdto::getPropObjectExtended);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_ENUM_REUSABLE = new Field<>("prop_enum_reusable",
            EchoBodyRdto::getPropEnumReusable);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_ENUM = new Field<>("prop_enum",
            EchoBodyRdto::getPropEnum);

    @JsonProperty("prop_int")
    @JsonRawValue
    private String propInt;
    @JsonProperty("prop_int_second")
    @JsonRawValue
    private String propIntSecond;
    @JsonProperty("prop_int_required")
    @JsonRawValue
    private String propIntRequired;
    @JsonProperty("prop_float")
    @JsonRawValue
    private String propFloat;
    @JsonProperty("prop_double")
    @JsonRawValue
    private String propDouble;
    @JsonProperty("prop_amount")
    private String propAmount;
    @JsonProperty("prop_amount_number")
    @JsonRawValue
    private String propAmountNumber;
    @JsonProperty("prop_string")
    private String propString;
    @JsonProperty("prop_string_pattern")
    private String propStringPattern;
    @JsonProperty("prop_default")
    private String propDefault = "value";
    @JsonProperty("prop_date")
    private String propDate;
    @JsonProperty("prop_date_second")
    private String propDateSecond;
    @JsonProperty("prop_date_time")
    private String propDateTime;
    @JsonProperty("prop_base64")
    private String propBase64;
    @JsonProperty("prop_boolean")
    @JsonRawValue
    private String propBoolean;
    @JsonProperty("prop_object")
    private SimpleObjectRdto propObject;
    @JsonProperty("prop_object_any")
    private Map<String, Object> propObjectAny;
    @JsonProperty("prop_object_extended")
    private ExtendedObjectRdto propObjectExtended;
    @JsonProperty("prop_enum_reusable")
    private String propEnumReusable = "a";
    @JsonProperty("prop_enum")
    private String propEnum;

}
