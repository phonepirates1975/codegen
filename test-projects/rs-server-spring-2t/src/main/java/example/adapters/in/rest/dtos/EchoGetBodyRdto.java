package example.adapters.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import example.commons.validator.Field;
import javax.annotation.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE)
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoGetBodyRdto {

    public static final Field<EchoGetBodyRdto, String> FIELD_PROP_INT_REQUIRED = new Field<>("prop_int_required",
            EchoGetBodyRdto::getPropIntRequired);
    public static final Field<EchoGetBodyRdto, String> FIELD_PROP_FLOAT = new Field<>("prop_float",
            EchoGetBodyRdto::getPropFloat);
    public static final Field<EchoGetBodyRdto, String> FIELD_PROP_ENUM = new Field<>("prop_enum",
            EchoGetBodyRdto::getPropEnum);

    @JsonProperty("prop_int_required")
    @JsonRawValue
    private String propIntRequired;
    @JsonProperty("prop_float")
    @JsonRawValue
    private String propFloat;
    @JsonProperty("prop_enum")
    private String propEnum;

}
