package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.EchoArraysPostRrequest;
import example.adapters.in.rest.validators.AuthorizationChecker;
import example.adapters.in.rest.validators.EchoArraysBodyValidator;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.dtos.EchoArraysPostRrequest.*;
import static example.commons.validator.CommonCheckers.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostValidator extends Validator<EchoArraysPostRrequest> {

    private final AuthorizationChecker authorizationChecker;
    private final EchoArraysBodyValidator echoArraysBodyValidator;

    public void check(ValidationContext<EchoArraysPostRrequest> ctx) {
        ctx.check(FIELD_AUTHORIZATION_PARAM, authorizationChecker, required());
        ctx.check(FIELD_REQUEST_BODY, required(), list(echoArraysBodyValidator));
    }

}
