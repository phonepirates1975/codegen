package example.adapters.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import example.commons.adapters.in.rest.dtos.SimpleObjectRdto;
import example.commons.validator.Field;
import javax.annotation.Generated;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE)
@Data
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Generated("pl.metaprogramming.codegen")
public class ExtendedObjectRdto extends SimpleObjectRdto {

    public static final Field<ExtendedObjectRdto, String> FIELD_EO_ENUM_REUSABLE = new Field<>("eo_enum_reusable",
            ExtendedObjectRdto::getEoEnumReusable);
    public static final Field<ExtendedObjectRdto, ExtendedObjectRdto> FIELD_SELF_PROPERTY = new Field<>("self_property",
            ExtendedObjectRdto::getSelfProperty);

    @JsonProperty("eo_enum_reusable")
    private String eoEnumReusable;
    @JsonProperty("self_property")
    private ExtendedObjectRdto selfProperty;

}
