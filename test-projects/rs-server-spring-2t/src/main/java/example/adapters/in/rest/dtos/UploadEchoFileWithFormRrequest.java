package example.adapters.in.rest.dtos;

import example.commons.validator.Field;
import javax.annotation.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.core.io.Resource;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileWithFormRrequest {

    public static final Field<UploadEchoFileWithFormRrequest, String> FIELD_ID = new Field<>("id (PATH parameter)",
            UploadEchoFileWithFormRrequest::getId);
    public static final Field<UploadEchoFileWithFormRrequest, Resource> FIELD_FILE = new Field<>(
            "file (FORMDATA parameter)", UploadEchoFileWithFormRrequest::getFile);

    private String id;
    private Resource file;

}
