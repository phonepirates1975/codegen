package example.adapters.in.rest.echo;

import example.commons.SerializationUtils;
import example.commons.adapters.in.rest.mappers.ErrorDescriptionMapper;
import example.ports.in.rest.dtos.EchoDateArrayGetResponse;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoDateArrayGetResponseMapper {

    private final ErrorDescriptionMapper errorDescriptionMapper;

    public ResponseEntity map(@Nonnull EchoDateArrayGetResponse response) {
        ResponseEntity.BodyBuilder responseBuilder = ResponseEntity.status(response.getStatus());
        response.getHeaders().forEach(responseBuilder::header);
        if (response.is200()) {
            return responseBuilder
                    .body(SerializationUtils.transformList(response.get200(), SerializationUtils::toString));
        }
        return responseBuilder.body(errorDescriptionMapper.map2ErrorDescriptionRdto(response.getOther()));
    }

}
