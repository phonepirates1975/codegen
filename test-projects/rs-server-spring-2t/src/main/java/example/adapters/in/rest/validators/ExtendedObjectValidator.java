package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.ExtendedObjectRdto;
import example.commons.adapters.in.rest.validators.SimpleObjectValidator;
import example.commons.ports.in.rest.dtos.ReusableEnumEnum;
import example.commons.validator.Checker;
import example.commons.validator.ValidationContext;
import javax.annotation.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.dtos.ExtendedObjectRdto.*;
import static example.commons.validator.CommonCheckers.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class ExtendedObjectValidator implements Checker<ExtendedObjectRdto> {

    private static final Checker<String> REUSABLE_ENUM_ENUM = allow(ReusableEnumEnum.values());

    private final ExtendedObjectChecker extendedObjectChecker;
    private final SimpleObjectValidator simpleObjectValidator;
    private final ExtendedObjectEnumChecker extendedObjectEnumChecker;

    public void check(ValidationContext<ExtendedObjectRdto> ctx) {
        ctx.check(extendedObjectChecker);
        simpleObjectValidator.checkWithParent(ctx);
        ctx.check(FIELD_EO_ENUM_REUSABLE, REUSABLE_ENUM_ENUM, extendedObjectEnumChecker);
        ctx.check(FIELD_SELF_PROPERTY, this);
    }

}
