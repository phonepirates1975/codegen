package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.EchoGetRrequest;
import example.commons.validator.Checker;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.DefaultEnumEnum;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.dtos.EchoGetRrequest.*;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoGetValidator extends Validator<EchoGetRrequest> {

    private static final Checker<String> FIELD_PROP_INT_REQUIRED_MIN_MAX = range(Long::new, "5", null);
    private static final Checker<String> FIELD_PROP_FLOAT_MIN_MAX = range(Float::new, null, "5.02");
    private static final Checker<String> DEFAULT_ENUM_ENUM = allow(DefaultEnumEnum.values());

    public void check(ValidationContext<EchoGetRrequest> ctx) {
        ctx.check(FIELD_CORRELATION_ID_PARAM, required());
        ctx.check(FIELD_PROP_INT_REQUIRED, required(), INT64, FIELD_PROP_INT_REQUIRED_MIN_MAX);
        ctx.check(FIELD_PROP_FLOAT, FLOAT, FIELD_PROP_FLOAT_MIN_MAX);
        ctx.check(FIELD_PROP_ENUM, DEFAULT_ENUM_ENUM);
    }

}
