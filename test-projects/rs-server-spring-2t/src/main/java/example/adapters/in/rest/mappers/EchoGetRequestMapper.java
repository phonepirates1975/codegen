package example.adapters.in.rest.mappers;

import example.adapters.in.rest.dtos.EchoGetRrequest;
import example.commons.SerializationUtils;
import example.ports.in.rest.dtos.DefaultEnumEnum;
import example.ports.in.rest.dtos.EchoGetRequest;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoGetRequestMapper {

    public EchoGetRrequest map2EchoGetRrequest(String correlationIdParam, String propIntRequired, String propFloat,
            String propEnum) {
        return new EchoGetRrequest().setCorrelationIdParam(correlationIdParam).setPropIntRequired(propIntRequired)
                .setPropFloat(propFloat).setPropEnum(propEnum);
    }

    public EchoGetRequest map2EchoGetRequest(EchoGetRrequest value) {
        return value == null
                ? null
                : new EchoGetRequest().setCorrelationIdParam(value.getCorrelationIdParam())
                        .setPropIntRequired(SerializationUtils.toLong(value.getPropIntRequired()))
                        .setPropFloat(SerializationUtils.toFloat(value.getPropFloat()))
                        .setPropEnum(DefaultEnumEnum.fromValue(value.getPropEnum()));
    }

}
