package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.UploadEchoFileRrequest;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.dtos.UploadEchoFileRrequest.*;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileValidator extends Validator<UploadEchoFileRrequest> {

    public void check(ValidationContext<UploadEchoFileRrequest> ctx) {
        ctx.check(FIELD_ID, required(), INT64);
        ctx.check(FIELD_REQUEST_BODY, required());
    }

}
