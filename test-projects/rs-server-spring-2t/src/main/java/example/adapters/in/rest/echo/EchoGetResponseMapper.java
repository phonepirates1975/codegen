package example.adapters.in.rest.echo;

import example.adapters.in.rest.mappers.EchoGetBodyMapper;
import example.commons.SerializationUtils;
import example.commons.adapters.in.rest.mappers.ErrorItemMapper;
import example.ports.in.rest.dtos.EchoGetResponse;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoGetResponseMapper {

    private final ErrorItemMapper errorItemMapper;
    private final EchoGetBodyMapper echoGetBodyMapper;

    public ResponseEntity map(@Nonnull EchoGetResponse response) {
        ResponseEntity.BodyBuilder responseBuilder = ResponseEntity.status(response.getStatus());
        response.getHeaders().forEach(responseBuilder::header);
        if (response.is400()) {
            return responseBuilder
                    .body(SerializationUtils.transformList(response.get400(), errorItemMapper::map2ErrorItemRdto));
        }
        return responseBuilder.body(echoGetBodyMapper.map2EchoGetBodyRdto(response.get200()));
    }

}
