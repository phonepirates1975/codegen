package example.ports.in.rest.dtos;

import example.commons.RestResponse200;
import example.commons.RestResponse400;
import example.commons.RestResponseBase;
import example.commons.ports.in.rest.dtos.ErrorItemDto;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.annotation.Generated;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
@Generated("pl.metaprogramming.codegen")
public class EchoGetResponse extends RestResponseBase<EchoGetResponse>
        implements
            RestResponse200<EchoGetResponse, EchoGetBodyDto>,
            RestResponse400<EchoGetResponse, List<ErrorItemDto>> {

    private static final Collection<Integer> DECLARED_STATUSES = Arrays.asList(200, 400);

    private EchoGetResponse(Integer status, Object body) {
        super(status, body);
    }

    public Collection<Integer> getDeclaredStatuses() {
        return DECLARED_STATUSES;
    }

    public EchoGetResponse self() {
        return this;
    }

    public static EchoGetResponse set200(EchoGetBodyDto body) {
        return new EchoGetResponse(200, body);
    }

    public static EchoGetResponse set400(List<ErrorItemDto> body) {
        return new EchoGetResponse(400, body);
    }

}
