package example.ports.in.rest.dtos;

import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileWithFormRequest {

    /**
     * id of file
     */
    @Nonnull
    private Long id;

    /**
     * file to upload
     */
    @Nonnull
    private byte[] file;

}
