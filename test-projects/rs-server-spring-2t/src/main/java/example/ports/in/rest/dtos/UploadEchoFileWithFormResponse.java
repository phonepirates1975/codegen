package example.ports.in.rest.dtos;

import example.commons.RestResponse204NoContent;
import example.commons.RestResponseBase;
import example.commons.RestResponseOther;
import example.commons.ports.in.rest.dtos.ErrorDescriptionDto;
import java.util.Collection;
import java.util.Collections;
import javax.annotation.Generated;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileWithFormResponse extends RestResponseBase<UploadEchoFileWithFormResponse>
        implements
            RestResponse204NoContent<UploadEchoFileWithFormResponse>,
            RestResponseOther<UploadEchoFileWithFormResponse, ErrorDescriptionDto> {

    private static final Collection<Integer> DECLARED_STATUSES = Collections.singletonList(204);

    private UploadEchoFileWithFormResponse(Integer status, Object body) {
        super(status, body);
    }

    public Collection<Integer> getDeclaredStatuses() {
        return DECLARED_STATUSES;
    }

    public UploadEchoFileWithFormResponse self() {
        return this;
    }

    public static UploadEchoFileWithFormResponse set204() {
        return new UploadEchoFileWithFormResponse(204, null);
    }

    public static UploadEchoFileWithFormResponse setOther(Integer status, ErrorDescriptionDto body) {
        if (DECLARED_STATUSES.contains(status)) {
            throw new IllegalArgumentException(
                    String.format("Status %s is declared. Use dedicated factory method for it.", status));
        }
        return new UploadEchoFileWithFormResponse(status, body);
    }

}
