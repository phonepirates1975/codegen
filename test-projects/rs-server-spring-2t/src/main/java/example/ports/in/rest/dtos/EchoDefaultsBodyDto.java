package example.ports.in.rest.dtos;

import example.commons.ports.in.rest.dtos.ReusableEnumEnum;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.annotation.Generated;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsBodyDto {

    @Nullable
    private Integer propInt;
    @Nullable
    private Float propFloat;
    @Nullable
    private Double propDouble;
    @Nullable
    private BigDecimal propNumber;
    @Nullable
    private String propString;
    @Nullable
    private ReusableEnumEnum propEnum;
    @Nullable
    private LocalDate propDate;
    @Nullable
    private LocalDateTime propDateTime;

}
