package example.ports.in.rest;

import example.ports.in.rest.dtos.DeleteFileRequest;
import example.ports.in.rest.dtos.DeleteFileResponse;
import example.ports.in.rest.dtos.DownloadEchoFileRequest;
import example.ports.in.rest.dtos.DownloadEchoFileResponse;
import example.ports.in.rest.dtos.EchoArraysPostRequest;
import example.ports.in.rest.dtos.EchoArraysPostResponse;
import example.ports.in.rest.dtos.EchoDateArrayGetRequest;
import example.ports.in.rest.dtos.EchoDateArrayGetResponse;
import example.ports.in.rest.dtos.EchoDefaultsPostRequest;
import example.ports.in.rest.dtos.EchoDefaultsPostResponse;
import example.ports.in.rest.dtos.EchoEmptyRequest;
import example.ports.in.rest.dtos.EchoEmptyResponse;
import example.ports.in.rest.dtos.EchoErrorRequest;
import example.ports.in.rest.dtos.EchoErrorResponse;
import example.ports.in.rest.dtos.EchoGetRequest;
import example.ports.in.rest.dtos.EchoGetResponse;
import example.ports.in.rest.dtos.EchoPostRequest;
import example.ports.in.rest.dtos.EchoPostResponse;
import example.ports.in.rest.dtos.UploadEchoFileRequest;
import example.ports.in.rest.dtos.UploadEchoFileResponse;
import example.ports.in.rest.dtos.UploadEchoFileWithFormRequest;
import example.ports.in.rest.dtos.UploadEchoFileWithFormResponse;
import javax.annotation.Generated;
import javax.annotation.Nonnull;

@Generated("pl.metaprogramming.codegen")
public interface EchoFacade {

    EchoPostResponse echoPost(@Nonnull EchoPostRequest request);

    EchoGetResponse echoGet(@Nonnull EchoGetRequest request);

    EchoDefaultsPostResponse echoDefaultsPost(@Nonnull EchoDefaultsPostRequest request);

    EchoArraysPostResponse echoArraysPost(@Nonnull EchoArraysPostRequest request);

    EchoDateArrayGetResponse echoDateArrayGet(@Nonnull EchoDateArrayGetRequest request);

    /**
     * upload file using multipart/form-data
     */
    UploadEchoFileWithFormResponse uploadEchoFileWithForm(@Nonnull UploadEchoFileWithFormRequest request);

    /**
     * upload file
     */
    UploadEchoFileResponse uploadEchoFile(@Nonnull UploadEchoFileRequest request);

    /**
     * Returns a file
     */
    DownloadEchoFileResponse downloadEchoFile(@Nonnull DownloadEchoFileRequest request);

    /**
     * deletes a file based on the id supplied
     */
    DeleteFileResponse deleteFile(@Nonnull DeleteFileRequest request);

    EchoEmptyResponse echoEmpty(@Nonnull EchoEmptyRequest request);

    EchoErrorResponse echoError(@Nonnull EchoErrorRequest request);

}
