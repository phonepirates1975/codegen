package example.ports.in.rest.dtos;

import example.commons.ports.in.rest.dtos.ReusableEnumEnum;
import example.commons.ports.in.rest.dtos.SimpleObjectDto;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Test object containing arrays properties
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoArraysBodyDto {

    @Nullable
    private List<Integer> propIntList;
    @Nullable
    private List<Float> propFloatList;
    @Nonnull
    private List<Double> propDoubleList;
    @Nullable
    private List<String> propAmountList;
    @Nullable
    private List<String> propStringList;
    @Nullable
    private List<LocalDate> propDateList;
    @Nullable
    private List<List<LocalDateTime>> propDateTimeListOfList;
    @Nullable
    private List<Boolean> propBooleanList;
    @Nullable
    private List<SimpleObjectDto> propObjectList;
    @Nullable
    private List<List<SimpleObjectDto>> propObjectListOfList;

    /**
     * enum property with external definition
     */
    @Nullable
    private List<ReusableEnumEnum> propEnumReusableList;
    @Nullable
    private List<EnumTypeEnum> propEnumList;

    /**
     * associative array with integer value
     */
    @Nullable
    private Map<String, Integer> propMapOfInt;

    /**
     * associative array with object as value
     */
    @Nullable
    private Map<String, SimpleObjectDto> propMapOfObject;

    /**
     * associative array with list of objects as value
     */
    @Nullable
    private Map<String, List<SimpleObjectDto>> propMapOfListOfObject;

}
