/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.in.rest


import org.springframework.web.util.UriComponentsBuilder

class EchoDateArrayGetSpec extends BaseSpecification {

    @Override
    String getOperationPath() {
        '/api/v1/echo-date-array'
    }

    def "should return query date list"() {
        given:
        def dateArray = ['2020-03-30', '2020-03-31']
        def uri = UriComponentsBuilder.fromUriString(getEndpoint())
                .queryParam("date_array", dateArray).build().toUri()
        when:
        def response = restTemplate.getForEntity(uri, List)

        then:
        response.statusCodeValue == 200
        response.body == dateArray
    }

    def "should fail query date list"() {
        given:
        def dateArray = ['2020-03-30', '2020-03-32']
        def uri = UriComponentsBuilder.fromUriString(getEndpoint())
                .queryParam("date_array", dateArray).build().toUri()
        when:
        def response = restTemplate.getForEntity(uri, Map)

        then:
        response.statusCodeValue == 400
        response.body.code == 400
        response.body.errors[0].field == 'date_array (QUERY parameter)[1]'
        response.body.errors[0].code == 'is_not_date'
    }
}
