/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.in.rest

import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import spock.lang.Unroll

class EchoPostSpec extends BaseSpecification {

    @Override
    String getOperationPath() {
        'api/v1/echo'
    }

    @Unroll
    def "should success (#params)"() {
        given:
        def request = makeRequest(params)

        when:
        def response = post(request, Map)

        then:
        response != null
        response.statusCode.value() == 200
        checkCorrelationIdHeader(response, request)

        when:
        if (!request.body['prop_enum_reusable']) {
            request.body['prop_enum_reusable'] = 'a'
        }
        request.body['prop_default'] = 'value'
        request.body['prop_date_time'] = toDateTime(request.body['prop_date_time'])
        response.body['prop_date_time'] = toDateTime(response.body['prop_date_time'])

        then:
        cleanMap(response.body) == cleanMap(request.body)

        where:
        params << [
                [:],
                ['header.timestamp': '2000-10-31T01:30:00.000-05:00'],
                ['header.timestamp': '2000-10-31T01:30:00.000Z'],
                ['header.Inline-Header-Param': null],
                [prop_int: null],
                [prop_int: -1],
                [prop_int_second: null],
                [prop_int_second: 2147483647],
                [prop_int_required: -1],
                [prop_int_required: 9223372036854775807],
                [prop_float: null],
                [prop_float: 0],
                [prop_float: 101.0000000],
                [prop_double: null],
                [prop_double: 0.01],
                [prop_double: 1],
                [prop_double: 9999.9999],
                [prop_amount: null],
                [prop_amount: '1.00'],
                [prop_amount: '200.00'],
                [prop_enum: '1', so_prop_string: '12'],
                [prop_string: null],
                [prop_date: null],
                [prop_date_second: null],
                [prop_date_time: null],
                [prop_date_time: '2019-09-28T22:22:00Z'],
                [prop_date_time: '2019-09-28T22:22:00.0Z'],
                [prop_date_time: '2019-09-28T22:22:00.00Z'],
                [prop_date_time: '2019-09-28T22:22:00.000Z'],
                [prop_date_time: '2019-09-28T22:22:00.000+01:00'],
                [prop_date_time: '2019-09-28T22:22:00.000-11:00'],
                [prop_base64: null],
                [prop_boolean: null],
                [prop_boolean: true],
                [prop_boolean: false],
                [prop_object: null],
                [prop_enum: null],
                [so_prop_date: null],
                [so_prop_date: '2020-02-29'],
                [so_prop_string: ''],
                [prop_enum_reusable: 'B', eo_enum_reusable: 'B'],
                [prop_enum_reusable: DO_NOT_SEND, eo_enum_reusable: 'a'],
                [prop_amount: '300.00', 'header.Authorization': 'Bearer 123'],
        ]
    }

    @Unroll
    def "should validation fail for #field #code (#params)"() {
        given:
        def request = makeRequest(params)

        when:
        def response = post(request, Map)

        then:
        response != null
        response.statusCode.value() == 400
        checkCorrelationIdHeader(response, request)
        response.body.code == 400
        response.body.errors.any {
            it.code == code && it.field == field && it.message == message
        }

        where:
        field                                               | code                       | params                                                      | message
        'X-Correlation-ID (HEADER parameter)'               | 'is_required'              | ['header.X-Correlation-ID': null]                           | 'X-Correlation-ID (HEADER parameter) is required'
        'timestamp (HEADER parameter)'                      | 'is_not_date_time'         | ['header.timestamp': '123123123']                           | 'timestamp (HEADER parameter) should be valid date time in ISO8601 format'
        'requestBody'                                       | 'is_required'              | [body: null]                                                | 'requestBody is required'
        'prop_int_required'                                 | 'is_required'              | [prop_int_required: null]                                   | 'prop_int_required is required'
        'prop_int_required'                                 | 'is_not_64bit_integer'     | [prop_int_required: 1.0]                                    | 'prop_int_required is not 64bit integer'
        'prop_int_required'                                 | 'is_not_64bit_integer'     | [prop_int_required: 'x']                                    | 'prop_int_required is not 64bit integer'
        'prop_int_required'                                 | 'is_too_small'             | [prop_int_required: -2]                                     | 'prop_int_required should be >= -1'
        'prop_int_required'                                 | 'is_not_64bit_integer'     | [prop_int_required: '9223372036854775808']                  | 'prop_int_required is not 64bit integer'
        'prop_int'                                          | 'is_not_32bit_integer'     | [prop_int: 1.0]                                             | 'prop_int is not 32bit integer'
        'prop_int'                                          | 'compare_prop_int_failed'  | [prop_int_second: 0]                                        | 'prop_int should be less than prop_int_second'
        'prop_int'                                          | 'is_not_32bit_integer'     | [prop_int: '2147483648']                                    | 'prop_int is not 32bit integer'
        'prop_float'                                        | 'is_too_big'               | [prop_float: 101.001]                                       | 'prop_float should be <= 101'
        'prop_float'                                        | 'is_not_float'             | [prop_float: 'x.x']                                         | 'prop_float is not float'
        'prop_double'                                       | 'is_too_small'             | [prop_double: 0.00999999]                                   | 'prop_double should be >= 0.01 and <= 9999.9999'
        'prop_double'                                       | 'is_too_big'               | [prop_double: 9999.9999001]                                 | 'prop_double should be >= 0.01 and <= 9999.9999'
        'prop_double'                                       | 'is_not_double'            | [prop_double: 'x.x']                                        | 'prop_double is not double'
        'prop_amount'                                       | 'is_too_small'             | [prop_amount: '0.99']                                       | 'prop_amount should be >= 1'
        'prop_amount'                                       | 'must_be_divisible_by_0.1' | [prop_amount: '1.01']                                       | 'prop_amount must be divisible by 0.1'
        'prop_amount'                                       | 'is_too_big'               | [prop_amount: '200.01']                                     | 'prop_amount should be <= 200'
        'prop_amount'                                       | 'is_not_match_pattern'     | [prop_amount: '1']                                          | 'prop_amount should match pattern: ^(0|([1-9][0-9]{0,}))\\.\\d{2}$'
        'prop_amount'                                       | 'is_not_match_pattern'     | [prop_amount: '1.0']                                        | 'prop_amount should match pattern: ^(0|([1-9][0-9]{0,}))\\.\\d{2}$'
        'prop_amount'                                       | 'is_not_match_pattern'     | [prop_amount: '1.001']                                      | 'prop_amount should match pattern: ^(0|([1-9][0-9]{0,}))\\.\\d{2}$'
        'prop_amount_number'                                | 'is_not_amount'            | [prop_amount_number: 0.1]                                   | 'prop_amount_number is not amount'
        'prop_amount_number'                                | 'is_not_amount'            | [prop_amount_number: 0.021]                                 | 'prop_amount_number is not amount'
        'prop_amount_number'                                | 'is_too_small'             | [prop_amount_number: 0.01]                                  | 'prop_amount_number should be >= 0.02 and <= 9999999999.99'
        'prop_amount_number'                                | 'is_too_big'               | [prop_amount_number: 19999999999.99]                        | 'prop_amount_number should be >= 0.02 and <= 9999999999.99'
        'prop_amount_number'                                | 'is_not_amount'            | [prop_amount_number: 'x.xx']                                | 'prop_amount_number is not amount'
        'prop_string'                                       | 'invalid_COLORS'           | [prop_string: 'INVALID']                                    | 'invalid_COLORS'
        'prop_string'                                       | 'is_too_short'             | [prop_string: '']                                           | 'prop_string has too short value, min allowed length is 5'
        'prop_string'                                       | 'is_too_short'             | [prop_string: 't']                                          | 'prop_string has too short value, min allowed length is 5'
        'prop_string'                                       | 'is_too_long'              | [prop_string: 't' * 11]                                     | 'prop_string has too long value, max allowed length is 10'
        'prop_date'                                         | 'is_not_date'              | [prop_date: '2019-08-32']                                   | 'prop_date is not yyyy-MM-dd'
        'prop_date'                                         | 'is_too_small'             | [prop_date_second: '2019-09-29']                            | 'prop_date should be greater equal than prop_date_second'
        'prop_date_time'                                    | 'is_not_date_time'         | [prop_date_time: '2019-08-30']                              | 'prop_date_time should be valid date time in ISO8601 format'
        'prop_date_time'                                    | 'is_not_date_time'         | [prop_date_time: '2019-08-30T22:22:00.000']                 | 'prop_date_time should be valid date time in ISO8601 format'
        'prop_date_time'                                    | 'is_not_date_time'         | [prop_date_time: '2019-08-32T22:22:00.000Z']                | 'prop_date_time should be valid date time in ISO8601 format'
        'prop_boolean'                                      | 'is_not_boolean'           | [prop_boolean: 'null']                                      | 'prop_boolean should have one of values: [true, false]'
        'prop_base64'                                       | 'is_not_base64'            | [prop_base64: '%']                                          | 'prop_base64 is not base64'
        'prop_enum'                                         | 'is_not_allowed_value'     | [prop_enum: 'null']                                         | 'prop_enum should have one of values: [A, b, 1]'
        'prop_object.so_prop_string'                        | 'is_too_long'              | [prop_enum: '1']                                            | 'prop_object.so_prop_string has too long value, max allowed length is 2'
        'prop_object.so_prop_string'                        | 'is_required'              | [so_prop_string: null]                                      | 'prop_object.so_prop_string is required'
        'prop_object.so_prop_date'                          | 'is_not_date'              | [so_prop_date: '919-01-01']                                 | 'prop_object.so_prop_date is not yyyy-MM-dd'
        'prop_object.so_prop_date'                          | 'is_not_date'              | [so_prop_date: '12019-02-29']                               | 'prop_object.so_prop_date is not yyyy-MM-dd'
        'prop_object.so_prop_date'                          | 'is_not_date'              | [so_prop_date: '2019-08-32']                                | 'prop_object.so_prop_date is not yyyy-MM-dd'
        'prop_object.so_prop_date'                          | 'is_not_date'              | [so_prop_date: '2019-13-01']                                | 'prop_object.so_prop_date is not yyyy-MM-dd'
        'prop_object.so_prop_date'                          | 'is_not_date'              | [so_prop_date: '2019-02-29']                                | 'prop_object.so_prop_date is not yyyy-MM-dd'
        'prop_object_extended.so_prop_string'               | 'is_required'              | ['eo.so_prop_string': null]                                 | 'prop_object_extended.so_prop_string is required'
        'prop_object_extended.so_prop_date'                 | 'is_not_date'              | ['eo.so_prop_date': '919-02-29']                            | 'prop_object_extended.so_prop_date is not yyyy-MM-dd'
        'prop_object_extended.eo_enum_reusable'             | 'is_not_allowed_value'     | [eo_enum_reusable: 'X#D']                                   | 'prop_object_extended.eo_enum_reusable should have one of values: [a, B, 3]'
        'prop_object_extended.eo_enum_reusable'             | 'custom_code'              | [eo_enum_reusable: 'B']                                     | 'prop_object_extended.eo_enum_reusable should be equal to prop_enum_reusable'
        'prop_object_extended.eo_enum_reusable'             | 'is_required'              | [eo_enum_reusable: null]                                    | 'prop_object_extended.eo_enum_reusable is required'
        'prop_object_extended.eo_enum_reusable'             | 'custom_code2'             | [prop_enum_reusable: '3', eo_enum_reusable: '3']            | "we don't want 3 in prop_object_extended.eo_enum_reusable"
        'prop_string_pattern'                               | 'custom_error_code'        | [prop_string_pattern: '1']                                  | 'prop_string_pattern should match pattern: ^[\\-\\".\',:;/\\\\!@#$%^&*()+_?|><=]{2,19}$'
        'prop_string_pattern'                               | 'custom_error_code'        | [prop_string_pattern: '1' * 20]                             | 'prop_string_pattern should match pattern: ^[\\-\\".\',:;/\\\\!@#$%^&*()+_?|><=]{2,19}$'
        'prop_object_extended.self_property.so_prop_string' | 'is_required'              | ['prop_object_extended.self_property.so_prop_string': null] | 'prop_object_extended.self_property.so_prop_string is required'
    }

    def "should validation fail for two fields at once"() {
        given:
        def request = makeRequest(['prop_int_required': null, 'eo.so_prop_string': null])
        when:
        def response = post(request, Map)

        then:
        response != null
        response.statusCode.value() == 400
        checkCorrelationIdHeader(response, request)
        response.body.code == 400

        when:
        def errors = response.body.errors

        then:
        errors.size() == 2
        errors[0].field == 'prop_int_required'
        errors[0].code == 'is_required'
        errors[1].field == 'prop_object_extended.so_prop_string'
        errors[1].code == 'is_required'
    }

    @Unroll
    def "validation FAILED for invalid authorization header (#param)"() {
        given:
        def request = makeRequest(['header.Authorization': param, 'eo.so_prop_string': null])
        when:
        def response = post(request, Map)

        then:
        response != null
        response.statusCode.value() == 403
        checkCorrelationIdHeader(response, request)
        response.body.code == 403

        when:
        def errors = response.body.errors

        then:
        errors.size() == 1
        errors[0].field == null
        errors[0].code == 'invalid_token'

        where:
        param << ['aaa', null]
    }

    static boolean equals(Map given, Map expected) {
        def notExpected = given.keySet() - expected.keySet()
        assert notExpected.empty
        def notPresent = expected.keySet() - given.keySet()
        assert notPresent.empty
        given.each { key, value ->
            assert expected[key] == value
        }
        true
    }

    def makeRequest(Map params) {
        def headers = new HttpHeaders()
        setHeader('Authorization', 'Bearer 123123123', headers, params)
        setHeader(HEADER_CORRELATION_ID, '1234567890', headers, params)
        setHeader('content-type', 'application/json', headers, params)
        setHeader('timestamp', null, headers, params)
        new HttpEntity<Map>(
                params.getOrDefault('body', cleanMap(makeBody(params), DO_NOT_SEND)) as Map,
                headers)
    }

    Map makeBody(Map params = [:]) {
        [prop_int            : params.getOrDefault('prop_int', 1),
         prop_int_second     : params.getOrDefault('prop_int_second', 2),
         prop_int_required   : params.getOrDefault('prop_int_required', 11),
         prop_float          : params.getOrDefault('prop_float', 100.001),
         prop_double         : params.getOrDefault('prop_double', 1000.0001),
         prop_amount         : params.getOrDefault('prop_amount', '10.10'),
         prop_amount_number  : params.getOrDefault('prop_amount_number', 1.01),
         prop_string         : params.getOrDefault('prop_string', 'WHITE'),
         prop_string_pattern : params.getOrDefault('prop_string_pattern', '/\\^%$#@"\'|)'),
         prop_date           : params.getOrDefault('prop_date', '2019-09-28'),
         prop_date_second    : params.getOrDefault('prop_date_second', '2019-09-28'),
         prop_date_time      : params.getOrDefault('prop_date_time', '2019-09-28T22:22:00.000Z'),
         prop_base64         : params.getOrDefault('prop_base64', 'dGVzdA=='),
         prop_boolean        : params.getOrDefault('prop_boolean', true),
         prop_enum           : params.getOrDefault('prop_enum', 'b'),
         prop_enum_reusable  : params.getOrDefault('prop_enum_reusable', 'a'),
         prop_object         : params.getOrDefault('prop_object', [
                 so_prop_string: params.getOrDefault('so_prop_string', 'inny tekst'),
                 so_prop_date  : params.getOrDefault('so_prop_date', '2019-09-30')
         ]),
         prop_object_extended: [
                 so_prop_string  : params.getOrDefault('eo.so_prop_string', 'inny tekst'),
                 so_prop_date    : params.getOrDefault('eo.so_prop_date', '2020-02-29'),
                 eo_enum_reusable: params.getOrDefault('eo_enum_reusable', 'a'),
                 self_property   : [
                         so_prop_string  : params.getOrDefault('prop_object_extended.self_property.so_prop_string', 'bla bla'),
                         so_prop_date    : null,
                         eo_enum_reusable: params.getOrDefault('eo_enum_reusable', 'a'),
                         self_property   : null
                 ]
         ]
        ]
    }
}
