plugins {
    kotlin("jvm") version "1.6.0"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("com.diffplug.spotless-changelog:spotless-changelog-lib:2.4.0")
    implementation("com.diffplug.spotless-changelog:spotless-changelog-plugin-gradle:2.4.0")
    implementation("org.osgi:org.osgi.framework:1.9.0")
    implementation("org.apache.commons:commons-lang3:3.12.0")
    implementation("com.lordcodes.turtle:turtle:0.6.0")
    implementation("com.google.code.gson:gson:2.9.0")
}
