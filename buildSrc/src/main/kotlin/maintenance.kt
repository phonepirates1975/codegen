/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.diffplug.spotless.changelog.NextVersionFunction
import com.diffplug.spotless.changelog.gradle.ChangelogExtension
import com.lordcodes.turtle.shellRun
import groovy.ant.FileNameFinder
import org.apache.commons.lang3.StringUtils.substringsBetween
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import com.google.gson.Gson
import  org.osgi.framework.Version.parseVersion
import java.io.File
import java.io.PrintWriter
import kotlin.io.path.writeLines

val EXT_2_CODE_FORMAT = mapOf(
    "gradle" to "groovy",
    "wsdl" to "xml"
)

const val QUICKSTART_PROJECTS = "quickstart-projects"

class CodegenVersionFunction : NextVersionFunction.Semver() {
    override fun nextVersion(unreleasedChanges: String?, lastVersion: String?): String {
        if (unreleasedChanges == null) {
            return "0.0.1"
        }
        val last = parseVersion(lastVersion)
        val breakingChanges = ifFoundBumpBreaking.any { unreleasedChanges.contains(it) }
        val addedFunctionality = ifFoundBumpAdded.any { unreleasedChanges.contains(it) }
        return if (breakingChanges) "${last.major + 1}.0.0"
        else if (addedFunctionality) "${last.major}.${last.minor + 1}.0"
        else "${last.major}.${last.minor}.${last.micro + 1}"
    }
}

abstract class MaintenanceTask : DefaultTask() {
    init {
        group = "maintenance"
    }

    protected fun file(path: String): File = project.file(path)
    protected fun copy(srcFile: String, destDir: String) {
        project.copy {
            it.from(srcFile)
            it.into(destDir)
        }
    }
}

abstract class SetupVersionTask : MaintenanceTask() {
    init {
        super.dependsOn("groovydoc", "dokkaJavadoc")
    }

    private val dokkaIndex: List<Map<String, String>> by lazy { loadDokkaIndex() }

    fun changelog(): ChangelogExtension = project.properties["spotlessChangelog"] as ChangelogExtension

    fun setProjectVersion(newVersion: String) {
        updateVersionInProject(newVersion)
        updateVersionInQuickstartProjects()
        updateWebsiteChangelog()
        updateWebsiteDocTemplates()
    }

    private fun updateVersionInProject(newVersion: String) {
        project.version = newVersion
        val propertiesFile = file("gradle.properties")
        propertiesFile.writeText(
            propertiesFile.readText().replace("version=.*".toRegex(), "version=${project.version}")
        )
    }

    private fun updateVersionInQuickstartProjects() {
        file(QUICKSTART_PROJECTS).walk()
            .forEach {
                when (it.name) {
                    "build.gradle" -> updateVersionInBuildGradle(it)
                    "pom.xml" -> updateVersionInPomXml(it)
                }
            }
    }

    private fun updateVersionInBuildGradle(file: File) {
        file.writeText(
            file.readText()
                .replace("'pl.metaprogramming:codegen:.*'".toRegex(), "'pl.metaprogramming:codegen:${project.version}'")
        )
    }

    private fun updateVersionInPomXml(file: File) {
        val newContent = mutableListOf<String>()
        file.readLines().forEach {
            val isCodegenVer =
                it.contains("<version>") && newContent.last().contains("<artifactId>codegen</artifactId>")
            newContent.add(
                if (isCodegenVer)
                    it.replace("<version>.*</version>".toRegex(), "<version>${project.version}</version>")
                else it
            )
        }
        file.toPath().writeLines(newContent)
    }

    private fun updateWebsiteChangelog() {
        val content = mutableListOf(
            "---",
            "id: changelog",
            "title: Changelog",
            "sidebar_label: Changelog",
            "---"
        )
        var releasedPartFlag = false
        file("CHANGELOG.md").readLines().forEach {
            if (!releasedPartFlag && it.startsWith("## ") && it != "## [Unreleased]")
                releasedPartFlag = true
            if (releasedPartFlag) content.add(it)
        }
        file("website/docs/changelog.md").toPath().writeLines(content)
    }

    protected fun updateWebsiteDocTemplates() {
        val rootDir = file("website/docs-templates")
        rootDir.walk().filter { it.isFile }.forEach { template ->
            val destFile = file("website/docs/${rootDir.toPath().relativize(template.toPath())}")
            destFile.parentFile.mkdirs()
            destFile.createNewFile()
            destFile.printWriter().use { writer ->
                template.readLines().forEach { line ->
                    val includeFile = line.between("<includeFile>", "</includeFile>")
                    if (includeFile.isNotEmpty()) {
                        handleIncludeFile(writer, includeFile[0])
                    } else {
                        writer.println(
                            line
                                .handleCodeLink(
                                    "javadoc", "build/docs/groovydoc/", "html",
                                    "https://javadoc.io/doc/pl.metaprogramming/codegen/latest"
                                ).handleCodeLink(
                                    "source", "src/main/groovy/", "groovy",
                                    "https://gitlab.com/metaprogramming/codegen/-/blob/develop/src/main/groovy"
                                )
                        )
                    }
                }
            }

        }
    }

    private fun handleIncludeFile(writer: PrintWriter, includeFile: String) {
        val file = file(includeFile.replace("\${codegen-quickstart-dir}", QUICKSTART_PROJECTS))
        writer.println("")
        writer.println("```${file.codeFormat()} title=\"${file.name}\"")
        writer.write(file.readText().trim())
        writer.println("")
        writer.println("```")
        writer.println("")
    }

    private fun File.codeFormat(): String {
        val ext = this.name.substringAfterLast('.')
        return EXT_2_CODE_FORMAT[ext] ?: ext
    }

    private fun String.handleCodeLink(
        linkType: String,
        srcPath: String,
        srcExt: String,
        urlPrefix: String
    ): String {
        var result = this
        this.between("<$linkType>", "</$linkType>").forEach {
            val parts = it.split("|")
            val clazz = parts[if (parts.size > 1) 1 else 0]
            val codePath = resolveCodePath(linkType, srcPath, srcExt, clazz)
            result = result.replace(
                "<$linkType>$it</$linkType>",
                "[${parts[0]}](${urlPrefix}/${codePath})"
            )
        }
        return result
    }

    private fun resolveCodePath(
        linkType: String,
        srcPath: String,
        srcExt: String,
        clazz: String
    ): String {
        val names = FileNameFinder().getFileNames(project.rootDir.path, "${srcPath}/**/${clazz}.${srcExt}")
        if (names.size == 1) {
            var fixedName = names[0].replace(File.separator, "/")
            return fixedName.substring(fixedName.indexOf(srcPath) + srcPath.length)
        } else if (linkType == "javadoc") {
            val matches = dokkaIndex.filter { it["l"] == clazz }
            if (matches.size == 1) {
                return matches[0]["url"]!!
            }
        }
        throw IllegalStateException("Can't find $linkType for: $clazz ($names)")
    }

    private fun String.between(open: String, close: String): Array<String> {
        return substringsBetween(this, open, close) ?: emptyArray()
    }

    private fun loadDokkaIndex(): List<Map<String, String>> {
        val indexFileBody = file("build/dokka/javadoc/type-search-index.js").readText(Charsets.UTF_8)
        val jsonString = indexFileBody.substring("var typeSearchIndex = ".length)
        return Gson().fromJson(jsonString, ArrayList<Map<String, String>>().javaClass)
    }

}

abstract class UpdateWebsite : SetupVersionTask() {
    @TaskAction
    fun action() {
        updateWebsiteDocTemplates()
    }
}

abstract class SetupNextVersion : SetupVersionTask() {
    @TaskAction
    fun action() {
        val ver = parseVersion(changelog().versionLast)
//        setProjectVersion("1.0.0-SNAPSHOT")
        setProjectVersion("${ver.major}.${ver.minor}.${ver.micro + 1}-SNAPSHOT")
    }
}

abstract class SetupReleaseVersion : SetupVersionTask() {
    init {
        super.dependsOn("changelogBump")
    }

    @TaskAction
    fun action() {
        setProjectVersion(changelog().versionNext)
    }
}

abstract class SetupGradleTestProjects : MaintenanceTask() {
    @TaskAction
    fun action() {
        listOf("test-projects", QUICKSTART_PROJECTS).flatMap { group ->
            file(group).listFiles { file ->
                file.isDirectory && File(file, "build.gradle").exists()
            }!!.map { "$group/${it.name}" }.toList()
        }.forEach {
            copy("gradle/wrapper", "$it/gradle/wrapper")
            copy("gradlew", it)
            copy("gradlew.bat", it)
            shellRun("git", listOf("add", "$it/gradlew"))
            shellRun("git", listOf("update-index", "--chmod=+x", "$it/gradlew"))
        }
    }
}

abstract class SetupApiInQuickstartProjects : MaintenanceTask() {
    @TaskAction
    fun action() {
        val projectTypes = listOf("maven", "gradle")
        val rsProjects = listOf("spring-rs-server", "spring-rs-client")
        val wsProjects = listOf("spring-soap-client")

        rsProjects.forEach { kind ->
            projectTypes.forEach { type ->
                copy(
                    "$QUICKSTART_PROJECTS/example-api.yaml",
                    "$QUICKSTART_PROJECTS/$kind-$type/generator/src/main/resources"
                )
            }
        }
        wsProjects.forEach { kind ->
            projectTypes.forEach { type ->
                copy(
                    "$QUICKSTART_PROJECTS/countries.wsdl",
                    "$QUICKSTART_PROJECTS/$kind-$type/generator/src/main/resources"
                )
            }
        }
    }
}