#!/bin/bash

sed -i "s|url: 'https://metaprogramming.gitlab.io/codegen/'|url: 'https://metaprogramming.gitlab.io/codegen/develop/'|g" docusaurus.config.js
sed -i "s|baseUrl: '/codegen/'|baseUrl: '/codegen/develop/'|g" docusaurus.config.js
