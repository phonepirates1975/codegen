---
id: class-type-class-index
title: Class type and class index
sidebar_label: Class type and class index
---

Class type and class index are essentials concepts which allow create simple but powerful code generators.

In general, the problem of code generation can be seen as the process of transforming the model into code implementing the solution that this model describes.

Both the code and the model consist of logical parts. The REST API model, consists of operations and the schemes of input and output messages.

## Class type   

The code is grouped into classes. For REST API, these will be classes that will represent controllers or input / output messages. The class type is for example abstraction of controller or DTO. Class type refers to design patterns. It represents a specific element from a given design pattern.

## Class index

All generated code are register in class index with the class type and the model (often model part) as a key.
This allows for a loose coupling between the class generators, while the classes themselves are related.

Generator for one class type which by design uses other class type does not need to know anything about the naming conventions - package and class name - for those class type. It just get the proper class from class index.