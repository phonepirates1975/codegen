---
id: introduction
title: Introduction
sidebar_label: Introduction
slug: /
---

## Goal

Codegen is a tool that aim to support software development with according to the API-first approach or (in future) Model-driven development.

It should be able to generate code that will comply with the coding standards of any team.

This means that modifying any aspect of the generated code should be easy.

## Main features

Codegen is not just a code generator, is actually a framework for creating your own code generators.

You can use any built-in generator or create a new one if none meets your needs.

Codegen generators do not need to be template-based. Codegen provide mechanisms that facilitate the creation of generators that will be model-based (first code model is created and saved to a text representation at the end).

This approach allows for better decomposition of generator codes, making them more compatible with DRY and open-closed principles. In other words, it allows you to easily customize existing generator to suit your needs.

If you want to know more details to be able to create your own generator then you should read the [core design](guides/core-design.md) article.

If you want to learn about using Codegen in general (and ready-to-use generators) read the [how to use it](getting-started/how-to-use.md) article.

## Ready-made generators

Currently, the following code generators are included in the Codegen distribution:
* [REST server generator](getting-started/quick-start-spring-rest-server.md)
* [REST client generator](getting-started/quick-start-spring-rest-client.md)
* [SOAP client generator](getting-started/quick-start-spring-soap-client.md)

All of them generates codes for Spring Framework.

## Something missing?

If you find issues with the documentation or have suggestions on how to improve the documentation or the project in general, please file an [issue](https://gitlab.com/metaprogramming/codegen/-/issues) or send a message via [Discord](https://discord.gg/tRW5JGk).