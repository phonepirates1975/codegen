---
id: core-design
title: Core design
sidebar_label: Core design
---

![Docusaurus](/img/core-design.png)

## Codegen entrypoint

The entry point for starting code generation is a <javadoc>Codegen</javadoc> object.
Using this object, you will add functional modules for which codes will be generated.
In addition, it is used to set parameters such as the target directory, character set (UTF-8 by default) and line separator (system line separator by default).

Its typical use is as follows:
```java
new Codegen()
        .setBaseDir(baseDir)
        .setIndexFile(indexFile)
        .addCommonModule(commonModuleGenerator)
        .addModule(moduleGenerator)
        .generate();
```

Common module provides utilities codes used by generated codes for other modules.
Contrary to normal (functional) modules, it does not have a model (API).

## Module generator
As you can guess from the code above. The hardest part will be creating a <javadoc>ModuleGenerator</javadoc> object.

The responsibility of the <javadoc>ModuleGenerator</javadoc> object is to traverse the model (API) and add tasks to generate a certain type of code for its particular elements.

### Type of code
The type of code is a kind of abstraction for the class. For example, for the REST service, the REST controller is the type of code. The types of codes will be correspond to the particular elements of the solution architecture.

To get a list of code types supported by ready-made generators, just look for static public fields with the prefix `TOC_` in the classes of these generators.

### Ready-made generators
There are available following ready-made generators:
- <javadoc>SpringRestServiceGenerator</javadoc> for generating Spring REST services,
- <javadoc>SpringRestClientGenerator</javadoc> for generating Spring REST client,
- <javadoc>SpringSoapClientGenerator</javadoc> for generating Spring WS client,
- <javadoc>SpringRestService2tGenerator</javadoc> for generating Spring REST services (two tier variant),
- <javadoc>SpringRestClient2tGenerator</javadoc> for generating Spring REST client (two tier variant),
- <javadoc>SpringCommonsGenerator</javadoc> for generating utils classes used by above generators.

Even if you plan to create your own generator, a good start would be to review the codes for any of the generators above.

To make instance of these generators you should use static method 'of'. In general, you should pass arguments:
- model (<javadoc>RestApi</javadoc>/<javadoc>WsdlApi</javadoc>)
- [codegen params](#codegen-params)
- generator configurator (actually consumer of configurator object)

Each configurator inherit from the <javadoc>JavaModuleConfigurator</javadoc> class, and they have, among other things:
- a 'typeOfCode' method (formerly 'update') to (re)configure code generation for particular type of code - see more details [here](#class-builder),
- a 'setRootPackage' method to set the base package,
- a 'setPackage' method to set the package for particular type of code,
- a 'setProjectDir' method to set the project directory (it is possible to set different projects for each type of code).

For example, the code for the REST server module generator looks like this:
```java
SpringRestServiceGenerator.of(OpenapiParser.parse("example-api.yaml"), params, cfg -> {
    cfg.setProjectDir("example-app-rs");
    cfg.setRootPackage("com.example.app");
});
```

In [how to use it](getting-started/how-to-use.md) article you will learn how to use the above generators in sample projects.

## Class builder

The responsibility of the <javadoc>ClassBuilder</javadoc> object is to generate a code of a certain type for particular model (API) element. This will be e.g. generating REST controller classes.

It is possible to use one of two approaches to code generation:
1. Template-based using [SimpleTemplateEngine](https://docs.groovy-lang.org/latest/html/api/groovy/text/SimpleTemplateEngine.html) (see <source>ClassGspBuilder</source>).
2. Based on building a code model using <javadoc>ClassCmBuilder</javadoc>, from which the code is finally generated.

While the first approach is simpler, the second is much more flexible and allows you to:
- reusing code generation logic in generating different types of code,
- dividing the code generation logic into independent parts,
- making nontrivial modifications to an existing generator without modifying the existing code generation logic and easily turning them on and off as needed.

### Class builder configurator

Use <javadoc>ClassBuilderConfigurator</javadoc> to configure a <javadoc>ClassBuilder</javadoc> object (<javadoc>ClassCmBuilder</javadoc> as well as <javadoc>ClassGspBuilder</javadoc>).
In order to modify the generation of the code of a given type in the existing generator, the "typeOfCode" method should be called on the [module configurator](#modulebuilderconfigurator) object. It might look like this:

```java
instanceOfSpringRestInAdapterConfigurator
    .typeOfCode(REST_CONTROLLER)
        .addStrategy(new ReactiveWrapperBuildStrategy()))
```

Using the <javadoc>ClassBuilderConfigurator</javadoc> object, you can:
- add, remove or replace a generation strategy (when the code model building approach is used),
- set the naming for the generated classes,
- set the location for the generated classes (package and project),
- set the template that will be used for code generation (when the template approach is used)

## Building Code Model

The code model is represented by the <javadoc>ClassCm</javadoc> class. Building a code model is about creating an object of this type.

The <javadoc>ClassCmBuildStrategy</javadoc> class should be used to build the code model. You can create your own class that inherits from it, and then apply it using the <javadoc>ClassBuilderConfigurator</javadoc>.addStrategy method.
Depending on the needs, only the following methods should be overwritten (the default implementations are empty): 'makeDeclaration', 'makeImplementation', 'makeDecoration'. These three methods correspond to the code generation phases.

To inject the code model building logic you can also use the 'onDeclaration', 'onImplementation', 'onDecoration' methods from the <javadoc>ClassBuilderConfigurator</javadoc> class. These methods take 'Consumer&lt;<javadoc>ClassCmBuildStrategy</javadoc>&gt;' as an argument and also correspond to the code generation phases. This way allows to use the lambda expressions to apply the logic that creates the code model.

In general <javadoc>ClassCmBuildStrategy</javadoc> objects should be stateless.
However, if you need it to be stateful, then you should override the implementation of the 'getInstance' method.
State of this object is only needed when the logic performed in different phases is closely related to each other and using the code model build state (<javadoc>ClassCm</javadoc>) is too inconvenient.
This is the case of mappers for which method signatures should be created in the declaration phase, and the method implementation in the implementation phase.

### Building methods and mappers

The method code model is represented by the <javadoc>MethodCm</javadoc> class.
It describes the input parameters, return type, visibility modifiers, and annotations. However, as for the method body, it is only represented by a string.

To add a method to a class, use the overloaded <javadoc>ClassCmBuildStrategy</javadoc>.addMethod method. It takes any number of <javadoc>MethodCm</javadoc> or <javadoc>MethodCmBuilder</javadoc> objects as a parameter. If the code generating the method body is a bit more complicated, encapsulate it in a class that inherits from <javadoc>BaseMethodCmBuilder</javadoc> (which provide many helper methods). To create implementation body you can use a codeBuf property (available in both <javadoc>BaseMethodCmBuilder</javadoc> and <javadoc>ClassCmBuildStrategy</javadoc>).

If you want the method to be found by the code register, you should use 'addMapper' instead of the 'addMethod' method.

### Code registry

The code registry provides the following functionalities:
- searching for a code (class) of a given type that supports a specific fragment of the model
- mapper (method) search by the type it returns and the types of its parameters
- not generating unnecessary codes

Consequently, the generator codes are:
- loosely coupled (less than the codes they generate!)
- more concise and readable
- oriented towards the abstraction of the model and type of code
- do not need to duplicate target package naming / class / method names logic

Moreover, in some problematic cases, instead of generating the wrong code, the generation will end with an error.

#### Showcase

To illustrate how the code register can be used, I will use the following generator requirements.
We need to generate REST server codes to have for each operation:
- REST controller class (REST_CONTROLLER)
- class for the request (REQUEST_DTO), which will contain all (indicated in API) parameters of the operation (body, query parameters, header, paths, cookies)
- the class for the response (RESPONSE_DTO)
- a class (SERVICE) with a method for the actual implementation of the operation handling, which will have a class object for the request (REQUEST_DTO) as the parameter and will return a class object (RESPONSE_DTO) for the response.

Assuming that all types of codes (REST_CONTROLLER, REQUEST_DTO, RESPONSE_DTO, SERVICE) are oriented to the same fragment of the model (REST operation), the generator configuration could look like the following pseudocode:

```java title="RestServerConfigurator.java"
...
typeOfCode(SERVICE).onDeclaration(b ->
                b.addMapper("execute", m -> {
                    m.setResultType(b.getClass(RESPONSE_DTO));
                    m.addParam("request", b.getClass(REQUEST_DTO));
                    m.setImplBody("// TODO implement me");
                }));
...
typeOfCode(REST_CONTROLLER).onImplementation(b -> b.addMethod(new BaseMethodCmBuilder<Operation>() {
        public MethodCm makeDeclaration() {
            // code that will create the signature of the method (including annotations for the method and its parameters)
            ...
        }
        public String makeImplBody() {
            // the code that will create the method's implementation body
            ...
            String callServiceExp = mapping().to(RESPONSE_DTO).from(REQUEST_DTO, "request").make();
            ...
            return codeBuf.take();
        }
}));
...
```

##### Search for classes

To search for classes (of a given code type for a given fragment of the model) use the 'getClass' method (available in both <javadoc>BaseMethodCmBuilder</javadoc> and <javadoc>ClassCmBuildStrategy</javadoc>).
The method is overloaded and can take two parameters:
- type of code
- model fragment

More advanced class search options are available via the 'classLocator' method.

##### Search for mappers

To search for a mapper you should use the 'mapping' method (available in both <javadoc>BaseMethodCmBuilder</javadoc> and <javadoc>ClassCmBuildStrategy</javadoc>). It returns an object on which the 'from' and 'to' methods should be called, to define what parameters the mapper should take and what type of value it should return.

Basically, the type of object (<javadoc>ClassCd</javadoc>) should be passed to the 'to' method. However, when it comes to the type of code (class) that refers to the same fragment of the model for which the code is generated, it is enough to provide only that type of code. The expression 'to(RESPONSE_DTO)' is equivalent to the expression 'to(getClass(RESPONSE_DTO))'.

In the case of the 'from' method, in addition to the type of the object, the name of the variable that will be passed to the mapper should be provide. This is because finally (by calling the 'make' or 'toString' method) the expression for calling the mapper is generated (with the injection of the object providing this mapper).


### Code model generation phases

We distinguish the following phases of building a code model:
- registration: when ModuleBuilder register for a given fragment of the model that the given type of code will be built
- declaration: in this phase the class elements that are used in building other classes must be declared (e.g. registering a mapper provided by the class)
- implementation: building the code model of the class
- decoration: decorate the class model (e.g. adding an annotation)

## Codegen params

<javadoc>CodegenParams</javadoc> allows you to configure selected aspects that affect the generation of various types of codes.
It is a composition of different sets of parameters for different parts of the generator.
This makes it possible to create one parameter set and use it for all modules (use it as a constructor parameter of the module configurator).
There is also an easy way to add your own parameters.

In the case of ready-made generators for this purpose, you will use the following classes:
- <javadoc>JavaModuleParams</javadoc> to parameterize:
  - the annotation used to mark the generated codes (@Generated),
  - aspects related to bean management and dependency injection,
  - the name mapping from the model to the java code,
  - the text processing of generated java codes (license header, code formatting),
  - classes to represent basic data (numbers, dates...),
  - the generation of the 'fromValue' method for enums (regardless of its use in the generated codes)
- <javadoc>ValidationParams</javadoc> to parameterize:
  - the error codes for standard validations,
  - the stopping of (or continuing) validation when a validation error is encountered,
  - validators for data formats,
- <javadoc>SpringRestParams</javadoc> to parameterize the generation of REST server/client codes
- <javadoc>SpringSoapParams</javadoc> to parameterize the generation of WS/SOAP client codes

Creating a <javadoc>CodegenParams</javadoc> object might look like this:
```java
new CodegenParams()
        .with(new JavaModuleParams()
                .setGeneratedAnnotationValue("my-generator"))
        .with(new ValidationParams()
                .setStopAfterFirstError(true))
        .with(new SpringRestParams()
                .setControllerPerOperation(true)
                .setPayloadField("payload")
                .injectBeanIntoRequest('session', 'example.SessionContext'))
        .with(new SpringSoapParams()
                .setNamespacePackage("http://domain.com/somename", "com.domain.somename"));
```

To add your own parameters you should create a class to wrap them and then pass an instance of that class using the 'with' method.
Then, to get the parameter value, you should use the 'getParams' method that is present in <javadoc>JavaModuleGenerator</javadoc> and <javadoc>ClassCmBuildStrategy</javadoc> objects.

### Modifying codegen params

However, sometimes it may be necessary to generate a certain module with slightly different parameters.
In this case, you can clone the <javadoc>CodegenParams</javadoc> object using the 'clone' method and make the required change on the new object.

In order to modify the parameter object, the 'update' method may be helpful.