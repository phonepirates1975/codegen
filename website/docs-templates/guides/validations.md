---
id: validations
title: Validation support
sidebar_label: Validation support
---

API request validation is one of the core parts of an API implementation. Therefore, Codegen provides support in this area as follows:
- generated codes take into account the constraints described in the OpenAPI specification,
- adds an extension to the OpenAPI specification that allows to include non-standard constraints in the API description (for which appropriate codes are also generated).


## Support for OAS constraints

The following constraints that are part of the OpenAPI specification are supported:
- maximum
- minimum
- maxLength
- minLength
- pattern
- maxItems
- minItems
- uniqueItems
- required
- enum

This means that if any of the above constraints are used in the API, an appropriate data validation code will be generated.
These codes, when a constraint is not met, will generate an error that clearly indicates which field does not meet which constraint.

### Error code

If the default error codes for unmet constraints are inappropriate for you, you can change them using <javadoc>ValidationParams</javadoc> via the [CodegenParams](core-design#codegen-params).

```java
new CodegenParams().with(new ValidationParams()
        .setIsRequiredErrorCode("validation.error.required"))
```

However, keep in mind that the given error code will apply to all fields that do not meet the given constraint.

### Format constraints

According to the [OpenAPI specification](https://swagger.io/specification/#data-types), primitives can have a format modifier. The format modifier is open and you can set any value in it.

The openness of the format modifier and the fact that it essentially describes data constraints makes it a very good point for injecting custom validations. You can do this with <javadoc>ValidationParams</javadoc>.

For example, for the following API specification:
```yaml
SomeObject:
  properties:
    prop_amount:
      type: number
      format: amount
```

You can set the validator for the format 'amount' as follows:
```java
new CodegenParams().with(new ValidationParams()
        .setFormat("amount", CheckerRef.of('example.commons.validator.AmountChecker')))
```

Where the CheckerRef object points to a validator implementation. The pointer might be a class that implements the 'Chercker<?>' interface, and since it's a functional interface, the pointer might be a reference to a method too. For simple validations, it can also be a public static field with a reference to the validator implementation object.

The following static methods are available for creating a CheckerRef object:
- of(className)
- ofMethodRef(className, method)
- ofStaticField(className, field)

To learn how to create a validator implementation, [go here](#creating-validator-implementation).

## OAS extension (x-constraints)

In order to be able to describe any data constraints, an [extension to the OpenAPI specification](https://swagger.io/specification/#specification-extensions) has been added.

Custom constraints are described using the 'x-constraints' element that contains a list of objects, with each object describing a separate constraint. The property 'x-constrains' can be included in an object property specification or directly in an object specification.

The API specification might look like this:
```yaml
RecurringTransfer:
  properties:
    date_from:
      type: string
      format: date
    date_to:
      type: string
      format: date
    amount:
      type: number
      format: amount
      x-constraints:
      - rule: USER_ACCOUNT_BALANCE
        description: Some description about this constraint
        priority: 102
      - rule: USER_DAILY_LIMIT
        error-code: date_to-must-be-greater-than-date_from
        priority: 101
    period:
      type: string
      x-constraints:
      - dictionary: PERIODS_FOR_RECURRING_TRANSFERS
  x-constraints:
  - condition: date_from < date_to
    error-code: date_to-must-be-greater-than-date_from

```

The following types of constraints are supported:
- rule
- dictionary
- condition

The 'rule' constraint is most universal, but others ('dictionary' and 'condition') improve information transfer (about input data requirements) and, more importantly, allow the more appropriate codes to be generated.


### Rule constraint

The 'rule' constraint is a universal construct that allows you to declare any validation rule.

The validation rule implementation is injected by that rule name. For the above API specification example, to handle the 'USER_ACCOUNT_BALANCE' rule you should add the following class:

```java
@Component
@Qualifier("USER_ACCOUNT_BALANCE")
public class UserAccountBalanceValidator implements Checker<BigDecimal> {
    public void check(ValidationContext<BigDecimal> ctx) {
        ...
    }
}
```

If you have configured dependency injection via constructor (the default), you should also add the following [lombok configuration](https://stackoverflow.com/questions/38549657/is-it-possible-to-add-qualifiers-in-requiredargsconstructoronconstructor/50287955#50287955):
``` title="lombok.config"
lombok.copyableAnnotations += org.springframework.beans.factory.annotation.Qualifier
```
It should be enough to put it in the lombok.config file in the main project directory.

Another way to bind the rule with the implementation is to use the 'ValidationParams.setRule' method, where the implementation is pointed by the 'CheckerRef' object - more on that [here](#format-constraints).
To learn how to create a validator implementation, [go here](#creating-validator-implementation).


### Dictionary constraint

The 'dictionary' constraint is similar to the enum construct, except that the allowed values are dynamic, are not encoded in the API specification, but are stored in some data source that can be changed while the application is running.

All 'dictionary' constraints, even if they refer to different dictionaries, are validated by one class (ClassType.VALIDATION_DICTIONARY_CHECKER). The class is generated but with empty implementation. Depending on how the different dictionaries are stored, it may not require additional work to handle the restriction for another dictionary.

To configure a package for dictionary validator class (and enumeration for dictionary and error codes), do so as follows:
```java
new CommonsModuleConfigurator()
    .setPackage("example.adapters.in.rest.validators.custom",
        VALIDATION_DICTIONARY_CHECKER,
        VALIDATION_DICTIONARY_CODES_ENUM,
        VALIDATION_ERROR_CODES_ENUM)
```



### Condition constraint

The 'condition' constraint should take a comparator expression that conforms to the syntax:
```
<OBJECT_PROPERTY_1> <COMPARE_OPERATOR> <OBJECT_PROPERTY_2>
```
Where <COMPARE_OPERATOR> can take the following values: '=', '>', '> =', '<', "<= '.

This constraint can only be used at the object schema specification level and allows you to specify the expected relationship between two properties of a given object.

For the 'condition' constraint, all code needed for message validation is generated. And as long as the field data types (referred by this constraint) implement the 'Comparable' interface, they don't need any coding.


### Error code

For a custom constraint, it is also possible to specify an error code when the constraint is not satisfied.

This should be done using the 'error-code' property.

This is not necessary, but may be helpful when this information is used by the consumer of the service.

The error code set in the API specification overwrites the error code that was used in the validator implementation.


### Error code for pattern constraint

The pattern constraint coming from the OpenAPI specification can be very useful and comprehensive. However, it is not possible to produce a human-readable error message from it.

In order to be able to set up a dedicated error message for the pattern constraint to do so, it is enough to use code similar to:
```yaml
...
  properties:
    prop_amount:
      type: string
      pattern: ^(0|([1-9][0-9]{0,}))\.\d{2}$
      x-constraints:
        - invalid-pattern-code: invalid_amount
```


### Validation order (priority)

The constraint checks are performed in a specific order. In most cases, the default order will be OK. In other case you can change the order in which it is checked using the 'priority' property (only for custom constraints). In order to properly set the priority of the constraint check, you should know that the constraints check is performed in the following order:
- custom constraints with priority <= 0,
- required,
- uniqueItems,
- minItems, maxItems,
- enum,
- format,
- pattern,
- minLength, maxLength,
- minimum, maximum
- custom constraints with priority > 0

If the priority of a constraint is not specified explicitly, it is set automatically based on the order in which it occurs in the API specification (within the context). The first constraint is given priority 100, the second constraint 101, and so on.

You should know that the validation priority only applies to the given context. For example, setting a priority of '-1' for a field constraint in an object only means that the given constraint will be checked before other constraints for that field, but does not cause the field to be validated before other fields in that object.


### Constraint description

Additional textual description can be added to the specification of a custom constraint using the 'description' property. It is not taken into account at all for code generation, but it can be helpful in explaining what the constraint is.


### Injecting constraints into API model

If for some reason you cannot put custom constraints in the API specification, you can add them in the generator configuration.

To add a constraint for the data schema, use the `add` method on the <javadoc>DataSchema</javadoc> object. This method takes the `DataConstraint` object as an argument. You can create such an object using the static methods `rule`, `dictionary` or `condition` from the `Constraints` class. You can also use the `CheckerRef` object with which you can indicate an implementation that will verify a given constraint.

The code that injects custom constraints into the API model might look like this:
```java
restApi.getSchema("RecurringTransfer").add(CheckerRef.of("com.example.RecurringTransferCustomValidation"));
restApi.getSchema("RecurringTransfer.period").add(Constraints.dictionary("PERIODS_FOR_RECURRING_TRANSFERS"));
```

To get the <javadoc>DataSchema</javadoc> object, you can use the `getSchema` method from the <javadoc>RestApi</javadoc> object. The `getSchema` method takes a schema name (from the API specification) as an argument. In the same way, you can get a <javadoc>DataSchema</javadoc> object representing a property of an object schema. Then the property name must be appended to the schema name after the period. Then, as an argument, you must pass a value according to the following template `<OBJECT_SCHEMA_NAME>.<PROPERTY_NAME>`.

## Creating validator implementation

To implement the validator, you need to implement the `Checker<T>` interface. This is a functional interface, and there is only one `void check(ValidationContext<T> context)` method to implement.

A simple validator might look like this:
```java
public class AmountValidator implements Checker<BigDecimal> {
    @Override
    public void check(ValidationContext<BigDecimal> ctx) {
        if (ctx.getValue().scale() > 2 && ctx.getValue().compareTo(BigDecimal.ZERO) < 0) {
            ctx.addError("invalid_amount");
        }
    }
}
```

The above validation checks that the number meets the general conditions of the monetary amount - it is a positive number and has no more than two decimal places. Despite its simplicity, you may also find out that:
- a validation error should be reported using the 'ValidationContext.addError(...)' method,
- the value to be checked should be retrieved using the 'ValidationContext.getValue()' method.

You may also notice that the above code is not null protected from the 'ValidationContext.getValue()' method. Most often, there is no need to handle null values because by default validations do not run on null values. If a given validator should be executed even for null values, the 'Checker.checkNull' method should be overridden (then it should return true). This is what was done in the example below:

```java
public class AuthorizationChecker implements Checker<String> {

    @Override
    public void check(ValidationContext<String> ctx) {
        if (ctx.getValue() == null || !ctx.getValue().startsWith("Bearer ")) {
            ctx.addError(ValidationError.builder()
                    .code("invalid_token")
                    .status(403)
                    .stopValidation(true)
                    .build());
        }
    }

    @Override
    public boolean checkNull() {
        return true;
    }
}
```

The above code also shows how in a validation error you can pass information that will be used to further handle the error - creating a response for the consumer of the service.
In this case, the HTTP response status is set to 403 (the default is 400), and further validation is forced to stop.


## Creating responses for validation errors

After validation, if there are any errors, 'ValidationException' is thrown. In order to return the appropriate message to the service consumer, it should be handled by the 'ExceptionHandler'. Using the <javadoc>SpringRestServiceGenerator</javadoc> generator, the 'RestExceptionHandler' class (SpringCommonsGenerator.TOC_REST_EXCEPTION_HANDLER) is also generated. It does not have a full implantation, and will certainly require manual modification.

It is also possible to set whether the validation should stop when the first error is encountered or continue. This can be done using the 'ValidationParams.setRule' method.

```java
new CodegenParams().with(new ValidationParams()
        .setStopAfterFirstError(true));
```
