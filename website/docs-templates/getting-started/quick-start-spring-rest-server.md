---
id: quick-start-spring-rest-server
title: Generate REST services to use with Spring
sidebar_label: Spring REST services
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

This guide walks you through the process of creating with codegen a REST service with Spring.

Two variants of the REST service generator are available:
- <javadoc>SpringRestServiceGenerator</javadoc>
- <javadoc>SpringRestService2tGenerator</javadoc> (two tier variant)

In most cases, the first option will be the most appropriate.

## Project structure

Our project will consist of two subprojects:
- `app`: contains Spring Boot application
- `generator`: contains API description and generator configuration. It is used to generate codes for the `app` subproject

It will look like this:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```sh
project-root
├── app
│   ├── src
│   │   └── main
│   │       ├── java
│   │       │   └── example
│   │       │       ├── adapters <GENERATED>
│   │       │       ├── commons <GENERATED>
│   │       │       ├── ports <GENERATED>
│   │       │       ├── process
│   │       │       │   └── EchoCommand.java <GENERATED STUB>
│   │       │       └── Application.java
│   │       └── resources
│   │           └── application.properties
│   └── pom.xml
├── generator
│   ├── src
│   │   └── main
│   │       ├── java
│   │       │   └── GeneratorRunner.java <GENERATOR CONFIGURATION>
│   │       └── resources
│   │           ├── example-api.yaml <API DESCRIPTION>
│   │           └── GeneratedFiles.yaml <GENERATED FILE INDEX>
│   └── pom.xml
└── pom.xml
.
```

</TabItem>
<TabItem value="groovy">

```sh
project-root
├── app
│   ├── src
│   │   └── main
│   │       ├── java
│   │       │   └── example
│   │       │       ├── adapters <GENERATED>
│   │       │       ├── commons <GENERATED>
│   │       │       ├── ports <GENERATED>
│   │       │       ├── process
│   │       │       │   └── EchoCommand.java <GENERATED STUB>
│   │       │       └── Application.java
│   │       └── resources
│   │           └── application.properties
│   └── build.gradle
├── generator
│   ├── src
│   │   └── main
│   │       ├── groovy
│   │       │   └── GeneratorRunner.groovy <GENERATOR CONFIGURATION>
│   │       └── resources
│   │           ├── example-api.yaml <API DESCRIPTION>
│   │           └── GeneratedFiles.yaml <GENERATED FILE INDEX>
│   └── build.gradle
├── build.gradle
└── settings.gradle
.
```

</TabItem>
</Tabs>


## Root project

First create a project directory and put `pom.xml` (if you prefer maven) or `build.gradle` and `settings.gradle` (if you prefer gradle):

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">
  <includeFile>${codegen-quickstart-dir}/spring-rs-server-maven/pom.xml</includeFile>
</TabItem>
<TabItem value="groovy">
  <includeFile>${codegen-quickstart-dir}/spring-rs-server-gradle/build.gradle</includeFile>
  <includeFile>${codegen-quickstart-dir}/spring-rs-server-gradle/settings.gradle</includeFile>
</TabItem>
</Tabs>


## Generator subproject

Then create the `generator` subproject with the following `pom.xml` / `build.gradle`:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">
  <includeFile>${codegen-quickstart-dir}/spring-rs-server-maven/generator/pom.xml</includeFile>
</TabItem>
<TabItem value="groovy">
  <includeFile>${codegen-quickstart-dir}/spring-rs-server-gradle/generator/build.gradle</includeFile>
</TabItem>
</Tabs>

### API description

Let's assume that we have the following API to implement:
<includeFile>${codegen-quickstart-dir}/spring-rs-server-maven/generator/src/main/resources/example-api.yaml</includeFile>

:::note
OAS2 (swagger 2) and OAS3 (OpenAPI 3) are supported.
:::

Place the API definition in the `src/main/resources/example-api.yaml` file.

### Generator configuration

To configure code generation you need something like this:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Java', value: 'java'},
    {label: 'Groovy', value: 'groovy'},
  ]
}>
<TabItem value="java">
  <includeFile>${codegen-quickstart-dir}/spring-rs-server-maven/generator/src/main/java/GeneratorRunner.java</includeFile>
</TabItem>
<TabItem value="groovy">
  <includeFile>${codegen-quickstart-dir}/spring-rs-server-gradle/generator/src/main/groovy/GeneratorRunner.groovy</includeFile>
</TabItem>
</Tabs>

Place above code into `src/main/java/GeneratorRunner.java` (if you prefer java) or into `src/main/groovy/GeneratorRunner.groovy` (if you prefer groovy).

The code performs:
- imports REST API from `example-api.yaml` OpenAPI specification file (see [guide](guides/oas-support.md) to get more about OAS import)
- configures `example-api` module: sets the project directory to `app` and root package to `example` for generated codes
- generates codes for modules
  - `commons` (API independent utility codes)
  - `example-api` (REST API adapter for example-api.yaml - actually adapter, port and implementation stub)

:::note
Codegen uses `modules` to distinguish different group of codes.

Codes of one module can be placed into different packages and even different projects - it makes sense, since is generated adapter, port, and implementation stub. By default, the codes are grouped in packages: adapters, ports and application.

Codes from many modules can also be placed into one package/project - maybe it makes sense sometimes, you can do it anyway.
:::


### Triggering code generation

To generate codes run following task:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```sh
mvn -f generator/pom.xml compile exec:java
```

</TabItem>
<TabItem value="groovy">

```sh
./gradlew :generator:generate
```

</TabItem>
</Tabs>


## App subproject

Most of the codes for this subproject was generated in the previous step, however there are a few things to do.

Create subproject definition with following `pom.xml` or `build.gradle`:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">
  <includeFile>${codegen-quickstart-dir}/spring-rs-server-maven/app/pom.xml</includeFile>
</TabItem>
<TabItem value="groovy">
  <includeFile>${codegen-quickstart-dir}/spring-rs-server-gradle/app/build.gradle</includeFile>
</TabItem>
</Tabs>


### Spring Boot application

To declare Spring Boot application create a class `example.Application` with the following content:
<includeFile>${codegen-quickstart-dir}/spring-rs-server-maven/app/src/main/java/example/Application.java</includeFile>

### Logging REST traffic

To enable logging for REST traffic, create a file `src/main/resources/application.properties` with the following content:
<includeFile>${codegen-quickstart-dir}/spring-rs-server-maven/app/src/main/resources/application.properties</includeFile>

### Service implementation

To implement service, fill the `execute` method body in the generated class `example.process.EchoCommand`:

```java
@Component
public class EchoCommand implements IEchoCommand {
    @Override
    public ResponseEntity<EchoBodyDto> execute(@Nonnull EchoRequest request) {
        return ResponseEntity.ok(request.getRequestBody());
    }
}
```

With the default settings, for each operation defined in the OpenAPI specification, a class is generated in the `process` package, which is responsible for the implementation of this operation. Depending on the HTTP method of the operation, the class will have a `Query` or `Command` postfix.

If the API specifies that the operation returns HTTP headers, the execute method should return a `ResponseEntity<T>` object, otherwise it should return a `T`.

The generated class is configured as a spring component with prototype scope, which allows for more object-oriented handling of requests. However, if this is undesirable, when implementing a class, you can simply remove the `@Scope` annotation (along with the `@Generated` annotation).

By setting the `SpringRestParams.delegateToFacade` parameter, you can configure the generator to create a facade that will contain methods implementing operations from a given group.

### RestExceptionHandler

Since the response message structure for bad requests can be freely defined in the API, it is necessary to define a mapping of validation errors to this message structure. To do this you should fix implementation of `ResponseEntity<?> makeResponse(ValidationResult result)` method in the generated class `example.commons.RestExceptionHandler`.

```java
private ResponseEntity<?> makeResponse(ValidationResult result) {
    ValidationError firstError = result.getErrors().get(0);
    return ResponseEntity
            .status(result.getStatus())
            .body(new ErrorDescriptionDto()
                    .setCode(firstError.getCode())
                    .setField(firstError.getField())
                    .setMessage(String.format("%s: %s", firstError.getField(), firstError.getCode())));
}
```

:::note
After changing generated class you should remove `@Generated` annotation.
:::

:::danger
In principle, you should avoid modifying classes other than implementation stubs, otherwise the codes will be more difficult to develop and maintain.
:::


## Run and test

To start the services, execute the command:


<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```sh
mvn -f app/pom.xml spring-boot:run
```

</TabItem>
<TabItem value="groovy">

```sh
./gradlew :app:bootRun
```

</TabItem>
</Tabs>


Then you can test them with the command:

```sh
curl --request POST \
  -H "Content-Type: application/json" -H "X-Correlation-ID: 123" \
  --data '{"prop_int":2,"prop_float":1.1,"prop_double":10.01,"prop_amount":"11.11"}' \
  http://localhost:8080/api/v1/echo
```


## Full project sources

You can download the full project source codes from the GitLab repository:
- [spring boot REST server - gradle](https://gitlab.com/metaprogramming/codegen/-/tree/master/quickstart-projects/spring-rs-server-gradle)
- [spring boot REST server - maven](https://gitlab.com/metaprogramming/codegen/-/tree/master/quickstart-projects/spring-rs-server-maven)
