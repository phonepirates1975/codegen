---
id: file-index
title: Generated files index
sidebar_label: File index
---

Generated files index is need as consequence to keep generated files in VCS.

If after the next code generation some files are no longer present then they should be deleted. The only way to designate files that should be deleted is to have a list of previously generated files.

In addition, the file index can be used to detect manual modification of generated codes.
If this happens, the code generation will be interrupted with information about what is wrong. 
To enable this functionality on the Codegen object, execute setForceMode(false).

```
Codegen.setForceMode(true);
```

However, it is possible to modify generated files. All generated java classes are mark by `javax.annotation.Generated` / `javax.annotation.processing.Generated` annotation. To edit the generated file you should delete this annotation.
