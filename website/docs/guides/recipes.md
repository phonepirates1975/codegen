---
id: recipes
title: Codegen recipes
sidebar_label: Recipes
---

## Setting the target directory

First of all, you can set the base directory, which means the location of the project directory or the parent project directory if you decide to split the codes into multiple projects. For this you use the 'setBaseDir' method on the [Codegen](https://javadoc.io/doc/pl.metaprogramming/codegen/latest/pl/metaprogramming/codegen/Codegen.html) object.

Then using the [ModuleBuilderConfigurator](core-design#module-builder-configurator) object for different [types of codes](core-design#type-of-code) from different modules, you can set the following parameters:
- project directory (setProjectDir) - specifies the subproject directory in relation to the base directory; the default value is empty - no code division into subprojects
- subdirectory with sources in the project (setProjectSubDir) - the default value is 'src/main/java'
- package (setRootPackage and setPackage) - 'setRootPackage' should be used before 'setPackage', because it sets the package for all [types of codes](core-design#type-of-code) according to a certain scheme encoded in the [module configurator](core-design#module-builder-configurator)

```java
new Codegen()
        .setBaseDir(new File("/home/user/projects/my-project"))
        .addCommonModule(new CommonsModuleConfigurator(getCodegenParams())
                .setProjectDir("utils-project")
                .setProjectSubDir('src/main/java-gen', VALIDATION_COMMON_CHECKERS)
                .setRootPackage("commons")
                .setProjectDir("app-project", VALIDATION_RESULT_MAPPER)
                .setPackage("app.mappers", VALIDATION_RESULT_MAPPER);
```

## Setting generation of a specific type of code

In general, to modify the generation of a given [type of code](core-design#type-of-code), add or remove [ClassCmBuildStrategy](https://javadoc.io/doc/pl.metaprogramming/codegen/latest/pl/metaprogramming/codegen/java/base/ClassCmBuildStrategy.html) object using [ClassBuilderConfigurator](core-design#class-builder-configurator) and [ModuleBuilderConfigurator](core-design#module-builder-configurator).

### Naming of the generated classes
```java
moduleBuilderConfigurator.typeOfCode(TYPE_OF_CODE).setNameSuffix("Suffix");
moduleBuilderConfigurator.typeOfCode(TYPE_OF_CODE).setNamePrefixAndSuffix("Prefix", "Suffix");
moduleBuilderConfigurator.typeOfCode(TYPE_OF_CODE).setClassNameBuilder(classNameBuilder);
```

### Mapping model names to java code
```java
new CodegenParams().with(new JavaModuleParams().setNameMapper(...);
```

### Remove generation strategy
```java
moduleBuilderConfigurator.typeOfCode(TYPE_OF_CODE).removeStrategy(buildStrategyClassOrExactObject);
```

### Add generation strategy
```java
moduleBuilderConfigurator.typeOfCode(TYPE_OF_CODE).addStrategy(buildStrategyObject);
```

### Replace generation strategy
```java
moduleBuilderConfigurator.typeOfCode(TYPE_OF_CODE).replaceStrategy(buildStrategyClassOrExactObject, newBuildStrategyObject);
```

### Ready-made building strategies

Create a BuildStrategy that adds an implemented interface to generated classes:
```java
ImplementsBuildStrategy.interfaces("java.io.Serializable");
```

Create a BuildStrategy that adds an annotation to generated classes:
```java
AnnotatedBuildStrategy.by('javax.enterprise.context.ApplicationScoped');
```

## Setting generation of module codes

In general, the [CodegenParams](core-design#codegen-params) object can be used to modify the generation of different types of codes.

### Change data types
```java
new CodegenParams().with(new JavaModuleParams().setDataTypeMapping(DataType.DOUBLE, 'java.math.BigDecimal'));
```

### Adding a license header
```java
new CodegenParams().with(new JavaModuleParams().setLicenceHeader(LICENCE_TEXT));
```

### Formatting the generated codes with other libraries
To use a code formatter (eg eclipse jdt) you should do it via the [CodeDecorator](https://javadoc.io/doc/pl.metaprogramming/codegen/latest/pl/metaprogramming/codegen/generator/CodeDecorator.html) interface.
```java
new CodegenParams().with(
        new JavaModuleParams().addCodeDecorator(new CodeDecorator() {
            String apply(String s) {
                return Roaster.format(s);
            }
        }));
```

### Skipping file generation for the module
```java
new CodegenParams().setSkipGeneration(true);
```

### Setting generation strategy for different types of code
```java
moduleBuilderConfigurator.updateConfigs(cfg -> cfg.addStrategy(buildStrategyObject), TYPE_OF_CODE1, TYPE_OF_CODE2...);
```
Using the 'updateConfigs' method you can add/remove/replace specific generation strategy for many types of code at once. If you do not specify any type of code, the change will be applied to all types of codes.


### Force generation of the 'fromValue' method for enums
```java
new CodegenParams().with(new JavaModuleParams().setAlwaysGenerateEnumFromValueMethod(true));
```
