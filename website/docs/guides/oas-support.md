---
id: oas-support
title: OpenAPI Specification support
sidebar_label: OpenAPI support
---

## Import OpenAPI specification

To load OpenAPI specification (OAS2 - swagger/OAS3 - OpenAPI 3) use the static 'of' method from the [RestApi](https://javadoc.io/doc/pl.metaprogramming/codegen/latest/pl/metaprogramming/model/oas/RestApi.html) class. This method takes the contents of the OAS specification as an argument, and optionally, dependencies (varargs).


### OpenapiParser

The [RestApi](https://javadoc.io/doc/pl.metaprogramming/codegen/latest/pl/metaprogramming/model/oas/RestApi.html).of method underneath uses the [OpenapiParser](https://javadoc.io/doc/pl.metaprogramming/codegen/latest/pl/metaprogramming/model/oas/OpenapiParser.html) class, which has more powerful features.
Use static 'parser' methods from this class as needed.
The method is overloaded and has the following versions:
- parse(filepath)
- parse(filepath, charset)
- parse(filepath, config)
- parse(filepath, charset, config)
- parse(openApiContent, dependencies)

Where the config parameter is an instance of the [OpenapiParserConfig](https://javadoc.io/doc/pl.metaprogramming/codegen/latest/pl/metaprogramming/model/oas/OpenapiParserConfig.html) class that allows you to set the following parameters:
- fixedResource: allows you to set a resource (the resource has a significant impact on the generated codes) for all endpoints specified in the OAS file
- resourceResolver: allows you to set different resources for different endpoints; it is a map where the resource is the key and the value is a function that checks if the given endpoint path should belong to this resource
- verbose: enables verbose logging
- weekValidation
- partialFile: flag indicating that a given file is not a full OpenAPI specification (contains only a data schema definition)
- dependsOn: modules on which a given specification depends (partial specifications containing data schema definition)
- additionalParameters: allows you to attach certain parameters (not listed in the OAS) to all endpoints


## On-the-fly editing of the OpenAPI model

Before you use the [RestApi](https://javadoc.io/doc/pl.metaprogramming/codegen/latest/pl/metaprogramming/model/oas/RestApi.html) model to generate the codes, you can modify it. This, of course, only makes sense for minor technical modifications.

### Skip code generation for selected operations
```java
api.removeOperations(op -> op.getCode().equals("operationId"));
```

### Grouping of operations
Ready-made generators require REST operations to be grouped. This can be done in the OpenAPI specification using "tags" or "x-swagger-router-controller". If it is not done in the specification, it can be done after import as follows:
```java
api.forEachOperation(op -> op.setGroup("group"));
```

### Addition of a technical parameter for all operations
```java
Parameter parameter = Parameter.of(...);
api.addParameter(parameter).forEachOperation(op -> op.getParameters().add(parameter));
```


## Export as OpenAPI specification

To save [RestApi](https://javadoc.io/doc/pl.metaprogramming/codegen/latest/pl/metaprogramming/model/oas/RestApi.html) model as OAS3 specification use 'write' method from the [OpenapiWriter](https://javadoc.io/doc/pl.metaprogramming/codegen/latest/pl/metaprogramming/model/oas/OpenapiWriter.html) class.
There are two variants of this method, one allows you to save the split API into multiple files, the other allows you to save a single flattened file.
