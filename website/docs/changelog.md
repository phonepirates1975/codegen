---
id: changelog
title: Changelog
sidebar_label: Changelog
---
## [1.1.0] - 2022-08-22
### Added
- OpenAPI specification loading by content ([#112](https://gitlab.com/metaprogramming/codegen/-/issues/112))
- Better error handling in the REST client - SpringRestClientGenerator ([#108](https://gitlab.com/metaprogramming/codegen/-/issues/108))
### Fixed
- OpenAPI parser error for PUT/PATCH methods when content type other than xml/json ([#110](https://gitlab.com/metaprogramming/codegen/-/issues/110))
- OpenAPI parser error when operationId contains a minus sign ([#111](https://gitlab.com/metaprogramming/codegen/-/issues/111))
- Generating classes for error response when defined as array - SpringRestServiceGenerator ([#109](https://gitlab.com/metaprogramming/codegen/-/issues/109))

## [1.0.0] - 2022-06-29
### Changed
- **BREAKING** Refactor module generators API ([#107](https://gitlab.com/metaprogramming/codegen/-/issues/107)) - see [migration guide](https://metaprogramming.gitlab.io/codegen/docs/migration-guide#breaking-changes-in-100) for more details.

## [0.10.0] - 2022-05-05
### Changed
- **BREAKING** Refactor API of ClassCd, FieldCm, MethodCm, AnnotationCm classes ([#92](https://gitlab.com/metaprogramming/codegen/-/issues/92)) - see [migration guide](https://metaprogramming.gitlab.io/codegen/docs/migration-guide#breaking-changes-in-0100) for more details.

## [0.9.3] - 2022-04-01
### Changed
- Stop generating ObjectFactory classes (spring soap client) ([#104](https://gitlab.com/metaprogramming/codegen/-/issues/104))
### Added
- Possibility to force generation of the 'fromValue' method for enums ([#105](https://gitlab.com/metaprogramming/codegen/-/issues/105))
### Fixed
- Changing the case of a file name for a GIT repository on Windows ([#106](https://gitlab.com/metaprogramming/codegen/-/issues/106))

## [0.9.2] - 2022-03-26
### Added
- Support for schema "complexContent/extension" (soap) ([#102](https://gitlab.com/metaprogramming/codegen/-/issues/102))
- Don't generate unnecessary code (spring soap client) ([#103](https://gitlab.com/metaprogramming/codegen/-/issues/103))

## [0.9.1] - 2022-03-16
### Added
- OpenAPI extension to describe constraints ([#47](https://gitlab.com/metaprogramming/codegen/-/issues/47)) - [x-constraints](https://metaprogramming.gitlab.io/codegen/docs/guides/validations)
- Support for upload files with content type other than multipart/form-data ([#40](https://gitlab.com/metaprogramming/codegen/-/issues/40))
- Parameterizing the mapping of model names to java ([#100](https://gitlab.com/metaprogramming/codegen/-/issues/100))
### Fixed
- The RestApi.getOperation method ([#96](https://gitlab.com/metaprogramming/codegen/-/issues/96))
- Deleting file when the new file name only differs by case ([#98](https://gitlab.com/metaprogramming/codegen/-/issues/98))
- The generated class and method names follow Java conventions regardless of model names ([#99](https://gitlab.com/metaprogramming/codegen/-/issues/99))
- The generated enums names follow Java conventions regardless of model names ([#100](https://gitlab.com/metaprogramming/codegen/-/issues/100))

## [0.9.0] - 2022-01-13
### Changed
- **BREAKING** Refactoring the way of creating method codes ([#71](https://gitlab.com/metaprogramming/codegen/-/issues/71)) - see [migration guide](https://metaprogramming.gitlab.io/codegen/docs/migration-guide#breaking-changes-in-090) for more details.
### Added
- Support for OAS data type: type=string, format=byte ([#93](https://gitlab.com/metaprogramming/codegen/-/issues/93))
### Fixed
- Support for OAS object data type without defined properties ([#94](https://gitlab.com/metaprogramming/codegen/-/issues/94))
- The notAllowed validator should not report an error for an empty array ([#95](https://gitlab.com/metaprogramming/codegen/-/issues/95))

## [0.8.3] - 2021-12-09
### Added
- Generating validation for the uniqueItems constraint ([#90](https://gitlab.com/metaprogramming/codegen/-/issues/90))
### Fixed
- Exporting array field constraints to OAS ([#89](https://gitlab.com/metaprogramming/codegen/-/issues/89))
- The minItems constraint should not make the array field required ([#88](https://gitlab.com/metaprogramming/codegen/-/issues/88))
- Problems in CommonCheckers identified by sonarqube ([#91](https://gitlab.com/metaprogramming/codegen/-/issues/91));
- Use required() and notAllowed() methods instead of REQUIRED and NOT_ALLOWED fields.

## [0.8.2] - 2021-11-25
### Fixed
- Generate error when validation is declared twice for the same dictionary ([#85](https://gitlab.com/metaprogramming/codegen/-/issues/85))
- (SpringRestServices generator) Wrong annotations for request executors classes ([#86](https://gitlab.com/metaprogramming/codegen/-/issues/86))
- (SpringRestClients generator) Invalid REST client codes are generated when the operation has path and header parameters ([#87](https://gitlab.com/metaprogramming/codegen/-/issues/87))

## [0.8.1] - 2021-11-17
### Added
- Support for validation by dictionaries ([#48](https://gitlab.com/metaprogramming/codegen/-/issues/48))

## [0.8.0] - 2021-11-13
### Changed
- **BREAKING** Configuring class name for generating codes should be done via setFixedName, setNameSuffix, setNamePrefixAndSuffix methods on ClassBuilderConfigurator ([#83](https://gitlab.com/metaprogramming/codegen/-/issues/83))
- **BREAKING** (SpringRestServices generator) Generating *Command/*Query classes instead of *Facade (Use SpringRestParams.delegateToFacade to stay with *Facade) ([#83](https://gitlab.com/metaprogramming/codegen/-/issues/83))
### Added
- Ability to set class name for generating codes using model object (ClassBuilderConfigurator.setNameByModel method) ([#83](https://gitlab.com/metaprogramming/codegen/-/issues/83))
- (SpringRestServices and SpringRestClients generators) DTOs fields are marked with @Nullable/@Nonnull annotations ([#82](https://gitlab.com/metaprogramming/codegen/-/issues/82))
- (SpringRestServices generators) Ability to easily inject beans to request objects - SpringRestParams.injectBeanIntoRequest ([#84](https://gitlab.com/metaprogramming/codegen/-/issues/84))

## [0.7.1] - 2021-11-01
### Added
- New Spring Rest Clients code generator ([#81](https://gitlab.com/metaprogramming/codegen/-/issues/81))

## [0.7.0] - 2021-10-18
### Changed
- **BREAKING** Setting up validation for the data format by ValidationParams ([#67](https://gitlab.com/metaprogramming/codegen/-/issues/67))
- **BREAKING** The schema method in the RestApi class has been renamed to updateSchema ([#67](https://gitlab.com/metaprogramming/codegen/-/issues/67))
### Added
- Ability to stop validation when a given check is not met ([#80](https://gitlab.com/metaprogramming/codegen/-/issues/80))
- Stop validation for a field if there is already a validation error for that field ([#80](https://gitlab.com/metaprogramming/codegen/-/issues/80))
- Introducing the priority for the performed validations ([#80](https://gitlab.com/metaprogramming/codegen/-/issues/80))
- New Spring Rest Services code generator ([#67](https://gitlab.com/metaprogramming/codegen/-/issues/67))

## [0.6.6] - 2021-09-25
### Added
- Ability to change data types in generated codes ([#73](https://gitlab.com/metaprogramming/codegen/-/issues/73))
- Improvements to the generated enum codes ([#79](https://gitlab.com/metaprogramming/codegen/-/issues/79))
### Fixed
- Do not copy method annotations to the extracted interface ([#78](https://gitlab.com/metaprogramming/codegen/-/issues/78))
- Method javadoc comment moved from implementation to interface ([#78](https://gitlab.com/metaprogramming/codegen/-/issues/78))
- Added an Override annotation to the implementing method ([#78](https://gitlab.com/metaprogramming/codegen/-/issues/78))

## [0.6.5] - 2021-09-07
### Added
- New fields in the ValidationResult class to create better validation messages ([#76](https://gitlab.com/metaprogramming/codegen/-/issues/76))
- Ability to easily set custom error code for pattern constraint ([#77](https://gitlab.com/metaprogramming/codegen/-/issues/77))
- Ability to continue validation when a validation error occurs ([#49](https://gitlab.com/metaprogramming/codegen/-/issues/49))
- A ValidationParams class to parameterize the generated validation codes

## [0.6.4] - 2021-08-31
### Added
- A new setPackages method to SpringRestOutAdapterConfigurator to easily configure class packages to meet te ports&adapters approach standards ([#75](https://gitlab.com/metaprogramming/codegen/-/issues/75))
### Fixed
- Formatting the generated code using an external library ([#70](https://gitlab.com/metaprogramming/codegen/-/issues/70))
- Validators being generated to include a default value when no value was set in the field ([#72](https://gitlab.com/metaprogramming/codegen/-/issues/72))
- Ability to set a null value for a field with a default value set ([#72](https://gitlab.com/metaprogramming/codegen/-/issues/72))
- Update of the index of the generated files ([#74](https://gitlab.com/metaprogramming/codegen/-/issues/74))

## [0.6.3] - 2021-08-23
### Changed
- The md5 tag is no longer stored in the index of generated files if 'forceMode' is enabled ([#68](https://gitlab.com/metaprogramming/codegen/-/issues/68))
### Added
- Ability to export split API to different files in OAS format ([#69](https://gitlab.com/metaprogramming/codegen/-/issues/69))
- The codegen 'addLastUpdateTag' parameter - in order not to save the 'lastUpdate' tag in the index of generated files ([#68](https://gitlab.com/metaprogramming/codegen/-/issues/68))

## [0.6.2] - 2021-08-05
### Added
- Possibility not to generate files for a module ([#14](https://gitlab.com/metaprogramming/codegen/-/issues/14))
- Formatting the generated code using an external library ([#39](https://gitlab.com/metaprogramming/codegen/-/issues/39))

## [0.6.1] - 2021-07-19
### Added
- Ability to add a header with a license to the generated code files ([#56](https://gitlab.com/metaprogramming/codegen/-/issues/56))
- Ability to set DumperOptions (snakeyaml) parameters when saving OAS yaml files ([#66](https://gitlab.com/metaprogramming/codegen/-/issues/66))
### Fixed
- In some cases the path in the index of generated files started with the '/' character
- The default line separator for saving OAS files depends on the running system ([#66](https://gitlab.com/metaprogramming/codegen/-/issues/66))

## [0.6.0] - 2021-07-13
### Changed
- **BREAKING** The structure of packages inside 'pl.metaprogramming.model' has changed - class names remain unchanged (e.g. the package for the OpenapiParser class has been changed pl.metaprogramming.model.parser.openapi -> pl.metaprogramming.model.oas)
- **BREAKING** Replace the xDescriptions, attributes, description fields in model classes (Operation, DataSchema / Parameter, ObjectType) with the new additives field
### Added
- Ability to set code generation to a directory other than 'src/main/java' ([#52](https://gitlab.com/metaprogramming/codegen/-/issues/52))
- Saving the RestApi model in OpenAPI format - use OpenapiImporter class ([#64](https://gitlab.com/metaprogramming/codegen/-/issues/64))
### Fixed
- OpenAPI parsing error when the object property type is same as object ([#65](https://gitlab.com/metaprogramming/codegen/-/issues/65))

## [0.5.0] - 2021-07-01
### Changed
- **BREAKING** ClassBuilderConfig has been renamed to ClassBuilderConfigurator
- **BREAKING** ClassAnnotationBuildStrategy has been renamed to AnnotatedBuildStrategy and its package has been changed
- **BREAKING** MapperBuilder has been renamed to MethodCmBuilder and its package has been changed
- **BREAKING** The generated BaseDataMapper class has been replaced by the SerializationUtils class
- **BREAKING** Generated DTO classes no longer implements 'java.io.Serializable' interface
- **BREAKING** The resourceResolvers, fixedResource, additionalParameters fields have been removed from OpenapiParserConfig (modify RestApi object to achieve desired effect)
- **BREAKING** A more aggressive policy of not generating unused codes has been used - if that causes a problem use the generateAlways method on the module configurator object  
### Added
- Support to skip code generation for selected REST operations - use the removeOperations method on the RestApi object ([#63](https://gitlab.com/metaprogramming/codegen/-/issues/63))

## [0.4.2] - 2021-06-12
### Added
- API improvements (fluent API support in java)
- Add the OpenapiParserConfig.fixedResource field to set a resource for all operations from OAS ([#60](https://gitlab.com/metaprogramming/codegen/-/issues/60))
### Fixed
- Code generation fails when no response content is specified in OAS3 ([#61](https://gitlab.com/metaprogramming/codegen/-/issues/61))

## [0.4.1] - 2021-05-03
### Added
- Support for java versions newer than 8 ([#38](https://gitlab.com/metaprogramming/codegen/-/issues/38))
- Option to ease remove generated codes ([#55](https://gitlab.com/metaprogramming/codegen/-/issues/55))
### Fixed
- Move quickstart projects to main code repository, add tests for them, and automate version update  ([#57](https://gitlab.com/metaprogramming/codegen/-/issues/57))

## [0.4.0] - 2021-02-12
### Changed
- Do not generate unused code ([#13](https://gitlab.com/metaprogramming/codegen/-/issues/13))
- WSDL parser configuration ([#36](https://gitlab.com/metaprogramming/codegen/-/issues/36))
- Module Builder should be produced by Module Builder Configurator ([#37](https://gitlab.com/metaprogramming/codegen/-/issues/37))
- Improvements to the generated REST response classes ([#22](https://gitlab.com/metaprogramming/codegen/-/issues/22))
### Added
- Support for OpenAPI 3 ([#6](https://gitlab.com/metaprogramming/codegen/-/issues/6))

## [0.3.1] - 2020-12-18
### Fixed
- Fix the data mapping contained in the map structures ([#30](https://gitlab.com/metaprogramming/codegen/-/issues/30))
- Code generation fails when REST endpoint has no parameters ([#35](https://gitlab.com/metaprogramming/codegen/-/issues/35))

## [0.3.0] - 2020-09-23
### Changed
- Default values for forceMode and addLastGenerationTag settings should be changed ([#32](https://gitlab.com/metaprogramming/codegen/-/issues/32))
- Improve ClassCmBuildStrategy API ([#33](https://gitlab.com/metaprogramming/codegen/-/issues/33))
### Added
- REST client generator for Spring Framework ([#29](https://gitlab.com/metaprogramming/codegen/-/issues/29))
### Fixed
- Invalid ISO8601 data-time validation ([#31](https://gitlab.com/metaprogramming/codegen/-/issues/31))

## [0.2.19] - 2020-07-23
### Added
- [project site](https://metaprogramming.gitlab.io/codegen/) ([#25](https://gitlab.com/metaprogramming/codegen/-/issues/25))
- integration tests ([#9](https://gitlab.com/metaprogramming/codegen/-/issues/9))
- Make possible use fluent code style in java when using CodeGenerationConfigurator class ([#23](https://gitlab.com/metaprogramming/codegen/-/issues/23)) 
### Fixed
- README ([#24](https://gitlab.com/metaprogramming/codegen/-/issues/24))
- Default line separator should match the system settings ([#26](https://gitlab.com/metaprogramming/codegen/-/issues/26))
- WSDL generated code causes runtime errors if element name starts with upperCase ([#27](https://gitlab.com/metaprogramming/codegen/-/issues/27))
- Enum generated from WSDL sometimes has unnecessary values ([#28](https://gitlab.com/metaprogramming/codegen/-/issues/28))

