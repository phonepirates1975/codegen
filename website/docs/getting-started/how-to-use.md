---
id: how-to-use
title: How to use Codegen
sidebar_label: How to use it
---

Codegen is available from the [Central Maven Repository](https://mvnrepository.com/artifact/pl.metaprogramming/codegen).

So it's relatively easy to use in a maven or gradle project, even though there is currently no plugin available for these building tools.

I encourage to create a subproject that will contain the model and code that will run the code generation. This approach allows unlimited generator configuration.

## Quick Start Projects

To get a better understanding of how to use Codegen, you can go through the following quickstart project guides:

- [Generate REST service to use with Spring](quick-start-spring-rest-server.md)
- [Generate REST client to use with Spring](quick-start-spring-rest-client.md)
- [Generate SOAP client to use with Spring](quick-start-spring-soap-client.md)

See also the [core design](guides/core-design.md) article that provides an explanation of the basic elements of the Codegen project.

## Using a development version

To use a currently development version, follow these steps:

1. Checkout git repository:
```
git clone https://gitlab.com/metaprogramming/codegen.git
```

2. Build and publish to maven local repository
```
cd codegen
gradlew publishToMavenLocal
```

To check the version number you just added to your local maven repository, take a look at the gradle.properties file (codegenVersion property).

3. Now in your project you can use unreleased version.

If you use gradle, you should also setup maven local repository to use it:
```
repositories {
    mavenLocal()
    mavenCentral()
}
```
