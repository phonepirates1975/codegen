---
id: quick-start-spring-rest-server
title: Generate REST services to use with Spring
sidebar_label: Spring REST services
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

This guide walks you through the process of creating with codegen a REST service with Spring.

Two variants of the REST service generator are available:
- [SpringRestServiceGenerator](https://javadoc.io/doc/pl.metaprogramming/codegen/latest/pl/metaprogramming/codegen/java/spring/SpringRestServiceGenerator.html)
- [SpringRestService2tGenerator](https://javadoc.io/doc/pl.metaprogramming/codegen/latest/pl/metaprogramming/codegen/java/spring/SpringRestService2tGenerator.html) (two tier variant)

In most cases, the first option will be the most appropriate.

## Project structure

Our project will consist of two subprojects:
- `app`: contains Spring Boot application
- `generator`: contains API description and generator configuration. It is used to generate codes for the `app` subproject

It will look like this:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```sh
project-root
├── app
│   ├── src
│   │   └── main
│   │       ├── java
│   │       │   └── example
│   │       │       ├── adapters <GENERATED>
│   │       │       ├── commons <GENERATED>
│   │       │       ├── ports <GENERATED>
│   │       │       ├── process
│   │       │       │   └── EchoCommand.java <GENERATED STUB>
│   │       │       └── Application.java
│   │       └── resources
│   │           └── application.properties
│   └── pom.xml
├── generator
│   ├── src
│   │   └── main
│   │       ├── java
│   │       │   └── GeneratorRunner.java <GENERATOR CONFIGURATION>
│   │       └── resources
│   │           ├── example-api.yaml <API DESCRIPTION>
│   │           └── GeneratedFiles.yaml <GENERATED FILE INDEX>
│   └── pom.xml
└── pom.xml
.
```

</TabItem>
<TabItem value="groovy">

```sh
project-root
├── app
│   ├── src
│   │   └── main
│   │       ├── java
│   │       │   └── example
│   │       │       ├── adapters <GENERATED>
│   │       │       ├── commons <GENERATED>
│   │       │       ├── ports <GENERATED>
│   │       │       ├── process
│   │       │       │   └── EchoCommand.java <GENERATED STUB>
│   │       │       └── Application.java
│   │       └── resources
│   │           └── application.properties
│   └── build.gradle
├── generator
│   ├── src
│   │   └── main
│   │       ├── groovy
│   │       │   └── GeneratorRunner.groovy <GENERATOR CONFIGURATION>
│   │       └── resources
│   │           ├── example-api.yaml <API DESCRIPTION>
│   │           └── GeneratedFiles.yaml <GENERATED FILE INDEX>
│   └── build.gradle
├── build.gradle
└── settings.gradle
.
```

</TabItem>
</Tabs>


## Root project

First create a project directory and put `pom.xml` (if you prefer maven) or `build.gradle` and `settings.gradle` (if you prefer gradle):

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```xml title="pom.xml"
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
                             http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>pl.metaprogramming.codegen-examples.spring-rs-server-maven</groupId>
    <artifactId>spring-rs-server-maven</artifactId>
    <packaging>pom</packaging>
    <version>0.0.1-SNAPSHOT</version>

    <modules>
        <module>app</module>
        <module>generator</module>
    </modules>

    <properties>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
    </properties>
</project>
```

</TabItem>
<TabItem value="groovy">

```groovy title="build.gradle"
group = 'pl.metaprogramming.codegen-examples.spring-rs-server-gradle'
version = '0.0.1-SNAPSHOT'
```


```groovy title="settings.gradle"
rootProject.name = 'spring-rs-server-gradle'

include 'app'
include 'generator'
```

</TabItem>
</Tabs>


## Generator subproject

Then create the `generator` subproject with the following `pom.xml` / `build.gradle`:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```xml title="pom.xml"
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
                             http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>pl.metaprogramming.codegen-examples.spring-rs-server-maven</groupId>
        <artifactId>spring-rs-server-maven</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </parent>
    <artifactId>generator</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>pl.metaprogramming</groupId>
            <artifactId>codegen</artifactId>
            <version>1.1.1-SNAPSHOT</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>exec-maven-plugin</artifactId>
                <version>3.0.0</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>java</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <mainClass>GeneratorRunner</mainClass>
                    <arguments>
                        <argument>${project.basedir}</argument>
                    </arguments>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
```

</TabItem>
<TabItem value="groovy">

```groovy title="build.gradle"
plugins {
    id 'groovy'
}

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    implementation 'pl.metaprogramming:codegen:1.1.1-SNAPSHOT'
}

task generate(type: JavaExec) {
    group = 'codegen'
    classpath = sourceSets.main.runtimeClasspath
    main = 'GeneratorRunner'
    args rootProject.rootDir.path,
            new File(project.projectDir.path, 'src/main/resources/GeneratedFiles.yaml').path
}
```

</TabItem>
</Tabs>

### API description

Let's assume that we have the following API to implement:

```yaml title="example-api.yaml"
openapi: 3.0.1
info:
  title: Example API
  version: "1.0"
servers:
  - url: http://example.io/api/v1
paths:
  /echo:
    post:
      tags:
        - echo
      operationId: echo
      parameters:
        - $ref: '#/components/parameters/correlationIdParam'
      requestBody:
        description: body param
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/EchoBody'
        required: true
      responses:
        200:
          description: request body
          headers:
            X-Correlation-ID:
              schema:
                type: string
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EchoBody'
        default:
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorDescription'
components:
  schemas:
    EchoBody:
      title: Echo object
      type: object
      properties:
        prop_int:
          type: integer
          format: int32
        prop_float:
          maximum: 101
          type: number
          format: float
        prop_double:
          maximum: 9999.9999
          minimum: 0.01
          type: number
          format: double
        prop_amount:
          pattern: ^(0|([1-9][0-9]{0,}))\.\d{2}$
          type: string
      description: Test object
    ErrorDescription:
      required:
        - code
        - message
      type: object
      properties:
        field:
          type: string
        code:
          type: string
        message:
          type: string
  parameters:
    correlationIdParam:
      name: X-Correlation-ID
      in: header
      description: 'Correlates HTTP requests between a client and server. '
      required: true
      schema:
        type: string
```


:::note
OAS2 (swagger 2) and OAS3 (OpenAPI 3) are supported.
:::

Place the API definition in the `src/main/resources/example-api.yaml` file.

### Generator configuration

To configure code generation you need something like this:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Java', value: 'java'},
    {label: 'Groovy', value: 'groovy'},
  ]
}>
<TabItem value="java">

```java title="GeneratorRunner.java"
import pl.metaprogramming.codegen.Codegen;
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator;
import pl.metaprogramming.codegen.java.spring.SpringRestServiceGenerator;
import pl.metaprogramming.model.oas.OpenapiParser;

import java.io.File;

public class GeneratorRunner {

    public static void main(String[] args) {
        generate(args[0]);
    }

    static void generate(String baseDir) {
        new Codegen()
                .setBaseDir(new File(baseDir + "/.."))
                .setIndexFile(new File(baseDir + "/src/main/resources/GeneratedFiles.yaml"))
                .addCommonModule(SpringCommonsGenerator.of(cfg -> cfg
                        .setRootPackage("example.commons")
                        .setProjectDir("app")))
                .addModule(SpringRestServiceGenerator.of(OpenapiParser.parse(baseDir + "/src/main/resources/example-api.yaml"), cfg -> cfg
                        .setRootPackage("example")
                        .setProjectDir("app")))
                .generate();
    }
}
```

</TabItem>
<TabItem value="groovy">

```groovy title="GeneratorRunner.groovy"
import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator
import pl.metaprogramming.codegen.java.spring.SpringRestServiceGenerator
import pl.metaprogramming.model.oas.RestApi

import static pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator.TOC_ENDPOINT_PROVIDER

class GeneratorRunner {

    static void main(String[] args) {
        generate(args[0], args[1])
    }

    static void generate(String baseDir, String indexFile) {
        new Codegen()
                .setBaseDir(new File(baseDir))
                .setIndexFile(new File(indexFile))
                .addCommonModule(SpringCommonsGenerator.of {
                    it.setRootPackage('example.commons')
                    it.setPackage("example", TOC_ENDPOINT_PROVIDER)
                    it.setProjectDir('app')
                })
                .addModule(SpringRestServiceGenerator.of(RestApi.of(getResource("/example-api.yaml"))) {
                    it.setRootPackage('example')
                    it.setProjectDir('app')
                })
                .generate()
    }

    static String getResource(String name) {
        GeneratorRunner.class.getResourceAsStream(name).text
    }
}
```

</TabItem>
</Tabs>

Place above code into `src/main/java/GeneratorRunner.java` (if you prefer java) or into `src/main/groovy/GeneratorRunner.groovy` (if you prefer groovy).

The code performs:
- imports REST API from `example-api.yaml` OpenAPI specification file (see [guide](guides/oas-support.md) to get more about OAS import)
- configures `example-api` module: sets the project directory to `app` and root package to `example` for generated codes
- generates codes for modules
  - `commons` (API independent utility codes)
  - `example-api` (REST API adapter for example-api.yaml - actually adapter, port and implementation stub)

:::note
Codegen uses `modules` to distinguish different group of codes.

Codes of one module can be placed into different packages and even different projects - it makes sense, since is generated adapter, port, and implementation stub. By default, the codes are grouped in packages: adapters, ports and application.

Codes from many modules can also be placed into one package/project - maybe it makes sense sometimes, you can do it anyway.
:::


### Triggering code generation

To generate codes run following task:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```sh
mvn -f generator/pom.xml compile exec:java
```

</TabItem>
<TabItem value="groovy">

```sh
./gradlew :generator:generate
```

</TabItem>
</Tabs>


## App subproject

Most of the codes for this subproject was generated in the previous step, however there are a few things to do.

Create subproject definition with following `pom.xml` or `build.gradle`:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```xml title="pom.xml"
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
                             http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>pl.metaprogramming.codegen-examples.spring-rs-server-maven</groupId>
        <artifactId>spring-rs-server-maven</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </parent>
    <artifactId>app</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web-services</artifactId>
        </dependency>
        <dependency>
            <groupId>org.zalando</groupId>
            <artifactId>logbook-spring-boot-starter</artifactId>
            <version>2.14.0</version>
        </dependency>
        <dependency>
            <groupId>com.google.code.findbugs</groupId>
            <artifactId>findbugs</artifactId>
            <version>3.0.1</version>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>2.7.1</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <version>2.7.1</version>
            </plugin>
        </plugins>
    </build>
</project>
```

</TabItem>
<TabItem value="groovy">

```groovy title="build.gradle"
plugins {
    id 'org.springframework.boot' version '2.7.1'
    id 'io.spring.dependency-management' version '1.0.11.RELEASE'
    id 'java'
}

sourceCompatibility = '1.8'

repositories {
    mavenCentral()
}

dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-web'
    implementation 'org.zalando:logbook-spring-boot-starter:2.14.0'
    implementation 'com.google.code.findbugs:findbugs:3.0.1'
    compileOnly 'org.projectlombok:lombok:1.18.24'
    annotationProcessor 'org.projectlombok:lombok:1.18.24'
}
```

</TabItem>
</Tabs>


### Spring Boot application

To declare Spring Boot application create a class `example.Application` with the following content:

```java title="Application.java"
package example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
```


### Logging REST traffic

To enable logging for REST traffic, create a file `src/main/resources/application.properties` with the following content:

```properties title="application.properties"
logging.level.org.zalando.logbook=TRACE
```


### Service implementation

To implement service, fill the `execute` method body in the generated class `example.process.EchoCommand`:

```java
@Component
public class EchoCommand implements IEchoCommand {
    @Override
    public ResponseEntity<EchoBodyDto> execute(@Nonnull EchoRequest request) {
        return ResponseEntity.ok(request.getRequestBody());
    }
}
```

With the default settings, for each operation defined in the OpenAPI specification, a class is generated in the `process` package, which is responsible for the implementation of this operation. Depending on the HTTP method of the operation, the class will have a `Query` or `Command` postfix.

If the API specifies that the operation returns HTTP headers, the execute method should return a `ResponseEntity<T>` object, otherwise it should return a `T`.

The generated class is configured as a spring component with prototype scope, which allows for more object-oriented handling of requests. However, if this is undesirable, when implementing a class, you can simply remove the `@Scope` annotation (along with the `@Generated` annotation).

By setting the `SpringRestParams.delegateToFacade` parameter, you can configure the generator to create a facade that will contain methods implementing operations from a given group.

### RestExceptionHandler

Since the response message structure for bad requests can be freely defined in the API, it is necessary to define a mapping of validation errors to this message structure. To do this you should fix implementation of `ResponseEntity<?> makeResponse(ValidationResult result)` method in the generated class `example.commons.RestExceptionHandler`.

```java
private ResponseEntity<?> makeResponse(ValidationResult result) {
    ValidationError firstError = result.getErrors().get(0);
    return ResponseEntity
            .status(result.getStatus())
            .body(new ErrorDescriptionDto()
                    .setCode(firstError.getCode())
                    .setField(firstError.getField())
                    .setMessage(String.format("%s: %s", firstError.getField(), firstError.getCode())));
}
```

:::note
After changing generated class you should remove `@Generated` annotation.
:::

:::danger
In principle, you should avoid modifying classes other than implementation stubs, otherwise the codes will be more difficult to develop and maintain.
:::


## Run and test

To start the services, execute the command:


<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```sh
mvn -f app/pom.xml spring-boot:run
```

</TabItem>
<TabItem value="groovy">

```sh
./gradlew :app:bootRun
```

</TabItem>
</Tabs>


Then you can test them with the command:

```sh
curl --request POST \
  -H "Content-Type: application/json" -H "X-Correlation-ID: 123" \
  --data '{"prop_int":2,"prop_float":1.1,"prop_double":10.01,"prop_amount":"11.11"}' \
  http://localhost:8080/api/v1/echo
```


## Full project sources

You can download the full project source codes from the GitLab repository:
- [spring boot REST server - gradle](https://gitlab.com/metaprogramming/codegen/-/tree/master/quickstart-projects/spring-rs-server-gradle)
- [spring boot REST server - maven](https://gitlab.com/metaprogramming/codegen/-/tree/master/quickstart-projects/spring-rs-server-maven)
