---
id: quick-start-spring-soap-client
title: Generate SOAP client to use with Spring
sidebar_label: Spring SOAP client
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

This guide walks you through the process of creating with codegen an application which consuming a SOAP-based web service with Spring.

The application consumes services created in this [guide](https://spring.io/guides/gs/producing-web-service/).

[Here](https://spring.io/guides/gs/consuming-web-service/) is a guide for creating this client in more traditional way.

## Project structure

Our project will consist of two subprojects:
- `app`: contains Spring Boot application
- `generator`: contains API description and generator configuration. It is used to generate codes for the `app` subproject

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```sh
project-root
├── app
│   ├── src
│   │   └── main
│   │       ├── java
│   │       │   └── example
│   │       │       ├── commons <GENERATED>
│   │       │       ├── ws
│   │       │       │   ├── schema <GENERATED>
│   │       │       │   ├── CountriesClient.java <GENERATED>
│   │       │       │   └── CountriesClientConfiguration.java <GENERATED>
│   │       │       └── Application.java
│   │       └── resources
│   │           └── logback-spring.xml
│   └── pom.xml
├── generator
│   ├── src
│   │   └── main
│   │       ├── java
│   │       │   └── GeneratorRunner.java <GENERATOR CONFIGURATION>
│   │       └── resources
│   │           ├── countries.wsdl <API DESCRIPTION>
│   │           └── GeneratedFiles.yaml <GENERATED FILE INDEX>
│   └── pom.xml
└── pom.xml
.
```

</TabItem>
<TabItem value="groovy">

```sh
project-root
├── app
│   ├── src
│   │   └── main
│   │       ├── java
│   │       │   └── example
│   │       │       ├── commons <GENERATED>
│   │       │       ├── ws
│   │       │       │   ├── schema <GENERATED>
│   │       │       │   ├── CountriesClient.java <GENERATED>
│   │       │       │   └── CountriesClientConfiguration.java <GENERATED>
│   │       │       └── Application.java
│   │       └── resources
│   │           └── logback-spring.xml
│   └── build.gradle
├── generator
│   ├── src
│   │   └── main
│   │       ├── java
│   │       │   └── GeneratorRunner.java <GENERATOR CONFIGURATION>
│   │       └── resources
│   │           ├── countries.wsdl <API DESCRIPTION>
│   │           └── GeneratedFiles.yaml <GENERATED FILE INDEX>
│   └── build.gradle
├── build.gradle
└── settings.gradle
.
```

</TabItem>
</Tabs>

## Root project

First create a project directory and put `pom.xml` (if you prefer maven) or `build.gradle` and `settings.gradle` (if you prefer gradle):

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```xml title="pom.xml"
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
                             http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>pl.metaprogramming.codegen-examples.spring-soap-client-maven</groupId>
    <artifactId>spring-soap-client-maven</artifactId>
    <packaging>pom</packaging>
    <version>0.0.1-SNAPSHOT</version>

    <modules>
        <module>app</module>
        <module>generator</module>
    </modules>

    <properties>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
    </properties>
</project>
```

</TabItem>
<TabItem value="groovy">

```groovy title="build.gradle"
group = 'pl.metaprogramming.codegen-examples.spring-soap-client-gradle'
version = '0.0.1-SNAPSHOT'
```


```groovy title="settings.gradle"
rootProject.name = 'spring-soap-client-gradle'

include 'app'
include 'generator'
```

</TabItem>
</Tabs>


## Generator subproject

Then create the `generator` subproject with the following `pom.xml` / `build.gradle`:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```xml title="pom.xml"
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
                             http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>pl.metaprogramming.codegen-examples.spring-soap-client-maven</groupId>
        <artifactId>spring-soap-client-maven</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </parent>
    <artifactId>generator</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>pl.metaprogramming</groupId>
            <artifactId>codegen</artifactId>
            <version>1.1.1-SNAPSHOT</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>exec-maven-plugin</artifactId>
                <version>3.0.0</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>java</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <mainClass>GeneratorRunner</mainClass>
                    <arguments>
                        <argument>${project.basedir}</argument>
                    </arguments>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
```

</TabItem>
<TabItem value="groovy">

```groovy title="build.gradle"
plugins {
    id 'groovy'
}

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    implementation 'pl.metaprogramming:codegen:1.1.1-SNAPSHOT'
}

task generate(type: JavaExec) {
    group = 'codegen'
    classpath = sourceSets.main.runtimeClasspath
    main = 'GeneratorRunner'
    args rootProject.rootDir.path,
            new File(project.projectDir.path, 'src/main/resources/GeneratedFiles.yaml').path
}
```

</TabItem>
</Tabs>


### API description

Let's assume that we have the following API to integrate with:

```xml title="countries.wsdl"
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<wsdl:definitions xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
                  xmlns:sch="http://spring.io/guides/gs-producing-web-service"
                  xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"
                  xmlns:tns="http://spring.io/guides/gs-producing-web-service"
                  targetNamespace="http://spring.io/guides/gs-producing-web-service">
    <wsdl:types>
        <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"
                   targetNamespace="http://spring.io/guides/gs-producing-web-service">

            <xs:element name="getCountryRequest">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="name" type="xs:string"/>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>

            <xs:element name="getCountryResponse">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="country" type="tns:country"/>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>

            <xs:complexType name="country">
                <xs:sequence>
                    <xs:element name="name" type="xs:string"/>
                    <xs:element name="population" type="xs:int"/>
                    <xs:element name="capital" type="xs:string"/>
                    <xs:element name="currency" type="tns:currency"/>
                </xs:sequence>
            </xs:complexType>

            <xs:simpleType name="currency">
                <xs:restriction base="xs:string">
                    <xs:enumeration value="GBP"/>
                    <xs:enumeration value="EUR"/>
                    <xs:enumeration value="PLN"/>
                </xs:restriction>
            </xs:simpleType>
        </xs:schema>
    </wsdl:types>
    <wsdl:message name="getCountryResponse">
        <wsdl:part element="tns:getCountryResponse" name="getCountryResponse">
        </wsdl:part>
    </wsdl:message>
    <wsdl:message name="getCountryRequest">
        <wsdl:part element="tns:getCountryRequest" name="getCountryRequest">
        </wsdl:part>
    </wsdl:message>
    <wsdl:portType name="CountriesPort">
        <wsdl:operation name="getCountry">
            <wsdl:input message="tns:getCountryRequest" name="getCountryRequest">
            </wsdl:input>
            <wsdl:output message="tns:getCountryResponse" name="getCountryResponse">
            </wsdl:output>
        </wsdl:operation>
    </wsdl:portType>
    <wsdl:binding name="CountriesPortSoap11" type="tns:CountriesPort">
        <soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
        <wsdl:operation name="getCountry">
            <soap:operation soapAction=""/>
            <wsdl:input name="getCountryRequest">
                <soap:body use="literal"/>
            </wsdl:input>
            <wsdl:output name="getCountryResponse">
                <soap:body use="literal"/>
            </wsdl:output>
        </wsdl:operation>
    </wsdl:binding>
    <wsdl:service name="CountriesPortService">
        <wsdl:port binding="tns:CountriesPortSoap11" name="CountriesPortSoap11">
            <soap:address location="http://localhost:8080/ws"/>
        </wsdl:port>
    </wsdl:service>
</wsdl:definitions>
```

Place the API definition in the `src/main/resources/countries.wsdl` file.

### Generator configuration

To configure code generation you need something like this:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Java', value: 'java'},
    {label: 'Groovy', value: 'groovy'},
  ]
}>
<TabItem value="java">

```java title="GeneratorRunner.java"
import pl.metaprogramming.codegen.Codegen;
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator;
import pl.metaprogramming.codegen.java.spring.SpringSoapClientGenerator;
import pl.metaprogramming.model.wsdl.WsdlApi;
import pl.metaprogramming.model.wsdl.WsdlParser;
import pl.metaprogramming.model.wsdl.WsdlParserConfig;

import java.io.File;

public class GeneratorRunner {

    public static void main(String[] args) {
        generate(args[0]);
    }

    private static void generate(String baseDir) {
        WsdlApi wsdlApi = WsdlParser.parse(
                baseDir + "/src/main/resources/countries.wsdl",
                new WsdlParserConfig()
                        .setServiceNameMapper(name -> name.replace("PortService", "")));
        new Codegen()
                .setBaseDir(new File(baseDir + "/.."))
                .setIndexFile(new File(baseDir + "/src/main/resource/GeneratedFiles.yaml"))
                .addCommonModule(SpringCommonsGenerator.of(cfg -> cfg
                        .setRootPackage("example.commons")
                        .setProjectDir("app")))
                .addModule(SpringSoapClientGenerator.of(wsdlApi, cfg -> cfg
                        .setRootPackage("example.ws")
                        .setNamespacePackage("http://spring.io/guides/gs-producing-web-service", "example.ws.schema")
                        .setProjectDir("app")))
                .generate();
    }

}
```

</TabItem>
<TabItem value="groovy">

```groovy title="GeneratorRunner.groovy"
import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator
import pl.metaprogramming.codegen.java.spring.SpringSoapClientGenerator
import pl.metaprogramming.model.wsdl.WsdlParser
import pl.metaprogramming.model.wsdl.WsdlParserConfig

class GeneratorRunner {

    static void main(String[] args) {
        generate(args[0], args[1])
    }

    static void generate(String baseDir, String indexFile) {
        def wsdlApi = WsdlParser.parse(
                'src/main/resources/countries.wsdl',
                new WsdlParserConfig(serviceNameMapper: { it.replace('PortService', '') }))
        new Codegen()
                .setBaseDir(new File(baseDir))
                .setIndexFile(new File(indexFile))
                .addCommonModule(SpringCommonsGenerator.of {
                    it.setRootPackage('example.commons')
                    it.setProjectDir('app')
                })
                .addModule(SpringSoapClientGenerator.of(wsdlApi) {
                    it.setRootPackage('example.ws')
                    it.setNamespacePackage('http://spring.io/guides/gs-producing-web-service', 'example.ws.schema')
                    it.setProjectDir('app')
                })
                .generate()
    }

}
```

</TabItem>
</Tabs>

Place above code into `src/main/java/GeneratorRunner.java` (if you prefer java) or into `src/main/groovy/GeneratorRunner.groovy` (if you prefer groovy).

The code performs:
- imports WSDL API from `countries.wsdl` file 
- configures `commons`
  - specifies needed classes,
  - sets the project directory to `app`,
  - sets root package to `example.commons`
- configures `countries-client` module:
  - sets the project directory to `app`,
  - sets root package to `example.ws` for generated codes,
  - sets mapping xml namespace to java package (consequence of using JAXB)
  - removes the 'PortService' suffix from the service name (to generate the 'CountriesClient' class instead of 'CountriesPortServiceClient')
- generates codes for above modules

### Triggering code generation

To generate codes run following task:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```sh
mvn -f generator/pom.xml compile exec:java
```

</TabItem>
<TabItem value="groovy">

```sh
./gradlew :generator:generate
```

</TabItem>
</Tabs>


## App subproject

Most of the codes for this subproject was generated in the previous step, however there are a few things to do.

Create subproject definition with following `pom.xml` or `build.gradle`:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```xml title="pom.xml"
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
                             http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>pl.metaprogramming.codegen-examples.spring-soap-client-maven</groupId>
        <artifactId>spring-soap-client-maven</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </parent>
    <artifactId>app</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web-services</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-starter-tomcat</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>com.google.code.findbugs</groupId>
            <artifactId>findbugs</artifactId>
            <version>3.0.1</version>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>2.7.1</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <version>2.7.1</version>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>java11</id>
            <activation>
                <jdk>[11,)</jdk>
            </activation>
            <dependencies>
                <dependency>
                    <groupId>org.glassfish.jaxb</groupId>
                    <artifactId>jaxb-runtime</artifactId>
                </dependency>
            </dependencies>
        </profile>
    </profiles>
</project>
```

</TabItem>
<TabItem value="groovy">

```groovy title="build.gradle"
plugins {
    id 'org.springframework.boot' version '2.7.1'
    id 'io.spring.dependency-management' version '1.0.11.RELEASE'
    id 'java'
}

sourceCompatibility = '1.8'

repositories {
    mavenCentral()
}

dependencies {
    implementation('org.springframework.boot:spring-boot-starter-web-services') {
        exclude group: 'org.springframework.boot', module: 'spring-boot-starter-tomcat'
    }
    implementation 'com.google.code.findbugs:findbugs:3.0.1'
    implementation 'org.glassfish.jaxb:jaxb-runtime'
    compileOnly 'org.projectlombok:lombok:1.18.24'
    annotationProcessor 'org.projectlombok:lombok:1.18.24'
}
```

</TabItem>
</Tabs>


### Spring Boot application

To declare Spring Boot application create a class `example.Application` with the following content:

```java title="Application.java"
package example;

import example.ws.CountriesClient;
import example.ws.schema.GetCountryRequest;
import example.ws.schema.GetCountryResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@Slf4j
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner start(CountriesClient quoteClient) {
        return args -> {
            GetCountryResponse response = quoteClient.getCountry(new GetCountryRequest().setName("Poland"));
            log.info("response: " + response);
        };
    }
}
```


### Logging SOAP traffic

To enable logging for REST traffic, create a file `src/main/resources/logback-spring.xml` with the following content:

```xml title="logback-spring.xml"
<?xml version="1.0" encoding="UTF-8"?>
<configuration>
    <include resource="org/springframework/boot/logging/logback/base.xml"/>
    <logger name="org.springframework.ws.client.MessageTracing" level="TRACE" additivity="false">
        <appender-ref ref="CONSOLE"/>
    </logger>
</configuration>
```


## Run and test

To call the SOAP service, just execute the command:


<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```sh
mvn -f app/pom.xml spring-boot:run
```

</TabItem>
<TabItem value="groovy">

```sh
./gradlew :app:bootRun
```

</TabItem>
</Tabs>

:::note
You must be running the server described in the [tutorial](https://spring.io/guides/gs/producing-web-service/).
:::


## Full project sources

You can download the full project source codes from the GitLab repository:
- [spring boot REST server - gradle](https://gitlab.com/metaprogramming/codegen/-/tree/master/quickstart-projects/spring-soap-client-gradle)
- [spring boot REST server - maven](https://gitlab.com/metaprogramming/codegen/-/tree/master/quickstart-projects/spring-soap-client-maven)
