---
id: quick-start-spring-rest-client
title: Generate REST client to use with Spring
sidebar_label: Spring REST client
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

This guide walks you through the process of creating with codegen a REST client with Spring.

## Project structure

Our project will consist of two subprojects:
- `app`: contains Spring Boot application
- `generator`: contains API description and generator configuration. It is used to generate codes for the `app` subproject

It will look like this:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```sh
project-root
├── app
│   ├── src
│   │   └── main
│   │       ├── java
│   │       │   └── example
│   │       │       ├── adapters <GENERATED>
│   │       │       ├── commons <GENERATED>
│   │       │       ├── ports <GENERATED>
│   │       │       ├── Application.java
│   │       │       ├── EndpointProvider.java
│   │       │       └── RestTemplateConfigurator.java
│   │       └── resources
│   │           └── application.properties
│   └── pom.xml
├── generator
│   ├── src
│   │   └── main
│   │       ├── java
│   │       │   └── GeneratorRunner.java <GENERATOR CONFIGURATION>
│   │       └── resources
│   │           ├── example-api.yaml <API DESCRIPTION>
│   │           └── GeneratedFiles.yaml <GENERATED FILE INDEX>
│   └── pom.xml
└── pom.xml
.
```

</TabItem>
<TabItem value="groovy">

```sh
project-root
├── app
│   ├── src
│   │   └── main
│   │       ├── java
│   │       │   └── example
│   │       │       ├── adapters <GENERATED>
│   │       │       ├── commons <GENERATED>
│   │       │       ├── ports <GENERATED>
│   │       │       ├── Application.java
│   │       │       ├── EndpointProvider.java
│   │       │       └── RestTemplateConfigurator.java
│   │       └── resources
│   │           └── application.properties
│   └── build.gradle
├── generator
│   ├── src
│   │   └── main
│   │       ├── groovy
│   │       │   └── GeneratorRunner.groovy <GENERATOR CONFIGURATION>
│   │       └── resources
│   │           ├── example-api.yaml <API DESCRIPTION>
│   │           └── GeneratedFiles.yaml <GENERATED FILE INDEX>
│   └── build.gradle
├── build.gradle
└── settings.gradle
.
```

</TabItem>
</Tabs>


## Root project

First create a project directory and put `pom.xml` (if you prefer maven) or `build.gradle` and `settings.gradle` (if you prefer gradle):

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```xml title="pom.xml"
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
                             http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>pl.metaprogramming.codegen-examples.spring-rs-client-maven</groupId>
    <artifactId>spring-rs-client-maven</artifactId>
    <packaging>pom</packaging>
    <version>0.0.1-SNAPSHOT</version>

    <modules>
        <module>app</module>
        <module>generator</module>
    </modules>

    <properties>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
    </properties>
</project>
```

</TabItem>
<TabItem value="groovy">

```groovy title="build.gradle"
group = 'pl.metaprogramming.codegen-examples.spring-rs-client-gradle'
version = '0.0.1-SNAPSHOT'
```


```groovy title="settings.gradle"
rootProject.name = 'spring-rs-client-gradle'

include 'app'
include 'generator'
```

</TabItem>
</Tabs>


## Generator subproject

Then create the `generator` subproject with the following `pom.xml` / `build.gradle`:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```xml title="pom.xml"
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
                             http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>pl.metaprogramming.codegen-examples.spring-rs-client-maven</groupId>
        <artifactId>spring-rs-client-maven</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </parent>
    <artifactId>generator</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>pl.metaprogramming</groupId>
            <artifactId>codegen</artifactId>
            <version>1.1.1-SNAPSHOT</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>exec-maven-plugin</artifactId>
                <version>3.0.0</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>java</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <mainClass>GeneratorRunner</mainClass>
                    <arguments>
                        <argument>${project.basedir}</argument>
                    </arguments>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
```

</TabItem>
<TabItem value="groovy">

```groovy title="build.gradle"
plugins {
    id 'groovy'
}

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    implementation 'pl.metaprogramming:codegen:1.1.1-SNAPSHOT'
}

task generate(type: JavaExec) {
    group = 'codegen'
    classpath = sourceSets.main.runtimeClasspath
    main = 'GeneratorRunner'
    args rootProject.rootDir.path,
            new File(project.projectDir.path, 'src/main/resources/GeneratedFiles.yaml').path
}
```

</TabItem>
</Tabs>

### API description

Let's assume that we have the following API to implement:


```yaml title="example-api.yaml"
openapi: 3.0.1
info:
  title: Example API
  version: "1.0"
servers:
  - url: http://example.io/api/v1
paths:
  /echo:
    post:
      tags:
        - echo
      operationId: echo
      parameters:
        - $ref: '#/components/parameters/correlationIdParam'
      requestBody:
        description: body param
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/EchoBody'
        required: true
      responses:
        200:
          description: request body
          headers:
            X-Correlation-ID:
              schema:
                type: string
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EchoBody'
        default:
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorDescription'
components:
  schemas:
    EchoBody:
      title: Echo object
      type: object
      properties:
        prop_int:
          type: integer
          format: int32
        prop_float:
          maximum: 101
          type: number
          format: float
        prop_double:
          maximum: 9999.9999
          minimum: 0.01
          type: number
          format: double
        prop_amount:
          pattern: ^(0|([1-9][0-9]{0,}))\.\d{2}$
          type: string
      description: Test object
    ErrorDescription:
      required:
        - code
        - message
      type: object
      properties:
        field:
          type: string
        code:
          type: string
        message:
          type: string
  parameters:
    correlationIdParam:
      name: X-Correlation-ID
      in: header
      description: 'Correlates HTTP requests between a client and server. '
      required: true
      schema:
        type: string
```


:::note
OAS2 (swagger 2) and OAS3 (OpenAPI 3) are supported.
:::

Place the API definition in the `src/main/resources/example-api.yaml` file.

Luckily the API is the same as in the [Generate REST services to use with Spring](quick-start-spring-rest-server.md) tutorial, so you can use the server to test this client.

### Generator configuration

To configure code generation you need something like this:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Java', value: 'java'},
    {label: 'Groovy', value: 'groovy'},
  ]
}>
<TabItem value="java">

```java title="GeneratorRunner.java"
import pl.metaprogramming.codegen.Codegen;
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator;
import pl.metaprogramming.codegen.java.spring.SpringRestClientGenerator;
import pl.metaprogramming.model.oas.OpenapiParser;

import java.io.File;

import static pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator.TOC_ENDPOINT_PROVIDER;

public class GeneratorRunner {

    public static void main(String[] args) {
        generate(args[0]);
    }

    static void generate(String baseDir) {
        new Codegen()
                .setBaseDir(new File(baseDir + "/.."))
                .setIndexFile(new File(baseDir + "/src/main/resources/GeneratedFiles.yaml"))
                .addCommonModule(SpringCommonsGenerator.of(cfg -> cfg
                        .setRootPackage("example.commons")
                        .setPackage("example", TOC_ENDPOINT_PROVIDER)
                        .setProjectDir("app")
                ))
                .addModule(SpringRestClientGenerator.of(OpenapiParser.parse(baseDir + "/src/main/resources/example-api.yaml"), cfg -> cfg
                        .setRootPackage("example")
                        .setProjectDir("app")
                ))
                .generate();
    }
}
```

</TabItem>
<TabItem value="groovy">

```groovy title="GeneratorRunner.groovy"
import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator
import pl.metaprogramming.codegen.java.spring.SpringRestClientGenerator
import pl.metaprogramming.model.oas.OpenapiParser

import static pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator.TOC_ENDPOINT_PROVIDER

class GeneratorRunner {

    static void main(String[] args) {
        generate(args[0], args[1])
    }

    static void generate(String baseDir, String indexFile) {
        new Codegen()
                .setBaseDir(new File(baseDir))
                .setIndexFile(new File(indexFile))
                .addCommonModule(SpringCommonsGenerator.of {
                    it.setRootPackage('example.commons')
                    it.setPackage("example", TOC_ENDPOINT_PROVIDER)
                    it.setProjectDir('app')
                })
                .addModule(SpringRestClientGenerator.of(OpenapiParser.parse('src/main/resources/example-api.yaml')) {
                    it.setRootPackage('example')
                    it.setProjectDir('app')
                })
                .generate()
    }
}
```

</TabItem>
</Tabs>

Place above code into `src/main/java/GeneratorRunner.java` (if you prefer java) or into `src/main/groovy/GeneratorRunner.groovy` (if you prefer groovy).

The code performs:
- imports REST API from `example-api.yaml` OpenAPI file (see [guide](guides/oas-support.md) to get more about OAS import)
- configures `commons` module (API independent utility codes): sets the project directory to `app` and package to `example.commons` for generated codes (except EndpointProvider class, which is part of application configuration, so is placed in main package)
- configures `example-api` module: sets the project directory to `app` and root package to `example` for generated codes
- generates codes for modules
  - `commons`
  - `example-api` (REST API adapter for example-api.yaml - client)

:::note
Codegen uses `modules` to distinguish different group of codes.

Codes of one module can be placed into different packages and even different projects - it makes sense, since is generated adapter, port, and implementation stub. By default, the codes are grouped in packages: adapters, ports and application.

Codes from many modules can also be placed into one package/project - maybe it makes sense sometimes, you can do it anyway.
:::


### Triggering code generation

To generate codes run following task:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```sh
mvn -f generator/pom.xml compile exec:java
```

</TabItem>
<TabItem value="groovy">

```sh
./gradlew :generator:generate
```

</TabItem>
</Tabs>


## App subproject

Most of the codes for this subproject was generated in the previous step, however there are a few things to do.

Create subproject definition with following `pom.xml` or `build.gradle`:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```xml title="pom.xml"
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
                             http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>pl.metaprogramming.codegen-examples.spring-rs-client-maven</groupId>
        <artifactId>spring-rs-client-maven</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </parent>
    <artifactId>app</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.zalando</groupId>
            <artifactId>logbook-spring-boot-starter</artifactId>
            <version>2.14.0</version>
        </dependency>
        <dependency>
            <groupId>com.google.code.findbugs</groupId>
            <artifactId>findbugs</artifactId>
            <version>3.0.1</version>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <scope>provided</scope>
        </dependency>
    </dependencies>


    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>2.7.1</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>


    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <version>2.7.1</version>
            </plugin>
        </plugins>
    </build>
</project>
```

</TabItem>
<TabItem value="groovy">

```groovy title="build.gradle"
plugins {
    id 'org.springframework.boot' version '2.7.1'
    id 'io.spring.dependency-management' version '1.0.11.RELEASE'
    id 'java'
}

sourceCompatibility = '1.8'

repositories {
    mavenCentral()
}

dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-web'
    implementation 'org.zalando:logbook-spring-boot-starter:2.14.0'
    implementation 'com.google.code.findbugs:findbugs:3.0.1'
    compileOnly 'org.projectlombok:lombok:1.18.24'
    annotationProcessor 'org.projectlombok:lombok:1.18.24'
}
```

</TabItem>
</Tabs>


### Spring Boot application and client calls

To declare Spring Boot application create a class `example.Application` with the following content:

```java title="Application.java"
package example;

import example.adapters.out.rest.EchoClient;
import example.ports.out.rest.dtos.EchoBodyDto;
import example.ports.out.rest.dtos.EchoRequest;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner start(EchoClient echoClient) {
        return args -> {
            callWithOK(echoClient);
            callWith400(echoClient);
        };
    }

    private void callWithOK(EchoClient echoClient) {
        EchoRequest request = new EchoRequest()
                .setCorrelationIdParam("correlation-id-123")
                .setRequestBody(new EchoBodyDto()
                        .setPropInt(123)
                        .setPropDouble(111.222)
                        .setPropFloat(3.1f)
                        .setPropAmount("12.10")
                );
        ResponseEntity<EchoBodyDto> response = echoClient.echo(request);
        checkEquals(200, response.getStatusCodeValue());
        checkEquals(request.getRequestBody(), response.getBody());
    }

    private void callWith400(EchoClient echoClient) {
        EchoRequest request = new EchoRequest()
                .setCorrelationIdParam("correlation-id-123")
                .setRequestBody(new EchoBodyDto()
                        .setPropAmount("12.123")
                );
        try {
            echoClient.echo(request);
            throw new IllegalStateException("Exception should be thrown");
        } catch (HttpClientErrorException.BadRequest e) {
            checkEquals(400, e.getRawStatusCode());
            checkContains("\"field\":\"prop_amount\",\"code\":\"is_not_match_pattern\"", e.getMessage());
        }
    }

    private void checkEquals(Object expected, Object given) {
        check(expected.equals(given), given + " is not equals to " + expected);
    }

    private void checkContains(String expected, String given) {
        check(given.contains(expected), String.format("expected @%s@ as part of @%s@", expected, given));
    }

    private void check(boolean asserCondition, String assertMessage) {
        if (!asserCondition) {
            throw new IllegalStateException(assertMessage);
        }
    }
}
```


### Logging REST traffic

To enable logging for REST traffic (and to disable starting tomcat), create a file `src/main/resources/application.properties` with the following content:

```properties title="application.properties"
spring.main.web-application-type=NONE
logging.level.org.zalando.logbook=TRACE
```


You also need to configure the RestTemplate accordingly. For this purpose create `RestTemplateConfigurator` class:

```java title="RestTemplateConfigurator.java"
package example;

import lombok.RequiredArgsConstructor;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.zalando.logbook.httpclient.LogbookHttpRequestInterceptor;
import org.zalando.logbook.httpclient.LogbookHttpResponseInterceptor;

@Configuration
@RequiredArgsConstructor
public class RestTemplateConfigurator {
    private final LogbookHttpRequestInterceptor logbookHttpRequestInterceptor;
    private final LogbookHttpResponseInterceptor logbookHttpResponseInterceptor;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplateBuilder()
                .requestFactory(this::clientHttpRequestFactory)
                .build();
    }

    private ClientHttpRequestFactory clientHttpRequestFactory() {
        CloseableHttpClient client = HttpClientBuilder.create()
                .addInterceptorFirst(logbookHttpRequestInterceptor)
                .addInterceptorFirst(logbookHttpResponseInterceptor)
                .build();
        return new HttpComponentsClientHttpRequestFactory(client);
    }
}
```


### Configure endpoint

To set endpoint for calling REST service set BASE_URL with application.properties. The default is `http://localhost:8080` so you probably won't need it for testing.

However, if your application will use multiple services located on different hosts, you will need to configure this by modifying the generated `example.EndpointProvider` class.


:::note
After changing generated class you should remove `@Generated` annotation.
:::

:::danger
In principle, you should avoid modifying generated classes, otherwise the codes will be more difficult to develop and maintain, but sometimes is necessary.
:::


## Run and test

To try created client, execute the command:


<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```sh
mvn -f app/pom.xml spring-boot:run
```

</TabItem>
<TabItem value="groovy">

```sh
./gradlew :app:bootRun
```

</TabItem>
</Tabs>

:::note
Remember to run server created in the [Generate REST services to use with Spring](quick-start-spring-rest-server.md) guide.
:::


## Full project sources

You can download the full project source codes from the GitLab repository:
- [spring boot REST server - gradle](https://gitlab.com/metaprogramming/codegen/-/tree/master/quickstart-projects/spring-rs-client-gradle)
- [spring boot REST server - maven](https://gitlab.com/metaprogramming/codegen/-/tree/master/quickstart-projects/spring-rs-client-maven)
