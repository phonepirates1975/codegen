const fs = require('fs')
const lineReader = require('line-reader')
const xmlParser = require('xml2js').parseString

const LINE_SEPARATOR = '\r\n'
const DOCS_DIR = 'docs'
const DOCS_TEMPLATES_DIR = 'docs-templates'
const INCLUDE_REGEX = /<includeFile(.+?)<\/includeFile>/g;
const QUICKSTART_DIR_PLACEHOLDER = '${codegen-quickstart-dir}'
const QUICKSTART_DIR = '../quickstart-projects'
const EXT_2_CODE_TYPE = { 'gradle': 'groovy', 'wsdl': 'xml' }

function toCodeType(fileName) {
    const fileExt = fileName.substring(fileName.lastIndexOf('.') + 1)
    const result = EXT_2_CODE_TYPE[fileExt]
    return result ? result : fileExt
}

function includeFile(filePath, writer) {
    const fileBody = fs.readFileSync(filePath.replace(QUICKSTART_DIR_PLACEHOLDER, QUICKSTART_DIR), 'utf8')
    const fileName = filePath.substring(filePath.lastIndexOf('/') + 1)
    writer.write(LINE_SEPARATOR)
    writer.write('```' + `${toCodeType(fileName)} title="${fileName}"`)
    writer.write(LINE_SEPARATOR)
    writer.write(fileBody.trim())
    writer.write(LINE_SEPARATOR)
    writer.write('```')
    writer.write(LINE_SEPARATOR)
    writer.write(LINE_SEPARATOR)
}

fs.readdir(DOCS_TEMPLATES_DIR, (err, files) => {
    if (err) {
        return console.log('Unable to scan directory: ' + err)
    }
    files.forEach(function (file) {
        console.log(`generate file: ${file}`)
        const writer = fs.createWriteStream(`${DOCS_DIR}/${file}`)
        lineReader.eachLine(`${DOCS_TEMPLATES_DIR}/${file}`, line => {
            const matches = line.match(INCLUDE_REGEX)
            if (matches) {
                matches.forEach(match => xmlParser(match, (err, includeData) =>
                    includeFile(includeData.includeFile, writer)
                ))
            } else {
                writer.write(line)
                writer.write(LINE_SEPARATOR)
            }
        })
    })
})