module.exports = {
  someSidebar: [
    {
      'Codegen': [
        'introduction',
        'why',
        'contribution'
      ]
    },
    {
      'Getting Started': [
        'getting-started/how-to-use',
        'getting-started/quick-start-spring-rest-server',
        'getting-started/quick-start-spring-rest-client',
        'getting-started/quick-start-spring-soap-client'
      ]
    },
    {
      'Guides': [
        'guides/core-design',
        'guides/oas-support',
        'guides/validations',
        'guides/recipes',
        'guides/file-index'
      ]
    },
    {
      'Changes': [
        'changelog',
        'migration-guide'
      ]
    }
  ]
};
