Sample project illustrating the use of the REST client code generator.

## Sub projects

### app

Project (as Spring Boot application) with REST client.

### metamodel

Project with an API model [example-api.yaml](metamodel/src/main/resources/example-api.yaml) and a generator configuration [GeneratorRunner.java](metamodel/src/main/java/GeneratorRunner.java).

## HOWTO

### generate client codes
`mvn -f metamodel/pom.xml compile exec:java`

### run server

To test client codes you need a server from project [spring-rs-server-maven](../spring-rs-server-maven).

### call server
`mvn -f app/pom.xml spring-boot:run`
