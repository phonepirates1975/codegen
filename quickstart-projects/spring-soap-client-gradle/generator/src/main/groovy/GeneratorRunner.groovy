import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator
import pl.metaprogramming.codegen.java.spring.SpringSoapClientGenerator
import pl.metaprogramming.model.wsdl.WsdlParser
import pl.metaprogramming.model.wsdl.WsdlParserConfig

class GeneratorRunner {

    static void main(String[] args) {
        generate(args[0], args[1])
    }

    static void generate(String baseDir, String indexFile) {
        def wsdlApi = WsdlParser.parse(
                'src/main/resources/countries.wsdl',
                new WsdlParserConfig(serviceNameMapper: { it.replace('PortService', '') }))
        new Codegen()
                .setBaseDir(new File(baseDir))
                .setIndexFile(new File(indexFile))
                .addCommonModule(SpringCommonsGenerator.of {
                    it.setRootPackage('example.commons')
                    it.setProjectDir('app')
                })
                .addModule(SpringSoapClientGenerator.of(wsdlApi) {
                    it.setRootPackage('example.ws')
                    it.setNamespacePackage('http://spring.io/guides/gs-producing-web-service', 'example.ws.schema')
                    it.setProjectDir('app')
                })
                .generate()
    }

}
