import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator
import pl.metaprogramming.codegen.java.spring.SpringRestClientGenerator
import pl.metaprogramming.model.oas.OpenapiParser

import static pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator.TOC_ENDPOINT_PROVIDER

class GeneratorRunner {

    static void main(String[] args) {
        generate(args[0], args[1])
    }

    static void generate(String baseDir, String indexFile) {
        new Codegen()
                .setBaseDir(new File(baseDir))
                .setIndexFile(new File(indexFile))
                .addCommonModule(SpringCommonsGenerator.of {
                    it.setRootPackage('example.commons')
                    it.setPackage("example", TOC_ENDPOINT_PROVIDER)
                    it.setProjectDir('app')
                })
                .addModule(SpringRestClientGenerator.of(OpenapiParser.parse('src/main/resources/example-api.yaml')) {
                    it.setRootPackage('example')
                    it.setProjectDir('app')
                })
                .generate()
    }
}
