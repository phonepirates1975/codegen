package example;

import example.adapters.out.rest.EchoClient;
import example.ports.out.rest.dtos.EchoBodyDto;
import example.ports.out.rest.dtos.EchoRequest;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner start(EchoClient echoClient) {
        return args -> {
            callWithOK(echoClient);
            callWith400(echoClient);
        };
    }

    private void callWithOK(EchoClient echoClient) {
        EchoRequest request = new EchoRequest()
                .setCorrelationIdParam("correlation-id-123")
                .setRequestBody(new EchoBodyDto()
                        .setPropInt(123)
                        .setPropDouble(111.222)
                        .setPropFloat(3.1f)
                        .setPropAmount("12.10")
                );
        ResponseEntity<EchoBodyDto> response = echoClient.echo(request);
        checkEquals(200, response.getStatusCodeValue());
        checkEquals(request.getRequestBody(), response.getBody());
    }

    private void callWith400(EchoClient echoClient) {
        EchoRequest request = new EchoRequest()
                .setCorrelationIdParam("correlation-id-123")
                .setRequestBody(new EchoBodyDto()
                        .setPropAmount("12.123")
                );
        try {
            echoClient.echo(request);
            throw new IllegalStateException("Exception should be thrown");
        } catch (HttpClientErrorException.BadRequest e) {
            checkEquals(400, e.getRawStatusCode());
            checkContains("\"field\":\"prop_amount\",\"code\":\"is_not_match_pattern\"", e.getMessage());
        }
    }

    private void checkEquals(Object expected, Object given) {
        check(expected.equals(given), given + " is not equals to " + expected);
    }

    private void checkContains(String expected, String given) {
        check(given.contains(expected), String.format("expected @%s@ as part of @%s@", expected, given));
    }

    private void check(boolean asserCondition, String assertMessage) {
        if (!asserCondition) {
            throw new IllegalStateException(assertMessage);
        }
    }
}
