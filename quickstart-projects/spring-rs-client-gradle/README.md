Sample project illustrating the use of the REST client code generator.

## Sub projects

### app

Project (as Spring Boot application) with REST client.

### generator

Project with an API model [example-api.yaml](generator/src/main/resources/example-api.yaml) and a generator configuration [GeneratorRunner.groovy](generator/src/main/groovy/GeneratorRunner.groovy).

## HOWTO

### generate client codes
`./gradlew :generator:generate`


### run server

To test client codes you need a server from project [spring-rs-server-gradle](../spring-rs-server-gradle).

### call server
`./gradlew :app:bootRun`
