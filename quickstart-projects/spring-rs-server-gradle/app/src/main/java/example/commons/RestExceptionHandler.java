package example.commons;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import example.commons.validator.ValidationError;
import example.commons.validator.ValidationException;
import example.commons.validator.ValidationResult;
import example.ports.in.rest.dtos.ErrorDescriptionDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.server.ResponseStatusException;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static example.commons.validator.CommonCheckers.*;

@Slf4j
@ControllerAdvice
public class RestExceptionHandler {

    private static final String REQ_BODY = "requestBody";

    @ExceptionHandler({Exception.class})
    public ResponseEntity<?> handleAll(Exception e, WebRequest request) {
        if (e instanceof ValidationException) {
            return makeResponse(((ValidationException) e).getResult());
        }
        if (e instanceof HttpMessageNotReadableException) {
            return makeResponse(toValidationError((HttpMessageNotReadableException) e));
        }
        if (e instanceof MethodArgumentTypeMismatchException) {
            return makeResponse(new MatmExceptionConverter((MethodArgumentTypeMismatchException) e).convert());
        }
        if (e instanceof ResponseStatusException) {
            ResponseStatusException rse = (ResponseStatusException) e;
            return new ResponseEntity<>(e.getMessage(), rse.getStatus());
        }
        log.error("Exception", e);
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<?> makeResponse(ValidationResult result) {
        ValidationError firstError = result.getErrors().get(0);
        return ResponseEntity
                .status(result.getStatus())
                .body(new ErrorDescriptionDto()
                        .setCode(firstError.getCode())
                        .setField(firstError.getField())
                        .setMessage(String.format("%s: %s", firstError.getField(), firstError.getCode())));
    }

    private ValidationResult makeValidationResult(String field, String code) {
        return new ValidationResult(null).addError(ValidationError.builder()
                .field(field)
                .code(code)
                .build());
    }

    private ValidationResult makeValidationResult(String field, List<String> allowedValues) {
        return new ValidationResult(null).addError(ValidationError.builder()
                .field(field)
                .code(ERR_CODE_IS_NOT_ALLOWED_VALUE)
                .messageArgs(new Object[]{allowedValues.toString()})
                .build());
    }

    private List<String> getEnumValues(Class<EnumValue> enumClass) {
        return Arrays.stream(enumClass.getEnumConstants()).map(EnumValue::getValue).collect(Collectors.toList());
    }

    private ValidationResult makeUnknownValidationResult() {
        return new ValidationResult(null).addError(ValidationError.builder()
                .code("invalid_input")
                .build());
    }

    @SuppressWarnings("unchecked")
    private ValidationResult toValidationError(HttpMessageNotReadableException e) {
        if (e.getMessage() != null && e.getMessage().startsWith("Required request body is missing")) {
            return makeValidationResult(REQ_BODY, ERR_CODE_IS_REQUIRED);
        }
        if (e.getCause() instanceof MismatchedInputException) {
            MismatchedInputException cex = (MismatchedInputException) e.getCause();
            String field = makeFieldPath(cex.getPath());
            String codeByClass = INVALID_CODE_BY_CLASS.get(cex.getTargetType());
            if (codeByClass != null) {
                return makeValidationResult(field, codeByClass);
            }
            if (cex.getTargetType().isEnum()) {
                return makeValidationResult(field, getEnumValues((Class<EnumValue>) cex.getTargetType()));
            }
        }
        if (e.getCause() instanceof JsonMappingException
                && e.getCause().getCause() instanceof NullPointerException) {
            JsonMappingException jme = (JsonMappingException) e.getCause();
            return makeValidationResult(makeFieldPath(jme.getPath()), ERR_CODE_IS_REQUIRED);
        }
        log.info("Invalid request", e);
        return makeUnknownValidationResult();
    }

    private String makeFieldPath(List<JsonMappingException.Reference> path) {
        StringBuilder buf = new StringBuilder();
        path.forEach(r -> {
            if (r.getFrom() instanceof Map) {
                buf.append(String.format("[%s]", r.getFieldName()));
            } else if (r.getFieldName() != null) {
                if (buf.length() > 0) {
                    buf.append('.');
                }
                buf.append(r.getFieldName());
            } else if (r.getFrom() instanceof List) {
                if (buf.length() == 0) {
                    buf.append(REQ_BODY);
                }
                buf.append(String.format("[%d]", r.getIndex()));
            } else {
                buf.append(".NULL");
            }
        });
        return buf.toString();
    }

    static final Map<Class<?>, String> INVALID_CODE_BY_CLASS = Stream.of(new Object[][]{
            {BigDecimal.class, ERR_CODE_IS_NOT_NUMBER},
            {byte[].class, ERR_CODE_IS_NOT_BASE64},
            {Boolean.class, ERR_CODE_IS_NOT_BOOLEAN},
            {boolean.class, ERR_CODE_IS_NOT_BOOLEAN},
            {Double.class, ERR_CODE_IS_NOT_DOUBLE},
            {double.class, ERR_CODE_IS_NOT_DOUBLE},
            {Float.class, ERR_CODE_IS_NOT_FLOAT},
            {float.class, ERR_CODE_IS_NOT_FLOAT},
            {Integer.class, ERR_CODE_IS_NOT_INT},
            {int.class, ERR_CODE_IS_NOT_INT},
            {Long.class, ERR_CODE_IS_NOT_LONG},
            {long.class, ERR_CODE_IS_NOT_LONG},
            {LocalDate.class, ERR_CODE_IS_NOT_DATE},
            {LocalDateTime.class, ERR_CODE_IS_NOT_DATE_TIME},
            {ZonedDateTime.class, ERR_CODE_IS_NOT_DATE_TIME},
    }).collect(Collectors.toMap(data -> (Class<?>) data[0], data -> (String) data[1]));

    @RequiredArgsConstructor
    class MatmExceptionConverter {
        private final MethodArgumentTypeMismatchException e;

        ValidationResult convert() {
            String code = INVALID_CODE_BY_CLASS.get(getDataType());
            return code != null ? makeValidationResult(getField(), code) : makeUnknownValidationResult();
        }

        private String getField() {
            return e.getName() + getParamLocation() + getListIndex();
        }

        private String getParamLocation() {
            return isAnnotated(RequestHeader.class) ? " (HEADER parameter)"
                    : isAnnotated(RequestParam.class) ? " (QUERY parameter)"
                    : "";
        }

        private String getListIndex() {
            if (isListParam()) {
                if (e.getCause() instanceof ConversionFailedException && e.getValue() instanceof String[]) {
                    ConversionFailedException cfe = (ConversionFailedException) e.getCause();
                    int index = Arrays.asList((Object[]) e.getValue()).indexOf(cfe.getValue());
                    return String.format("[%d]", index);
                }
                return "[?]";
            }
            return "";
        }

        private Class<?> getDataType() {
            if (isListParam()) {
                Type parameterizedType = e.getParameter().getParameter().getParameterizedType();
                if (parameterizedType instanceof ParameterizedType) {
                    Type[] params = ((ParameterizedType) parameterizedType).getActualTypeArguments();
                    if (params.length > 0 && params[0] instanceof Class) {
                        return (Class<?>) params[0];
                    }
                }
            }
            return e.getParameter().getParameterType();
        }

        private boolean isListParam() {
            return e.getParameter().getParameterType() == List.class;
        }

        private boolean isAnnotated(Class<? extends Annotation> annotation) {
            return e.getParameter().getParameterAnnotation(annotation) != null;
        }
    }
}
