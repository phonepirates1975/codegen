Sample project illustrating the use of the REST server code generator.

## Sub projects

### app

Project (as Spring Boot application) with REST server.

### metamodel

Project with an API model [example-api.yaml](metamodel/src/main/resources/example-api.yaml) and a generator configuration [GeneratorRunner.java](metamodel/src/main/java/GeneratorRunner.java).

## HOWTO

### generate server codes
`mvn -f metamodel/pom.xml compile exec:java`

After generating the code, you need to add code implementing the echo operation.

Simple put code:

`return new EchoResponse().set200(request.getRequestBody());`

as implementation of the example.application.EchoFacadeImpl.echo method.

Remove also the @Generated annotation. This will prevent class overwriting when you regenerate the code.

### run server
`mvn -f app/pom.xml spring-boot:run`

### test server
```
curl --request POST \
  -H "Content-Type: application/json" -H "X-Correlation-ID: 123" \
  --data '{"prop_int":2,"prop_float":1.1,"prop_double":10.01,"prop_amount":"11.11"}' \
  http://localhost:8080/api/v1/echo
```