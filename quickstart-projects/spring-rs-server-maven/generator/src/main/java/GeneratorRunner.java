import pl.metaprogramming.codegen.Codegen;
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator;
import pl.metaprogramming.codegen.java.spring.SpringRestServiceGenerator;
import pl.metaprogramming.model.oas.OpenapiParser;

import java.io.File;

public class GeneratorRunner {

    public static void main(String[] args) {
        generate(args[0]);
    }

    static void generate(String baseDir) {
        new Codegen()
                .setBaseDir(new File(baseDir + "/.."))
                .setIndexFile(new File(baseDir + "/src/main/resources/GeneratedFiles.yaml"))
                .addCommonModule(SpringCommonsGenerator.of(cfg -> cfg
                        .setRootPackage("example.commons")
                        .setProjectDir("app")))
                .addModule(SpringRestServiceGenerator.of(OpenapiParser.parse(baseDir + "/src/main/resources/example-api.yaml"), cfg -> cfg
                        .setRootPackage("example")
                        .setProjectDir("app")))
                .generate();
    }
}
