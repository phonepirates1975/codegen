import pl.metaprogramming.codegen.Codegen;
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator;
import pl.metaprogramming.codegen.java.spring.SpringSoapClientGenerator;
import pl.metaprogramming.model.wsdl.WsdlApi;
import pl.metaprogramming.model.wsdl.WsdlParser;
import pl.metaprogramming.model.wsdl.WsdlParserConfig;

import java.io.File;

public class GeneratorRunner {

    public static void main(String[] args) {
        generate(args[0]);
    }

    private static void generate(String baseDir) {
        WsdlApi wsdlApi = WsdlParser.parse(
                baseDir + "/src/main/resources/countries.wsdl",
                new WsdlParserConfig()
                        .setServiceNameMapper(name -> name.replace("PortService", "")));
        new Codegen()
                .setBaseDir(new File(baseDir + "/.."))
                .setIndexFile(new File(baseDir + "/src/main/resource/GeneratedFiles.yaml"))
                .addCommonModule(SpringCommonsGenerator.of(cfg -> cfg
                        .setRootPackage("example.commons")
                        .setProjectDir("app")))
                .addModule(SpringSoapClientGenerator.of(wsdlApi, cfg -> cfg
                        .setRootPackage("example.ws")
                        .setNamespacePackage("http://spring.io/guides/gs-producing-web-service", "example.ws.schema")
                        .setProjectDir("app")))
                .generate();
    }

}
