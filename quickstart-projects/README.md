Quick start projects to help make first steps with codegen:
 - [spring-rs-client-gradle](spring-rs-client-gradle)
 - [spring-rs-client-maven](spring-rs-client-maven)
 - [spring-rs-server-gradle](spring-rs-server-gradle)
 - [spring-rs-server-maven](spring-rs-server-maven)
 - [spring-soap-client-gradle](spring-soap-client-gradle)
 - [spring-soap-client-maven](spring-soap-client-maven)
 