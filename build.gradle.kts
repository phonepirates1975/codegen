val groovyVersion: String by project
val snakeyamlVersion: String by project
val spockVersion: String by project
val logbackVersion: String by project
val roasterVersion: String by project
val eclipseJdtVersion: String by project
val testcontainersVersion: String by project
val commonsIoVersion: String by project
val junitVersion: String by project

plugins {
    groovy
    `java-library`
    `java-test-fixtures`
    `maven-publish`
    signing
    jacoco
    kotlin("jvm") version "1.6.0"
    id("org.jetbrains.dokka") version "1.6.0"
    id("org.sonarqube") version "3.3"
    id("com.diffplug.spotless-changelog")
    id("io.github.gradle-nexus.publish-plugin") version "1.1.0"
    id("com.adarshr.test-logger") version "3.0.0"
}

repositories {
    mavenCentral()
}

java {
    targetCompatibility = JavaVersion.VERSION_1_8
    sourceCompatibility = JavaVersion.VERSION_1_8
    withJavadocJar()
    withSourcesJar()
    sourceSets.create("smokeTest") {
        compileClasspath += sourceSets.main.get().output
        runtimeClasspath += sourceSets.main.get().output
    }
}

configurations.named("compileClasspath") {
    resolutionStrategy.force("org.codehaus.groovy:groovy-xml:$groovyVersion")
}

dependencies {
    api("org.codehaus.groovy:groovy:$groovyVersion")
    api("org.codehaus.groovy:groovy-templates:$groovyVersion")
    api("org.codehaus.groovy:groovy-json:$groovyVersion")
    api("org.codehaus.groovy:groovy-dateutil:$groovyVersion")
    api("org.yaml:snakeyaml:$snakeyamlVersion")
    api("com.predic8:soa-model-core:1.6.4")

    testFixturesApi("org.spockframework:spock-core:$spockVersion")
    testFixturesApi("ch.qos.logback:logback-classic:$logbackVersion")
    testFixturesApi("org.jboss.forge.roaster:roaster-jdt:$roasterVersion")

    testImplementation("org.eclipse.jdt:org.eclipse.jdt.core:$eclipseJdtVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:$junitVersion")

    val smokeTestImplementation = configurations.getByName("smokeTestImplementation") {
        extendsFrom(configurations["implementation"])
    }
    smokeTestImplementation("org.testcontainers:spock:$testcontainersVersion")
    smokeTestImplementation("commons-io:commons-io:$commonsIoVersion")
    smokeTestImplementation(testFixtures(project))
}

spotlessChangelog {
    versionSchema(CodegenVersionFunction::class.java)
}

if (project.hasProperty("sonatypeUsername")) {
    nexusPublishing {
        repositories {
            sonatype()
        }
    }

    publishing {
        publications {
            create<MavenPublication>("mavenJava") {
                from(components["java"])
                pom {
                    name.set("pl.metaprogramming:codegen")
                    description.set("Java code generator framework")
                    url.set("http://metaprogramming.pl")
                    licenses {
                        license {
                            name.set("The Apache License, Version 2.0")
                            url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
                        }
                    }
                    developers {
                        developer {
                            id.set("waldenko")
                            name.set("Dawid Walczak")
                            email.set("dawid.m.walczak@gmail.com")
                        }
                    }
                    scm {
                        url.set("https://gitlab.com/metaprogramming/codegen")
                        connection.set("scm:git:https://gitlab.com/metaprogramming/codegen.git")
                        developerConnection.set("scm:git:ssh://git@gitlab.com:metaprogramming/codegen.git")
                    }
                }
            }
        }
    }

    signing {
        sign(publishing.publications["mavenJava"])
    }
} else {
    publishing {
        publications {
            create<MavenPublication>("mavenJava") {
                from(components["java"])
            }
        }
    }
}

tasks {

    compileGroovy {
        groovyOptions.configurationScript = file("gradle/config/groovyc.groovy")
        classpath += files("${buildDir}/classes/kotlin/main")
        //classpath += files(sourceSets.main.get().kotlin.classesDirectory)
        // It doesn't work (https://discuss.gradle.org/t/how-to-access-kotlin-source-set-directory-using-the-kotlin-dsl/36116/3)
        // sourceSets.main.get().kotlin.classesDirectory is undefined
        //val SourceSet.kotlin: SourceDirectorySet
        //    get() = project.extensions.getByType<org.jetbrains.kotlin.gradle.dsl.KotlinJvmProjectExtension>().sourceSets.getByName(name).kotlin
    }

    test {
        useJUnitPlatform()
        extensions.configure(JacocoTaskExtension::class) {
            setDestinationFile(file("$buildDir/jacoco/test-java-${JavaVersion.current()}.exec"))
        }
    }

    jacocoTestReport {
        executionData(fileTree(project.rootDir.absolutePath).include("**/build/jacoco/*.exec"))
        reports {
            xml.required.set(true)
            html.required.set(true)
        }
    }

    named<Jar>("javadocJar") {
        archiveClassifier.set("javadoc")
        from(named("groovydoc"), named("dokkaJavadoc"))
        duplicatesStrategy = DuplicatesStrategy.INCLUDE
    }

    register<UpdateWebsite>("updateWebsite")
    register<SetupNextVersion>("setupNextVersion")
    register<SetupReleaseVersion>("setupReleaseVersion")
    register<SetupGradleTestProjects>("setupGradleInTestProjects")
    register<SetupApiInQuickstartProjects>("setupApiInQuickstartProjects")

    register<Test>("smokeTest") {
        group = "verification"
        dependsOn("publishToMavenLocal")
        useJUnitPlatform()
        outputs.upToDateWhen { false }
        val smokeSourceSets = sourceSets.getByName("smokeTest")
        testClassesDirs = smokeSourceSets.output.classesDirs
        classpath = smokeSourceSets.runtimeClasspath
        testLogging {
            showStandardStreams = true
        }
        extensions.configure(JacocoTaskExtension::class) {
            setDestinationFile(file("$buildDir/jacoco/smoke-test-java-${JavaVersion.current()}.exec"))
        }
    }
}
