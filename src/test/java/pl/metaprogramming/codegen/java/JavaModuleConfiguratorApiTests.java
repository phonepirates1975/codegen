/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class JavaModuleConfiguratorApiTests {

    private final String SERVICE = "SERVICE";
    private final String RESPONSE_DTO = "RESPONSE_DTO";
    private final String REQUEST_DTO = "REQUEST_DTO";

    static class TestConfigurator extends JavaModuleConfigurator<TestConfigurator> {
    }

    @Test
    void verifyTypeOfCode() {
        TestConfigurator configurator = new TestConfigurator();

        configurator.typeOfCode(SERVICE).onDeclaration(b ->
                b.addMapper("execute", m -> {
                    m.setResultType(b.getClass(RESPONSE_DTO));
                    m.addParam("request", b.getClass(REQUEST_DTO));
                    m.setImplBody("// TODO implement me");
                }));

        assertNotNull(configurator.getClassBuilderConfigs().get(SERVICE));
    }
}
