/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java;

import kotlin.Unit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static pl.metaprogramming.codegen.java.ClassCd.stringType;

class MethodCmApiTests {

    @Test
    void verifyCreate() {
        String implBody = "impl;";
        String name = "methodName";
        String paramName = "param1";
        ClassCd resultType = ClassCd.of("packageName.ClassName");
        MethodCm value = MethodCm.of(name)
                .resultType(resultType)
                .staticModifier()
                .addParam(paramName, ClassCd.stringType())
                .implBody(implBody);

        assertEquals(name, value.getName());
        assertEquals(1, value.getParams().size());
        assertEquals(paramName, value.getParams().get(0).getName());
        assertTrue(value.isStatic());
        assertTrue(value.isPublic());
        assertFalse(value.isConstructor());
    }

    @Test
    void verifyCreateWithKtBuilder() {
        String implBody = "impl;";
        String name = "methodName";
        String paramName = "param1";
        ClassCd resultType = ClassCd.of("packageName.ClassName");
        MethodCm value = MethodCm.of(name, it -> {
            it.setResultType(resultType);
            it.staticModifier();
            it.addParam(paramName, ClassCd.stringType());
            it.setImplBody(implBody);
            return Unit.INSTANCE;
        });

        assertEquals(name, value.getName());
        assertEquals(1, value.getParams().size());
        assertEquals(paramName, value.getParams().get(0).getName());
        assertTrue(value.isStatic());
        assertTrue(value.isPublic());
        assertFalse(value.isConstructor());
    }
}
