/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.spring

import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.GeneratorSpecification
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator
import pl.metaprogramming.codegen.java.spring.SpringRestService2tGenerator
import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.java.JavaModuleParams
import pl.metaprogramming.codegen.java.spring.SpringRestParams
import pl.metaprogramming.codegen.java.spring.base.DiAutowiredBuildStrategy
import pl.metaprogramming.codegen.java.spring.base.ScopePrototypeBuildStrategy
import pl.metaprogramming.codegen.java.validation.ValidationParams
import pl.metaprogramming.utils.CheckUtils
import pl.metaprogramming.utils.ClassShadow
import pl.metaprogramming.utils.RestMetaModels

import static pl.metaprogramming.utils.JavaCodeGenerationTestUtils.methodShadow

class SpringRestService2tGeneratorRestApiSpec extends GeneratorSpecification {

    static String MODULE_COMMONS_API = 'commons-api'
    static String MODULE_ECHO_API = 'echo-api'

    static String CUSTOM_CLASS_TYPE = 'OPERATION_CUSTOM_CLASS'

    @Override
    Codegen makeCodegen() {
        def params = new CodegenParams()
                .with(new SpringRestParams(
                        controllerPerOperation: false,
                        staticFactoryMethodForRestResponse: false,
                        payloadField: 'payload'
                ))
                .with(new JavaModuleParams(diStrategy: new DiAutowiredBuildStrategy()))
                .with(new ValidationParams(throwExceptionIfValidationFailed: false))

        new Codegen()
                .addCommonModule(SpringCommonsGenerator.of(params) {
                    it.setRootPackage('commons')
                })
                .addModule(SpringRestService2tGenerator.of(RestMetaModels.commonsRestServices, params) {
                    it.setRootPackage('echo')
                })
                .addModule(SpringRestService2tGenerator.of(RestMetaModels.echoRestServices, params) {
                    it.typeOfCodesForOperation.add(CUSTOM_CLASS_TYPE)
                    it.addClass(CUSTOM_CLASS_TYPE, 'OperationAction', ScopePrototypeBuildStrategy.instance)
                            .generateAlways(CUSTOM_CLASS_TYPE)
                            .setRootPackage('echo')
                            .setPackage('echo.op', CUSTOM_CLASS_TYPE)
                })
    }

    @Override
    Map<String, List<ClassShadow>> makeClassesToCheck() {
        [(MODULE_ECHO_API)   : classesToCheckForEchoApi(),
         (MODULE_COMMONS_API): classesToCheckForCommonsApi()]
    }

    @Override
    Map<String, List<String>> makeExpectedClasses() {
        [(MODULE_COMMONS)    : expectedClassesForCommons(),
         (MODULE_COMMONS_API): expectedClassesForCommonsApi(),
         (MODULE_ECHO_API)   : expectedClassesForEchoApi()
        ]
    }

    @Override
    protected List<ClassShadow> classesToCheckForCommons() {
        def result = super.classesToCheckForCommons()
        result.removeAll {
            [
                    'commons.SerializationUtils',
                    'commons.RestResponseBase']
                    .contains(it.name)
        }
        result
    }

    def classesToCheckForCommonsApi() {
        [new ClassShadow(
                name: 'echo.ports.in.rest.dtos.BadRequestDto',
                classHeader: 'public class BadRequestDto',
                annotations: CheckUtils.DTO_ANNOTATIONS,
                imports: CheckUtils.DTO_IMPORTS + CheckUtils.NULLABLE_IMPORTS + ['java.util.List'],
                fields: ['@Nullable private List<FieldValidationDto> errors']
        ),
         new ClassShadow(
                 name: 'echo.adapters.in.rest.dtos.BadRequestRdto',
                 classHeader: 'public class BadRequestRdto',
                 annotations: CheckUtils.RAW_DTO_ANNOTATIONS,
                 fields: [
                         '@JsonProperty("errors") private List<FieldValidationRdto> errors',
                         'public static final Field<BadRequestRdto,List<FieldValidationRdto>> FIELD_ERRORS = new Field<>("errors", BadRequestRdto::getErrors)',
                 ],
                 imports: [
                         'com.fasterxml.jackson.annotation.JsonAutoDetect',
                         'com.fasterxml.jackson.annotation.JsonProperty',
                         'commons.validator.Field',
                         'java.util.List',
                         'lombok.Data',
                         'lombok.NoArgsConstructor',
                         'lombok.experimental.Accessors',
                 ],
         ),
         new ClassShadow(
                 name: 'echo.adapters.in.rest.mappers.BadRequestMapper',
                 classHeader: 'public class BadRequestMapper',
                 annotations: CheckUtils.COMPONENT_ANNOTATIONS_LEGACY,
                 fields: [
                         '@Autowired private FieldValidationMapper fieldValidationMapper'],
                 methods: [
                         methodShadow('map2BadRequestRdto', 'public BadRequestRdto map2BadRequestRdto(BadRequestDto value)',
                                 ['return value == null ? null : map(new BadRequestRdto(), value);']),
                         methodShadow('map', 'public BadRequestRdto map(BadRequestRdto result, BadRequestDto value)',
                                 ['return result',
                                  '        .setErrors(SerializationUtils.transformList(value.getErrors(), fieldValidationMapper::map2FieldValidationRdto))',
                                  '        ;',
                                 ]),
                 ],
                 imports: [
                         'commons.SerializationUtils',
                         'echo.adapters.in.rest.dtos.BadRequestRdto',
                         'echo.ports.in.rest.dtos.BadRequestDto',
                         'org.springframework.beans.factory.annotation.Autowired',
                         'org.springframework.stereotype.Component',
                 ],
         ),
        ]
    }

    List<ClassShadow> classesToCheckForEchoApi() {
        [new ClassShadow(
                name: 'echo.ports.in.rest.dtos.DataFormDto',
                classHeader: 'public class DataFormDto',
                annotations: CheckUtils.DTO_ANNOTATIONS,
                imports: CheckUtils.DTO_IMPORTS + CheckUtils.NONNULL_NULLABLE_IMPORTS + ['java.math.BigDecimal', 'java.time.LocalDate', 'java.util.List'],
                fields: [
                        '@Nonnull private Integer dInt',
                        '@Nonnull private Long dLong',
                        '@Nonnull private String dRString',
                        '@Nullable private BigDecimal dDecimal',
                        '@Nullable private List<Long> dListLong',
                        '@Nullable private LocalDate dDate',
                        '@Nullable private String dOString',
                ]
        ),
         new ClassShadow(
                 name: 'echo.adapters.in.rest.dtos.DataFormRdto',
                 classHeader: 'public class DataFormRdto',
                 annotations: CheckUtils.RAW_DTO_ANNOTATIONS,
                 fields: [
                         '@JsonProperty("d_date") private String dDate',
                         '@JsonProperty("d_decimal") @JsonRawValue private String dDecimal',
                         '@JsonProperty("d_int") @JsonRawValue private String dInt',
                         '@JsonProperty("d_list_long") @JsonRawValue private List<String> dListLong',
                         '@JsonProperty("d_long") @JsonRawValue private String dLong',
                         '@JsonProperty("d_o_string") private String dOString',
                         '@JsonProperty("d_r_string") private String dRString',
                         'public static final Field<DataFormRdto,List<String>> FIELD_D_LIST_LONG = new Field<>("d_list_long", DataFormRdto::getDListLong)',
                         'public static final Field<DataFormRdto,String> FIELD_D_DATE = new Field<>("d_date", DataFormRdto::getDDate)',
                         'public static final Field<DataFormRdto,String> FIELD_D_DECIMAL = new Field<>("d_decimal", DataFormRdto::getDDecimal)',
                         'public static final Field<DataFormRdto,String> FIELD_D_INT = new Field<>("d_int", DataFormRdto::getDInt)',
                         'public static final Field<DataFormRdto,String> FIELD_D_LONG = new Field<>("d_long", DataFormRdto::getDLong)',
                         'public static final Field<DataFormRdto,String> FIELD_D_O_STRING = new Field<>("d_o_string", DataFormRdto::getDOString)',
                         'public static final Field<DataFormRdto,String> FIELD_D_R_STRING = new Field<>("d_r_string", DataFormRdto::getDRString)',
                 ],
                 imports: [
                         'com.fasterxml.jackson.annotation.JsonAutoDetect',
                         'com.fasterxml.jackson.annotation.JsonProperty',
                         'com.fasterxml.jackson.annotation.JsonRawValue',
                         'commons.validator.Field',
                         'java.util.List',
                         'lombok.Data',
                         'lombok.NoArgsConstructor',
                         'lombok.experimental.Accessors',
                 ],
         ),
         new ClassShadow(
                 name: 'echo.ports.in.rest.dtos.EchoPostResponse',
                 classHeader: 'public class EchoPostResponse extends RestResponseBase<EchoPostResponse> implements RestResponse200<EchoPostResponse,DataFormDto>, RestResponse400<EchoPostResponse,BadRequestDto>, RestResponse404<EchoPostResponse,ErrorDto>, RestResponse405<EchoPostResponse,ErrorDto>',
                 annotations: CheckUtils.GEN_ANNOTATIONS,
                 fields: [
                         'private static final Collection<Integer> DECLARED_STATUSES = Arrays.asList(200, 400, 404, 405)',
                 ],
                 methods: [
                         methodShadow('getDeclaredStatuses', 'public Collection<Integer> getDeclaredStatuses()',
                                 ['return DECLARED_STATUSES;']),
                         methodShadow('self', 'public EchoPostResponse self()',
                                 ['return this;']),
                 ],
                 imports: [
                         'commons.RestResponse200',
                         'commons.RestResponse400',
                         'commons.RestResponse404',
                         'commons.RestResponse405',
                         'commons.RestResponseBase',
                         'java.util.Arrays',
                         'java.util.Collection',
                 ],
         ),
         new ClassShadow(
                 name: 'echo.adapters.in.rest.EchoController',
                 classHeader: 'public class EchoController',
                 annotations: CheckUtils.REST_CONTROLLER_ANNOTATIONS,
                 fields: [
                         '@Autowired private EchoComplexRequestMapper echoComplexRequestMapper',
                         '@Autowired private EchoComplexResponseMapper echoComplexResponseMapper',
                         '@Autowired private EchoComplexValidator echoComplexValidator',
                         '@Autowired private EchoFacade echoFacade',
                         '@Autowired private EchoGetRequestMapper echoGetRequestMapper',
                         '@Autowired private EchoGetResponseMapper echoGetResponseMapper',
                         '@Autowired private EchoGetValidator echoGetValidator',
                         '@Autowired private EchoPostRequestMapper echoPostRequestMapper',
                         '@Autowired private EchoPostResponseMapper echoPostResponseMapper',
                         '@Autowired private EchoPostValidator echoPostValidator',
                         '@Autowired private ValidationResultMapper validationResultMapper',
                 ],
                 methods: [
                         methodShadow(
                                 'echoGet',
                                 'public ResponseEntity echoGet(@RequestParam(value = "d_r_string", required = false) String dRString, @RequestParam(value = "d_o_string", required = false) String dOString, @RequestParam(value = "d_int", required = false) String dInt, @RequestParam(value = "d_long", required = false) String dLong, @RequestParam(value = "d_list_long", required = false) List<String> dListLong, @RequestParam(value = "d_decimal", required = false) String dDecimal, @RequestParam(value = "d_date", required = false) String dDate)',
                                 ['return RestRequestHandler.handle(',
                                  '        echoGetRequestMapper.map2EchoGetRrequest(dRString, dOString, dInt, dLong, dListLong, dDecimal, dDate),',
                                  '        echoGetValidator::validate,',
                                  '        validationResultMapper::map,',
                                  '        echoGetRequestMapper::map2EchoGetRequest,',
                                  '        echoFacade::echoGet,',
                                  '        echoGetResponseMapper::map);',
                                 ],
                                 ['@GetMapping(value="/api/v1/echo",produces={"application/json"})']),
                         methodShadow('echoPost', 'public ResponseEntity echoPost(@RequestBody String payload, @RequestBody DataFormRdto requestBody)',
                                 ['return RestRequestHandler.handle(',
                                  '        echoPostRequestMapper.map2EchoPostRrequest(payload, requestBody),',
                                  '        echoPostValidator::validate,',
                                  '        validationResultMapper::map,',
                                  '        echoPostRequestMapper::map2EchoPostRequest,',
                                  '        echoFacade::echoPost,',
                                  '        echoPostResponseMapper::map);',
                                 ],
                                 ['@PostMapping(value="/api/v1/echo",produces={"application/json"},consumes={"application/json"})']),
                 ],
                 imports: [
                         'commons.RestRequestHandler',
                         'commons.validator.ValidationResultMapper',
                         'echo.adapters.in.rest.dtos.DataFormRdto',
                         'echo.adapters.in.rest.dtos.TreeRootRdto',
                         'echo.adapters.in.rest.mappers.EchoComplexRequestMapper',
                         'echo.adapters.in.rest.mappers.EchoComplexResponseMapper',
                         'echo.adapters.in.rest.mappers.EchoGetRequestMapper',
                         'echo.adapters.in.rest.mappers.EchoGetResponseMapper',
                         'echo.adapters.in.rest.mappers.EchoPostRequestMapper',
                         'echo.adapters.in.rest.mappers.EchoPostResponseMapper',
                         'echo.adapters.in.rest.validators.EchoComplexValidator',
                         'echo.adapters.in.rest.validators.EchoGetValidator',
                         'echo.adapters.in.rest.validators.EchoPostValidator',
                         'echo.ports.in.rest.EchoFacade',
                         'java.util.List',
                         'org.springframework.beans.factory.annotation.Autowired',
                         'org.springframework.http.*',
                         'org.springframework.web.bind.annotation.*',
                 ],
         ),
         new ClassShadow(
                 name: 'echo.ports.in.rest.EchoFacade',
                 classHeader: 'public interface EchoFacade',
                 annotations: CheckUtils.GEN_ANNOTATIONS,
                 methods: [
                         methodShadow('echoPost', 'EchoPostResponse echoPost(@Nonnull EchoPostRequest request)', null),
                         methodShadow('echoGet', 'EchoGetResponse echoGet(@Nonnull EchoGetRequest request)', null)
                 ],
                 imports: [
                         'echo.ports.in.rest.dtos.EchoComplexRequest',
                         'echo.ports.in.rest.dtos.EchoComplexResponse',
                         'echo.ports.in.rest.dtos.EchoGetRequest',
                         'echo.ports.in.rest.dtos.EchoGetResponse',
                         'echo.ports.in.rest.dtos.EchoPostRequest',
                         'echo.ports.in.rest.dtos.EchoPostResponse',
                         'javax.annotation.Nonnull',
                 ],
         ),
         new ClassShadow(
                 name: 'echo.application.EchoFacadeImpl',
                 classHeader: 'public class EchoFacadeImpl implements EchoFacade',
                 annotations: CheckUtils.COMPONENT_ANNOTATIONS_LEGACY,
                 methods: [
                         methodShadow('echoPost', 'public EchoPostResponse echoPost(@Nonnull EchoPostRequest request)', ['return null;'], ['@Override']),
                         methodShadow('echoGet', 'public EchoGetResponse echoGet(@Nonnull EchoGetRequest request)', ['return null;'], ['@Override'])
                 ],
                 imports: [
                         'echo.ports.in.rest.EchoFacade',
                         'echo.ports.in.rest.dtos.EchoComplexRequest',
                         'echo.ports.in.rest.dtos.EchoComplexResponse',
                         'echo.ports.in.rest.dtos.EchoGetRequest',
                         'echo.ports.in.rest.dtos.EchoGetResponse',
                         'echo.ports.in.rest.dtos.EchoPostRequest',
                         'echo.ports.in.rest.dtos.EchoPostResponse',
                         'javax.annotation.Nonnull',
                         'org.springframework.stereotype.Component',
                 ],
         ),

         new ClassShadow(
                 name: 'echo.adapters.in.rest.mappers.TreeRootMapper',
                 classHeader: 'public class TreeRootMapper',
                 annotations: CheckUtils.COMPONENT_ANNOTATIONS_LEGACY,
                 fields: ['@Autowired private TreeLeafMapper treeLeafMapper'],
                 methods: [
                         methodShadow('map2TreeRootDto', 'public TreeRootDto map2TreeRootDto(TreeRootRdto value)',
                                 ['return value == null ? null : map(new TreeRootDto(), value);']),
                         methodShadow('map', 'public TreeRootDto map(TreeRootDto result, TreeRootRdto value)',
                                 ['return result',
                                  '        .setComplexField(treeLeafMapper.map2TreeLeafDto(value.getComplexField()))',
                                  '        ;',
                                 ]),
                         methodShadow('map2TreeRootRdto', 'public TreeRootRdto map2TreeRootRdto(TreeRootDto value)',
                                 ['return value == null ? null : map(new TreeRootRdto(), value);']),
                         methodShadow('map', 'public TreeRootRdto map(TreeRootRdto result, TreeRootDto value)',
                                 ['return result',
                                  '        .setComplexField(treeLeafMapper.map2TreeLeafRdto(value.getComplexField()))',
                                  '        ;',
                                 ]),
                 ],
                 imports: [
                         'echo.adapters.in.rest.dtos.TreeRootRdto',
                         'echo.ports.in.rest.dtos.TreeRootDto',
                         'org.springframework.beans.factory.annotation.Autowired',
                         'org.springframework.stereotype.Component',
                 ],
         ),
        ]
    }

    List<String> expectedClassesForCommons() {
        ['commons.EnumValue',
         'commons.RestRequestHandler',
         'commons.RestResponse',
         'commons.RestResponse200',
         'commons.RestResponse400',
         'commons.RestResponse404',
         'commons.RestResponse405',
         'commons.RestResponseBase',
         'commons.ValueHolder',
         'commons.validator.Checker',
         'commons.validator.CommonCheckers',
         'commons.validator.Field',
         'commons.validator.SimpleChecker',
         'commons.validator.ValidationContext',
         'commons.validator.ValidationError',
         'commons.validator.ValidationResult',
         'commons.validator.ValidationResultMapper',
         'commons.validator.Validator',
         'commons.SerializationUtils',
        ]
    }

    List<String> expectedClassesForCommonsApi() {
        ['echo.adapters.in.rest.dtos.BadRequestRdto',
         'echo.adapters.in.rest.dtos.ErrorRdto',
         'echo.adapters.in.rest.dtos.FieldValidationRdto',
         'echo.adapters.in.rest.mappers.BadRequestMapper',
         'echo.adapters.in.rest.mappers.ErrorMapper',
         'echo.adapters.in.rest.mappers.FieldValidationMapper',
         'echo.ports.in.rest.dtos.BadRequestDto',
         'echo.ports.in.rest.dtos.ErrorDto',
         'echo.ports.in.rest.dtos.FieldValidationDto',]
    }

    List<String> expectedClassesForEchoApi() {
        ['echo.adapters.in.rest.EchoController',
         'echo.adapters.in.rest.dtos.DataFormRdto',
         'echo.adapters.in.rest.dtos.EchoComplexRrequest',
         'echo.adapters.in.rest.dtos.EchoGetRrequest',
         'echo.adapters.in.rest.dtos.EchoPostRrequest',
         'echo.adapters.in.rest.dtos.TreeLeafRdto',
         'echo.adapters.in.rest.dtos.TreeRootRdto',
         'echo.adapters.in.rest.mappers.DataFormMapper',
         'echo.adapters.in.rest.mappers.EchoComplexRequestMapper',
         'echo.adapters.in.rest.mappers.EchoComplexResponseMapper',
         'echo.adapters.in.rest.mappers.EchoGetRequestMapper',
         'echo.adapters.in.rest.mappers.EchoGetResponseMapper',
         'echo.adapters.in.rest.mappers.EchoPostRequestMapper',
         'echo.adapters.in.rest.mappers.EchoPostResponseMapper',
         'echo.adapters.in.rest.mappers.TreeLeafMapper',
         'echo.adapters.in.rest.mappers.TreeRootMapper',
         'echo.adapters.in.rest.validators.DataFormValidator',
         'echo.adapters.in.rest.validators.EchoComplexValidator',
         'echo.adapters.in.rest.validators.EchoGetValidator',
         'echo.adapters.in.rest.validators.EchoPostValidator',
         'echo.adapters.in.rest.validators.TreeLeafValidator',
         'echo.adapters.in.rest.validators.TreeRootValidator',
         'echo.application.EchoFacadeImpl',
         'echo.op.EchoComplexOperationAction',
         'echo.op.EchoGetOperationAction',
         'echo.op.EchoPostOperationAction',
         'echo.ports.in.rest.EchoFacade',
         'echo.ports.in.rest.dtos.DataFormDto',
         'echo.ports.in.rest.dtos.EchoComplexRequest',
         'echo.ports.in.rest.dtos.EchoComplexResponse',
         'echo.ports.in.rest.dtos.EchoGetRequest',
         'echo.ports.in.rest.dtos.EchoGetResponse',
         'echo.ports.in.rest.dtos.EchoPostRequest',
         'echo.ports.in.rest.dtos.EchoPostResponse',
         'echo.ports.in.rest.dtos.TreeLeafDto',
         'echo.ports.in.rest.dtos.TreeRootDto',
        ]
    }
}
