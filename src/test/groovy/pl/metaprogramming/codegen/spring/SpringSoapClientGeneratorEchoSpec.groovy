/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.spring

import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.GeneratorSpecification
import pl.metaprogramming.codegen.java.base.PackageInfoCm
import pl.metaprogramming.codegen.java.formatter.PackageInfoFormatter
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator
import pl.metaprogramming.codegen.java.spring.SpringSoapClientGenerator
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.wsdl.WsdlApi
import pl.metaprogramming.model.wsdl.WsdlParser
import pl.metaprogramming.model.wsdl.WsdlParserConfig
import pl.metaprogramming.model.wsdl.parser.XsTypeParser
import pl.metaprogramming.utils.CheckUtils
import pl.metaprogramming.utils.ClassShadow

import static pl.metaprogramming.codegen.java.jaxb.JaxbTypeOfCode.LOCAL_DATE_ADAPTER
import static pl.metaprogramming.codegen.java.jaxb.JaxbTypeOfCode.LOCAL_DATE_TIME_ADAPTER
import static pl.metaprogramming.codegen.java.spring.SpringCommonsTypeOfCode.ENDPOINT_PROVIDER
import static pl.metaprogramming.utils.CheckUtils.checkList
import static pl.metaprogramming.utils.JavaCodeGenerationTestUtils.methodShadow

class SpringSoapClientGeneratorEchoSpec extends GeneratorSpecification {

    static final String MODULE_ECHO = 'echoService'

    def "check package-info"() {
        when:
        def packageInfoTask = tasks[MODULE_ECHO].find { it.destFilePath.endsWith('package-info.java') }
        then:
        packageInfoTask.codeModel instanceof PackageInfoCm
        packageInfoTask.formatter instanceof PackageInfoFormatter

        when:
        def body = packageInfoTask.formatter.format(packageInfoTask.codeModel)
        then:
        checkList('package-info.java body', body.readLines(), [
                "@${generatedAnnotationClass()}(\"pl.metaprogramming.codegen\")",
                '@javax.xml.bind.annotation.XmlSchema(namespace = "http://example.com/echo")',
                'package example.ws.schema;',
        ])
    }

    @Override
    Codegen makeCodegen() {
        new Codegen()
                .addCommonModule(SpringCommonsGenerator.of {
                    it.setRootPackage('commons')
                    it.setPackage('commons.jaxb', LOCAL_DATE_ADAPTER, LOCAL_DATE_TIME_ADAPTER)
                    it.setPackage('example.ws', ENDPOINT_PROVIDER)
                })
                .addModule(SpringSoapClientGenerator.of(loadApi()) {
                    it.setRootPackage('example.ws')
                    it.setNamespacePackage('http://example.com/echo', 'example.ws.schema')
                    it.setNamespacePackage('http://example.com/echo1', 'example.ws.schema.ns1')
                    it.setNamespacePackage('http://example.com/echo2', 'example.ws.schema.ns2')
                })
    }

    @Override
    Map<String, List<ClassShadow>> makeClassesToCheck() {
        [(MODULE_COMMONS): classesToCheckForCommons(),
         (MODULE_ECHO)                        : classesToCheckForEchoApi()]
    }

    @Override
    Map<String, List<String>> makeExpectedClasses() {
        [(MODULE_COMMONS): expectedClassesForCommons(),
         (MODULE_ECHO)                        : expectedClassesForEchoApi()]
    }

    private WsdlApi loadApi() {
        WsdlParser.parse(
                'src/test/resources/wsdl/echo.wsdl',
                new WsdlParserConfig(
                        serviceNameMapper: { it.replace('PortService', '') },
                        xsTypeParser: new XsTypeParser().set('double', DataType.DECIMAL)
                )
        ).removeOperations { it.name == 'echoExtendedSkipped' }
    }

    List<ClassShadow> classesToCheckForCommons() {
        [new ClassShadow(
                name: 'example.ws.EndpointProvider',
                annotations: CheckUtils.COMPONENT_ANNOTATIONS_LEGACY,
                classHeader: 'public class EndpointProvider',
                fields: ['@Value("${BASE_URL:http://localhost:8080}") private String baseUrl'],
                methods: [
                        methodShadow('getEndpoint', 'public String getEndpoint(String path)',
                                ['return baseUrl + path;'],
                        ),
                ],
                imports: [
                        'org.springframework.beans.factory.annotation.Value',
                        'org.springframework.stereotype.Component',
                ],
        ),
        ]
    }

    List<ClassShadow> classesToCheckForEchoApi() {
        [new ClassShadow(
                name: 'example.ws.EchoServiceClient',
                annotations: CheckUtils.GEN_ANNOTATIONS,
                classHeader: 'public class EchoServiceClient extends WebServiceGatewaySupport',
                methods: [
                        methodShadow('echo', 'public Message echo(Message request)',
                                ['return (Message) getWebServiceTemplate().marshalSendAndReceive(request);'],
                        ),
                ],
                imports: [
                        'example.ws.schema.ExtendedType',
                        'example.ws.schema.Message',
                        'org.springframework.ws.client.core.support.WebServiceGatewaySupport',
                ],
        ),
         new ClassShadow(
                 name: 'example.ws.EchoServiceClientConfiguration',
                 annotations: ['@RequiredArgsConstructor',
                               '@Configuration',
                               '@Generated("pl.metaprogramming.codegen")',
                 ],
                 classHeader: 'public class EchoServiceClientConfiguration',
                 fields: ['private final EndpointProvider endpointProvider'],
                 methods: [
                         methodShadow('createEchoServiceClient', 'public EchoServiceClient createEchoServiceClient()',
                                 ['Jaxb2Marshaller marshaller = new Jaxb2Marshaller();',
                                  'marshaller.setPackagesToScan("example.ws.schema", "example.ws.schema.ns1");',
                                  'EchoServiceClient client = new EchoServiceClient();',
                                  'client.setDefaultUri(endpointProvider.getEndpoint("/ws/echo"));',
                                  'client.setMarshaller(marshaller);',
                                  'client.setUnmarshaller(marshaller);',
                                  'return client;',
                                 ],
                                 ['@Bean']
                         ),
                 ],
                 imports: [
                         'lombok.RequiredArgsConstructor',
                         'org.springframework.context.annotation.Bean',
                         'org.springframework.context.annotation.Configuration',
                         'org.springframework.oxm.jaxb.Jaxb2Marshaller',
                 ],
         ),
         new ClassShadow(
                 name: 'example.ws.schema.Message',
                 annotations: [
                         '@Accessors(chain=true)',
                         '@Data',
                         '@Generated("pl.metaprogramming.codegen")',
                         '@NoArgsConstructor',
                         '@XmlAccessorType(XmlAccessType.FIELD)',
                         '@XmlRootElement(name="message")',
                         '@XmlType(name="",propOrder={"stringField","restrictedField","nillableField","objectField","listStringField","nillableListField","listObjectField","enumField","doubleField","dateField","dateTimeField","uPerCaseField"})',
                 ],
                 fields: [
                         '@Nonnull @XmlElement(required = true) private ObjectType objectField',
                         '@Nonnull @XmlElement(required = true) private String restrictedField',
                         '@Nonnull @XmlElement(required = true) private String stringField',
                         '@Nullable @XmlElement(nillable = true) private List<String> nillableListField',
                         '@Nullable @XmlElement(required = true, nillable = true) private String nillableField',
                         '@Nullable private BigDecimal doubleField',
                         '@Nullable private EnumType enumField',
                         '@Nullable private List<ObjectType> listObjectField',
                         '@Nullable private List<String> listStringField',
                         '@Nullable @XmlJavaTypeAdapter(LocalDateAdapter.class) private LocalDate dateField',
                         '@Nullable @XmlJavaTypeAdapter(LocalDateTimeAdapter.class) private LocalDateTime dateTimeField',
                         '@Nullable @XmlElement(name = "UPerCaseField") private String uPerCaseField',
                 ],
                 imports: [
                         'java.math.BigDecimal',
                         'java.time.LocalDate',
                         'java.time.LocalDateTime',
                         'java.util.List',
                         'javax.annotation.Nonnull',
                         'javax.annotation.Nullable',
                         'javax.xml.bind.annotation.XmlAccessType',
                         'javax.xml.bind.annotation.XmlAccessorType',
                         'javax.xml.bind.annotation.XmlElement',
                         'javax.xml.bind.annotation.XmlRootElement',
                         'javax.xml.bind.annotation.XmlType',
                         'javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter',
                         'lombok.Data',
                         'lombok.NoArgsConstructor',
                         'lombok.experimental.Accessors',
                         'commons.jaxb.LocalDateAdapter',
                         'commons.jaxb.LocalDateTimeAdapter',
                 ],
         ),
         new ClassShadow(
                 name: 'example.ws.schema.EnumType',
                 annotations: ['@Generated("pl.metaprogramming.codegen")',
                               '@XmlEnum',
                               '@XmlType(name="enumType")',
                 ],
                 classHeader: 'public enum EnumType implements EnumValue',
                 enums: ['LOWERCASE("lowercase")',
                         'V_0("0")',
                 ],
                 fields: ['@Getter private final String value'],
                 methods: [
                         methodShadow('EnumType', 'EnumType(String value)',
                                 ['this.value = value;'],
                         ),
//                         methodShadow('fromValue', 'public static EnumType fromValue(String value)',
//                                 ['return EnumValue.fromValue(value, EnumType.class);']
//                         ),
                 ],
                 imports: ['commons.EnumValue',
                           'javax.xml.bind.annotation.XmlEnum',
                           'javax.xml.bind.annotation.XmlEnumValue',
                           'javax.xml.bind.annotation.XmlType',
                           'lombok.Getter',
                 ],
         ),
        ]
    }

    List<String> expectedClassesForCommons() {
        ['commons.EnumValue',
         'commons.jaxb.LocalDateAdapter',
         'commons.jaxb.LocalDateTimeAdapter',
         'example.ws.EndpointProvider',
        ]
    }

    List<String> expectedClassesForEchoApi() {
        ['example.ws.EchoServiceClient',
         'example.ws.EchoServiceClientConfiguration',
         'example.ws.schema.EnumType',
         'example.ws.schema.ExtendedType',
         'example.ws.schema.Message',
         'example.ws.schema.ObjectType',
         'example.ws.schema.ns1.BaseRequest',
         'example.ws.schema.ns1.package-info',
         'example.ws.schema.package-info',
        ]
    }
}
