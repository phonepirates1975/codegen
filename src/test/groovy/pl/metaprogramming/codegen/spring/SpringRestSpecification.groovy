/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.spring

import pl.metaprogramming.codegen.GeneratorSpecification
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.fixtures.ExampleApiModels
import spock.lang.Shared

abstract class SpringRestSpecification extends GeneratorSpecification {

    static final String MODULE_EXAMPLE_API = 'Example API'
    static final String MODULE_COMMONS_API = 'Commons for Example API'

    static final ClassCd AUTH_CTX_CLASS = ClassCd.of('example.auth.AuthContext')

    @Shared
    def commonsApi = ExampleApiModels.loadExampleApiDepsModel(getOpenapiVer())

    @Shared
    def exampleApi = ExampleApiModels.loadExampleApiModel(commonsApi, getOpenapiVer())

    ExampleApiModels.OpenapiVer getOpenapiVer() {
        ExampleApiModels.OpenapiVer.V3
    }

    @Override
    Map<String, List<String>> makeExpectedClasses() {
        [(MODULE_COMMONS): expectedClassesForCommons(),
         (MODULE_COMMONS_API)                 : expectedClassesForCommonsApi(),
         (MODULE_EXAMPLE_API)                 : expectedClassesForExampleApi()]
    }

    abstract List<String> expectedClassesForCommons()

    abstract List<String> expectedClassesForCommonsApi()

    abstract List<String> expectedClassesForExampleApi()
}
