/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.spring

import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.java.JavaModuleParams
import pl.metaprogramming.codegen.java.spring.SpringRestParams
import pl.metaprogramming.codegen.java.spring.base.DiAutowiredBuildStrategy
import pl.metaprogramming.codegen.java.spring.base.SpringDefs
import pl.metaprogramming.fixtures.CodegenParamsProvider
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.utils.CheckUtils
import pl.metaprogramming.utils.ClassShadow

import static pl.metaprogramming.utils.JavaCodeGenerationTestUtils.methodShadow

class SpringRestService2tGeneratorLegacySpec extends SpringRestService2tGeneratorSpecification {

    @Override
    CodegenParams getCodegenParams() {
        new CodegenParams()
                .with(new SpringRestParams(
                        controllerPerOperation: false,
                        staticFactoryMethodForRestResponse: false,
                ))
                .with(new JavaModuleParams()
                        .setDiStrategy(new DiAutowiredBuildStrategy())
                        .setGeneratedAnnotationClass('javax.annotation.Generated')
                        .setDataTypeMapping(DataType.BINARY, SpringDefs.RESOURCE.canonicalName))
                .with(CodegenParamsProvider.makeValidationParams()
                        .setFormatValidator('amount', 'di-bean:example.commons.validator.AmountChecker')
                        .setThrowExceptionIfValidationFailed(false))

    }

    @Override
    protected List<ClassShadow> classesToCheckForCommons() {
        fixClassShadows(super.classesToCheckForCommons()).each {
            if (it.name.endsWith('RestResponseBase')) {
                it.fields = [
                        '@Getter private Integer status',
                        '@Getter private Object body',
                        '@Getter private final Map<String,String> headers = new java.util.TreeMap<>()',]
                it.imports = [
                        'java.util.Map',
                        'javax.annotation.Nonnull',
                        'lombok.Getter',]
            }
        }
    }

    @Override
    protected String generatedAnnotationClass() {
        "javax.annotation.Generated"
    }

    @Override
    List<ClassShadow> classesToCheckForCommonsApi() {
        fixClassShadows(super.classesToCheckForCommonsApi())
    }

    @Override
    List<ClassShadow> classesToCheckForExampleApi() {
        def result = fixClassShadows(super.classesToCheckForExampleApi())
        result.removeAll { it.name.endsWith('Controller') }
        result.addAll(additionalClassesToCheck())
        result
    }

    private List<ClassShadow> fixClassShadows(List<ClassShadow> result) {
        result.each {
            it.annotations.remove('@RequiredArgsConstructor')
            def diFields = it.fields.findAll { it.startsWith('private final') || it.startsWith('@Qualifier')}
            if (diFields) {
                it.fields.removeAll(diFields)
                it.fields.addAll(diFields.collect { it.replace('private final', '@Autowired private') })
                it.imports.remove('lombok.RequiredArgsConstructor')
                it.imports.add('org.springframework.beans.factory.annotation.Autowired')
            }
            if (it.name.endsWith('Validator')
                    && it.name != 'example.commons.adapters.in.rest.validators.SimpleObjectValidator') {
                it.name = replacePackage(it.name, 'example.adapters.in.rest.validators')
                it.imports.removeAll { it.startsWith('example.adapters.in.rest.validators') }
            }
            if (it.name.endsWith('Mapper')
                    && !it.name.endsWith('ErrorDescriptionMapper')
                    && !it.name.endsWith('ValidationResultMapper')) {
                it.name = replacePackage(it.name, 'example.adapters.in.rest.mappers')
                it.imports.removeAll { it.startsWith('example.adapters.in.rest.mappers') }
            }
            if (it.name.endsWith('Response') && it.name != 'commons.RestResponse') {
                it.annotations.remove('@ParametersAreNonnullByDefault')
                it.imports.remove('javax.annotation.ParametersAreNonnullByDefault')
                it.methods.removeAll { it.signature.contains(' static ') }
            }
            fixBinaryDataType(it)
        }
        result
    }

    protected void fixBinaryDataType(ClassShadow shadow) {
        if (shadow.name.endsWith('UploadEchoFileRequest')) {
            shadow.fields.remove('@Nonnull private byte[] requestBody')
            shadow.fields.add('@Nonnull private Resource requestBody')
            shadow.imports.add('org.springframework.core.io.Resource')
        }
        if (shadow.name.endsWith('UploadEchoFileWithFormRequest')) {
            shadow.fields.remove('@Nonnull private byte[] file')
            shadow.fields.add('@Nonnull private Resource file')
            shadow.imports.add('org.springframework.core.io.Resource')
        }
        if (shadow.name.endsWith('DownloadEchoFileResponse')) {
            shadow.classHeader = 'public class DownloadEchoFileResponse extends RestResponseBase<DownloadEchoFileResponse> implements RestResponse200<DownloadEchoFileResponse,Resource>, RestResponse404NoContent<DownloadEchoFileResponse>, RestResponseOther<DownloadEchoFileResponse,ErrorDescriptionDto>'
            shadow.imports.add('org.springframework.core.io.Resource')
        }
        if (shadow.name.endsWith('UploadEchoFileRequestMapper')) {
            shadow.methods.each {
                it.implBody = it.implBody.replace(
                        '.setRequestBody(SerializationUtils.toBytes(value.getRequestBody()))',
                        '.setRequestBody(value.getRequestBody())')
            }
        }
        if (shadow.name.endsWith('UploadEchoFileWithFormRequestMapper')) {
            shadow.methods.each {
                it.implBody = it.implBody.replace(
                        '.setFile(SerializationUtils.toBytes(value.getFile()))',
                        '.setFile(value.getFile())')
            }
        }
        if (shadow.name.endsWith('SerializationUtils')) {
            shadow.imports.removeAll(['lombok.SneakyThrows', 'org.apache.commons.io.IOUtils', 'org.springframework.core.io.Resource'])
            shadow.methods.removeIf({ it.signature == 'public static byte[] toBytes(Resource value)' })
        }
    }

    protected String replacePackage(String canonicalName, String newPackage) {
        newPackage + canonicalName.substring(canonicalName.lastIndexOf('.'))
    }

    protected List<ClassShadow> additionalClassesToCheck() {
        [new ClassShadow(
                name: 'example.adapters.in.rest.EchoController',
                annotations: CheckUtils.REST_CONTROLLER_ANNOTATIONS,
                classHeader: 'public class EchoController',
                fields: [
                        '@Autowired private DeleteFileRequestMapper deleteFileRequestMapper',
                        '@Autowired private DeleteFileResponseMapper deleteFileResponseMapper',
                        '@Autowired private DeleteFileValidator deleteFileValidator',
                        '@Autowired private DownloadEchoFileRequestMapper downloadEchoFileRequestMapper',
                        '@Autowired private DownloadEchoFileResponseMapper downloadEchoFileResponseMapper',
                        '@Autowired private DownloadEchoFileValidator downloadEchoFileValidator',
                        '@Autowired private EchoArraysPostRequestMapper echoArraysPostRequestMapper',
                        '@Autowired private EchoArraysPostResponseMapper echoArraysPostResponseMapper',
                        '@Autowired private EchoArraysPostValidator echoArraysPostValidator',
                        '@Autowired private EchoDateArrayGetRequestMapper echoDateArrayGetRequestMapper',
                        '@Autowired private EchoDateArrayGetResponseMapper echoDateArrayGetResponseMapper',
                        '@Autowired private EchoDateArrayGetValidator echoDateArrayGetValidator',
                        '@Autowired private EchoDefaultsPostRequestMapper echoDefaultsPostRequestMapper',
                        '@Autowired private EchoDefaultsPostResponseMapper echoDefaultsPostResponseMapper',
                        '@Autowired private EchoDefaultsPostValidator echoDefaultsPostValidator',
                        '@Autowired private EchoEmptyRequestMapper echoEmptyRequestMapper',
                        '@Autowired private EchoEmptyResponseMapper echoEmptyResponseMapper',
                        '@Autowired private EchoEmptyValidator echoEmptyValidator',
                        '@Autowired private EchoErrorRequestMapper echoErrorRequestMapper',
                        '@Autowired private EchoErrorResponseMapper echoErrorResponseMapper',
                        '@Autowired private EchoErrorValidator echoErrorValidator',
                        '@Autowired private EchoFacade echoFacade',
                        '@Autowired private EchoGetRequestMapper echoGetRequestMapper',
                        '@Autowired private EchoGetResponseMapper echoGetResponseMapper',
                        '@Autowired private EchoGetValidator echoGetValidator',
                        '@Autowired private EchoPostRequestMapper echoPostRequestMapper',
                        '@Autowired private EchoPostResponseMapper echoPostResponseMapper',
                        '@Autowired private EchoPostValidator echoPostValidator',
                        '@Autowired private UploadEchoFileRequestMapper uploadEchoFileRequestMapper',
                        '@Autowired private UploadEchoFileResponseMapper uploadEchoFileResponseMapper',
                        '@Autowired private UploadEchoFileValidator uploadEchoFileValidator',
                        '@Autowired private UploadEchoFileWithFormRequestMapper uploadEchoFileWithFormRequestMapper',
                        '@Autowired private UploadEchoFileWithFormResponseMapper uploadEchoFileWithFormResponseMapper',
                        '@Autowired private UploadEchoFileWithFormValidator uploadEchoFileWithFormValidator',
                        '@Autowired private ValidationResultMapper validationResultMapper',
                ],
                methods: [
                        methodShadow(
                                'echoPost',
                                'public ResponseEntity echoPost(@RequestHeader(value = "Authorization", required = false) String authorizationParam, @RequestHeader(value = "X-Correlation-ID", required = false) String correlationIdParam, @RequestHeader(value = "timestamp", required = false) String timestampParam, @RequestHeader(value = "Inline-Header-Param", required = false) String inlineHeaderParam, @RequestBody EchoBodyRdto requestBody)',
                                ['return RestRequestHandler.handle(',
                                 '        echoPostRequestMapper.map2EchoPostRrequest(authorizationParam, correlationIdParam, timestampParam, inlineHeaderParam, requestBody),',
                                 '        echoPostValidator::validate,',
                                 '        validationResultMapper::map,',
                                 '        echoPostRequestMapper::map2EchoPostRequest,',
                                 '        echoFacade::echoPost,',
                                 '        echoPostResponseMapper::map);',
                                ],
                                ['@PostMapping(value="/api/v1/echo",produces={"application/json"},consumes={"application/json"})']),
                        methodShadow(
                                'echoArraysPost',
                                'public ResponseEntity echoArraysPost(@RequestHeader(value = "Authorization", required = false) String authorizationParam, @RequestHeader(value = "Inline-Header-Param", required = false) String inlineHeaderParam, @RequestBody List<EchoArraysBodyRdto> requestBody)',
                                ['return RestRequestHandler.handle(',
                                 '        echoArraysPostRequestMapper.map2EchoArraysPostRrequest(authorizationParam, inlineHeaderParam, requestBody),',
                                 '        echoArraysPostValidator::validate,',
                                 '        validationResultMapper::map,',
                                 '        echoArraysPostRequestMapper::map2EchoArraysPostRequest,',
                                 '        echoFacade::echoArraysPost,',
                                 '        echoArraysPostResponseMapper::map);',
                                ],
                                ['@PostMapping(value="/api/v1/echo-arrays",produces={"application/json"},consumes={"application/json"})']),
                        methodShadow(
                                'echoEmpty',
                                'public ResponseEntity echoEmpty()',
                                ['return RestRequestHandler.handle(',
                                 '        echoEmptyRequestMapper.map2EchoEmptyRrequest(),',
                                 '        echoEmptyValidator::validate,',
                                 '        validationResultMapper::map,',
                                 '        echoEmptyRequestMapper::map2EchoEmptyRequest,',
                                 '        echoFacade::echoEmpty,',
                                 '        echoEmptyResponseMapper::map);',
                                ],
                                ['@GetMapping(value="/api/v1/echo-empty",produces={})']),
                ],
                imports: [
                        'commons.RestRequestHandler',
                        'commons.validator.ValidationResultMapper',
                        'example.adapters.in.rest.dtos.EchoArraysBodyRdto',
                        'example.adapters.in.rest.dtos.EchoBodyRdto',
                        'example.adapters.in.rest.dtos.EchoDefaultsBodyRdto',
                        'example.adapters.in.rest.mappers.DeleteFileRequestMapper',
                        'example.adapters.in.rest.mappers.DeleteFileResponseMapper',
                        'example.adapters.in.rest.mappers.DownloadEchoFileRequestMapper',
                        'example.adapters.in.rest.mappers.DownloadEchoFileResponseMapper',
                        'example.adapters.in.rest.mappers.EchoArraysPostRequestMapper',
                        'example.adapters.in.rest.mappers.EchoArraysPostResponseMapper',
                        'example.adapters.in.rest.mappers.EchoDateArrayGetRequestMapper',
                        'example.adapters.in.rest.mappers.EchoDateArrayGetResponseMapper',
                        'example.adapters.in.rest.mappers.EchoDefaultsPostRequestMapper',
                        'example.adapters.in.rest.mappers.EchoDefaultsPostResponseMapper',
                        'example.adapters.in.rest.mappers.EchoEmptyRequestMapper',
                        'example.adapters.in.rest.mappers.EchoEmptyResponseMapper',
                        'example.adapters.in.rest.mappers.EchoErrorRequestMapper',
                        'example.adapters.in.rest.mappers.EchoErrorResponseMapper',
                        'example.adapters.in.rest.mappers.EchoGetRequestMapper',
                        'example.adapters.in.rest.mappers.EchoGetResponseMapper',
                        'example.adapters.in.rest.mappers.EchoPostRequestMapper',
                        'example.adapters.in.rest.mappers.EchoPostResponseMapper',
                        'example.adapters.in.rest.mappers.UploadEchoFileRequestMapper',
                        'example.adapters.in.rest.mappers.UploadEchoFileResponseMapper',
                        'example.adapters.in.rest.mappers.UploadEchoFileWithFormRequestMapper',
                        'example.adapters.in.rest.mappers.UploadEchoFileWithFormResponseMapper',
                        'example.adapters.in.rest.validators.DeleteFileValidator',
                        'example.adapters.in.rest.validators.DownloadEchoFileValidator',
                        'example.adapters.in.rest.validators.EchoArraysPostValidator',
                        'example.adapters.in.rest.validators.EchoDateArrayGetValidator',
                        'example.adapters.in.rest.validators.EchoDefaultsPostValidator',
                        'example.adapters.in.rest.validators.EchoEmptyValidator',
                        'example.adapters.in.rest.validators.EchoErrorValidator',
                        'example.adapters.in.rest.validators.EchoGetValidator',
                        'example.adapters.in.rest.validators.EchoPostValidator',
                        'example.adapters.in.rest.validators.UploadEchoFileValidator',
                        'example.adapters.in.rest.validators.UploadEchoFileWithFormValidator',
                        'example.ports.in.rest.EchoFacade',
                        'java.util.List',
                        'org.springframework.beans.factory.annotation.Autowired',
                        'org.springframework.core.io.Resource',
                        'org.springframework.http.*',
                        'org.springframework.web.bind.annotation.*',
                        'org.springframework.web.multipart.MultipartFile',
                ]
        ),
        ]
    }

    @Override
    List<String> expectedClassesForExampleApi() {
        def result = super.expectedClassesForExampleApi()
        def classesToFix = result.findAll { it.startsWith('example.adapters.in.rest.books') || it.startsWith('example.adapters.in.rest.echo') }
        result.removeAll(classesToFix)
        result.addAll(classesToFix
                .findAll { it.endsWith('Validator') }
                .collect { replacePackage(it, 'example.adapters.in.rest.validators') })
        result.addAll(classesToFix
                .findAll { it.endsWith('Mapper') }
                .collect { replacePackage(it, 'example.adapters.in.rest.mappers') })
        result.addAll([
                'example.adapters.in.rest.EchoController'
        ])
        result
    }

    @Override
    List<String> expectedClassesForCommons() {
        super.expectedClassesForCommons() + [
                'commons.RestRequestHandler',
                'commons.ValueHolder'
        ]
    }
}
