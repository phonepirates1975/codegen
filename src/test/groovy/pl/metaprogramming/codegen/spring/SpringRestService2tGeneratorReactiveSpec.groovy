/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.spring

import pl.metaprogramming.codegen.java.spring.SpringRestService2tGenerator
import pl.metaprogramming.codegen.java.spring.rs2t.ReactiveWrapperBuildStrategy
import pl.metaprogramming.utils.ClassShadow

class SpringRestService2tGeneratorReactiveSpec extends SpringRestService2tGeneratorSpec {

    @Override
    void configure(SpringRestService2tGenerator.Configurator it, String rootPackage) {
        super.configure(it, rootPackage)
        it.typeOfCode(SpringRestService2tGenerator.TOC_REST_CONTROLLER, { it.strategies.add(new ReactiveWrapperBuildStrategy()) })
    }

    @Override
    List<ClassShadow> classesToCheckForExampleApi() {
        def result = super.classesToCheckForExampleApi()
        def controllerClassShadow = result.find { it.name == 'example.adapters.in.rest.echo.EchoPostController' }
        controllerClassShadow.fields.add('private final Scheduler scheduler = Schedulers.newElastic("EchoPost")')
        controllerClassShadow.imports.addAll([
                'reactor.core.publisher.Mono',
                'reactor.core.scheduler.Scheduler',
                'reactor.core.scheduler.Schedulers'])
        def methodShadow = controllerClassShadow.methods[0]
        methodShadow.signature = 'public Mono<ResponseEntity> echoPost(@RequestHeader(value = "Authorization", required = false) String authorizationParam, @RequestHeader(value = "X-Correlation-ID", required = false) String correlationIdParam, @RequestHeader(value = "timestamp", required = false) String timestampParam, @RequestHeader(value = "Inline-Header-Param", required = false) String inlineHeaderParam, @RequestBody EchoBodyRdto requestBody)'
        methodShadow.implBody = [
                'return Mono.fromSupplier(() -> {',
                '    EchoPostRrequest request = echoPostRequestMapper.map2EchoPostRrequest(authorizationParam, correlationIdParam, timestampParam, inlineHeaderParam, requestBody);',
                '    ValidationResult validationResult = echoPostValidator.validate(request);',
                '    return validationResult.isValid()',
                '            ? echoPostResponseMapper.map(echoFacade.echoPost(echoPostRequestMapper.map2EchoPostRequest(request)))',
                '            : validationResultMapper.map(validationResult);',
                '    }).subscribeOn(scheduler);',
        ].join('\n')
        result
    }
}
