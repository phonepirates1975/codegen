/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.generator

import pl.metaprogramming.codegen.Model
import pl.metaprogramming.codegen.ModuleGenerator
import pl.metaprogramming.codegen.java.formatter.JavaCodeFormatter
import pl.metaprogramming.codegen.java.formatter.PackageInfoFormatter
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.ClassCm
import pl.metaprogramming.codegen.java.base.PackageInfoCm
import spock.lang.Shared

import java.util.function.Consumer

import static pl.metaprogramming.codegen.generator.MainGenerator.FileStatus.*

class MainGeneratorSpec extends MainGeneratorSpecification {

    @Shared
    def componentV1 = new DummyModuleGenerator(
            codesToGenerate: [prepareSampleGenerationTask(['package': 'pkg2', 'class': 'Class1']),
                              prepareSampleGenerationTask(['package': 'pkg1', 'class': 'Class2']),
                              preparePackageInfoGenerationTask(['package': 'pkg1']),
            ])

    @Shared
    def componentV2 = new DummyModuleGenerator(
            codesToGenerate: [prepareSampleGenerationTask(['package': 'pkg2', 'class': 'Class1', 'fields': ['textProperty']]),
                              prepareSampleGenerationTask(['package': 'pkg1', 'class': 'Class2']),
                              preparePackageInfoGenerationTask(['package': 'pkg1']),
            ])

    @Shared
    def componentV3 = new DummyModuleGenerator(
            codesToGenerate: [prepareSampleGenerationTask(['package': 'pkg1', 'class': 'Class2'])]
    )

    @Shared
    def componentV4 = new DummyModuleGenerator(
            codesToGenerate: [prepareSampleGenerationTask(['package': 'pkg2', 'class': 'class1']),
                              prepareSampleGenerationTask(['package': 'pkg1', 'class': 'Class2']),
                              preparePackageInfoGenerationTask(['package': 'pkg1']),
            ])


    def "first generation"() {
        expect:
        outDir.isDirectory()
        !indexFile.exists()
        !class1File.exists()
        !class2File.exists()
        !packageInfoFile.exists()

        when:
        def generator = makeCodegen(componentV1).generate()

        then:
        outDir.isDirectory()
        indexFile.exists()
        checkFile class1File, CLASS1_FILE_BODY
        checkFile class2File, CLASS2_FILE_BODY
        checkFile packageInfoFile, PACKAGE_INFO_BODY
        checkGeneration(generator, [
                [path: class2FilePath, md5: class2FileMd5, status: CREATED],
                [path: packageInfoPath, md5: packageInfoMd5, status: CREATED],
                [path: class1FilePath, md5: class1FileMd5, status: CREATED],
        ])
    }

    def "next generation - no changes"() {
        expect:
        runAndCheckGeneration(componentV1)
        changeLastGenerationDate()
        runAndCheckNextGenerationV1()
    }

    def "next generation - no changes - don't set lastGeneration tag"() {
        when:
        makeCodegen(componentV1).setAddLastGenerationTag(false).generate()
        Map fileIndex = loadYaml(indexFile)
        then:
        assert fileIndex['lastGeneration'] == null
    }

    def "next generation - no changes - no index file"() {
        expect:
        runAndCheckGeneration(componentV1)
        deleteIndexFile()

        when:
        def generator = makeCodegen(componentV1).generate()

        then:
        checkFile class1File, CLASS1_FILE_BODY
        checkFile class2File, CLASS2_FILE_BODY
        checkFile packageInfoFile, PACKAGE_INFO_BODY
        checkGeneration(generator, [
                [path: class2FilePath, md5: class2FileMd5, status: UNCHANGED],
                [path: packageInfoPath, md5: packageInfoMd5, status: UNCHANGED],
                [path: class1FilePath, md5: class1FileMd5, status: UNCHANGED],
        ])
    }

    def "next generation - change file name Class1 -> class1"() {
        expect:
        runAndCheckGeneration(componentV1)

        when:
        def generator = makeCodegen(componentV4).generate()

        then:
        checkFile class1LowercaseFile, CLASS1_LOWERCASE_FILE_BODY
        checkFile class2File, CLASS2_FILE_BODY
        checkFile packageInfoFile, PACKAGE_INFO_BODY
        checkGeneration(generator, [
                [path: class2FilePath, md5: class2FileMd5, status: UNCHANGED],
                [path: packageInfoPath, md5: packageInfoMd5, status: UNCHANGED],
                [path: class1FilePath, md5: class1FileMd5, status: ABANDONED],
                [path: class1LowercaseFilePath, md5: class1LowercaseFileMd5, status: CREATED],
        ])
    }

    def "manually updated"() {
        expect:
        runAndCheckGeneration(componentV1)
        overwriteFile(class1File, CLASS1_V2_FILE_BODY)

        when:
        makeCodegen(componentV1).generate()
        then:
        RuntimeException ex = thrown()
        ex.message == 'The file src/main/java/pkg2/Class1.java has been manually modified. Set foreceMode to proceed.'
    }

    def "manually updated -  force mode"() {
        expect:
        runAndCheckGeneration(componentV1)
        overwriteFile(class1File, CLASS1_V2_FILE_BODY)
        runAndCheckNextGenerationV1(true, {
            it.each { it.remove('md5') }
            it.find { it.path == class1FilePath }.status = UPDATED
        })
    }

    def "manually deleted"() {
        expect:
        runAndCheckGeneration(componentV1)
        class1File.delete()

        when:
        makeCodegen(componentV1).generate()
        then:
        RuntimeException ex = thrown()
        ex.message == 'The file src/main/java/pkg2/Class1.java has been manually deleted. Set foreceMode to proceed.'
    }

    def "manually deleted - force mode"() {
        expect:
        runAndCheckGeneration(componentV1)
        class1File.delete()
        runAndCheckNextGenerationV1(true, {
            it.each { it.remove('md5') }
            it.find { it.path == class1FilePath }.status = UPDATED
        })
    }

    def "next generation - updated file"() {
        expect:
        runAndCheckGeneration(componentV1)
        changeLastGenerationDate()

        when:
        def generator = makeCodegen(componentV2).generate()
        then:
        checkFile class1File, CLASS1_V2_FILE_BODY
        checkFile class2File, CLASS2_FILE_BODY
        checkFile packageInfoFile, PACKAGE_INFO_BODY
        checkGeneration(generator, [
                [path: class2FilePath, md5: class2FileMd5, status: UNCHANGED],
                [path: packageInfoPath, md5: packageInfoMd5, status: UNCHANGED],
                [path: class1FilePath, md5: class1V2FileMd5, status: UPDATED],
        ])
    }

    def "next generation - updated file - no index file - force mode"() {
        expect:
        runAndCheckGeneration(componentV1)
        deleteIndexFile()

        when:
        def generator = makeCodegen(componentV2).setForceMode(true).generate()
        then:
        checkFile class1File, CLASS1_V2_FILE_BODY
        checkFile class2File, CLASS2_FILE_BODY
        checkFile packageInfoFile, PACKAGE_INFO_BODY
        // first and second are UNCHANGED but no md5 computed due to "forceMode"
        checkGeneration(generator, [
                [path: class2FilePath, status: UNCHANGED],
                [path: packageInfoPath, status: UNCHANGED],
                [path: class1FilePath, status: UPDATED],
        ])
    }

    def "next generation - updated file - no index file"() {
        expect:
        runAndCheckGeneration(componentV1)
        deleteIndexFile()

        when:
        def generator = makeCodegen(componentV2).generate()
        then:
        checkFile class1File, CLASS1_V2_FILE_BODY
        checkFile class2File, CLASS2_FILE_BODY
        checkFile packageInfoFile, PACKAGE_INFO_BODY
        checkGeneration(generator, [
                [path: class2FilePath, md5: class2FileMd5, status: UNCHANGED],
                [path: packageInfoPath, md5: packageInfoMd5, status: UNCHANGED],
                [path: class1FilePath, md5: class1V2FileMd5, status: UPDATED],
        ])
    }

    def "next generation - delete"() {
        expect:
        runAndCheckGeneration(componentV1)
        changeLastGenerationDate()

        when:
        def generator = makeCodegen(componentV3).generate()
        then:
        !class1File.exists()
        !packageInfoFile.exists()
        checkFile class2File, CLASS2_FILE_BODY
        checkGeneration(generator, [
                [path: class2FilePath, md5: class2FileMd5, status: UNCHANGED],
                [path: packageInfoPath, status: ABANDONED],
                [path: class1FilePath, status: ABANDONED],
        ])
    }

    def "next generation - no @Generated annotation - skip changes"() {
        expect:
        runAndCheckGeneration(componentV1)
        changeLastGenerationDate()

        when:
        class1File.write(CLASS1_TO_SKIP_FILE_BODY)
        def generator = makeCodegen(componentV2).generate()
        then:
        checkFile class1File, CLASS1_TO_SKIP_FILE_BODY
        checkFile class2File, CLASS2_FILE_BODY
        checkFile packageInfoFile, PACKAGE_INFO_BODY
        checkGeneration(generator, [
                [path: class2FilePath, md5: class2FileMd5, status: UNCHANGED],
                [path: packageInfoPath, md5: packageInfoMd5, status: UNCHANGED],
                [path: class1FilePath, md5: class1V2FileMd5, status: DETACHED],
        ])

        when:
        changeLastGenerationDate()
        def generator2 = makeCodegen(componentV2).generate()
        then:
        checkFile class1File, CLASS1_TO_SKIP_FILE_BODY
        checkFile class2File, CLASS2_FILE_BODY
        checkFile packageInfoFile, PACKAGE_INFO_BODY
        checkGeneration(generator2, [
                [path: class2FilePath, md5: class2FileMd5, status: UNCHANGED, lastUpdate: generator.prevGenerationDate],
                [path: packageInfoPath, md5: packageInfoMd5, status: UNCHANGED, lastUpdate: generator.prevGenerationDate],
                [path: class1FilePath, md5: class1V2FileMd5, status: DETACHED, lastUpdate: generator.prevGenerationDate],
        ])
    }

    def "next generation - no @Generated annotation - no index file"() {
        expect:
        runAndCheckGeneration(componentV1)
        deleteIndexFile()

        when:
        class1File.write(CLASS1_TO_SKIP_FILE_BODY)
        def generator = makeCodegen(componentV2).generate()
        then:
        checkFile class1File, CLASS1_TO_SKIP_FILE_BODY
        checkFile class2File, CLASS2_FILE_BODY
        checkFile packageInfoFile, PACKAGE_INFO_BODY
        checkGeneration(generator, [
                [path: class2FilePath, md5: class2FileMd5, status: UNCHANGED],
                [path: packageInfoPath, md5: packageInfoMd5, status: UNCHANGED],
                [path: class1FilePath, md5: class1V2FileMd5, status: DETACHED],
        ])
    }

    def "cleanup - remove generated files only"() {
        expect:
        runAndCheckGeneration(componentV1)
        overwriteFile(class1File, CLASS1_TO_SKIP_FILE_BODY)

        when:
        def generator = makeCodegen(componentV1).cleanup()

        then:
        generator.fileIndex.values().collect { it.path } == [class1FilePath]
    }

    def "cleanup - remove also manually updated files"() {
        expect:
        runAndCheckGeneration(componentV1)
        overwriteFile(class1File, CLASS1_TO_SKIP_FILE_BODY)

        when:
        def generator = makeCodegen(componentV1).totalCleanup()

        then:
        generator.fileIndex.isEmpty()
        !new File(outDir, 'src').exists()
    }

    /*
    def "next generation - no @Generated annotation - add new method"() {
        expect:
        true
    }

    def "next generation - no @Generated annotation - add new field"() {
        expect:
        true
    }
     */

    MainGenerator runAndCheckGeneration(ModuleGenerator componentGenerator) {
        def generation = makeCodegen(componentGenerator).generate()
        assert outDir.isDirectory()
        assert indexFile.exists()
        assert class1File.exists()
        assert class2File.exists()
        generation
    }

    boolean runAndCheckNextGenerationV1(boolean forceMode = false, Consumer<List<Map>> indexUpdater = {}) {
        def generation = makeCodegen(componentV1).setForceMode(forceMode).generate()
        assert outDir.isDirectory()
        assert indexFile.exists()
        assert class1File.exists()
        assert class2File.exists()
        assert packageInfoFile.exists()
        assert checkFile(class1File, CLASS1_FILE_BODY)
        assert checkFile(class2File, CLASS2_FILE_BODY)
        assert checkFile(packageInfoFile, PACKAGE_INFO_BODY)
        def index = [
                [path: class2FilePath, md5: class2FileMd5, status: UNCHANGED],
                [path: packageInfoPath, md5: packageInfoMd5, status: UNCHANGED],
                [path: class1FilePath, md5: class1FileMd5, status: UNCHANGED],
        ]
        indexUpdater.accept(index)
        checkGeneration(generation, index)
        true
    }

    CodeGenerationTask prepareSampleGenerationTask(Map params) {
        def model = new ClassCm(params['package'] as String, params['class'] as String)
        params['fields'].each {
            model.addField(ClassCd.stringType().asField(it as String))
        }
        model.addAnnotation(GENERATED_ANNOTATION)
        new CodeGenerationTask(
                destFilePath: "src/main/java/${params['package']}/${params['class']}.java",
                formatter: new JavaCodeFormatter(model),
                codeModel: model
        )
    }

    CodeGenerationTask preparePackageInfoGenerationTask(Map params) {
        def model = new PackageInfoCm(
                packageName: params['package'],
                annotations: [GENERATED_ANNOTATION],
        )
        new CodeGenerationTask(
                destFilePath: "src/main/java/${params['package']}/package-info.java",
                formatter: new PackageInfoFormatter(),
                codeModel: model,
        )
    }

    static class DummyModuleGenerator implements ModuleGenerator {
        List<CodeGenerationTask> codesToGenerate
        Model model = null
        void setDependencies(List<ModuleGenerator> modules) {}
        void generate() {}
    }

}
