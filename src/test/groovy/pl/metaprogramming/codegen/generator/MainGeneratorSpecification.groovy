/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.generator

import org.yaml.snakeyaml.Yaml
import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.ModuleGenerator
import pl.metaprogramming.codegen.java.formatter.JavaCodeFormatter
import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.utils.CheckUtils
import spock.lang.Shared
import spock.lang.Specification

import static pl.metaprogramming.codegen.generator.MainGenerator.FileStatus.*

abstract class MainGeneratorSpecification extends Specification {

    String fixFileBody(String fileBody) {
        fileBody
    }

    def setupSpec() {
        BACKUP_DATETIME_FORMAT = MainGenerator.DATETIME_FORMAT
        BACKUP_NEW_lINE = Codegen.NEW_LINE
        MainGenerator.DATETIME_FORMAT = 'yyyy-MM-dd HH:mm:ss.SSS'
    }

    def cleanupSpec() {
        Codegen.NEW_LINE = BACKUP_NEW_lINE
        MainGenerator.DATETIME_FORMAT = BACKUP_DATETIME_FORMAT
    }

    def setup() {
        outDir.deleteDir()
        assert !outDir.exists()
        outDir.mkdirs()
    }

    String tmpDirPath = 'out/gentest'
    String indexFilename = 'generatedFiles.yaml'
    String class1FilePath = 'src/main/java/pkg2/Class1.java'
    String class1LowercaseFilePath = 'src/main/java/pkg2/class1.java'
    String class2FilePath = 'src/main/java/pkg1/Class2.java'
    String packageInfoPath = 'src/main/java/pkg1/package-info.java'
    @Shared String class1FileMd5 = HashBuilder.build(fixFileBody(CLASS1_FILE_BODY))
    @Shared String class1LowercaseFileMd5 = HashBuilder.build(fixFileBody(CLASS1_LOWERCASE_FILE_BODY))
    @Shared String class1V2FileMd5 = HashBuilder.build(fixFileBody(CLASS1_V2_FILE_BODY))
    @Shared String class2FileMd5 = HashBuilder.build(fixFileBody(CLASS2_FILE_BODY))
    @Shared String packageInfoMd5 = HashBuilder.build(fixFileBody(PACKAGE_INFO_BODY))

    File outDir = new File(tmpDirPath)
    File indexFile = new File(outDir, indexFilename)
    File class1File = new File(outDir, class1FilePath)
    File class1LowercaseFile = new File(outDir, class1LowercaseFilePath)
    File class2File = new File(outDir, class2FilePath)
    File packageInfoFile = new File(outDir, packageInfoPath)

    static def GENERATED_ANNOTATION = AnnotationCm.of('javax.annotation.Generated', ['value': ValueCm.escaped('pl.metaprogramming.codegen')])

    static def BACKUP_DATETIME_FORMAT
    static def BACKUP_NEW_lINE

    Codegen makeCodegen(ModuleGenerator componentGenerator) {
        def codegen = new Codegen()
                .setBaseDir(outDir)
                .setIndexFile(indexFile)
                .setLineSeparator('\n')
                .addModule(componentGenerator)
                .setForceMode(false)
                .setAddLastGenerationTag(true)
        codegen.cfg.verbose = true
        codegen.cfg.storeAnyKindOfStatusesInIndexFile = true
        codegen
    }

    boolean checkFile(File file, String fileBody) {
        assert file.exists()
        assert file.parentFile.list().contains(file.name)
        assert file.text == fixFileBody(fileBody)
        true
    }

    boolean checkGeneration(MainGenerator generator, List<Map> indexFiles) {
        CheckUtils.checkList('files paths', generator.fileIndex.keySet() as List, indexFiles.collect { it.path })
        indexFiles.each {
            def given = generator.fileIndex.get(it.path)
            assert given.status == it.status
        }
        checkFileIndex(generator.generationDate, generator.prevGenerationDate, indexFiles.findAll { it.status != ABANDONED })
        true
    }

    boolean checkFileIndex(String generationDate, String prevGenerationDate, List<Map> expectedFiles) {
        Map fileIndex = loadYaml(indexFile)
        assert fileIndex['lastGeneration'] == generationDate
        List<Map> givenFiles = (List<Map>) fileIndex['files']
        assert expectedFiles.size() == givenFiles.size()
        expectedFiles.eachWithIndex { Map expectedFile, int i ->
            if (!expectedFile.containsKey('lastUpdate')) {
                if ([CREATED, UPDATED].contains(expectedFile.status)) {
                    expectedFile.lastUpdate = generationDate
                } else {
                    expectedFile.lastUpdate = prevGenerationDate ?: 'unknown'
                }
            }
            assert checkFile(expectedFile, givenFiles[i])
        }
        true
    }

    boolean deleteIndexFile() {
        indexFile.delete()
        !indexFile.exists()
    }

    Map loadYaml(File file) {
        new Yaml().load(file.text)
    }

    void changeLastGenerationDate() {
        def newLastGeneration = (new Date() - 1).format(MainGenerator.DATETIME_FORMAT)
        def text = indexFile.text
        def idx1 = text.indexOf("'") + 1
        String oldLastGeneration = text.substring(idx1, text.indexOf("'", idx1))
        overwriteFile(indexFile, text.replace(oldLastGeneration, newLastGeneration))
    }

    boolean checkFile(Map expectedFile, Map givenFile) {
        def expectedProperties = expectedFile.keySet()
        def givenProperties = givenFile.keySet()
        assert expectedProperties == givenProperties
        expectedProperties.each {
            assert givenFile[it] == '' + expectedFile[it]
        }
        true
    }

    void overwriteFile(File file, String content) {
        file.delete()
        file.createNewFile()
        file.write(content, 'UTF-8')
    }


    final static String CLASS1_FILE_BODY = '''package pkg2;

import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public class Class1 {
    

}
'''

    final static String CLASS1_LOWERCASE_FILE_BODY = CLASS1_FILE_BODY.replace("class Class1", "class class1")

    final static String CLASS1_TO_SKIP_FILE_BODY = '''package pkg2;

public class Class1 {
    private String textProperty;
}
'''

    final static String CLASS1_V2_FILE_BODY = '''package pkg2;

import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public class Class1 {
    
    private String textProperty;

}
'''

    final static String CLASS2_FILE_BODY = '''package pkg1;

import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public class Class2 {
    

}
'''

    final static String PACKAGE_INFO_BODY = '''@javax.annotation.Generated("pl.metaprogramming.codegen")
package pkg1;'''
}
