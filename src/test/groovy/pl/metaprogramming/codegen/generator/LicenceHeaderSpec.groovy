/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.generator

import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.DummyModuleGenerator
import pl.metaprogramming.codegen.ModuleGenerator
import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.java.JavaModuleConfigurator
import pl.metaprogramming.codegen.java.JavaModuleParams

import static pl.metaprogramming.codegen.generator.MainGenerator.FileStatus.CREATED

class LicenceHeaderSpec extends MainGeneratorSpecification {

    String fixFileBody(String fileBody) {
        LICENCE_HEADER + fileBody
    }

    def "should generate codes with licence header"() {
        expect:
        outDir.isDirectory()
        !indexFile.exists()
        !class2File.exists()

        when:
        Codegen.NEW_LINE = '\n'
        def generator = makeCodegen(makeModuleBuilder()).generate()

        then:
        outDir.isDirectory()
        indexFile.exists()
        checkFile class2File, MainGeneratorSpecification.CLASS2_FILE_BODY
        checkGeneration(generator, [
                [path: class2FilePath, md5: class2FileMd5, status: CREATED],
        ])
    }

    ModuleGenerator makeModuleBuilder() {
        def configurator = new JavaModuleConfigurator<>(new CodegenParams().with(
                new JavaModuleParams()
                        .setLicenceHeader(LICENCE_TEXT)
                        .setGeneratedAnnotationClass('javax.annotation.Generated')
        )).typeOfCode('Class2', {
            it.setFixedName('Class2')
            it.packageName = 'pkg1'
        })
        new DummyModuleGenerator(configurator)
    }

    private static String LICENCE_TEXT = '''LICENCE line1
  
  LICENCE line3
'''

    private static String LICENCE_HEADER = '''/*
 * LICENCE line1
 *
 *   LICENCE line3
 */

'''
}
