/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.generator

import pl.metaprogramming.codegen.DummyModuleGenerator
import pl.metaprogramming.codegen.ModuleGenerator
import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.java.JavaModuleConfigurator
import pl.metaprogramming.codegen.java.JavaModuleParams

import static pl.metaprogramming.codegen.generator.MainGenerator.FileStatus.CREATED

class SkipModuleGenerationSpec extends MainGeneratorSpecification {

    def "should generate codes only for module1"() {
        expect:
        outDir.isDirectory()
        !indexFile.exists()
        !class1File.exists()
        !class2File.exists()

        when:
        def params = new CodegenParams().with(new JavaModuleParams()
                .setGeneratedAnnotationClass('javax.annotation.Generated'))
        def codegen = makeCodegen(makeModuleBuilder(
                params,
                'module1',
                'pkg2',
                'Class1'))
        codegen.addModule(makeModuleBuilder(
                params.clone().setSkipGeneration(true),
                'module2',
                'pkg1',
                'Class2'))
        def generator = codegen.generate()

        then:
        outDir.isDirectory()
        indexFile.exists()
        !class2File.exists()
        checkFile class1File, MainGeneratorSpecification.CLASS1_FILE_BODY
        checkGeneration(generator, [
                [path: class1FilePath, md5: class1FileMd5, status: CREATED],
        ])
    }


    ModuleGenerator makeModuleBuilder(CodegenParams params, String name, String packageName, String className) {
        def configurator = new JavaModuleConfigurator<>(params)
        configurator.typeOfCode(className).setFixedName(className).setPackageName(packageName)
        new DummyModuleGenerator(configurator)
    }

}
