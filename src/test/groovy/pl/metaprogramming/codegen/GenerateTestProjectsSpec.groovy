/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen

import pl.metaprogramming.fixtures.TestProjectCodegenProvider
import spock.lang.Specification
import spock.lang.Unroll

import java.util.function.Function

import static pl.metaprogramming.fixtures.CodegenParamsProvider.*

class GenerateTestProjectsSpec extends Specification {

    @Unroll
    def "generate-codes-for: #testProject"() {
        given:
        def provider = new TestProjectCodegenProvider(testProject: testProject, params: params)
        when:
        def codegen = (factoryMethod as Function<TestProjectCodegenProvider, Codegen>).apply(provider)
        def genResult = codegen.generate()
        then:
        genResult != null

        where:
        testProject               | params                           | factoryMethod
        'rs-server-spring-1t'     | makeSpringRestServicesParams()   | { it.forRsServer() }
        'rs-server-spring-1t-jbv' | makeSpring1tJbvParams()          | { it.forRsServer() }
        'rs-server-spring-2t'     | makeSpringRestServices2tParams() | { it.forRsServerLegacyOas3() }
        'rs-server-spring-legacy' | makeSpringRsLegacyParams()       | { it.forRsServerLegacyOas2() }
        'rs-client-spring-1t'     | makeDefaultJavaParams(false)     | { it.forRsClient() }
        'rs-client-spring-2t'     | makeDefaultJavaParams()          | { it.forRsClientLegacy() }
        'ws-client-spring'        | makeWsClientParams()             | { it.forWsClient() }
    }
}
