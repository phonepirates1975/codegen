/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codemodel.builder.java

import pl.metaprogramming.codegen.java.base.ClassBuilderConfigurator
import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import spock.lang.Specification

class ClassBuilderConfigSpec extends Specification {

    def "should add strategy"() {
        given:
        def config = new ClassBuilderConfigurator()
        def strategy = new ClassCmBuildStrategy<>()

        when:
        config.addStrategy(strategy)
        then:
        config.strategies.contains(strategy)
    }

    def "should remove strategy by class"() {
        given:
        def config = new ClassBuilderConfigurator()
        def strategy = new SomeStrategy()

        when:
        config.addStrategy(strategy)
                .addStrategy(new ClassCmBuildStrategy<Object>())
                .removeStrategy(SomeStrategy)
        then:
        !config.strategies.contains(strategy)
        config.strategies.size() == 1
    }

    def "should remove strategy by value"() {
        given:
        def config = new ClassBuilderConfigurator()
        def strategy = new SomeStrategy()

        when:
        config.addStrategy(strategy)
                .addStrategy(new SomeStrategy())
                .removeStrategy(strategy)
        then:
        !config.strategies.contains(strategy)
        config.strategies.size() == 1
    }

    def "should replace strategy by class"() {
        given:
        def config = new ClassBuilderConfigurator()
        def strategy = new SomeStrategy()
        def s1 = new ClassCmBuildStrategy()
        def s2 = new ClassCmBuildStrategy()

        when:
        config.addStrategy(strategy)
                .addStrategy(s1)
                .replaceStrategy(SomeStrategy, s2)
        then:
        !config.strategies.contains(strategy)
        config.strategies.contains(s1)
        config.strategies.contains(s2)
        config.strategies.size() == 2
    }

    def "should replace strategy by value"() {
        given:
        def config = new ClassBuilderConfigurator()
        def strategy = new SomeStrategy()
        def s1 = new SomeStrategy()
        def s2 = new ClassCmBuildStrategy()

        when:
        config.addStrategy(strategy)
                .addStrategy(s1)
                .replaceStrategy(strategy, s2)
        then:
        !config.strategies.contains(strategy)
        config.strategies.contains(s1)
        config.strategies.contains(s2)
        config.strategies.size() == 2
    }

    static class SomeStrategy extends ClassCmBuildStrategy {
    }
}
