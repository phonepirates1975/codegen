/*
 * Copyright (c) 2019 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.utils

import java.util.function.Consumer

class MethodShadow {
    ClassShadow ownerClass
    String name
    List<String> annotations
    String signature
    String implBody

    String getFullName() {
        def ownerClassName = ownerClass.name.contains('.') ? ownerClass.name.substring(ownerClass.name.lastIndexOf('.') + 1) : ownerClass.name
        "${ownerClassName}.${signature.substring(signature.indexOf(name))}"
    }

    MethodShadow addLine(String line, int idx = 0) {
        updateBody { it.add(idx, line) }
    }

    MethodShadow moveLine(String line, int idx) {
        updateBody {
            it.remove(line)
            it.add(idx - 1, line)
        }
    }

    MethodShadow replaceLine(String oldLine, String newLine) {
        updateBody {
            def oldLineIdx = it.indexOf(oldLine)
            if (oldLineIdx < 0) {
                throw new IllegalArgumentException("Line not exists: $oldLine")
            }
            it.remove(oldLineIdx)
            it.add(oldLineIdx, newLine)
        }
    }

    MethodShadow updateBody(Consumer<List<String>> updater) {
        def lines = implBody.split('\n').toList()
        updater.accept(lines)
        implBody = lines.join('\n')
        this
    }
}
