/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.utils


import pl.metaprogramming.model.data.*

import static pl.metaprogramming.model.data.DataType.*

class DataSchemaFactory {

    static DataType STRING_TYPE = TEXT
    static DataType INTEGER_TYPE = INT32
    static DataType LONG_TYPE = INT64
    static DataType DECIMAL_TYPE = DECIMAL
    static DataType DATE_TYPE = DATE

    static DataSchema listData(DataSchema schema, int minItems, int maxItems, String description) {
        new DataSchema(new ArrayType(itemsSchema: schema, minItems: minItems, maxItems: maxItems)).setAdditive('description', description)
    }

    static DataSchema listData(DataSchema schema, int minItems, String description) {
        listData(schema, minItems, Integer.MAX_VALUE, description)
    }

    static DataSchema listData(DataType dataType, int minItems, int maxItems, String description) {
        listData(new DataSchema(dataType), minItems, maxItems, description)
    }

    static DataSchema listData(DataType schema, int minItems, String description) {
        listData(schema, minItems, Integer.MAX_VALUE, description)
    }

    static DataSchema list(DataSchema schema, int minItems, int maxItems, String description) {
        listData(schema, minItems, maxItems, description)
    }

    static DataSchema list(DataSchema schema, int minItems, String description) {
        listData(schema, minItems, Integer.MAX_VALUE, description)
    }

    static DataSchema stringType(boolean isRequired = false, String description = null) {
        new DataSchema(STRING_TYPE)
                .setIsRequired(isRequired)
                .setAdditive('description', description)
    }

    static DataSchema intType(boolean isRequired = false, String description = null) {
        dataElement(INTEGER_TYPE, isRequired, description)
    }

    static DataSchema longType(boolean isRequired = false, String description = null) {
        dataElement(LONG_TYPE, isRequired, description)
    }

    static DataSchema decimalType(boolean isRequired = false, String description = null, String format = null) {
        dataElement(DECIMAL_TYPE, isRequired, description, format)
    }

    static DataSchema dateType(String description = null, String format = null) {
        dataElement(DATE_TYPE, false, description, format)
    }

    static DataSchema dataElement(DataType dataType, boolean isRequired = false, String description = null, String format = null) {
        new DataSchema(dataType)
                .setIsRequired(isRequired)
                .setFormat(format)
                .setAdditive('description', description)
    }
}
