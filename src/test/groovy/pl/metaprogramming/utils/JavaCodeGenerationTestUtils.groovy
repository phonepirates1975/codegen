/*
 * Copyright (c) 2019 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.utils

import pl.metaprogramming.codegen.generator.CodeGenerationTask
import pl.metaprogramming.codegen.java.formatter.JavaCodeFormatter
import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassCm
import pl.metaprogramming.codegen.java.MethodCm

import static pl.metaprogramming.utils.CheckUtils.checkList
import static pl.metaprogramming.utils.CheckUtils.printList

class JavaCodeGenerationTestUtils {

    static CodeGenerationTask findGenerationTask(List<CodeGenerationTask> generationTasks, ClassShadow shadow) {
        generationTasks.find { it.codeModel instanceof ClassCm && it.codeModel.canonicalName == shadow.name }
    }

    static List<MethodShadow> findMethodsToCheck(List<ClassShadow> classes) {
        (List<MethodShadow>) classes.findAll { it.methods }.collect { classShadow ->
            classShadow.methods.each { it.ownerClass = classShadow }
            classShadow.methods
        }.flatten()
    }

    static MethodShadow methodShadow(String name, String signature, List<String> implBody = null, List<String> annotations = []) {
        new MethodShadow(
                name: name,
                signature: signature,
                annotations: annotations,
                implBody: implBody?.join('\n'))
    }

    static boolean checkClass(CodeGenerationTask generationTask, ClassShadow expected, String generatedAnnotationClass) {
        def restrictive = expected.checkRestrictive
        def formatter = (JavaCodeFormatter) generationTask.formatter
        def fullCode = formatter.format()
        assert fullCode
        def annotations = formatAnnotations(formatter, formatter.classCm.annotations)
        checkList('annotations', annotations, expected.annotations, restrictive)
        if (expected.classHeader) {
            def classHeader = formatter.formatClassHeader()
            assert classHeader == expected.classHeader
            assert fullCode.contains(classHeader)
        }
        def fields = formatter.classCm.fields.collect {
            formatter.formatField(it)
        }
        def enums = formatter.classCm.enumItems.collect {
            "${it.name}(${it.value})" + (it.description ? " //${it.description}" : '')
        }
        checkList("fields", fields, expected.fields, restrictive)
        checkList("imports", formatter.imports, expected.imports + [generatedAnnotationClass], restrictive)
        checkList("enums", enums, expected.enums, restrictive)
        if (expected.noMoreMethods) {
            def givenMethods = formatter.classCm.methodsToGenerate.collect { formatter.formatSignature(it) }
            def expectedMethods = expected.methods.collect { it.signature }
            checkList("methods", givenMethods, expectedMethods, true)
        }
        true
    }

    static boolean checkMethod(CodeGenerationTask generationTask, MethodShadow expected) {
        def formatter = (JavaCodeFormatter) generationTask.formatter
        def methodCm = findMethod(formatter, expected)
        def annotations = formatAnnotations(formatter, methodCm.annotations)
        checkList('annotations', annotations, expected.annotations)
        def signature = formatter.formatSignature(methodCm)
        def expectedSignature = expected.signature
        assert signature == expectedSignature
        checkMethodBody(methodCm, expected)
        true
    }

    static MethodCm findMethod(JavaCodeFormatter formatter, MethodShadow shadow) {
        def result = formatter.classCm.methodsToGenerate.findAll { it.name == shadow.name }
        assert result, "Method $shadow.name not present"
        if (result.size() == 1) {
            return result[0]
        }
        if (result.size() > 1) {
            def bySignature = result.find { formatter.formatSignature(it) == shadow.signature }
            assert bySignature, "Method $shadow.signature not present (candidates: $result)"
            return bySignature
        }
        null
    }

    static boolean checkMethodBody(MethodCm methodCm, MethodShadow expected) {
        def implBody = methodCm.implBody ? methodCm.implBody.readLines() : []
        def expectedImplBody = expected.implBody ? expected.implBody.readLines() : []
        if (implBody.size() != expectedImplBody.size()) {
            printList(implBody)
        }
        assert implBody.size() == expectedImplBody.size()
        for (int i = 0; i < implBody.size(); i++) {
            def implLine = implBody[i]
            def expectedLine = expectedImplBody[i]
            if (implLine != expectedLine) {
                printList(implBody)
            }
            assert implLine == expectedLine
        }
        true
    }

    static List<String> formatAnnotations(JavaCodeFormatter formatter, List<AnnotationCm> annotations) {
        def notNullAnnotations = annotations.findAll { it != null }
        formatter.formatAnnotations(notNullAnnotations).collect {
            it.replaceAll("\\s", "")
        }
    }
}
