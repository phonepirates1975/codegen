/*
 * Copyright (c) 2019 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.utils


import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.data.ObjectType
import pl.metaprogramming.model.oas.OperationType
import pl.metaprogramming.model.oas.ParamLocation
import pl.metaprogramming.model.oas.HttpRequestBody
import pl.metaprogramming.model.oas.HttpResponse
import pl.metaprogramming.model.oas.Operation
import pl.metaprogramming.model.oas.Parameter
import pl.metaprogramming.model.oas.RestApi

class RestMetaModels {

    static RestApi commonsRestServices = new RestApiBuilder() {
        {
            model.title = 'commons-api'
            dataDef('FieldValidation', 'Informacje o błędzie walidacji danych wejściowych', [
                    field    : DataSchemaFactory.stringType(true, 'Pole z niepoprawną wartością'),
                    errorCode: DataSchemaFactory.stringType(true, 'Kod błędu walidacji'),
            ])
            dataDef('BadRequest', 'Informacje o błędzie danych wejściowych', [
                    errors: DataSchemaFactory.listData(schemaRef('FieldValidation'), 1, 'Lista błędów walidacji'),
            ])
            dataDef('Error', 'Komunikat błędu', [
                    errorCode: DataSchemaFactory.stringType(true, 'Kod błędu'),
            ])

            paramDef('Authorization', ParamLocation.HEADER, DataSchemaFactory.STRING_TYPE, true, 'OAuth 2.0 bearer token')
        }
    }.model

    static RestApi echoRestServices = new RestApiBuilder() {
        {
            model.title = 'echo-api'
            dependsOn([commonsRestServices])

            dataDef('DataForm', 'Formularz z danymi', [
                    d_r_string : DataSchemaFactory.stringType(true, 'Tekst - wymagany'),
                    d_o_string : DataSchemaFactory.stringType(false, 'Tekst - opcjonalny'),
                    d_int      : DataSchemaFactory.intType(true, 'Liczba całkowita - int'),
                    d_long     : DataSchemaFactory.longType(true, 'Liczba całkowita - long'),
                    d_list_long: DataSchemaFactory.listData(DataSchemaFactory.LONG_TYPE, 0, 'Lista liczb całkowitych'),
                    d_decimal  : DataSchemaFactory.dataElement(DataSchemaFactory.DECIMAL_TYPE, false, 'Liczba dziesiętna - decimal', ',|2'),
                    d_date     : DataSchemaFactory.dataElement(DataSchemaFactory.DATE_TYPE, false, 'Data yyyy-MM-dd'),
            ])

            dataDef('TreeLeaf', null, [
                    simpleField: DataSchemaFactory.stringType()
            ])

            dataDef('TreeRoot', null, [
                    complexField: schemaRef('TreeLeaf')
            ])

            addOperation new Operation(
                    code: 'echoPost',
                    path: '/api/v1/echo',
                    type: OperationType.POST,
                    additives: [description: 'Usługa w odpowiedzi zwraca dane otrzymane na wejściu (POST)'],
                    requestBody: requestBody('DataForm'),
                    responses: [
                            response(200, 'DataForm', 'Echo'),
                            response400(),
                            response404(),
                            response405()]
            )

            addOperation new Operation(
                    code: 'echoGet',
                    path: '/api/v1/echo',
                    type: OperationType.GET,
                    additives: [description: 'Usługa w odpowiedzi zwraca dane otrzymane na wejściu (URL)'],
                    parameters: params([['DataForm', ParamLocation.QUERY]]),
                    responses: [response(200, 'DataForm', 'Echo'), response400()]
            )

            addOperation new Operation(
                    code: 'echoComplex',
                    path: '/api/v1/echoComplex',
                    type: OperationType.POST,
                    additives: [description: 'Test obsługii strutkur zagnieżdżonych'],
                    requestBody: requestBody('TreeRoot'),
                    responses: [response(200, 'TreeRoot', 'Echo complex'), response400()]
            )

        }

        @Override
        void addOperation(Operation operation) {
            operation.group = 'Echo'
            super.addOperation(operation)
        }
    }.model


    static class RestApiBuilder extends pl.metaprogramming.model.oas.RestApiBuilder {

        HttpRequestBody requestBody(String code) {
            new HttpRequestBody(
                    code: 'requestBody',
                    contents: ['application/json': schemaRef(code)]
            )
        }

        HttpResponse response400() {
            response(400, 'BadRequest', 'Odpowiedź na niepoprawne żądanie')
        }

        HttpResponse response404() {
            response(404, 'Error', 'Not Found')
        }

        HttpResponse response405() {
            response(405, 'Error', 'Method Not Allowed')
        }

        HttpResponse response(int code, String schemaCode, String description) {
            new HttpResponse(status: code, contents: ['application/json': schemaRef(schemaCode)], description: description)
        }

        List<Parameter> params(List paramList) {
            List<Parameter> result = []
            paramList.each {
                if (it instanceof String) {
                    result.addAll(paramRef(it))
                } else if (it instanceof List) {
                    if (it.size() == 2) {
                        def schema = schemaRef((String) it.get(0))
                        def location = (ParamLocation) it.get(1)
                        schema.objectType.fields.each {
                            result.add(new Parameter(location, it))
                        }
                    } else if (it.size() == 5) {
                        result.add(new Parameter((ParamLocation) it.get(1), new DataSchema((String) it.get(0), (DataType) it.get(2))
                                .setIsRequired((Boolean) it.get(3))
                                .setAdditive('description', it.get(4))))
                    } else {
                        throw new RuntimeException("Can't define param: $it")
                    }
                }
            }
            result
        }

        void dataDef(String code, String description, Map<String, DataSchema> properties) {
            dataDef(code, description, properties.collect {
                it.value.code = it.key
                it.value
            })
        }

        void dataDef(String code, String description, List<DataSchema> properties) {
            addSchema(new ObjectType(code: code, fields: properties, additives: [description: description]).makeSchema())
        }

        void paramDef(String name, ParamLocation location, DataType schema, boolean isRequired, String description) {
            addParam(new Parameter(location, new DataSchema(name, schema)
                    .setIsRequired(isRequired)
                    .setAdditive('description', description)))
        }

    }

}
