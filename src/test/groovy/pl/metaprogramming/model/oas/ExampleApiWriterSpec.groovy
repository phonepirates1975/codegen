/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas


import pl.metaprogramming.fixtures.ExampleApiModels
import spock.lang.Shared

class ExampleApiWriterSpec extends OpenapiWriterSpecification {

    @Shared
    RestApi apiCommons

    @Shared
    RestApi api

    RestApi loadOriginalApi() {
        if (api == null) {
            apiCommons = ExampleApiModels.loadExampleApiDepsModel()
            api = ExampleApiModels.loadExampleApiModel(apiCommons)
        }
        api
    }

    def getOutFilename() {
        'example-api-flatten.yaml'
    }

    RestApi fixApi(RestApi api) {
        api.forEachOperation { it.group = 'echo' }
        api
    }

    def "restored api should have array constraints"() {
        when:
        def original = originalApi.getSchema('EchoArraysBody.prop_double_list').arrayType
        def restored = restoredApi.getSchema('EchoArraysBody.prop_double_list').arrayType

        then:
        original.minItems == 2
        original.maxItems == 4
        original.uniqueItems
        restored.minItems == original.minItems
        restored.maxItems == original.maxItems
        restored.uniqueItems == original.uniqueItems
    }

}
