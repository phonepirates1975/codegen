/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas

import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.DataTypeCode
import pl.metaprogramming.model.data.EnumType
import pl.metaprogramming.utils.CheckUtils
import spock.lang.Specification
import spock.lang.Unroll

import java.util.function.Consumer

class OpenapiParserSpec extends Specification {

    /*
      Test Import swagger 2.0 specifications:
        https://petstore.swagger.io/v2/swagger.jyaml
        https://petstore.swagger.io/v2/swagger.json
        https://petstore3.swagger.io/api/v3/openapi.yaml
        https://github.com/OAI/OpenAPI-Specification/blob/master/examples/v2.0/yaml/petstore-simple.yaml
        https://docs.polishapi.org/files/ver2.1.1/PolishAPI-ver2_1_1.yaml
     */

    @Unroll
    def "read #file"() {
        when:
        def result = OpenapiParser.parse("src/testFixtures/resources/openapi/$file")
        modelFixer == null || modelFixer.accept(result)

        then:
        result != null
        validator.call(result)

        where:
        file                         | modelFixer           | validator
        'petstore.yaml'              | null                 | { validatePetstore(it) }
        'petstore.yaml'              | setGroup('petstore') | { validatePetstore(it, ['petstore']) }
        'v2/petstore.yaml'           | null                 | { validatePetstore(it) }
        'v2/petstore.json'           | null                 | { validatePetstore(it) }
        'v2/petstore-simple.yaml'    | setGroup('pets')     | { validatePetstoreSimple(it, 'pets') }
        'v2/petstore-simple.yaml'    | setGroup('petstore') | { validatePetstoreSimple(it, 'petstore') }
        'v2/PolishAPI-ver2_1_1.yaml' | null                 | { validatePolishAPI(it) }
    }

    boolean validatePetstore(RestApi rs, def resources = ['pet', 'store', 'user']) {
        assert new HashSet(rs.operations.collect { it.group }) == new HashSet(resources)
        def enums = collectEnums(rs)
        assert enums == ['ENUM[OrderStatus, allowed: [placed, approved, delivered]]',
                         'ENUM[Status, allowed: [available, pending, sold]]']
        true
    }

    Consumer<RestApi> setGroup(String group) {
        { api -> api.forEachOperation { it.group = group } }
    }

    boolean validatePetstoreSimple(RestApi rs, String resource) {
        CheckUtils.checkList('schemas', rs.schemas.collect { it.code },
                ['Pet', 'NewPet', 'ErrorModel'])
        checkSchema(rs.getSchema('Pet'), "OBJECT[Pet, fields: [id INT64[required]], inherits: [OBJECT[NewPet, fields: [name STRING[required], tag STRING]]]]")
        checkSchema(rs.getSchema('NewPet'), "OBJECT[NewPet, fields: [name STRING[required], tag STRING]]")
        checkSchema(rs.getSchema('ErrorModel'), "OBJECT[ErrorModel, fields: [code INT32[required], message STRING[required]]]")

        def operations = rs.operations.collect {
            "${it.group}.${it.code} ${it.type} ${it.path}"
        }
        CheckUtils.checkList('operations', operations, [
                "${resource}.findPets GET /api/pets",
                "${resource}.addPet POST /api/pets",
                "${resource}.findPetById GET /api/pets/{id}",
                "${resource}.deletePet DELETE /api/pets/{id}"])

        def findPets = rs.getOperation('findPets')
        assert findPets != null
        CheckUtils.checkList('responses', findPets.responses.collect { it.toString() }, [
                '200 LIST OF OBJECT[Pet, fields: [id INT64[required]], inherits: [OBJECT[NewPet, fields: [name STRING[required], tag STRING]]]]',
                'default OBJECT[ErrorModel, fields: [code INT32[required], message STRING[required]]]',
        ])

        def findPetById = rs.getOperation('findPetById')
        assert findPetById != null
        CheckUtils.checkList('responses', findPetById.responses.collect { it.toString() }, [
                '200 OBJECT[Pet, fields: [id INT64[required]], inherits: [OBJECT[NewPet, fields: [name STRING[required], tag STRING]]]]',
                'default OBJECT[ErrorModel, fields: [code INT32[required], message STRING[required]]]',
        ])

        true
    }

    boolean validatePolishAPI(RestApi rs) {
        checkParam(rs, 'authorizationParam', 'Authorization', ParamLocation.HEADER, true, DataTypeCode.STRING)
        checkParam(rs, 'acceptEncodingParam', 'Accept-Encoding', ParamLocation.HEADER, true, new EnumType(allowedValues: ['gzip', 'deflate']))
        checkParam(rs, 'acceptLanguageParam', 'Accept-Language', ParamLocation.HEADER, true, DataTypeCode.STRING)
        checkParam(rs, 'acceptCharsetParam', 'Accept-Charset', ParamLocation.HEADER, true, DataTypeCode.ENUM)
        checkParam(rs, 'xjwsSignatureParam', 'X-JWS-SIGNATURE', ParamLocation.HEADER, true, DataTypeCode.STRING)
        def schema = rs.getSchema('RequestHeaderWithoutTokenAS')
        checkSchema(schema, "OBJECT[RequestHeaderWithoutTokenAS, 'Klasa zawierająca informacje o PSU na potrzeby usług autoryzacji / PSU Information Class dedicated for authorization services', fields: [isCompanyContext BOOLEAN['(true / false) Znacznik oznaczający czy żądanie jest wysyłane w kontekście PSU korporacyjnego'], psuIdentifierType STRING['Typ identyfikatora PSU, służy do wskazania przez TPP na podstawie jakiej informacji zostanie zidentyfikowany PSU, który będzie podlegał uwierzytelnieniu. Wartość słownikowa. / PSU identifier type, used by TPP to indicate type of information based on which the PSU is to be authenticated. Dictionary value.'], psuIdentifierValue STRING['Wartość identyfikatora PSU. Wymagany warunkowo - w przypadku gdy została przekazana niepusta wartość typu identyfikatora PSU. / The value of the PSU's identifier. Required conditionally - in case non empty value of PSU identifier type was passed.'], psuContextIdentifierType STRING['Typ identyfikatora kontekstu w jakim występuje PSU. Wartość słownikowa. Wymagany warunkowo - w przypadku wysyłania żądania dla takiego PSU, który może występować w więcej niż jednym kontekście w wybranym ASPSP. / Identifier of context that is used by PSU. Dictionary value. Required conditionally - in case context of the request is used and PSU may use more then one context for the ASPSP.'], psuContextIdentifierValue STRING['Wartość identyfikatora kontekstu w jakim występuje PSU. Wymagany warunkowo - w przypadku wysyłania żądania dla takiego PSU, który może występować w więcej niż jednym kontekście w wybranym ASPSP. / The value of context that is used by PSU. Required conditionally - in case context of the request is used and PSU may use more then one context for the ASPSP.']], inherits: [OBJECT[RequestHeaderWithoutToken, 'Klasa zawierająca informacje o PSU / PSU Information Class', fields: [requestId STRING[required, 'Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1) zgodnym ze standardem RFC 4122 / Request ID using UUID format (Variant 1, Version 1) described in RFC 4122 standard'], userAgent STRING['Browser agent dla PSU / PSU browser agent'], ipAddress STRING['Adres IP końcowego urządzenia PSU. Wymagany dla isDirectPsu=true. / IP address of PSU's terminal device. Required when isDirectPsu=true.'], sendDate DATE_TIME['Oryginalna data wysłania, format: YYYY-MM-DDThh:mm:ss[.mmm] / Send date'], tppId STRING['Identyfikator TPP / TPP ID']]]]]")
        def enums = collectEnums(rs)
        CheckUtils.checkList('en', enums,
                ['ENUM[AcceptCharsetParam, allowed: [utf-8]]',
                 'ENUM[AcceptEncodingParam, allowed: [gzip, deflate]]',
                 'ENUM[AccountHolderType, allowed: [individual, corporation]]',
                 'ENUM[AdditionalPayorIdType, allowed: [P, R, 1, 2], descriptions: [P:Pesel, R:Regon, 1:Dowód osobisty, 2:Paszport]]',
                 'ENUM[BundleStatus, allowed: [inProgress, cancelled, done, partiallyDone]]',
                 'ENUM[DayOffOffsetType, allowed: [before, after]]',
                 'ENUM[DeliveryMode, allowed: [ExpressD0, StandardD1]]',
                 'ENUM[DeliveryMode2, allowed: [ExpressD0, UrgentD1, StandardD2]]',
                 'ENUM[ExecutionMode, allowed: [Immediate, FutureDated]]',
                 'ENUM[HoldRequestType, allowed: [DEBIT]]',
                 'ENUM[PaymentStatus, \'Słownik statusów płatności\', allowed: [submitted, cancelled, pending, done, rejected, scheduled]]',
                 'ENUM[PayorIdType, allowed: [N, P, R, 1, 2, 3], descriptions: [N:NIP, P:Pesel, R:Regon, 1:Dowód osobisty, 2:Paszport, 3:Inny]]',
                 'ENUM[PeriodType, allowed: [day, week, month]]',
                 'ENUM[RecurringPaymentStatus, allowed: [submitted, inProgress, cancelled, close]]',
                 'ENUM[ScopeGroupType, allowed: [ais-accounts, ais, pis]]',
                 'ENUM[ScopeUsageLimit, allowed: [single, multiple]]',
                 'ENUM[System, allowed: [Elixir, ExpressElixir, Sorbnet, BlueCash, Internal]]',
                 'ENUM[System2, allowed: [SEPA, InstantSEPA, Target]]',
                 'ENUM[System3, allowed: [Elixir, ExpressElixir]]',
                 'ENUM[System4, allowed: [Swift]]',
                 'ENUM[ThrottlingPolicy, allowed: [psd2Regulatory]]',
                 'ENUM[TransactionCategory, allowed: [CREDIT, DEBIT]]',
                 'ENUM[TypeOfPayment, allowed: [domestic, EEA, nonEEA, tax]]'
                ])

        true
    }

    static boolean checkParam(RestApi rs, String code, String name, ParamLocation location, boolean required, def schema) {
        def paramDef = rs.getParameter(code)
        assert paramDef, "No parameter with code $code, existing parameters: ${rs.parameters.collect { it.code }}"
        assert paramDef.name == name, "Missmatch name for parameter $code"
        assert paramDef.location == location, "Missmatch location for parameter $code"
        assert paramDef.isRequired == required
        if (schema instanceof DataTypeCode) {
            assert paramDef.dataType.typeCode == schema
        }
        if (schema instanceof EnumType) {
            assert paramDef.dataType instanceof EnumType
            assert schema.allowedValues == paramDef.enumType.allowedValues
        }
        true
    }

    static boolean checkSchema(DataSchema schema, String expected) {
        def schemaString = schema.toString()
        assert schemaString == expected
        true
    }

    static List<String> collectEnums(RestApi rs) {
        rs.schemas.findAll { it.isEnum() }.collect { it.toString() }.sort()
    }
}
