/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas

class ExampleApiSplitWriterSpec extends ExampleApiWriterSpec {

    def getOutFilename() {
        [(api): 'example-api.yaml', (apiCommons): 'example-api-deps.yaml']
    }

    def saveApi() {
        def outCfg = getOutFilename().collectEntries {
            [(it.key): getOutFile(it.value)]
        } as Map<RestApi, File>
        OpenapiWriter.write(api, outCfg)
    }

    RestApi loadStoredApi() {
        def deps = OpenapiParser.parse(
                getOutFile('example-api-deps.yaml').getPath(),
                new OpenapiParserConfig(partialFile: true))
        fixApi(OpenapiParser.parse(
                getOutFile('example-api.yaml').getPath(),
                new OpenapiParserConfig(dependsOn: [deps])))
    }

}
