/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas

import static pl.metaprogramming.fixtures.ExampleApiModels.loadExampleApiDepsModel
import static pl.metaprogramming.fixtures.ExampleApiModels.loadExampleApiModel

class ExampleApiParserSpec extends ExampleApiParserSpecification {

    def setupSpec() {
        depsApi = loadExampleApiDepsModel()
        api = loadExampleApiModel(depsApi)
    }

    def "should parse x-constraints for EchoBody.prop_amount definition"() {
        when:
        def schema = getSchema('EchoBody')
        def field = schema.objectType.fields.find { it.code == 'prop_amount' }
        then:
        field != null
        field.additives['x-constraints'] == [
                [rule       : 'USER_AMOUNT',
                 description: 'UserDataValidationBean:checkAmountByUser should be used'],
                ['rule'       : 'AMOUNT_SCALE',
                 'description': 'Checkers.AMOUNT_SCALE_CHECKER should be used']]
    }

    def "should parse x-constraints for ExtendedObject definition"() {
        when:
        def schema = getSchema('ExtendedObject')
        then:
        schema.additives['x-constraints'] == [
                [rule       : 'EXTENDED_OBJECT_CONSTRAINT',
                 priority   : 0,
                 description: 'ExtendedObjectChecker should be used']]
    }

    def "should parse x-constraints for ExtendedObject.eo_enum_reusable definition"() {
        when:
        def schema = getSchema('ExtendedObject')
        def field = schema.objectType.fields.find { it.code == 'eo_enum_reusable' }
        then:
        field != null
        field.additives['x-constraints'] == [
                [rule       : 'EXTENDED_OBJECT_ENUM_CONSTRAINT',
                 description: 'ExtendedObjectEnumChecker should be used']]
    }

    static List<String> collectEnums(RestApi rs) {
        rs.schemas.findAll { it.isEnum() }.collect { it.enumType.toString() }.sort()
    }

}
