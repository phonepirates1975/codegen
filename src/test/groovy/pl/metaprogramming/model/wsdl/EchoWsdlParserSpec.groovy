/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.wsdl

import pl.metaprogramming.model.data.DataSchema
import spock.lang.Shared
import spock.lang.Specification

import static pl.metaprogramming.utils.CheckUtils.checkList

class EchoWsdlParserSpec extends Specification {

    @Shared WsdlApi api
    @Shared String namespace = 'http://example.com/echo'

    def setupSpec() {
        api = WsdlParser.parse('src/test/resources/wsdl/echo.wsdl')
    }

    def "check service name"() {
        expect:
        api.name == 'echoService'
    }

    def "check service uri"() {
        expect:
        api.uri == 'http://localhost:8080/ws/echo'
    }

    def "check namespaceElementFormDefault"() {
        expect:
        api.namespaceElementFormDefault['http://example.com/echo'] == 'unqualified'
    }

    def "check schemas"() {
        expect:
        checkList(
                'message type fields',
                api.schemas.collect { it.key },
                ['{http://example.com/echo1}baseRequest',
                 '{http://example.com/echo1}skippedBaseRequest',
                 '{http://example.com/echo2}skippedExtendedType',
                 '{http://example.com/echo}enumType',
                 '{http://example.com/echo}extendedType',
                 '{http://example.com/echo}message',
                 '{http://example.com/echo}objectType',
                ]
        )
    }

    def "check echo operation"() {
        when:
        def operation = api.operations.find { it.name == 'echo'}
        then:
        operation != null
        operation.input.qname == toQname(namespace, 'messageIn')
        operation.output.qname == toQname(namespace, 'messageOut')
    }

    def "check message type"() {
        when:
        def schema = api.schemas[toQname(namespace, 'message')]
        then:
        schema.object
        schema.objectType.isRootElement
        checkList(
                'message type fields',
                schema.objectType.fields.collect { it.toString() },
                ['UPerCaseField STRING[namespace: http://www.w3.org/2001/XMLSchema]',
                 'dateField DATE[namespace: http://www.w3.org/2001/XMLSchema]',
                 'dateTimeField DATE_TIME[namespace: http://www.w3.org/2001/XMLSchema]',
                 'doubleField DOUBLE[namespace: http://www.w3.org/2001/XMLSchema]',
                 'enumField ENUM[enumType, namespace: http://example.com/echo, allowed: [lowercase, 0]]',
                 'listObjectField LIST OF OBJECT[objectType, fields: [stringField STRING[required, namespace: http://www.w3.org/2001/XMLSchema]]]',
                 'listStringField LIST OF STRING',
                 'nillableField STRING[required, nillable, namespace: http://www.w3.org/2001/XMLSchema]',
                 'nillableListField LIST OF STRING',
                 'objectField OBJECT[objectType, required, namespace: http://example.com/echo, fields: [stringField STRING[required, namespace: http://www.w3.org/2001/XMLSchema]]]',
                 'restrictedField STRING[required, namespace: http://example.com/echo]',
                 'stringField STRING[required, namespace: http://www.w3.org/2001/XMLSchema]',
                ]
        )
    }

    def "check objectType type"() {
        when:
        def schema = api.schemas[toQname(namespace, 'objectType')]
        then:
        schema.object
        !schema.objectType.isRootElement
        checkList(
                'objectType type fields',
                schema.objectType.fields.collect { it.toString() },
                ['stringField STRING[required, namespace: http://www.w3.org/2001/XMLSchema]']
        )
    }

    static String toString(DataSchema dataSchema) {
        def attributes = [] + dataSchema.toStringAttributes
        if (dataSchema.object || dataSchema.enum) {
            "$dataSchema.code $dataSchema.dataType.typeCode $dataSchema.qname${attributes?:''}"
        } else {
            "$dataSchema.code $dataSchema.dataType${attributes?:''}"
        }
    }

    static String toQname(String namespace, String name) {
        "{$namespace}$name"
    }
}
