/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.wsdl

import pl.metaprogramming.model.data.DataSchema
import spock.lang.Shared
import spock.lang.Specification

import static pl.metaprogramming.utils.CheckUtils.checkList

class CountriesWsdlParserSpec extends Specification {

    @Shared WsdlApi api
    @Shared String namespace = 'http://spring.io/guides/gs-producing-web-service'

    def setupSpec() {
        api = WsdlParser.parse('src/test/resources/wsdl/countries.wsdl')
    }

    def "check service name"() {
        expect:
        api.name == 'CountriesPortService'
    }

    def "check service uri"() {
        expect:
        api.uri == 'http://localhost:8080/ws'
    }

    def "check namespaceElementFormDefault"() {
        expect:
        api.namespaceElementFormDefault['http://spring.io/guides/gs-producing-web-service'] == 'qualified'
    }

    def "check getCountry operation"() {
        when:
        def operation = api.operations.find { it.name == 'getCountry'}
        then:
        operation != null
        operation.input.qname == toQname(namespace, 'getCountryRequest')
        operation.output.qname == toQname(namespace, 'getCountryResponse')
    }

    def "check currency type"() {
        when:
        def schema = api.schemas[toQname(namespace, 'currency')]
        then:
        schema.enum
        checkList(
                'currency allowed values',
                schema.enumType.allowedValues,
                ['GBP', 'EUR', 'PLN'])
    }

    def "check country type"() {
        when:
        def schema = api.schemas[toQname(namespace, 'country')]
        then:
        schema.object
        checkList(
                'country type fields',
                schema.objectType.fields.collect { toString(it) },
                ['name STRING[required]',
                 'population INT32[required]',
                 'capital STRING[required]',
                 "currency ENUM {$schema.namespace}currency[required]"
                ]
        )
    }

    def "check getCountryRequest type"() {
        when:
        def schema = api.schemas[toQname(namespace, 'getCountryRequest')]
        then:
        schema.object
        schema.objectType.isRootElement
        checkList(
                'getCountryRequest type fields',
                schema.objectType.fields.collect { toString(it) },
                ['name STRING[required]']
        )
    }

    def "check getCountryResponse type"() {
        when:
        def schema = api.schemas[toQname(namespace, 'getCountryResponse')]
        then:
        schema.object
        schema.objectType.isRootElement
        checkList(
                'getCountryResponse type fields',
                schema.objectType.fields.collect { toString(it) },
                ["country OBJECT {$schema.namespace}country[required]"]
        )
    }

    static String toString(DataSchema dataSchema) {
        def attributes = []
        if (dataSchema.isRequired) {
            attributes.add('required')
        }
        if (dataSchema.object || dataSchema.enum) {
            "$dataSchema.code $dataSchema.dataType.typeCode $dataSchema.qname${attributes?:''}"
        } else {
            "$dataSchema.code $dataSchema.dataType${attributes?:''}"
        }
    }

    static String toQname(String namespace, String name) {
        "{$namespace}$name"
    }
}
