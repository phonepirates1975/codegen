<%
 def cm = d.classCd;
%>package ${cm.packageName};

import lombok.Getter;

import java.util.function.Function;
import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public class Field<T, R> {
    @Getter private final String name;
    private final Function<T, R> valueGetter;

    public Field(String name, Function<T, R> valueGetter) {
        this.name = name;
        this.valueGetter = valueGetter;
    }

    public R getValue(T context) {
        return context != null ? valueGetter.apply(context) : null;
    }
}
