<%
 def cm = d.classCd;
%>package ${cm.packageName};

import javax.annotation.Generated;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;

@Generated("pl.metaprogramming.codegen")
public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {

    public LocalDate unmarshal(String v) throws Exception {
        return LocalDate.parse(v);
    }

    public String marshal(LocalDate v) throws Exception {
        return v.toString();
    }
}
