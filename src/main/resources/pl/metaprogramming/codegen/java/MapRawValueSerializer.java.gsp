<%
 def cm = d.classCd;
%>package ${cm.packageName};

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import javax.annotation.Generated;
import java.io.IOException;
import java.util.Map;

@Generated("pl.metaprogramming.codegen")
public class MapRawValueSerializer extends JsonSerializer<Map<String, String>> {

    @Override
    public void serialize(Map<String, String> value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartObject();
        for (Map.Entry<String, String> e: value.entrySet()) {
            gen.writeFieldName(e.getKey());
            gen.writeRawValue(e.getValue());
        }
        gen.writeEndObject();
    }
}
