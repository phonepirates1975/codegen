<%
 def cm = d.classCd;
 def vp = d.params.get(pl.metaprogramming.codegen.java.validation.ValidationParams)
%>package ${cm.packageName};

import ${d.ENUM_VALUE_INTERFACE.canonicalName};

import javax.annotation.Generated;
import java.util.*;
import java.util.function.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Generated("pl.metaprogramming.codegen")
public class CommonCheckers {

    // see https://www.debuggex.com/r/_G6Mvw1eoYJF2Bgf
    private static final String DATE_REGEX = "(?:[1-9]\\\\d{3}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1\\\\d|2[0-8])|(?:0[13-9]|1[0-2])-(?:29|30)|(?:0[13578]|1[02])-31)|(?:[1-9]\\\\d(?:0[48]|[2468][048]|[13579][26])|(?:[2468][048]|[13579][26])00)-02-29)";
    private static final String TIME_REGEX = "(?:[01]\\\\d|2[0-3]):[0-5]\\\\d:[0-5]\\\\d(?:|.\\\\d+)(?:Z|[+-][01]\\\\d:[0-5]\\\\d)";
    private static final Pattern ISO8601_DATE_TIME_PATTERN = Pattern.compile(String.format("^%sT%s\$", DATE_REGEX, TIME_REGEX));
    private static final Pattern ISO8601_DATE_PATTERN = Pattern.compile(String.format("^%s\$", DATE_REGEX));

    public static final String ERR_CODE_IS_REQUIRED = "${vp.isRequiredErrorCode}";
    public static final String ERR_CODE_IS_INVALID_DATA = "${vp.isInvalidDataErrorCode}";
    public static final String ERR_CODE_IS_NOT_ALLOWED = "${vp.isNotAllowedErrorCode}";
    public static final String ERR_CODE_IS_NOT_BASE64 = "${vp.isNotBase64ErrorCode}";
    public static final String ERR_CODE_IS_NOT_BOOLEAN = "${vp.isNotBooleanErrorCode}";
    public static final String ERR_CODE_IS_NOT_LONG = "${vp.isNotLongErrorCode}";
    public static final String ERR_CODE_IS_NOT_INT = "${vp.isNotIntErrorCode}";
    public static final String ERR_CODE_IS_NOT_FLOAT = "${vp.isNotFloatErrorCode}";
    public static final String ERR_CODE_IS_NOT_DOUBLE = "${vp.isNotDoubleErrorCode}";
    public static final String ERR_CODE_IS_NOT_NUMBER = "${vp.isNotNumberErrorCode}";
    public static final String ERR_CODE_IS_NOT_DATE = "${vp.isNotDateErrorCode}";
    public static final String ERR_CODE_IS_NOT_DATE_TIME = "${vp.isNotDateTimeErrorCode}";
    public static final String ERR_CODE_IS_NOT_ALLOWED_VALUE = "${vp.isNotAllowedValueErrorCode}";
    public static final String ERR_CODE_IS_TOO_SMALL = "${vp.isTooSmallErrorCode}";
    public static final String ERR_CODE_IS_TOO_BIG = "${vp.isTooBigErrorCode}";
    public static final String ERR_CODE_IS_TOO_SHORT = "${vp.isTooShortErrorCode}";
    public static final String ERR_CODE_IS_TOO_LONG = "${vp.isTooLongErrorCode}";
    public static final String ERR_CODE_IS_NOT_EQUAL = "${vp.isNotEqualErrorCode}";
    public static final String ERR_CODE_IS_NOT_MATCH_PATTERN = "${vp.isNotMatchPatternErrorCode}";
    public static final String ERR_CODE_HAS_TOO_FEW_ITEMS = "${vp.hasTooFewItemsErrorCode}";
    public static final String ERR_CODE_HAS_TOO_MANY_ITEMS = "${vp.hasTooManyItems}";
    public static final String ERR_CODE_HAS_DUPLICATED_ITEMS = "${vp.hasDuplicatedItems}";

    public interface DangerousConsumer<T> {
        @SuppressWarnings("java:S112")
        void accept(T value) throws Exception;
    }

    public static void writeError(ValidationContext<?> ctx, String message) {
        ctx.addError(null, message);
    }

    public static void writeError(ValidationContext<?> ctx, String message, String field, String code) {
        ctx.addError(ValidationError.builder().message(message).field(field).code(code).build());
    }

    public static boolean isNull(Object value) {
        return value == null;
    }

    public static boolean isNotNull(Object value) {
        return !isNull(value);
    }

    public static boolean isNoException(String value, DangerousConsumer<String> consumer) {
        try {
            consumer.accept(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static Checker<String> noException(DangerousConsumer<String> consumer, String dataType) {
        return noException(consumer, dataType, ERR_CODE_IS_INVALID_DATA);
    }

    public static Checker<String> noException(DangerousConsumer<String> consumer, String dataType, String code) {
        return new SimpleChecker<>(
                ctx -> isNoException(ctx.getValue(), consumer),
                ctx -> ctx.addError(code, String.format("%s is not %s", ctx.getName(), dataType)));
    }

    @SafeVarargs
    public static <T> Checker<List<T>> list(Checker<T>...checkers) {
        return context -> {
            for (int idx = 0; idx < context.getValue().size() && context.isValid(); idx++) {
                new ValidationContext<>(
                        context.getValue().get(idx),
                        String.format("%s[%d]", context.getName(), idx),
                        context).check(checkers);
            }
        };
    }

    @SafeVarargs
    public static <T> Checker<Map<String,T>> mapValues(Checker<T>...checkers) {
        return context -> {
            Iterator<String> keys = context.getValue().keySet().iterator();
            while (keys.hasNext() && context.isValid()) {
                String key = keys.next();
                new ValidationContext<>(
                        context.getValue().get(key),
                        String.format("%s[%s]", context.getName(), key),
                        context).check(checkers);
            }
        };
    }

    public static Checker<String> allow(EnumValue...values) {
        return allow(Stream.of(values).map(EnumValue::getValue).collect(Collectors.toList()), ERR_CODE_IS_NOT_ALLOWED_VALUE);
    }

    public static Checker<String> allow(Collection<String> allowedValues) {
        return allow(allowedValues, ERR_CODE_IS_NOT_ALLOWED_VALUE);
    }

    public static Checker<String> allow(Collection<String> allowedValues, String code) {
        return new SimpleChecker<>(
                ctx -> allowedValues.contains(ctx.getValue()),
                ctx -> ctx.addError(code, ctx.getName() + " should have one of values: " + allowedValues));
    }

    public static Checker<String> allow(Collection<String> allowedValues, UnaryOperator<String> preprocessor, String code) {
        return new SimpleChecker<>(
                ctx -> allowedValues.contains(preprocessor.apply(ctx.getValue())),
                ctx -> ctx.addError(code, ctx.getName() + " should have one of values: " + allowedValues));
    }

    public static Checker<String> matches(Pattern pattern) {
        return matches(pattern, ERR_CODE_IS_NOT_MATCH_PATTERN);
    }

    public static Checker<String> matches(Pattern pattern, String code) {
        return matches(pattern, code, "should match pattern: " + pattern.pattern());
    }

    public static Checker<String> matches(Pattern pattern, String code, String message) {
        return new SimpleChecker<>(
                ctx -> pattern.matcher(ctx.getValue()).matches(),
                ctx -> ctx.addError(code, ctx.getName() + " " + message));
    }

    public static <T> Checker<List<T>> size(Integer minSize, Integer maxSize) {
        return new SimpleChecker<>(
                ctx -> (minSize == null || minSize <= ctx.getValue().size()) &&
                        (maxSize == null || maxSize >= ctx.getValue().size()),
                ctx -> {
                    if (minSize != null && minSize >= ctx.getValue().size()) {
                        ctx.addError(ERR_CODE_HAS_TOO_FEW_ITEMS, ctx.getName() + " has not enough elements, min allowed size is " + minSize, minSize);
                    } else {
                        ctx.addError(ERR_CODE_HAS_TOO_MANY_ITEMS, ctx.getName() + " has too many elements, max allowed size is " + maxSize, maxSize);
                    }
                });
    }

    public static <T> Checker<List<T>> unique() {
        return ctx -> {
            if (!ctx.getValue().stream().allMatch(new HashSet<>()::add)) {
                ctx.addError(ERR_CODE_HAS_DUPLICATED_ITEMS, ctx.getName() + " has duplicated elements");
            }
        };
    }

    public static Checker<String> length(Integer minLength, Integer maxLength) {
        return new SimpleChecker<>(
                ctx -> (minLength == null || minLength <= ctx.getValue().length()) &&
                        (maxLength == null || maxLength >= ctx.getValue().length()),
                ctx -> {
                    if (minLength != null && minLength >= ctx.getValue().length()) {
                        ctx.addError(ERR_CODE_IS_TOO_SHORT, ctx.getName() + " has too short value, min allowed length is " + minLength, minLength);
                    } else {
                        ctx.addError(ERR_CODE_IS_TOO_LONG, ctx.getName() + " has too long value, max allowed length is " + maxLength, maxLength);
                    }
                });
    }

    public static <T extends Comparable<T>> Checker<T> range(T min, T max) {
        return ctx -> {
            if (min != null && min.compareTo(ctx.getValue()) > 0) {
                ctx.addError(ERR_CODE_IS_TOO_SMALL, rangeMessage(ctx.getName(), min, max));
            } else if (max != null && max.compareTo(ctx.getValue()) < 0) {
                ctx.addError(ERR_CODE_IS_TOO_BIG, rangeMessage(ctx.getName(), min, max));
            }
        };
    }

    public static <T extends Comparable<T>> Checker<String> range(Function<String,T> mapper, String minValue, String maxValue) {
        return ctx -> {
            T value = mapper.apply(ctx.getValue());
            if (minValue != null && mapper.apply(minValue).compareTo(value) > 0) {
                ctx.addError(ERR_CODE_IS_TOO_SMALL, rangeMessage(ctx.getName(), minValue, maxValue));
            } else if (maxValue != null && mapper.apply(maxValue).compareTo(value) < 0){
                ctx.addError(ERR_CODE_IS_TOO_BIG, rangeMessage(ctx.getName(), minValue, maxValue));
            }
        };
    }

    private static String rangeMessage(String field, Object min, Object max) {
        if (min != null && max != null) {
            return field + " should be >= " + min + " and <= " + max;
        }
        return field + " should be " + (min != null ? (">= " + min) : ("<= " + max));
    }

    public static <T extends Comparable<T>, P> Checker<P> gt(Field<P, String> field1, Field<P, String> field2, Function<String, T> mapper) {
        return compare(field1, field2, mapper, c -> c > 0, "%s should be greater than %s", ERR_CODE_IS_TOO_SMALL);
    }

    public static <T extends Comparable<T>, P> Checker<P> ge(Field<P, String> field1, Field<P, String> field2, Function<String, T> mapper) {
        return compare(field1, field2, mapper, c -> c >= 0, "%s should be greater equal than %s", ERR_CODE_IS_TOO_SMALL);
    }

    public static <T extends Comparable<T>, P> Checker<P> lt(Field<P, String> field1, Field<P, String> field2, Function<String, T> mapper) {
        return compare(field1, field2, mapper, c -> c < 0, "%s should be less than %s", ERR_CODE_IS_TOO_BIG);
    }

    public static <T extends Comparable<T>, P> Checker<P> le(Field<P, String> field1, Field<P, String> field2, Function<String, T> mapper) {
        return compare(field1, field2, mapper, c -> c <= 0, "%s should be less equal than %s", ERR_CODE_IS_TOO_BIG);
    }

    public static <T extends Comparable<T>, P> Checker<P> eq(Field<P, String> field1, Field<P, String> field2, Function<String, T> mapper) {
        return compare(field1, field2, mapper, c -> c == 0, "%s should be the same as %s", ERR_CODE_IS_NOT_EQUAL);
    }

    public static <T extends Comparable<T>, P> Checker<P> compare(
            Field<P, String> field1,
            Field<P, String> field2,
            Function<String, T> mapper,
            IntPredicate isComparisonValid,
            String messageTemplate,
            String code
    ) {
        return new SimpleChecker<>(
                ctx -> {
                    String value1 = field1.getValue(ctx.getValue());
                    String value2 = field2.getValue(ctx.getValue());
                    return value1 == null || value2 == null
                            || isComparisonValid.test(mapper.apply(value1).compareTo(mapper.apply(value2)));
                },
                ctx -> ctx.addError(ValidationError.builder()
                                .field(field1.getName())
                                .code(code)
                                .message(String.format(messageTemplate, field1.getName(), field2.getName()))
                                .messageArgs(new Object[]{field1.getName(), field2.getName()})
                                .build()));
    }


    public static <T extends Comparable, P> Checker<P> gt(Field<P, T> field1, Field<P, T> field2) {
        return compare(field1, field2, c -> c > 0, "%s should be greater than %s", ERR_CODE_IS_TOO_SMALL);
    }

    public static <T extends Comparable, P> Checker<P> ge(Field<P, T> field1, Field<P, T> field2) {
        return compare(field1, field2, c -> c >= 0, "%s should be greater equal than %s", ERR_CODE_IS_TOO_SMALL);
    }

    public static <T extends Comparable, P> Checker<P> lt(Field<P, T> field1, Field<P, T> field2) {
        return compare(field1, field2, c -> c < 0, "%s should be less than %s", ERR_CODE_IS_TOO_BIG);
    }

    public static <T extends Comparable, P> Checker<P> le(Field<P, T> field1, Field<P, T> field2) {
        return compare(field1, field2, c -> c <= 0, "%s should be less equal than %s", ERR_CODE_IS_TOO_BIG);
    }

    public static <T extends Comparable, P> Checker<P> eq(Field<P, T> field1, Field<P, T> field2) {
        return compare(field1, field2, c -> c == 0, "%s should be the same as %s", ERR_CODE_IS_NOT_EQUAL);
    }

    @SuppressWarnings("unchecked")
    public static <T extends Comparable, P> Checker<P> compare(
            Field<P, T> field1,
            Field<P, T> field2,
            IntPredicate isComparisonValid,
            String messageTemplate,
            String code
    ) {
        return new SimpleChecker<>(ctx -> {
            T value1 = field1.getValue(ctx.getValue());
            T value2 = field2.getValue(ctx.getValue());
            return value1 == null || value2 == null
                    || isComparisonValid.test(value1.compareTo(value2));
        }, ctx -> ctx.addError(ValidationError.builder()
                .field(field1.getName())
                .code(code)
                .message(String.format(messageTemplate, field1.getName(), field2.getName()))
                .messageArgs(new Object[]{field1.getName(), field2.getName()})
                .build()));
    }


    @SuppressWarnings("unchecked")
    public static <T> Checker<T> required() {
        return (Checker<T>) REQUIRED_CHECKER;
    }

    @SuppressWarnings("unchecked")
    public static <T> Checker<T> notAllowed() {
        return (Checker<T>) NOT_ALLOWED_CHECKER;
    }

    private static final Checker<Object> REQUIRED_CHECKER = new SimpleChecker<>(
            ctx -> isNotNull(ctx.getValue()),
            ctx -> writeError(ctx, ctx.getName() + " is required", ctx.getName(), ERR_CODE_IS_REQUIRED),
            true);

    private static final Checker<Object> NOT_ALLOWED_CHECKER = new SimpleChecker<>(
            ctx -> ctx.getValue() instanceof Collection ? ((Collection) ctx.getValue()).isEmpty() : isNull(ctx.getValue()),
            ctx -> writeError(ctx,ctx.getName() + " is not allowed", ctx.getName(), ERR_CODE_IS_NOT_ALLOWED));

    public static final Checker<String> BOOLEAN = allow(Arrays.asList("true", "false"), String::toLowerCase, ERR_CODE_IS_NOT_BOOLEAN);

    public static final Checker<String> INT32 = noException(Integer::parseInt, "32bit integer", ERR_CODE_IS_NOT_INT);

    public static final Checker<String> INT64 = noException(Long::parseLong, "64bit integer", ERR_CODE_IS_NOT_LONG);

    public static final Checker<String> FLOAT = noException(Float::parseFloat, "float", ERR_CODE_IS_NOT_FLOAT);

    public static final Checker<String> DOUBLE = noException(Double::parseDouble, "double", ERR_CODE_IS_NOT_DOUBLE);

    public static final Checker<String> ISO_DATE = matches(ISO8601_DATE_PATTERN, ERR_CODE_IS_NOT_DATE, "is not yyyy-MM-dd");

    public static final Checker<String> ISO_DATE_TIME = matches(ISO8601_DATE_TIME_PATTERN, ERR_CODE_IS_NOT_DATE_TIME, "should be valid date time in ISO8601 format");

    public static final Checker<String> BASE64 = noException(Base64.getDecoder()::decode, "base64", ERR_CODE_IS_NOT_BASE64);
}
