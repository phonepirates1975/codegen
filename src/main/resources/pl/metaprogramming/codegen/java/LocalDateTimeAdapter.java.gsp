<%
 def cm = d.classCd;
%>package ${cm.packageName};

import javax.annotation.Generated;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDateTime;

@Generated("pl.metaprogramming.codegen")
public class LocalDateTimeAdapter extends XmlAdapter<String, LocalDateTime> {

    public LocalDateTime unmarshal(String v) throws Exception {
        return LocalDateTime.parse(v);
    }

    public String marshal(LocalDateTime v) throws Exception {
        return v.toString();
    }
}
