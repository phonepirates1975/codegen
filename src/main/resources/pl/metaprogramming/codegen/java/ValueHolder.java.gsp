<%
 def cm = d.classCd;
%>package ${cm.packageName};

import javax.annotation.Generated;
import java.io.Serializable;

@Generated("pl.metaprogramming.codegen")
public class ${cm.className}<T> implements Serializable {

    private T value;

    public boolean isPresent() {
        return value != null;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        if (isPresent()) {
            throw new RuntimeException("Value already set");
        }
        this.value = value;
    }
}
