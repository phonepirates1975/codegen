/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas

import groovy.json.JsonOutput
import org.yaml.snakeyaml.DumperOptions
import org.yaml.snakeyaml.Yaml

import java.nio.charset.StandardCharsets

class OpenapiWriter {

    static void write(RestApi api, File file) {
        new OpenapiWriter(rootApi: api, outputCfg: [(api): file]).flush()
    }

    static void write(RestApi api, Map<RestApi, File> outputCfg) {
        new OpenapiWriter(rootApi: api, outputCfg: outputCfg).flush()
    }

    String charset = StandardCharsets.UTF_8.name()
    String lineSeparator = System.lineSeparator()
    RestApi rootApi
    Map<RestApi, File> outputCfg
    DumperOptions yamlDumperOptions = defaultDumperOptions()

    void flush() {
        outputCfg.each {
            flush(it.key, it.value)
        }
    }

    void flush(RestApi api, File file) {
        file.parentFile.mkdirs()
        Map openApi = RestApiMapper.toOpenApi(api, outputCfg)
        String outText = file.name.endsWith('json')
                ? JsonOutput.prettyPrint(JsonOutput.toJson(openApi))
                : new Yaml(yamlDumperOptions).dump(openApi)
        file.withWriter(charset) { writer ->
            outText.eachLine { line ->
                writer.write(line)
                writer.write(lineSeparator)
            }
        }
    }

    private static DumperOptions defaultDumperOptions() {
        DumperOptions options = new DumperOptions()
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK)
        options.setDefaultScalarStyle(DumperOptions.ScalarStyle.PLAIN)
        options.setPrettyFlow(true)
        options
    }

}
