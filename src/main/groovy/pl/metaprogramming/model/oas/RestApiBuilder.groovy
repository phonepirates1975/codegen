/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas


import pl.metaprogramming.model.data.DataSchema

class RestApiBuilder {

    RestApi model

    RestApiModelChecker checker

    RestApiBuilder(boolean isOas2 = false) {
        model = new RestApi()
        checker = new RestApiModelChecker(isOas2: isOas2)
    }

    void dependsOn(List<RestApi> dependsOn) {
        this.model.dependsOn = dependsOn
    }

    void addOperation(Operation operation) {
        checker.checkOperation(operation)
        operation.api = model
        model.operations.add(operation)
    }

    void addSchema(DataSchema schema) {
        checker.checkDataCode(schema.code)
        model.addSchema(schema)
    }

    void addParam(Parameter param) {
        checker.checkCommonParamCode(param.code)
        model.parameters.add(param)
    }

    DataSchema schemaRef(String code) {
        def result = model.getSchema(code)
        if (!result) {
            throw new IllegalStateException("Unknown schema $code")
        }
        result
    }

    Parameter paramRef(String code) {
        def result = model.getParameter(code)
        if (!result) {
            throw new IllegalStateException("Unknown parameter $code")
        }
        result
    }

}
