/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas

import pl.metaprogramming.codegen.Model
import pl.metaprogramming.model.data.*

import java.util.function.Consumer
import java.util.function.Predicate

class RestApi implements Model {

    List<Operation> operations = []

    List<Parameter> parameters = []
    List<DataSchema> schemas = []

    List<RestApi> dependsOn = []

    Map<String, Object> additives = [:]

    String title
    String version

    static RestApi of(String openApiContent, RestApi... dependencies) {
        OpenapiParser.parse(openApiContent, dependencies)
    }

    @Override
    String getName() {
        title
    }

    def collectObjectTypes(Set<ObjectType> result, DataType dataType) {
        if (dataType instanceof ObjectType) {
            result.add((ObjectType) dataType)
            dataType.inherits.each { collectObjectTypes(result, it) }
            dataType.fields.each { collectObjectTypes(result, it.dataType) }
        } else if (dataType instanceof ArrayType) {
            collectObjectTypes(result, dataType.itemsSchema.dataType)
        } else if (dataType instanceof MapType) {
            collectObjectTypes(result, dataType.valuesSchema.dataType)
        }
    }

    Operation getOperation(String operationCode) {
        operations.find { it.code == operationCode }
    }

    Operation getOperation(String group, String operationCode) {
        operations.find { it.group == group && it.code == operationCode }
    }

    Map<String, List<Operation>> getGroupedOperations() {
        RestApiModelChecker checker = new RestApiModelChecker()
        operations
                .each { checker.checkOperationNames(it) }
                .groupBy { it.group }
    }

    RestApi addParameter(Parameter parameter) {
        if (parameters.any { it.code == parameter.code }) {
            throw new IllegalStateException("Parameter with code '${parameter.code}' is already defined")
        }
        parameters.add(parameter)
        this
    }

    Parameter getParameter(String codeOrName) {
        parameters.find { it.code == codeOrName || it.name == codeOrName }
                ?: dependsOn.findResult { it.getParameter(codeOrName) }
    }

    RestApi forEachOperation(Consumer<Operation> consumer) {
        operations.each { it ->
            consumer.accept(it)
        }
        this
    }

    RestApi removeOperations(Predicate<Operation> predicate) {
        operations.removeIf(predicate)
        this
    }

    RestApi addSchema(DataSchema schema) {
        if (schemas.any { it.code == schema.code }) {
            throw new IllegalStateException("Schema with code '${schema.code}' is already defined")
        }
        schemas.add(schema)
        this
    }

    DataSchema getSchema(String code) {
        if (code.contains('.')) {
            def split = code.split('\\.')
            getSchema(split[0]).objectType.fields.find { it.code == split[1] }
        } else {
            schemas.find { it.code == code }
                    ?: dependsOn.findResult { it.getSchema(code) }
        }
    }

    RestApi updateSchema(String code, Consumer<DataSchema> updater) {
        updater.accept(getSchema(code))
        this
    }

    DataType getDefaultErrorResponse() {
        UsageCounter<DataType> errorTypesCounter = new UsageCounter<>()
        operations.each {
            errorTypesCounter.count(it.defaultResponse?.schema?.dataType)
        }
        if (errorTypesCounter.dominant == null) {
            operations.each {
                it.errorResponses.each {
                    errorTypesCounter.count(it.schema?.dataType)
                }
            }
        }
        errorTypesCounter.dominant
    }

    static class UsageCounter<T> {
        private Map<T, Integer> counter = [:]
        int all

        void count(T obj) {
            if (obj) {
                counter[obj] = (counter[obj] ?: 0) + 1
                ++all
            }
        }

        T getDominant() {
            for (def v : counter) {
                if (v.value > (all / 2)) {
                    return v.key
                }
            }
            return null
        }
    }
}
