/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas

import pl.metaprogramming.model.oas.parser.*

import java.nio.charset.Charset
import java.nio.charset.StandardCharsets

class OpenapiParser extends BaseParser {

    private OasVersion version
    private DefinitionsParser definitionsParser
    private ParametersParser parametersParser
    private OperationsParser operationsParser
    private EnumParser enumParser

    static RestApi parse(String filepath) {
        parse(filepath, StandardCharsets.UTF_8)
    }

    static RestApi parse(String filepath, Charset charset) {
        parse(filepath, charset, new OpenapiParserConfig())
    }

    static RestApi parse(String filepath, OpenapiParserConfig config) {
        parse(filepath, StandardCharsets.UTF_8, config)
    }

    static RestApi parse(String filepath, Charset charset, OpenapiParserConfig config) {
        new OpenapiParser(JsonYamlParser.read(new File(filepath), charset), config).read()
    }

    static RestApi parse(String openApiContent, RestApi...dependencies) {
        def isJson = openApiContent.startsWith('{')
        def model = isJson ? JsonYamlParser.readJson(openApiContent): JsonYamlParser.readYaml(openApiContent)
        OpenapiParserConfig config = new OpenapiParserConfig(dependsOn: Arrays.asList(dependencies))
        new OpenapiParser(model, config).read()
    }

    OpenapiParser(Map spec, OpenapiParserConfig config) {
        this.spec = spec
        this.config = config
    }

    RestApi read() {
        setup()
        definitionsParser.readDefinitions()
        parametersParser.readParameters()
        operationsParser.readOperations()
        enumParser.addEnums()
        builder.model
    }

    def getWarnings() {
        builder.checker.warnings
    }

    void setup() {
        readVersion()
        setupBuilder()
        readInfo()
        enumParser = new EnumParser(this)
        definitionsParser = new DefinitionsParser(this, enumParser)
        parametersParser = new ParametersParser(this, definitionsParser)
        operationsParser = makeOperationsParser().setup(this, definitionsParser, parametersParser)
    }

    void readVersion() {
        if (spec.swagger == '2.0' || partialOas2) {
            verParsingStrategy = new Oas2ParsingStrategy()
            version = OasVersion.OAS2
        } else if ((spec.openapi as String)?.startsWith('3.0') || isPartialOas3()) {
            verParsingStrategy = new Oas3ParsingStrategy()
            version = OasVersion.OAS3
        } else {
            throw new IllegalArgumentException("The API specification is neither Swagger 2.0 nor OpenAPI 3.0.x")
        }
    }

    void setupBuilder() {
        builder = new RestApiBuilder(version == OasVersion.OAS2)
        builder.dependsOn(config.dependsOn)
        builder.checker.weekValidation = config.weekValidation
    }

    void readInfo() {
        if (spec.info instanceof Map) {
            def info = spec.info as Map<String, String>
            builder.model.version = info.version
            builder.model.title = info.title
        }
    }

    boolean isPartialOas2() {
        config.partialFile && (spec.schemas || spec.parameters)
    }

    boolean isPartialOas3() {
        config.partialFile && spec.components
    }

    private OperationsParser makeOperationsParser() {
        version == OasVersion.OAS3 ? new Oas3OperationsParser() : new Oas2OperationsParser()
    }

    enum OasVersion {
        OAS2, OAS3
    }
}
