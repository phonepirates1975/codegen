/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas

import groovy.transform.ToString
import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.ObjectType

@ToString(includes = ['code', 'type', 'path', 'group'], includePackage = false)
class Operation {

    static final List FORM_CONTENT_TYPES = ['multipart/form-data', 'application/x-www-form-urlencoded']

    RestApi api
    String group
    String code
    String path
    OperationType type
    List<Parameter> parameters = []
    HttpRequestBody requestBody
    List<HttpResponse> responses
    Map additives = [:]

    ObjectType requestSchema

    def methodMissing(String methodName, Object... args) {
        if (this.hasProperty(methodName)) {
            this.setProperty(methodName, args[0])
        } else {
            throw new MissingMethodException(methodName, this.class, args)
        }
    }

    ObjectType getRequestSchema() {
        if (!requestSchema) {
            def reqAdditives = [:]
            additives.each {
                if (it.key instanceof String && (it.key as String).startsWith('x-')) {
                    reqAdditives.put(it.key, it.value)
                }
            }
            requestSchema = new ObjectType(additives: reqAdditives)
            parameters.each {
                requestSchema.fields.add(it)
            }
            if (requestBody) {
                if (multipartFormDataRequestBody) {
                    multipartFormDataRequestBody.fields.each {
                        requestSchema.fields.add(new Parameter(ParamLocation.FORMDATA, it))
                    }
                } else {
                    requestSchema.fields.add(requestBody.schema)
                }
            }
        }
        requestSchema
    }

    ObjectType getMultipartFormDataRequestBody() {
        multipartFormDataRequestSchema?.objectType
    }

    DataSchema getMultipartFormDataRequestSchema() {
        requestBody?.contents
                ?.find { FORM_CONTENT_TYPES.contains(it.key) }
                ?.value
    }

    DataSchema getRequestBodySchema() {
        multipartFormDataRequestSchema ?: requestBody?.schema
    }

    List<Parameter> getParameters(ParamLocation location) {
        parameters.findAll { it.location == location }
    }

    HttpResponse getSuccessResponse() {
        responses.find { it.successResponse }
    }

    DataSchema getSuccessResponseSchema() {
        successResponse?.schema
    }

    Collection<String> getSuccessResponseHeaders() {
        successResponse?.headers
    }

    HttpResponse getDefaultResponse() {
        responses.find { it.default }
    }

    Collection<HttpResponse> getErrorResponses() {
        responses.findAll { (it.default || it.status >= 400) && it.schema }
    }

    String getDescription() {
        [additives.summary, additives.description]
                .findAll { it != null }
                .join(Codegen.NEW_LINE)
    }

    List<String> getProduces() {
        List<String> result = []
        responses.each { it.contents?.keySet()?.each { result.add(it as String) } }
        result.unique()
    }

    List<String> getConsumes() {
        if (requestBody) {
            List<String> result = []
            requestBody.contents.keySet().each { result.add(it as String) }
            result.unique()
        } else {
            Collections.emptyList()
        }
    }

    boolean isMultipart() {
        getParameters(ParamLocation.FORMDATA) || multipartFormDataRequestBody
    }

    boolean isOctetStream() {
        requestBody?.contents?.values()?.any { it.isBinary() }
    }

    boolean isConsumeJson() {
        consumes.contains('application/json')
    }
}
