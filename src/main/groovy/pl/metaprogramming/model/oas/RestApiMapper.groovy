/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas

import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.DataTypeCode
import pl.metaprogramming.model.data.ObjectType

import static pl.metaprogramming.model.data.DataTypeCode.*

class RestApiMapper {
    RestApi api
    Map<RestApi, File> outputCfg
    Map rootNode = [:]
    private List<RestApi> allApis


    static Map toOpenApi(RestApi api, Map<RestApi, File> outputCfg) {
        def mapper = new RestApiMapper(api: api, outputCfg: outputCfg)
        mapper.map()
        mapper.rootNode
    }

    void map() {
        rootNode.openapi = '3.0.3'
        rootNode.info = [title: api.title, version: api.version]
        rootNode.putAll(api.additives as Map)
        allApis = [api] + api.dependsOn
        addOperations()
        addSchemas()
        addParameters()
    }

    boolean getFlatten() {
        outputCfg.size() == 1
    }

    void addOperations() {
        api.operations.each {
            def out = getNode('paths', it.path, it.type.name().toLowerCase())
            out.operationId = it.code
            out.putAll(it.additives)
            setNode(out, 'parameters', it.parameters.collect { parameterRefSpec(it) })
            setNode(out, 'requestBody', requestBodySpec(it))
            setNode(out, 'responses', responsesSpec(it))
        }
    }

    void addParameters() {
        getParameters(api).each {
            setNode(rootNode, "components.parameters.${it.code}", parameterSpec(it))
        }
    }

    void addSchemas() {
        getSchemas(api).each {
            def out = getNode('components', 'schemas', it.code)
            setNode(out, 'description', it.description)
            schemaDefSpec(it, out)
        }
    }

    private Map getNode(String... path) {
        getNode(Arrays.asList(path), [:])
    }

    private void setNode(Map root, String nodeKey, def nodeValue) {
        if (nodeValue) {
            def path = nodeKey.contains('.') ? Arrays.asList(nodeKey.split('\\.')) : [nodeKey]
            getNode(path, nodeValue, root)
        }
    }

    private <T> T getNode(List<String> path, T defaultValue, Map root = rootNode) {
        Map subNode = root
        for (int i = 0; i < path.size() - 1; i++) {
            def key = path[i]
            if (!subNode.containsKey(key)) {
                subNode.put(key, [:])
            }
            subNode = subNode[key] as Map
        }
        String key = path.last()
        if (!subNode[key]) {
            subNode[key] = defaultValue
        }
        subNode[key] as T
    }

    private Map parameterRefSpec(Parameter param) {
        def paramRef = getParamRef(param.code)
        paramRef ? ['$ref': paramRef] : parameterSpec(param)
    }

    private Map parameterSpec(Parameter param) {
        def out = [:]
        out.name = param.name
        out.in = param.location.name().toLowerCase()
        setNode(out, 'description', param.description)
        setNode(out, 'required', param.isRequired ? true : null)
        out.schema = schemaRefSpec(param)
        out
    }

    private Map requestBodySpec(Operation operation) {
        if (!operation.requestBody?.contents) {
            return null
        }
        def result = [:]
        setNode(result, 'description', operation.requestBody.description)
        setNode(result, 'required', operation.requestBody.required)
        operation.requestBody.contents.each {
            setNode(result, "content.${it.key}.schema", schemaRefSpec(it.value))
        }
        result
    }

    private Map responsesSpec(Operation operation) {
        if (!operation.responses) {
            return null
        }
        def result = [:]
        operation.responses.each {
            def res = [:]
            setNode(res, 'description', it.description)
            it.contents?.each {
                setNode(res, "content.${it.key}.schema", schemaRefSpec(it.value))
            }
            result.put(it.default ? 'default' : it.status, res)
        }
        result
    }

    private Map schemaRefSpec(DataSchema schema) {
        def out = [:]
        def ref = schema.object && schema.objectType.code
                ? getSchemaRef(schema.objectType.code)
                : schema.enum ? getSchemaRef(schema.enumType.code)
                : null
        if (ref) {
            setNode(out, '$ref', ref)
        } else {
            schemaDefSpec(schema, out)
        }
        out.putAll(schema.additives.findAll { it.key != 'x-ms-enum' })
        out
    }

    private Map schemaDefSpec(DataSchema schema, Map out) {
        out.type = DATA_TYPE_MAP.get(schema.dataType.typeCode)
        if (schema.object) {
            objectSpec(schema.objectType, out)
        } else if (schema.enum) {
            out.enum = schema.enumType.allowedValues
            setNode(out, "x-ms-enum.values", schema.enumType.descriptions?.collect {
                [value: it.key, description: it.value]
            })
        } else if (schema.array) {
            out.items = schemaRefSpec(schema.arrayType.itemsSchema)
            setNode(out, 'minItems', schema.arrayType.minItems)
            setNode(out, 'maxItems', schema.arrayType.maxItems)
            setNode(out, 'uniqueItems', schema.arrayType.uniqueItems)
        } else if (schema.map) {
            out.additionalProperties = schemaRefSpec(schema.mapType.valuesSchema)
        } else if (['number', 'integer'].contains(out.type)) {
            setNode(out, 'maximum', toNumber(schema.maximum))
            setNode(out, 'minimum', toNumber(schema.minimum))
            setNode(out, 'format', dataFormatSpec(schema))
        } else if (out.type == 'string') {
            setNode(out, 'maxLength', schema.maxLength)
            setNode(out, 'minLength', schema.minLength)
            setNode(out, 'format', dataFormatSpec(schema))
            setNode(out, 'pattern', schema.pattern)
        }
        out
    }

    private static Object toNumber(String value) {
        if (value) {
            value.contains('.') ? new BigDecimal(value) : Long.valueOf(value)
        } else {
            null
        }
    }

    private void objectSpec(ObjectType obj, Map out) {
        if (obj.inherits) {
            out.allOf = [['$ref': getSchemaRef(obj.inherits[0].code)],
                         objectSpec(obj)]
        } else {
            out.putAll(objectSpec(obj))
        }
        out.putAll(obj.additives)
    }

    private Map objectSpec(ObjectType obj) {
        def out = [type: 'object']
        setNode(out, 'required', obj.fields.findAll { it.isRequired }.collect { it.code })
        if (obj.fields) {
            out.properties = obj.fields.collectEntries {
                def fieldSpec = schemaRefSpec(it)
                setNode(fieldSpec, 'default', it.defaultValue)
                fieldSpec.putAll(it.additives)
                [(it.code): fieldSpec]
            }
        }
        out
    }

    private List<Parameter> getParameters(RestApi api) {
        List<Parameter> result = [] + api.parameters
        if (flatten) {
            api.dependsOn.each {
                result.addAll(getParameters(it))
            }
        }
        result.unique()
    }

    private List<DataSchema> getSchemas(RestApi api) {
        List<DataSchema> result = [] + api.schemas
        if (flatten) {
            api.dependsOn.each {
                result.addAll(getSchemas(it))
            }
        }
        result.unique()
    }

    private String getParamRef(String paramCode) {
        RestApi ownerApi = allApis.findResult {
            it.parameters.find { it.code == paramCode } ? it : null
        } as RestApi
        ownerApi ? "${getApiFilename(ownerApi)}#/components/parameters/$paramCode" : null
    }

    private String getSchemaRef(String schemaCode) {
        RestApi ownerApi = allApis.findResult {
            it.schemas.find { it.code == schemaCode } ? it : null
        } as RestApi
        ownerApi ? "${getApiFilename(ownerApi)}#/components/schemas/$schemaCode" : null
    }

    private String getApiFilename(RestApi restApi) {
        flatten || api == restApi ? '' : outputCfg[restApi].name
    }

    private static String dataFormatSpec(DataSchema schema) {
        schema.format ?: FORMAT_DATA_TYPE_MAP.get(schema.dataType.typeCode)
    }

    private static Map<DataTypeCode, String> DATA_TYPE_MAP = [
            (NUMBER)   : 'number',
            (DECIMAL)  : 'number',
            (FLOAT)    : 'number',
            (DOUBLE)   : 'number',
            (INT16)    : 'integer',
            (INT32)    : 'integer',
            (INT64)    : 'integer',
            (BOOLEAN)  : 'boolean',
            (STRING)   : 'string',
            (ENUM)     : 'string',
            (DATE)     : 'string',
            (DATE_TIME): 'string',
            (BASE64)   : 'string',
            (BINARY)   : 'string',
            (OBJECT)   : 'object',
            (MAP)      : 'object',
            (ARRAY)    : 'array',
    ]

    private static Map<DataTypeCode, String> FORMAT_DATA_TYPE_MAP = [
            (FLOAT)    : 'float',
            (DOUBLE)   : 'double',
            (INT32)    : 'int32',
            (INT64)    : 'int64',
            (DATE)     : 'date',
            (DATE_TIME): 'date-time',
    ]

}
