/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.parser

import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.oas.Parameter
import pl.metaprogramming.model.oas.ParamLocation

class ParametersParser extends BaseParser {

    DefinitionsParser definitionsParser

    ParametersParser(BaseParser template, DefinitionsParser definitionsParser) {
        configure(template)
        this.definitionsParser = definitionsParser
    }

    void readParameters() {
        log "Going to parse parameters..."
        verParsingStrategy.getParameters(spec)?.each { code, spec ->
            log "  $code"
            def paramDef = toParameter(spec, code)
            builder.addParam(paramDef)
        }
    }

    Parameter toParameter(Map spec, String code = null) {
        Parameter paramDef = new Parameter(getParamLocation(spec), spec.name as String, code)
        paramDef.setIsRequired(Boolean.TRUE == spec.required)
        definitionsParser.fillDataSchema(paramDef, verParsingStrategy.getParameterSchema(spec))
        setAdditives(paramDef, spec)
        fixEnumCode(paramDef, paramDef.code)
        paramDef
    }

    static ParamLocation getParamLocation(Map paramSpec) {
        ParamLocation.valueOf(((String) paramSpec.in).toUpperCase())
    }

    static DataSchema fixEnumCode(DataSchema schema, String code) {
        if (schema.isEnum()) {
            if (!schema.enumType.code) {
                schema.enumType.code = code
            }
        } else if (schema.isArray()) {
            fixEnumCode(schema.arrayType.itemsSchema, code)
        }
        schema
    }

}
