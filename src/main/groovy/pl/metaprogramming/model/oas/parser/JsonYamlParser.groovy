/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.parser

import groovy.json.JsonSlurper
import org.yaml.snakeyaml.DumperOptions
import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.constructor.Constructor
import org.yaml.snakeyaml.nodes.Tag
import org.yaml.snakeyaml.representer.Representer
import org.yaml.snakeyaml.resolver.Resolver

import java.nio.charset.Charset
import java.nio.charset.StandardCharsets

class JsonYamlParser {

    static Map read(File file, Charset charset = StandardCharsets.UTF_8) {
        read(file.absolutePath, file.getText(charset.name()))
    }

    static Map read(String filename, String content) {
        if (isJson(filename)) {
            return readJson(content)
        } else if (isYaml(filename)) {
            return readYaml(content)
        }
        throw new RuntimeException("Can't determine file type (json/yaml) for $filename")

    }

    static Map readYaml(String body) {
        new Yaml(new Constructor(), new Representer(), new DumperOptions(), new YamlTypeResolver())
                .load(body)
    }

    static Map readJson(String body) {
        (Map) new JsonSlurper().parseText(body)
    }

    static class YamlTypeResolver extends Resolver {
        protected void addImplicitResolvers() {
            addImplicitResolver(Tag.BOOL, BOOL, "yYnNtTfFoO")
            addImplicitResolver(Tag.INT, INT, "-+0123456789")
            addImplicitResolver(Tag.NULL, NULL, "~nN\0")
            addImplicitResolver(Tag.NULL, EMPTY, null)
        }
    }

    private static boolean isJson(String filename) {
        filename.toLowerCase().endsWith('.json')
    }

    private static boolean isYaml(String filename) {
        filename.toLowerCase().endsWith('.yaml') || filename.toLowerCase().endsWith('.yml')
    }

}
