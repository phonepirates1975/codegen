/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.parser

import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.oas.HttpRequestBody

class Oas3OperationsParser extends OperationsParser {
    @Override
    HttpRequestBody getRequestBody(OpenapiOperation operation) {
        def requestBody = operation.spec.requestBody as Map
        if (!requestBody) {
            return null
        }
        HttpRequestBody result = new HttpRequestBody(
                description: requestBody.description as String,
                required: (requestBody.required as Boolean) ?: false,
                code: (operation.spec['x-codegen-request-body-name'] as String) ?: 'requestBody',
                contents: ((requestBody.content as Map<String, Map>).collectEntries {
                    [(it.key): toSchema(it.value.schema as Map)]
                } as Map<String, DataSchema>)
        )
        result
    }

    @Override
    Map<String, DataSchema> getResponseSchema(Map responseSpec, OpenapiOperation operation) {
        def content = responseSpec.content as Map<String, Map>
        if (content == null) {
            return null
        }
        content.collectEntries {
            [(it.key): toSchema(it.value.schema as Map)]
        } as Map<String, DataSchema>
    }
}
