/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.parser

import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.oas.HttpRequestBody
import pl.metaprogramming.model.oas.Operation
import pl.metaprogramming.model.oas.OperationType
import pl.metaprogramming.model.oas.RestApiModelChecker

class Oas2OperationsParser extends OperationsParser {

    @Override
    HttpRequestBody getRequestBody(OpenapiOperation operation) {
        parseBodyParam(operation) ?: formDataParamsToRequestBody(operation)
    }

    HttpRequestBody parseBodyParam(OpenapiOperation operation) {
        def bodyParam = operation.spec.parameters?.find { Map it -> it.in == 'body' } as Map
        if (!bodyParam) {
            return null
        }
        def result = new HttpRequestBody(
                code: bodyParam.name as String,
                description: bodyParam.description as String,
                required: bodyParam.required as Boolean,
                contents: getOperationConsumes(operation).collectEntries {
                    [(it): toSchema(bodyParam.schema as Map)]
                } as Map<String, DataSchema>
        )
        result
    }

    HttpRequestBody formDataParamsToRequestBody(OpenapiOperation operation) {
        def formDataParams = operation.spec.parameters?.findAll { Map it -> it.in == 'formData' } as List<Map>
        if (!formDataParams) {
            return null
        }
        def consumes = getOperationConsumes(operation).findAll { Operation.FORM_CONTENT_TYPES.contains(it) }
        if (!consumes) {
            throw new IllegalStateException("Operation with 'formData' parameters should consumes one of content type: ${Operation.FORM_CONTENT_TYPES}")
        }
        def schemaSpec = [
                type      : 'object',
                properties: formDataParams.collectEntries {
                    [(it.name): [
                            description: it.description,
                            type       : it.type]]
                },
                required  : formDataParams
                        .findAll { it.required }
                        .collect { it.name }
        ] as Map<String, Object>
        def schema = definitionsParser.toRootDataSchema(schemaSpec, 'requestBody')
        def result = new HttpRequestBody(
                code: 'requestBody',
                required: true,
                contents: consumes.collectEntries {
                    [(it): schema]
                } as Map<String, DataSchema>
        )
        result
    }

    @Override
    Map<String, DataSchema> getResponseSchema(Map responseSpec, OpenapiOperation operation) {
        def schema = toSchema(responseSpec.schema as Map)
        getOperationProduces(operation).collectEntries {
            [(it): schema]
        }
    }

    List<String> getOperationConsumes(OpenapiOperation operation) {
        if (operation.spec.consumes) {
            return (List<String>) operation.spec.consumes
        }
        if (spec.consumes) {
            return (List<String>) spec.consumes
        }
        OperationType type = OperationType.valueOf(operation.operationType.toUpperCase())
        if (RestApiModelChecker.isOperationWithBody(type)) {
            return ['application/json']
        }
        null
    }

    List<String> getOperationProduces(OpenapiOperation operation) {
        (operation.spec.produces ?: spec.produces) as List<String>
    }
}
