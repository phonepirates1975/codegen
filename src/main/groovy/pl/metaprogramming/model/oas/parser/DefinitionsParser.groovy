/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.parser

import org.codehaus.groovy.runtime.InvokerHelper
import pl.metaprogramming.model.data.*

import static pl.metaprogramming.model.data.DataType.*

class DefinitionsParser extends BaseParser {

    static final String FORMAT_DATE_TIME = 'date-time'
    static final String FORMAT_DATE = 'date'

    private Map<String, Closure<? extends DataType>> DATA_TYPE_CREATORS = [
            'string' : { Map spec ->
                if (spec.enum) {
                    enumParser.toEnumType(spec)
                } else if (spec.format == FORMAT_DATE) {
                    DATE
                } else if (spec.format == FORMAT_DATE_TIME) {
                    DATE_TIME
                } else if (spec.format == 'binary') {
                    BINARY
                } else if (spec.format == 'byte') {
                    BASE64
                } else {
                    TEXT
                }
            },
            'integer': { Map spec -> spec.format == 'int64' ? INT64 : INT32 },
            'number' : { Map spec -> spec.format == 'float' ? FLOAT : spec.format == 'double' ? DOUBLE : DECIMAL },
            'boolean': { BOOLEAN },
            'array'  : { Map spec -> new ArrayType(itemsSchema: toDataSchema((Map) spec.items)) },
            'map'    : { Map spec -> new MapType(valuesSchema: toDataSchema((Map) spec.additionalProperties)) },
            'object' : { Map spec -> toObjectType(spec) },
            'file'   : { BINARY }, // OAS2
            'oauth2' : { TEXT },
            'apiKey' : { TEXT }
    ]

    EnumParser enumParser

    DefinitionsParser(BaseParser template, EnumParser enumParser) {
        configure(template)
        this.enumParser = enumParser
    }

    void readDefinitions() {
        log "Going to parse definitions..."
        definitions.collect {
            new DataSchemaBuilder(it.key, it.value)
        }.each {
            it.make()
        }
    }

    Map<String, Map> getDefinitions() {
        Map<String, Map> result = new HashMap<>(verParsingStrategy.getSchemas(spec))
        if (spec.securityDefinitions) {
            result.putAll((spec.securityDefinitions as Map<String, Map>))
        }
        result
    }

    ObjectType toObjectType(Map spec) {
        List<DataSchema> properties = []
        List<ObjectType> inherits = []
        if (spec.allOf) {
            spec.allOf.each { Map childSpec ->
                if (childSpec[REF]) {
                    inherits.add(builder.schemaRef(getRefObjectName(childSpec)).objectType)
                } else {
                    def childType = toObjectType(childSpec)
                    properties.addAll(childType.fields)
                    inherits.addAll(childType.inherits)
                }
            }
        } else {
            def specProperties = (spec['properties'] as Map<String, Map<String, String>>)
            check(specProperties != null && !specProperties.isEmpty(),
                    'object should have properties', 'object should have properties')
            specProperties?.each {
                properties.add(toDataSchema(it.value, it.key))
            }
            spec.required.each { propName ->
                def property = properties.find { it.code == propName }
                if (property) {
                    property.isRequired = true
                } else {
                    log "FAILED: Can't set required flag on $propName in object spec: $spec"
                }
            }
        }
        new ObjectType(fields: properties, inherits: inherits)
    }

    public <T extends DataSchema> T fillDataSchema(T schema, Map<String, String> spec) {
        schema.dataType = toDataType(spec)
        assert schema.dataType, "Can't handle $spec"
        schema.defaultValue = spec.default
        schema.format = spec.format
        schema.pattern = spec.pattern
        schema.minLength = toInt(spec.minLength)
        schema.maxLength = toInt(spec.maxLength)
        schema.minimum = spec.minimum
        schema.maximum = spec.maximum
        if (schema.isArray()) {
            schema.arrayType.minItems = toInt(spec.minItems)
            schema.arrayType.maxItems = toInt(spec.maxItems)
            schema.arrayType.uniqueItems = Boolean.parseBoolean(spec.uniqueItems)
        }
        setAdditives(schema, spec)
    }

    DataSchema toDataSchema(Map<String, String> spec, String code = null) {
        if (!code) {
            code = spec[REF] ? getRefObjectName(spec) : null
        }
        fillDataSchema(new DataSchema(code ?: spec[REF] ? getRefObjectName(spec) : null), spec)
    }

    DataSchema toRootDataSchema(Map<String, Object> spec, String code) {
        def dataType = spec.enum ? enumParser.parse(spec, code) : toDataType(spec)
        setAdditives(new DataSchema(code, dataType), spec)
    }

    DataType toDataType(Map spec) {
        def type = getDataTypeCode(spec)
        if (type) {
            if (!DATA_TYPE_CREATORS.containsKey(type)) {
                throw new RuntimeException("Can't handle data schema: $spec")
            }
            try {
                DATA_TYPE_CREATORS.get(type).call(spec)
            } catch (Exception e) {
                throw new RuntimeException("Can't handle data schema: $spec", e)
            }
        } else if (spec[REF]) {
            builder.schemaRef(getRefObjectName(spec)).dataType
        } else {
            throw new RuntimeException("Can't handle data schema: $spec")
        }
    }

    String getDataTypeCode(Map spec) {
        def type = spec.type ?: spec['properties'] || spec.allOf ? 'object' : null
        type == 'object' && spec.additionalProperties ? 'map' : type
    }

    static Integer toInt(Object value) {
        if (value instanceof Integer) return value
        if (value instanceof String) return Integer.valueOf(value)
        if (value != null) throw new RuntimeException("Can't transform to int: $value")
        null
    }

    class DataSchemaBuilder {
        DataSchema schema
        Map spec

        DataSchemaBuilder(String code, Map spec) {
            this.spec = spec
            def dataType = spec.enum ? enumParser.parse(spec, code) : toDataTypeDeclaration(spec)
            schema = setAdditives(new DataSchema(code, dataType), spec)
            builder.addSchema(schema)
        }

        DataType toDataTypeDeclaration(Map spec) {
            switch (getDataTypeCode(spec)) {
                case 'object':
                    return new ObjectType()
                case 'map':
                    return new MapType()
                case 'array':
                    return new ArrayType()
            }
            toDataType(spec)
        }

        void make() {
            if (schema.object || schema.array || schema.map) {
                def dataType = toDataType(spec)
                InvokerHelper.setProperties(schema.dataType, dataType.properties)
                if (dataType.object) {
                    def obj = schema.dataType.objectType
                    obj.code = schema.code
                    obj.additives = schema.additives
                }
            }
        }
    }
}
