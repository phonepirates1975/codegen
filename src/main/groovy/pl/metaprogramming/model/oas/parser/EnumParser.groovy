/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.parser

import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.EnumType

class EnumParser extends BaseParser {

    Map<String, EnumType> enumByKey = [:]
    Map<String, Integer> enumCodeUsages = [:]

    Set<EnumType> enumsToProcess
    Set<EnumUsage> enumUsages

    EnumParser(BaseParser template) {
        configure(template)
    }

    EnumType parse(Map spec, String code = null) {
        def result = new EnumType(allowedValues: spec.enum.collect { "$it".toString() })
        Map xmsenum = (Map) spec['x-ms-enum']
        result.code = code ?: xmsenum?.name
        (xmsenum?.values as List<Map>)?.each {
            result.setDescription("$it.value".toString(), "$it.description")
        }
        result
    }

    EnumType toEnumType(Map spec) {
        def key = spec.enum.collect { "$it".toString() }.sort().join('_')
        if (enumByKey.containsKey(key)) {
            enumByKey.get(key)
        } else {
            def enumType = parse(spec)
            enumByKey.put(key, enumType)
            enumType
        }
    }

    void addEnums() {
        enumsToProcess = new LinkedHashSet<>(enumByKey.values())

        selectWithFixedCodes()
        collectEnumUsages()
        selectByFieldCode()
        selectByObjectAndFieldCode()
        selectRest()

        assert enumsToProcess.isEmpty()

        enumByKey.values().each {
            assert it.code
            builder.addSchema(it.makeSchema())
        }
    }

    /**
     * Wybiera enumy z ustaloną wartością kodową (parametry oraz z rozszerzeniem 'x-ms-enum.name')
     */
    private void selectWithFixedCodes() {
        def selected = enumsToProcess.findAll { it.code }
        enumsToProcess.removeAll(selected)
        selected.each { reserveEnumCode(it) }
    }

    private void collectEnumUsages() {
        enumUsages = []
        (builder.model.schemas.findAll { it.isObject() } as List<DataSchema>).each { schema ->
            schema.objectType.fields.each { field ->
                def enumType = getEnumFromField(field)
                if (enumsToProcess.contains(enumType)) {
                    enumUsages.add(new EnumUsage(enumType, fixCode(schema.code), fixCode(field.code)))
                }
            }
        }
    }

    private EnumType getEnumFromField(DataSchema field) {
        field.isEnum() ? field.enumType : field.isArray() ? getEnumFromField(field.arrayType.itemsSchema) : null
    }

    /**
     * Wybiera enumy, które są użyte w polach o nazwach nie kolidujących z innymi polami-enumami.
     * Takim enumom są nadawane kody zgodne z kodami pól.
     */
    private void selectByFieldCode() {
        enumUsages.groupBy {
            it.fieldCode
        }.findAll {
            !isCodeUsed(it.key) && it.value.groupBy { it.enumType }.size() == 1
        }.each {
            select(it.value, it.key)
        }
    }

    /**
     * Wybiera enumy, które są użyte tylko w jednym obiekcie.
     * Takim enumom są nadawane kody zgodne z konkatenacją kodu objektu i pola.
     */
    private void selectByObjectAndFieldCode() {
        enumUsages.groupBy {
            it.enumType
        }.findAll {
            it.value.size() == 1
        } each {
            select(it.value, it.value[0].objectAndFieldCode)
        }
    }

    /**
     * Wybiera pozostałe enumy i nadaje kody zgodne z kodami pól - z postfixem numerycznym
     */
    private void selectRest() {
        enumUsages.groupBy {
            it.enumType
        }.sort { a, b ->
            b.value.size() <=> a.value.size() ?: toSortValue(b.key) <=> toSortValue(a.key)
        }.each {
            String code = it.value.groupBy { it.fieldCode }.sort { it.value.size() }.collect { it.key }.last()
            select(it.value, code)
        }
    }

    private void select(List<EnumUsage> values, String code) {
        def enumType = values[0].enumType
        enumUsages.removeAll(values)
        enumsToProcess.remove(enumType)
        reserveEnumCode(enumType, code)
    }

    static private String toSortValue(EnumType enumType) {
        "$enumType.code ${String.format('%04d', enumType.allowedValues.size())} ${enumType.allowedValues.join(',')}"
    }


    private void reserveEnumCode(EnumType enumType, String code = null) {
        if (code) {
            enumType.code = enumType.code ?: giveCode(code)
        } else {
            assert enumType.code
            enumType.code = giveCode(enumType.code)
        }
    }

    private String giveCode(String code) {
        String fixedCode = fixCode(code)
        if (enumCodeUsages.containsKey(fixedCode)) {
            def nextNumber = enumCodeUsages.get(fixedCode) + 1
            enumCodeUsages.put(fixedCode, nextNumber)
            fixedCode = fixedCode + nextNumber
        } else {
            enumCodeUsages.put(fixedCode, 1)
        }
        fixedCode
    }

    static private String fixCode(String code) {
        code.capitalize()
    }

    private boolean isCodeUsed(String code) {
        enumCodeUsages.containsKey(fixCode(code))
    }


    static class EnumUsage {
        EnumType enumType
        String objectCode
        String fieldCode
        String objectAndFieldCode

        EnumUsage(EnumType enumType, String objectCode, String fieldCode) {
            this.enumType = enumType
            this.objectCode = objectCode
            this.fieldCode = fieldCode
            this.objectAndFieldCode = "$objectCode$fieldCode"
        }
    }


}
