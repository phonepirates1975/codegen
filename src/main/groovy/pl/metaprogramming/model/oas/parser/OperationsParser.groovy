/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.parser

import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.oas.HttpRequestBody
import pl.metaprogramming.model.oas.HttpResponse
import pl.metaprogramming.model.oas.Operation
import pl.metaprogramming.model.oas.Parameter
import pl.metaprogramming.model.oas.OperationType

abstract class OperationsParser extends BaseParser {

    static final List OAS2_BODY_PARAMS = ['body', 'formData']
    static final String CONTROLLER_TAG = 'x-swagger-router-controller'
    static final List OPERATION_TYPES = OperationType.values().collect { it.name().toLowerCase() }
    static final List NO_OPERATION_PATH_ELEMENTS = [CONTROLLER_TAG]

    DefinitionsParser definitionsParser
    ParametersParser parametersParser

    abstract HttpRequestBody getRequestBody(OpenapiOperation operation)

    abstract Map<String, DataSchema> getResponseSchema(Map responseSpec, OpenapiOperation operation)

    static class OpenapiOperation {
        String path
        String operationType
        String operationId
        String controller
        Map spec
    }

    OperationsParser setup(BaseParser template, DefinitionsParser definitionsParser, ParametersParser parametersParser) {
        configure(template)
        this.parametersParser = parametersParser
        this.definitionsParser = definitionsParser
        this
    }

    void readOperations() {
        log "Going to parse operations..."
        getOperations().each { operation ->
            log "  $operation.operationId"
            try {
                builder.addOperation(new Operation(
                        group: operation.controller,
                        code: operation.operationId,
                        path: operation.path,
                        type: toOperationType(operation.operationType),
                        requestBody: getRequestBody(operation),
                        parameters: getOperationParameters(operation.spec),
                        responses: getOperationResponses(operation),
                        additives: getAdditives(operation.spec)
                ))
            } catch (RuntimeException e) {
                throw new IllegalStateException("Invalid operation data: $operation.operationId", e)
            }
        }

    }


    static OperationType toOperationType(String code) {
        OperationType.valueOf(OperationType, code.toUpperCase())
    }

    List<OpenapiOperation> getOperations() {
        List<OpenapiOperation> result = []
        (spec.paths as Map<String, Map>)?.each { path, operations ->
            (operations as Map<String, Map>).each { operationType, operationSpec ->
                if (OPERATION_TYPES.contains(operationType)) {
                    result.add(new OpenapiOperation(
                            path: getPath(path),
                            operationType: operationType,
                            operationId: operationSpec.operationId as String,
                            controller: getController(operationSpec, operations[CONTROLLER_TAG] as String),
                            spec: operationSpec
                    ))
                } else if (!NO_OPERATION_PATH_ELEMENTS.contains(operationType)) {
                    log "Ignore operation type: $operationType"
                }
            }
        }
        result
    }

    String getPath(String path) {
        def basePath = verParsingStrategy.getBasePath(spec)
        if (basePath) {
            if (basePath.endsWith('/') && path.startsWith('/')) {
                basePath + path.substring(1)
            } else {
                basePath + path
            }
        } else {
            path
        }
    }

    String getController(Map operationSpec, String controllerTag) {
        String result = controllerTag ?: getResourceCodeByTag(operationSpec)
        if (!result) {
            println "Can't determine operation group for $operationSpec.operationId. " +
                    "Use 'tags' or 'x-swagger-router-controller' elements to specify group for operation."
        }
        result
    }

    String getResourceCodeByTag(Map operationSpec) {
        List tags = (List) operationSpec.tags
        if (tags && tags.size() == 1) {
            return tags[0]
        }
        if (!tags) {
            log "WARNING. No tags specified for operation $operationSpec.operationId"
        } else if (tags.size() > 1) {
            log "WARNING. Too many tags for operation $operationSpec.operationId: $tags"
        }
        null
    }

    List<Parameter> getOperationParameters(Map spec) {
        if (!spec.parameters) {
            return []
        }
        List<Parameter> result = (spec.parameters as List<Map>)
                .findAll { !OAS2_BODY_PARAMS.contains(it.in) }
                .collect { paramSpec ->
                    if (paramSpec.containsKey(REF)) {
                        builder.paramRef(getRefObjectName(paramSpec))
                    } else {
                        parametersParser.toParameter((Map) paramSpec)
                    }
                }
        result
    }

    List<HttpResponse> getOperationResponses(OpenapiOperation operation) {
        def result = [] as List<HttpResponse>
        if (operation.spec.responses == null) {
            throw new IllegalStateException("No responses defined for operation: $spec.operationId")
        }
        (operation.spec.responses as Map<String, Map>).each { status, desc ->

            result.add(new HttpResponse(
                    status: 'default' == status ? 0 : Integer.valueOf(status),
                    description: desc.description as String,
                    headers: (desc.headers ? (desc.headers as Map).keySet() : []) as Collection<String>,
                    contents: getResponseSchema(desc, operation)
            ))
        }
        result
    }

    DataSchema toSchema(Map schema) {
        schema ? definitionsParser.toDataSchema(schema) : null
    }

}
