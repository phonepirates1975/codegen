/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.parser

import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.oas.OpenapiParserConfig
import pl.metaprogramming.model.oas.RestApiBuilder

class BaseParser {

    public static final String REF = '$ref'

    Map spec
    RestApiBuilder builder
    OpenapiParserConfig config
    List<String> externalSchemas = []
    VerParsingStrategy verParsingStrategy

    void configure(BaseParser template) {
        spec = template.spec
        builder = template.builder
        config = template.config
        verParsingStrategy = template.verParsingStrategy
        config.dependsOn.collect {
            externalSchemas.addAll(it.schemas.collect { it.code })
        }
    }

    void log(String message) {
        if (config.verbose) {
            println message
        }
    }

    void check(boolean condition, String invalidMessage, String warnMessage = null) {
        builder.checker.check(condition, invalidMessage, warnMessage)
    }

    static Map<String, ?> getAdditives(Map<String, ?> spec) {
        def standardElements = ['description', 'summary', 'tags']
        spec.findAll { standardElements.contains(it.key) || it.key.startsWith('x-') }
                .collectEntries { it } as Map<String, ?>
    }

    static <T extends DataSchema> T setAdditives(T schema, Map<String, ?> spec) {
        getAdditives(spec).each { schema.setAdditive(it.key, it.value) }
        schema
    }

    String getRefObjectName(Map spec) {
        def ref = (String) spec[REF]
        ref.substring(ref.lastIndexOf('/') + 1)
    }

    protected static interface VerParsingStrategy {
        String getBasePath(Map spec)

        Map<String, Map> getSchemas(Map<String, Map> spec)

        Map<String, Map> getParameters(Map spec)

        Map getParameterSchema(Map parameterSpec)
    }

    protected static class Oas2ParsingStrategy implements VerParsingStrategy {
        String getBasePath(Map spec) {
            spec.basePath as String
        }

        Map<String, Map> getSchemas(Map<String, Map> spec) {
            spec.definitions as Map
        }

        Map<String, Map> getParameters(Map spec) {
            spec.parameters as Map
        }

        Map getParameterSchema(Map parameterSpec) {
            parameterSpec
        }
    }

    protected static class Oas3ParsingStrategy implements VerParsingStrategy {
        String getBasePath(Map spec) {
            def paths = spec.servers.collect { Map it -> new URI(it.url as String).path }.unique()
            paths.size() == 1 ? paths[0] : ''
        }

        Map<String, Map> getSchemas(Map<String, Map> spec) {
            spec.components.schemas as Map
        }

        Map<String, Map> getParameters(Map spec) {
            (spec.components as Map).parameters as Map
        }

        Map getParameterSchema(Map parameterSpec) {
            parameterSpec.schema as Map
        }
    }

}
