/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas


import static OperationType.*

class RestApiModelChecker {

    private static final String SUB_SCHEMA = 'Schema'
    private static final String SUB_PARAM = 'Parameter'
    private static final String SUB_OPID = 'OperationId'
    private static final String SUB_PATH = 'Path'
    private static final String SUB_REQS = 'requestBody'

    private static final List<OperationType> OPERATIONS_WITH_BODY = [POST, UPDATE, PUT, PATCH]

    private static final Set<String> JAVA_KEYWORDS = [
            "abstract", "assert", "boolean", "break", "byte",
            "case", "catch", "char", "class", "const",
            "continue", "default", "do", "double", "else",
            "enum", "extends", "false", "final", "finally",
            "float", "for", "goto", "if", "implements",
            "import", "instanceof", "int", "interface", "long",
            "native", "new", "null", "package", "private",
            "protected", "public", "return", "short", "static",
            "strictfp", "super", "switch", "synchronized", "this",
            "throw", "throws", "transient", "true", "try",
            "void", "volatile", "while"
    ] as Set

    boolean isOas2
    private String currentOperation
    private Set<String> operations = []
    private Set<String> paths = []
    private Set<String> dataSchemas = []
    private Set<String> params = []

    List<String> warnings = []

    boolean weekValidation = true

    void checkDataCode(String code) {
        addUnique(SUB_SCHEMA, dataSchemas, code)
    }

    void checkCommonParamCode(String code) {
        addUnique(SUB_PARAM, params, code)
    }

    void checkOperationNames(Operation operation) {
        checkName(operation.code, "operationId")
    }

    void checkOperation(Operation operation) {
        currentOperation = null
        addUnique(SUB_OPID, operations, operation.code)
        this.currentOperation = operation.code
        addUnique(SUB_PATH, paths, "${operation.path}.${operation.type}")
        checkPathParameters(operation)
        if (operation.type != POST) {
            requiredElseEmpty(isOperationWithBody(operation.type), operation.requestBodySchema, SUB_REQS)
        }
    }

    void checkPathParameters(Operation operation) {
        List<String> pathParams = operation.path.findAll("\\{[^\\}]*\\}").collect { it.substring(1, it.length() - 1) }.sort()
        List<String> declaredPathParams = operation.parameters.findAll { ParamLocation.PATH == it.location }.collect { it.name }.sort()
        check pathParams == declaredPathParams, "Mismatch between path value [$operation.path] and declared path parameters $declaredPathParams"
    }

    private String addCtxToMsg(String msg) {
        currentOperation ? "$msg [for operation $currentOperation]" : msg
    }

    private void addUnique(String subject, Set<String> set, String code) {
        check code != null && !code.empty, "$subject: '$code' should not be empty"
        check !set.contains(code), "$subject: '$code' is already used"
        set.add(code)
    }

    void requiredElseEmpty(boolean required, def value, String subject) {
        if (required) {
            check value != null, "$subject is required", weekValidation ? "$subject should be present" : null
        } else {
            check value == null, "$subject must be empty"
        }
    }

    void check(boolean condition, String invalidMessage, String warnMessage = null) {
        if (warnMessage && !condition) {
            warnings.add(warnMessage)
            System.err.println "[WARNING] ${addCtxToMsg(warnMessage)}"
        } else if (!condition) {
            throw new IllegalStateException(addCtxToMsg(invalidMessage))
        }
    }

    private void checkName(String value, String valueName) {
        check !JAVA_KEYWORDS.contains(value) && value ==~ /[a-zA-Z]+[a-zA-Z0-9_\-$]*/, "$valueName with value '$value' can't be use to generate valid code"
    }

    static boolean isOperationWithBody(OperationType operationType) {
        OPERATIONS_WITH_BODY.contains(operationType)
    }


}
