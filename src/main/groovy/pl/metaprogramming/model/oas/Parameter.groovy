/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas

import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.DataType

import java.util.function.Consumer

class Parameter extends DataSchema {
    final String name
    final ParamLocation location

    Parameter(ParamLocation location, String name, String code) {
        super(code ?: name)
        this.name = name
        this.location = location
    }

    Parameter(ParamLocation location, DataSchema it) {
        super(it)
        this.location = location
        this.name = it.code
    }

    static Parameter of(ParamLocation location, String name, DataType dataType, Consumer < DataSchema > setter = {}) {
        def result = new Parameter(location, name, name)
        result.dataType = dataType
        setter.accept(result)
        result
    }
}
