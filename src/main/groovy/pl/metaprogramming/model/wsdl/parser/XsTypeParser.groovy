/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.wsdl.parser

import groovy.xml.QName
import pl.metaprogramming.model.data.DataType

class XsTypeParser implements DataTypeParser<QName> {

    static String XS_URI = 'http://www.w3.org/2001/XMLSchema'

    Map<String, DataType> xsTypes = [
            'string'            : DataType.TEXT,
            'byte'              : DataType.BYTE,
            'unsignedByte'      : DataType.BYTE,
            'short'             : DataType.INT16,
            'unsignedShort'     : DataType.INT16,
            'int'               : DataType.INT32,
            'integer'           : DataType.INT32,
            'unsignedInt'       : DataType.INT32,
            'negativeInteger'   : DataType.INT32,
            'nonNegativeInteger': DataType.INT32,
            'nonPositiveInteger': DataType.INT32,
            'positiveInteger'   : DataType.INT32,
            'long'              : DataType.INT64,
            'unsignedLong'      : DataType.INT64,
            'float'             : DataType.FLOAT,
            'double'            : DataType.DOUBLE,
            'decimal'           : DataType.DECIMAL,
            'date'              : DataType.DATE,
            'dateTime'          : DataType.DATE_TIME,
    ]

    XsTypeParser set(String xsType, DataType dataType) {
        xsTypes.put(xsType, dataType)
        this
    }

    @Override
    DataType parse(QName qName) {
        if (qName.namespaceURI == XS_URI) {
            String xsType = qName.localPart
            if (!xsTypes.containsKey(xsType)) {
                throw new RuntimeException("Can't handle $qName type")
            }
            xsTypes[xsType]
        } else {
            null
        }
    }
}
