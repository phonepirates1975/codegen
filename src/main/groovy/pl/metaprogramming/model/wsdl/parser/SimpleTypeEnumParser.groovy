/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.wsdl.parser


import com.predic8.schema.SimpleType
import com.predic8.schema.restriction.facet.EnumerationFacet
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.data.EnumType

class SimpleTypeEnumParser implements DataTypeParser<SimpleType> {
    DataType parse(SimpleType component) {
        isEnumeration(component) ? toEnum(component) : null
    }

    static boolean isEnumeration(SimpleType simpleType) {
        simpleType.restriction != null &&
                simpleType.restriction.facets &&
                simpleType.restriction.facets[0] instanceof EnumerationFacet
    }

    static EnumType toEnum(SimpleType simpleType) {
        new EnumType(
                code: simpleType.name,
                allowedValues: simpleType.restriction.facets
                        .findAll { it instanceof EnumerationFacet }
                        .collect { it.value })
    }
}
