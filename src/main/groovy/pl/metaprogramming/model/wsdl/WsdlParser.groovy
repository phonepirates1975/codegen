/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.wsdl

import com.predic8.schema.*
import com.predic8.wsdl.AbstractPortTypeMessage
import com.predic8.wsdl.Definitions
import com.predic8.wsdl.PortType
import com.predic8.wsdl.WSDLParser
import groovy.xml.QName
import pl.metaprogramming.model.data.ArrayType
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.data.EnumType
import pl.metaprogramming.model.data.ObjectType
import pl.metaprogramming.model.wsdl.parser.ObjectTypeRef
import pl.metaprogramming.model.wsdl.parser.UnknownDataType

class WsdlParser {
    //https://www.membrane-soa.org/soa-model-doc/1.4/java-api/parse-wsdl-java-api.htm

    static String UNBOUNDED = 'unbounded'

    WsdlParserConfig config

    WsdlApi api = new WsdlApi()
    Map<String, DataSchema> allSchemas = [:]

    static WsdlApi parse(String filepath, WsdlParserConfig config = new WsdlParserConfig()) {
        new WsdlParser(config: config)
                .map(new WSDLParser().parse(filepath))
    }

    private WsdlApi map(Definitions defs) {
        parseDataSchemas(defs.schemas)
        resolveUnknownDataTypes(api.schemas.values())
        parseOperations(defs.portTypes)
        assert defs.services.size() == 1
        api.name = config.serviceNameMapper.apply(defs.services[0].name)
        api.uri = defs.services[0].ports[0].address.location
        api
    }

    private parseOperations(List<PortType> portTypes) {
        portTypes.each { pt ->
            pt.operations.each { op ->
                api.operations.add(new WsdlOperation(
                        name: op.name,
                        input: toDataSchema(op.input),
                        output: toDataSchema(op.output)
                ))
            }
        }
    }

    private DataSchema toDataSchema(AbstractPortTypeMessage message) {
        def qname = toQname(message)
        def findMessage = message.definitions.messages.find { it.qname == qname }
        assert findMessage.parts.size() == 1
        def result = toDataSchema(findMessage.parts[0].element.qname)
        result.objectType.isRootElement = true
        result
    }

    static private QName toQname(AbstractPortTypeMessage message) {
        new QName(
                message.getNamespace(message.messagePrefixedName.prefix) as String,
                message.messagePrefixedName.localName,
                message.messagePrefixedName.prefix)
    }

    private parseDataSchemas(List<Schema> schemas) {
        schemas.each {
            api.namespaceElementFormDefault.put(it.targetNamespace, it.elementFormDefault)
            it.complexTypes.each { addSchema(it) }
            it.simpleTypes.each { addSchema(it) }
            it.allElements.each { addSchema(it) }
        }
    }

    private void addSchema(ComplexType component) {
        addSchema(component, toObject(component.model, component.name))
    }

    private void addSchema(Element component) {
        if (component.embeddedType instanceof ComplexType) {
            addSchema(component, toObject((component.embeddedType as ComplexType).model, component.name))
        } else if (component.type) {
            addSchema(component, toDataType(component.type), false)
        } else {
            throw new RuntimeException("Can't handle $component")
        }
    }

    private void addSchema(SimpleType component) {
        def dataType = config.simpleTypeParsers.findResult { it.parse(component) }
        assert dataType, "Can't handle $component"
        addSchema(component, dataType, dataType instanceof EnumType)
    }

    private void addSchema(SchemaComponent component, DataType dataType, boolean addToApi = true) {
        def dataSchema = new DataSchema(component.name, dataType).setNamespace(component.namespaceUri)
        allSchemas.put(dataSchema.qname, dataSchema)
        if (addToApi) {
            api.schemas.put(dataSchema.qname, dataSchema)
        }
    }

    private ObjectType toObject(SchemaComponent component, String code) {
        if (component instanceof Sequence) {
            return new ObjectType(
                    code: code,
                    fields: component.elements.collect {
                        def dataType = toDataType(it.type)
                        isList(it) ?
                                new DataSchema(it.name, new ArrayType(
                                        itemsSchema: new DataSchema(dataType),
                                        minItems: Integer.valueOf(it.minOccurs),
                                        maxItems: it.maxOccurs != UNBOUNDED ? Integer.valueOf(it.maxOccurs) : null,
                                )).setIsNillable(it.nillable)
                                :
                                new DataSchema(it.name, dataType)
                                        .setNamespace(it.type.namespaceURI)
                                        .setIsRequired(it.minOccurs != '0')
                                        .setIsNillable(it.nillable)
                    })
        }
        if (component instanceof ComplexContent) {
            def objectType = toObject(component.derivation.model, code)
            objectType.inherits.add(new ObjectTypeRef(component.derivation.base))
            return objectType
        }
        throw new RuntimeException("Can't handle: $code, $component")
    }

    private static boolean isList(Element it) {
        it.maxOccurs == UNBOUNDED || Integer.parseInt(it.maxOccurs) > 1
    }

    private DataSchema toDataSchema(QName qname, String code = null) {
        new DataSchema(code ?: qname.localPart, toDataType(qname, true))
                .setNamespace(qname.namespaceURI)
    }

    private DataType toDataType(QName qName, boolean required = false) {
        config.xsTypeParser.parse(qName) ?: findDataType(qName, required)
    }

    private void resolveUnknownDataTypes(Collection<DataSchema> dataSchemas) {
        dataSchemas.each { resolveUnknownDataType(it) }
    }

    void resolveUnknownDataType(DataSchema dataSchema) {
        if (dataSchema.dataType instanceof UnknownDataType) {
            dataSchema.dataType = findDataType((dataSchema.dataType as UnknownDataType).qname, true)
        }
        if (dataSchema.object) {
            resolveUnknownDataTypes(dataSchema.objectType.fields)
            resolveObjectTypeRefs(dataSchema.objectType.inherits)
        }
        if (dataSchema.array) {
            resolveUnknownDataType(dataSchema.arrayType.itemsSchema)
        }
    }

    void resolveObjectTypeRefs(List<ObjectType> list) {
        for (int idx = 0; idx < list.size(); idx++) {
            if (list.get(idx) instanceof ObjectTypeRef) {
                list.set(idx, findDataType((list.get(idx) as ObjectTypeRef).qname, true).objectType)
            }
        }
    }

    DataType findDataType(QName qname, boolean required = false) {
        if (allSchemas.containsKey(qname.toString())) {
            return allSchemas[qname.toString()].dataType
        }
        if (required) {
            throw new RuntimeException("Unknown data type: " + qname)
        }
        new UnknownDataType(qname)

    }
}
