/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.wsdl

import com.predic8.schema.SimpleType
import groovy.transform.builder.Builder
import groovy.transform.builder.SimpleStrategy
import pl.metaprogramming.model.wsdl.parser.DataTypeParser
import pl.metaprogramming.model.wsdl.parser.SimpleTypeBaseParser
import pl.metaprogramming.model.wsdl.parser.SimpleTypeEnumParser
import pl.metaprogramming.model.wsdl.parser.XsTypeParser

import java.util.function.Function

@Builder(builderStrategy = SimpleStrategy)
class WsdlParserConfig {

    Function<String, String> serviceNameMapper = { name -> name } as Function<String, String>

    List<DataTypeParser<SimpleType>> simpleTypeParsers = [
            new SimpleTypeEnumParser(),
            new SimpleTypeBaseParser()
    ]

    XsTypeParser xsTypeParser = new XsTypeParser()

}
