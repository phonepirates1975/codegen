package pl.metaprogramming.model.data

import groovy.transform.builder.Builder
import groovy.transform.builder.SimpleStrategy
import pl.metaprogramming.model.data.constraint.ConstraintsSupport
import pl.metaprogramming.model.data.constraint.DataConstraint

import java.util.function.Supplier

@Builder(builderStrategy = SimpleStrategy)
class DataSchema {

    /**
     * Nazwa kodowa danych.
     */
    String code

    /**
     * Namespace (xsd).
     * Raczej powinien być przeniesiony do DateType
     */
    String namespace

    /**
     * Flaga określająca czy dane są wymagane.
     */
    boolean isRequired = false

    /**
     * Typ danych.
     */
    DataType dataType

    String defaultValue

    /**
     *  Dotyczy pola DATE_TIME
     */
    String format

    /**
     * Dotyczy pola tekstowego - wyrażenie regularne.
     */
    String pattern

    /**
     * Dotyczy pola tekstowego - min/max długość
     */
    Integer minLength
    Integer maxLength

    /**
     * Dotyczy pola liczbowego
     */
    String minimum
    String maximum

    /**
     * Dotyczy xsd
     */
    boolean isNillable

    final Map additives

    private final ConstraintsSupport constraints

    DataSchema(DataType dataType) {
        this(null, dataType)
    }

    DataSchema(String code, DataType dataType = null) {
        this.code = code
        this.dataType = dataType
        additives = [:]
        constraints = new ConstraintsSupport(additives)
    }

    DataSchema(DataSchema schema) {
        code = schema.code
        dataType = schema.dataType
        isRequired = schema.isRequired
        format = schema.format
        pattern = schema.pattern
        minLength = schema.minLength
        additives = schema.additives
        constraints = new ConstraintsSupport(additives)
    }


    DataType getBaseDataType() {
        if (isArray()) {
            arrayType.itemsSchema.baseDataType
        } else {
            dataType
        }
    }

    String toString() {
        ToString.toString(this)
    }

    boolean isType(DataTypeCode... typeCodes) {
        typeCodes.any { dataType.is(it) }
    }

    boolean isTypeOrItemType(DataTypeCode... typeCodes) {
        typeCodes.any { dataType.isTypeOrItemType(it) }
    }

    boolean isEnum() {
        dataType.isEnum()
    }

    boolean isObject() {
        dataType.isObject()
    }

    boolean isArray() {
        dataType.isArray()
    }

    boolean isMap() {
        dataType.isMap()
    }

    boolean isBinary() {
        dataType.binary
    }

    boolean isEnumOrItemEnum() {
        dataType.enumOrItemEnum
    }

    EnumType getEnumType() {
        dataType.enumType
    }

    ObjectType getObjectType() {
        dataType.objectType
    }

    ArrayType getArrayType() {
        dataType.arrayType
    }

    MapType getMapType() {
        dataType.mapType
    }

    String getFormat() {
        if (dataType instanceof ArrayType) {
            dataType.itemsSchema.format
        } else {
            this.format
        }
    }

    String getDescription() {
        additives.description ?: isObject() ? getObjectType().description : null
    }

    DataSchema setAdditive(String key, Object value) {
        additives.put(key, value)
        this
    }

    public <T> T getAdditive(String key, Supplier<T> supplier) {
        if (!additives.containsKey(key)) {
            setAdditive(key, supplier.get())
        }
        additives[key] as T
    }

    void setAdditiveIfNotSet(String key, Supplier<Object> value) {
        if (!additives.containsKey(key)) {
            setAdditive(key, value.get())
        }
    }

    void addAdditiveItems(String key, Object...items) {
        setAdditiveIfNotSet(key) { [] }
        (additives[key] as List).addAll(Arrays.asList(items))
    }

    String getAdditive(String key, boolean required = true) {
        if (required && !additives.containsKey(key)) {
            throw new IllegalStateException("No '$key' attribute found in data schema '${toString()}' - $additives")
        }
        additives.get(key)
    }

    String getQname() {
        "{$namespace}$code"
    }

    DataSchema add(DataConstraint constraint) {
        constraints.add(constraint)
        this
    }

    List<DataConstraint> getConstraints() {
        constraints.constraints
    }

    DataSchema setInvalidPatternCode(String invalidPatternCode) {
        constraints.setInvalidPatternCode(invalidPatternCode)
        this
    }

    String getInvalidPatternCode() {
        constraints.invalidPatternCode
    }

}
