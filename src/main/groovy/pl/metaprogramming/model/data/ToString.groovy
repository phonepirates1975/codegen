/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.data

class ToString {

    private Set<ObjectType> doNotDescribeTypes = []

    static String toString(DataSchema schema) {
        new ToString().schemaToString(schema)
    }

    private String schemaToString(DataSchema schema) {
        if (schema.enum) {
            enumToString(schema.enumType, schema)
        } else if (schema.object) {
            objectToString(schema.objectType, schema)
        } else if (schema.array) {
            arrayToString(schema.arrayType, schema)
        } else {
            "${fieldCode(schema, null)}${schema.dataType}${getAttributes(schema) ?: ''}"
        }
    }

    private String objectToString(ObjectType type, DataSchema schema) {
        List<String> attr = [type.code]
        if (!doNotDescribeTypes.contains(type)) {
            Set<ObjectType> ignoreTypes = this.doNotDescribeTypes + type
            if (schema) {
                attr.addAll(getAttributes(schema))
            } else {
                if (type.description) {
                    attr.add("'$type.description'" as String)
                }
            }
            attr.add('fields: ' + type.fields.collect {
                new ToString(doNotDescribeTypes: ignoreTypes).schemaToString(it)
            })
            if (type.inherits) {
                attr.add('inherits: ' + type.inherits.collect {
                    objectToString(it, null)
                })
            }
        }
        "${fieldCode(schema, type.code)}OBJECT" + attr
    }

    private String arrayToString(ArrayType type, DataSchema schema) {
        "${fieldCode(schema, null)}LIST OF " + new ToString(doNotDescribeTypes: doNotDescribeTypes).schemaToString(type.itemsSchema)
    }

    private String enumToString(EnumType type, DataSchema schema) {
        List<String> attr = getAttributes(schema)
        attr.add(0, type.code)
        attr.add('allowed: ' + type.allowedValues)
        if (type.descriptions) {
            attr.add('descriptions: ' + type.descriptions)
        }
        "${fieldCode(schema, type.code)}ENUM${attr}"
    }

    private List<String> getAttributes(DataSchema schema) {
        def result = [] as List<String>
        if (schema.isRequired) result.add('required')
        if (schema.isNillable) result.add('nillable')
        if (schema.description) result.add("'$schema.description'" as String)
        if (schema.namespace) result.add("namespace: ${schema.namespace}" as String)
        result
    }

    private String fieldCode(DataSchema schema, String typeCode) {
        schema?.code && schema?.code != typeCode ? schema.code + ' ' : ''
    }
}
