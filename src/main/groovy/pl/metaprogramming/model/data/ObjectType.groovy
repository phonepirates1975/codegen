/*
 * Copyright (c) 2018 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.data

import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.java.formatter.JavaCodeFormatter

class ObjectType extends NamedDataType {

    Map additives = [:]
    List<ObjectType> inherits = []
    List<DataSchema> fields = []

    boolean isRootElement

    ObjectType() {
        super(DataTypeCode.OBJECT)
    }

    String toString() {
        "OBJECT[${code ?: fields.collect { it.code }.join(', ')}]"
    }

    DataSchema field(String code) {
        fields.find { it.code == code }
    }

    String getDescription() {
        [additives.summary, additives.description]
                .findAll { it != null }
                .join(Codegen.NEW_LINE)
    }

}
