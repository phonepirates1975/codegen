/*
 * Copyright (c) 2018 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.data

class DataType {

    public static final DataType BINARY = new DataType(DataTypeCode.BINARY)
    public static final DataType BASE64 = new DataType(DataTypeCode.BASE64)
    public static final DataType TEXT = new DataType(DataTypeCode.STRING)
    public static final DataType BOOLEAN = new DataType(DataTypeCode.BOOLEAN)
    public static final DataType DATE = new DataType(DataTypeCode.DATE)
    public static final DataType DATE_TIME = new DataType(DataTypeCode.DATE_TIME)
    public static final DataType NUMBER = new DataType(DataTypeCode.NUMBER)
    public static final DataType BYTE = new DataType(DataTypeCode.INT16)
    public static final DataType INT16 = new DataType(DataTypeCode.INT16)
    public static final DataType INT32 = new DataType(DataTypeCode.INT32)
    public static final DataType INT64 = new DataType(DataTypeCode.INT64)
    public static final DataType DECIMAL = new DataType(DataTypeCode.DECIMAL)
    public static final DataType FLOAT = new DataType(DataTypeCode.FLOAT)
    public static final DataType DOUBLE = new DataType(DataTypeCode.DOUBLE)

    public static final List<DataType> NUMBER_TYPES = [NUMBER, INT16, INT32, INT64, DECIMAL, FLOAT, DOUBLE]

    DataTypeCode typeCode

    protected DataType(DataTypeCode typeCode) {
        this.typeCode = typeCode
    }

    DataSchema makeSchema(String code) {
        new DataSchema(code, this)
    }

    String toString() {
        return typeCode.toString()
    }

    boolean is(DataTypeCode typeCode) {
        this.typeCode == typeCode
    }

    boolean isTypeOrItemType(DataTypeCode code) {
        is(code) ||
                (isArray() && arrayType.itemsSchema.isType(code)) ||
                (isMap() && mapType.valuesSchema.isType(code))
    }

    boolean isComplex() {
        isObject() || isArray() || isMap()
    }

    boolean isEnum() {
        is(DataTypeCode.ENUM)
    }

    boolean isEnumOrItemEnum() {
        isEnum() ||
                (isArray() && arrayType.itemsSchema.isEnum()) ||
                (isMap() && mapType.valuesSchema.isEnum())
    }

    boolean isObject() {
        typeCode == DataTypeCode.OBJECT
    }

    boolean isBinary() {
        typeCode == DataTypeCode.BINARY
    }

    boolean isArray() {
        typeCode == DataTypeCode.ARRAY
    }

    boolean isMap() {
        typeCode == DataTypeCode.MAP
    }

    ArrayType getArrayType() {
        assert typeCode == DataTypeCode.ARRAY
        this as ArrayType
    }

    MapType getMapType() {
        assert typeCode == DataTypeCode.MAP
        this as MapType
    }

    ObjectType getObjectType() {
        assert typeCode == DataTypeCode.OBJECT
        this as ObjectType
    }

    EnumType getEnumType() {
        assert typeCode == DataTypeCode.ENUM
        this as EnumType
    }

}
