/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen

import groovy.transform.builder.Builder
import groovy.transform.builder.SimpleStrategy
import org.codehaus.groovy.runtime.InvokerHelper

import java.util.function.Consumer

/**
 * Code generation parameters container.
 * These should be general purpose parameters for the different types of codes.
 */
@Builder(builderStrategy = SimpleStrategy)
class CodegenParams {

    Map<Class, Object> paramsMap = [:]

    /**
     * Marks that module files will not be generated.
     */
    boolean skipGeneration = false

    /**
     * Adds params object.
     * If an object of the same class was previously added, it will be overwritten.
     * @param params
     * @return self
     */
    CodegenParams with(Object params) {
        this.paramsMap.put(params.class, params)
        this
    }

    /**
     * Adds the params object if no object of the same class has not been added before.
     * @param params
     * @return self
     */
    CodegenParams withIfNotSet(Object params) {
        if (!paramsMap.containsKey(params.class)) {
            this.paramsMap.put(params.class, params)
        }
        this
    }

    /**
     * Allows to change values in parameter object of a given class.
     * @param clazz
     * @param updater
     * @return self
     */
    public <T> CodegenParams update(Class<T> clazz, Consumer<T> updater) {
        updater.accept(paramsMap.get(clazz) as T)
        this
    }

    /**
     * Retrieves an object with parameters of the given class.
     * @param clazz
     * @return
     */
    public <T> T get(Class<T> clazz) {
        clazz.cast(paramsMap.get(clazz))
    }

    /**
     * Creates a copy of the parameters.
     * @return
     */
    CodegenParams clone() {
        def other = new CodegenParams()
        paramsMap.each {
            def params = it.key.newInstance()
            InvokerHelper.setProperties(params, it.value.properties)
            other.paramsMap.put(it.key, params)
        }
        other
    }

}
