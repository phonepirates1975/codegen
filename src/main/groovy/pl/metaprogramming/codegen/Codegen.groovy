/*
 * Copyright (c) 2018,2019 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen

import pl.metaprogramming.codegen.generator.CodegenConfig
import pl.metaprogramming.codegen.generator.MainGenerator

/**
 * Entrypoint for using Codegen.
 *
 * Here you add modules and run code generation for them.
 */
class Codegen {
    static String NEW_LINE = System.lineSeparator()

    private CodegenConfig cfg = new CodegenConfig()
    private List<ModuleGenerator> commons = []

    /**
     * Starts code generation for added modules.
     */
    MainGenerator generate() {
        MainGenerator.run(cfg)
    }

    /**
     * Deletes generated files present in the index file.
     * Files with removed 'Generated' annotation will not be deleted.
     */
    MainGenerator cleanup() {
        MainGenerator.cleanup(cfg, false)
    }

    /**
     * Deletes generated files present in the index file.
     * Also will be deleted files with removed 'Generated' annotation.
     */
    MainGenerator totalCleanup() {
        MainGenerator.cleanup(cfg, true)
    }

    /**
     * Adds a module builder to the code generation schedule.
     *
     * @param builder
     * @return self
     */
    Codegen addModule(ModuleGenerator builder) {
        builder.setDependencies(collectDependencies(builder.model))
        cfg.modules.add(builder)
        this
    }

    /**
     * Adds a module builder for common module.
     *
     * The common module contains common purpose (model / API independent) codes
     * that will be used in codes generated for other modules.
     *
     * @param builder
     * @return self
     */
    Codegen addCommonModule(ModuleGenerator builder) {
        addModule(builder)
        commons.add(builder)
        this
    }


    /**
     * Sets the base (root) directory for the generated codes.
     *
     * @param baseDir directory reference
     * @return
     */
    Codegen setBaseDir(File baseDir) {
        cfg.baseDir = baseDir
        this
    }

    /**
     * Sets location for generated codes index file.
     *
     * @param indexFile file reference
     * @return self
     */
    Codegen setIndexFile(File indexFile) {
        cfg.indexFile = indexFile
        this
    }

    /**
     * Defines the behavior of the generator when a manual change of generated codes is detected.
     *
     * Manually modified codes should have "@Generated" annotation removed.
     *
     * @param forceMode - true/false
     *      true - generating will overwrite manual changes (default)
     *      false - generation will fail
     * @return self
     */
    Codegen setForceMode(boolean forceMode) {
        cfg.forceMode = forceMode
        this
    }

    /**
     * Specifies whether the index of the generated codes should contain the 'lastGeneration' tag with the last generation execution timestamp.
     *
     * @param addLastGenerationTag - true/false
     * @return self
     */
    Codegen setAddLastGenerationTag(boolean addLastGenerationTag) {
        cfg.addLastGenerationTag = addLastGenerationTag
        this
    }

    /**
     * Specifies whether the entries in the index of generated codes should contain the tag "lastUpdate" with the timestamp of the last change of the generated code.
     *
     * @param addLastUpdateTag - true/false
     * @return self
     */
    Codegen addLastUpdateTag(boolean addLastUpdateTag) {
        cfg.addLastUpdateTag = addLastUpdateTag
        this
    }

    /**
     * Sets charset for generated codes.
     *
     * @param charset
     * @return self
     */
    Codegen setCharset(String charset) {
        cfg.charset = charset
        this
    }

    /**
     * Sets line separator for generated codes.
     *
     * @param lineSeparator
     * @return self
     */
    Codegen setLineSeparator(String lineSeparator) {
        NEW_LINE = lineSeparator
        this
    }

    private List<ModuleGenerator> collectDependencies(Model model) {
        if (model == null) return commons
        def dependencies = model.dependsOn.collect { otherComponent ->
            def c = cfg.modules.find { it.model == otherComponent }
            if (c == null) {
                throw new IllegalStateException("Nieznany komponent $otherComponent")
            }
            c
        } as List<ModuleGenerator>
        dependencies.addAll(commons)
        dependencies
    }
}
