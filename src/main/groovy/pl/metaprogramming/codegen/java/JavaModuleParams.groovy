/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

import groovy.transform.builder.Builder
import groovy.transform.builder.SimpleStrategy
import pl.metaprogramming.codegen.generator.CodeDecorator
import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.codegen.java.builders.AnnotatedBuildStrategy
import pl.metaprogramming.codegen.java.spring.base.DiConstructorBuildStrategy
import pl.metaprogramming.codegen.java.formatter.LicenceDecorator
import pl.metaprogramming.codegen.java.base.DataTypeMapper
import pl.metaprogramming.model.data.DataType

/**
 * Representation of a set of parameters allowing the parameterization
 * (in selected aspects) of the java code generator.
 */
@Builder(builderStrategy = SimpleStrategy)
class JavaModuleParams {

    static final JavaModuleParams DEFAULTS = new JavaModuleParams()

    final DataTypeMapper dataTypeMapper = new DataTypeMapper([
            (DataType.TEXT)     : 'java.lang.String',
            (DataType.DATE)     : 'java.time.LocalDate',
            (DataType.DATE_TIME): 'java.time.LocalDateTime',
            (DataType.DECIMAL)  : 'java.math.BigDecimal',
            (DataType.FLOAT)    : 'java.lang.Float',
            (DataType.DOUBLE)   : 'java.lang.Double',
            (DataType.BYTE)     : 'java.lang.Byte',
            (DataType.INT16)    : 'java.lang.Short',
            (DataType.INT32)    : 'java.lang.Integer',
            (DataType.INT64)    : 'java.lang.Long',
            (DataType.BOOLEAN)  : 'java.lang.Boolean',
            (DataType.BINARY)   : 'byte[]',
            (DataType.BASE64)   : 'byte[]',
    ])

    /**
     * Name mapper from model to java.
     */
    JavaNameMapper nameMapper = new DefaultJavaNameMapper()

    /**
     * Build strategy for dependency injection code.
     *
     * Using this parameter you will be able to specify
     * whether dependencies will be injected using a constructor
     * or an appropriate annotation.
     */
    ClassCmBuildStrategy diStrategy = new DiConstructorBuildStrategy()

    /**
     * Build strategy for components used by dependency injection framework.
     *
     * Using this parameter you will be able to specify annotation used
     * to mark beans managed by dependency injection framework.
     */
    ClassCmBuildStrategy componentStrategy = AnnotatedBuildStrategy.by('org.springframework.stereotype.Component')

    /**
     * It allows to force generation of the 'fromValue' method for enums.
     */
    boolean alwaysGenerateEnumFromValueMethod = false

    /**
     * Specify annotation class used to marking generated classes.
     * By default it is 'javax.annotation.Generated' or
     * 'javax.annotation.processing.Generated' if generation is run
     * with java newer than 1.8.
     *
     * It is not taken into account when the parameter 'generatedAnnotation' or 'generatedStrategy' is set.
     */
    String generatedAnnotationClass = javaVersion > 8 ? 'javax.annotation.processing.Generated' : 'javax.annotation.Generated'

    /**
     * Specify annotation value used to marking generated classes.
     * By default it is 'pl.metaprogramming.codegen'.
     *
     * It is not taken into account when the parameter 'generatedAnnotation' or 'generatedStrategy' is set.
     */
    String generatedAnnotationValue = 'pl.metaprogramming.codegen'

    /**
     * Specify annotation used to marking generated classes.
     *
     * It is not taken into account when the parameter 'generatedStrategy' is set.
     */
    AnnotationCm generatedAnnotation

    /**
     * Using this parameter you will be able to specify build strategy,
     * which will be applied to every class generation.
     *
     * By default is used to marking them with 'Generated' annotation.
     */
    ClassCmBuildStrategy generatedStrategy

    /**
     * Code contents decorators.
     * Used to modify / decorate the text form of the generated code.
     */
    List<CodeDecorator> codeDecorators = []

    /**
     * Adds code content decorator.
     * @param decorator
     * @return self
     */
    JavaModuleParams addCodeDecorator(CodeDecorator decorator) {
        codeDecorators.add(decorator)
        this
    }

    /**
     * Licence file header for generated files.
     * The LicenseDecorator object will be added to the codeDecorators list.
     */
    JavaModuleParams setLicenceHeader(String licenceHeader) {
        addCodeDecorator(new LicenceDecorator(licenceHeader))
    }

    JavaModuleParams setDataTypeMapping(DataType dataType, String className) {
        dataTypeMapper.setMapping(dataType, className)
        this
    }

    AnnotationCm getGeneratedAnnotation() {
        if (generatedAnnotation == null) {
            generatedAnnotation = AnnotationCm.of(generatedAnnotationClass, [value: ValueCm.escaped(generatedAnnotationValue)])
        }
        generatedAnnotation
    }

    ClassCmBuildStrategy getGeneratedStrategy() {
        if (generatedStrategy == null) {
            generatedStrategy = AnnotatedBuildStrategy.by(getGeneratedAnnotation())
        }
        generatedStrategy
    }

    private static int getJavaVersion() {
        String version = System.getProperty("java.version")
        if (version.startsWith("1.")) {
            version = version.substring(2, 3)
        } else {
            int dot = version.indexOf(".")
            if (dot != -1) {
                version = version.substring(0, dot)
            }
        }
        return Integer.parseInt(version)
    }
}
