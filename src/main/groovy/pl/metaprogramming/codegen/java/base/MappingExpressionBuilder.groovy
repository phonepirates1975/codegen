/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.ClassCm
import pl.metaprogramming.codegen.java.FieldCm
import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.codegen.java.ValueCm

import static pl.metaprogramming.codegen.java.ClassCd.*

class MappingExpressionBuilder {
    static final ClassCd FUN_T_R = of("java.util.function.Function<T, R>")
    static final ClassCd LIST_R = of("java.util.List<R>")
    static final ClassCd LIST_T = of("java.util.List<T>")
    static final ClassCd MAP_KR = of("java.util.Map<K, R>")
    static final ClassCd MAP_KT = of("java.util.Map<K, T>")

    ClassCmBuilder builder
    ClassIndex classIndex
    Object model

    private ClassCd to
    private String fromString
    private List<FieldCm> from

    MappingExpressionBuilder(ClassCmBuilder builder, Object model) {
        this.builder = builder
        this.model = model
        classIndex = builder.classIndex
    }

    MappingExpressionBuilder withModel(Object model) {
        this.model = model
        this
    }

    MappingExpressionBuilder to(ClassCd toClassCd) {
        this.to = toClassCd
        this
    }

    MappingExpressionBuilder to(Object typeOfCode) {
        this.to = getClass(typeOfCode)
        this
    }

    MappingExpressionBuilder from(String stringValue) {
        fromString = stringValue
        this
    }

    MappingExpressionBuilder from(MappingExpressionBuilder transformationBuilder) {
        from = [transformationBuilder.makeField()]
        this
    }

    MappingExpressionBuilder from(List<FieldCm> from) {
        this.from = from
        this
    }

    MappingExpressionBuilder from(FieldCm from) {
        this.from = [from]
        this
    }

    MappingExpressionBuilder from(Object typeOfCode) {
        this.from = [getClass(typeOfCode).asField('')]
        this
    }

    MappingExpressionBuilder from(Object typeOfCode, String valueExp) {
        this.from = [getClass(typeOfCode).asExpression(valueExp)]
        this
    }

    @Override
    String toString() {
        if (to != null) {
            make()
        } else {
            "[to: $to; from: $from; fromString: $fromString]"
        }
    }

    String make() {
        if (from != null) {
            if (from.size() == 1 && !(from[0].name || from[0].value)) {
                makeLambdaExp()
            } else {
                makeTransformation(to, from)
            }
        } else {
            makeValueTransformation(to, fromString)
        }
    }

    String makeLambdaExp() {
        makeTransformation(to, from, 1, true)
    }

    FieldCm makeField(String name = null) {
        to.asField(name).assign(make())
    }

    ValueCm makeValue() {
        ValueCm.value(make())
    }

    static String getter(String path) {
        getter(Arrays.asList(path.split('\\.')))
    }

    static String getter(List<String> path) {
        path[0] + path.drop(1).collect {
            // ".get${it.capitalize()}()" - sometimes path contains model names
//            '.get' + builder.nameMapper.toClassName(it) + '()'
            ".get${it.capitalize()}()"
        }.join('')
    }


    private String makeValueTransformation(ClassCd type, String value) {
        if (value == null) {
            'null'
        } else if (type == stringType()) {
            "\"$value\""
        } else if (["java.lang.Integer", "java.lang.Short"].contains(type.canonicalName)) {
            value
        } else if (type == longType()) {
            value + 'l'
        } else if (type == floatType()) {
            value + 'f'
        } else if (type == doubleType()) {
            value + (value.contains('.') ? '' : 'd')
        } else if (type == bigDecimalType()) {
            "new BigDecimal(\"${value}\")"
        } else if (type.canonicalName == "java.time.ZonedDateTime") {
            "ZonedDateTime.parse(\"${value}\")"
        } else if (type instanceof ClassCm && type.isEnum()) {
            "${type.className}.${builder.nameMapper.toConstantName(value)}"
        } else {
            makeTransformation(type, [FieldCm.stringValue(value)])
        }
    }

    private String makeTransformation(
            ClassCd toType,
            List<FieldCm> from,
            int level = 1,
            boolean allowMethodReference = false) {
        if (isCollectionTransformation(toType, from)) {
            makeCollectionTransformation(toType, from, level)
        } else if (from.size() == 1 && from[0].type.isEnum() && toType == stringType()) {
            makeEnumToStringTransformation(from[0])
        } else if ([toType] == from.type) {
            makeTransformationParams(from)
        } else {
            def mapper = findMapper(toType, from.type, from.size() != 0)
            if (mapper == null) {
                builder.addImport(toType)
                "new ${toType.className}()"
            } else if (mapper.params.empty && from.size() == 1 && from[0].type == mapper.ownerClass) {
                "${from[0].name}.${mapper.name}()"
            } else if (mapper.static) {
                builder.addImport(mapper.owner)
                makeCall(mapper.owner.className, mapper.name, from, allowMethodReference)
            } else {
                def mapperField = builder.injectDependency(mapper.owner)
                makeCall(mapperField.name, mapper.name, from, allowMethodReference)
            }
        }
    }

    private String makeCollectionTransformation(ClassCd toType, List<FieldCm> from, int level) {
        def isList = toType.isList()
        // e.g. baseDataMapper.transformList(raw.getAuthors(), v -> baseDataMapper.toLong(v))
        def toItemType = toType.genericParams.get(isList ? 0 : 1)
        def fromItemType = from[0].type.genericParams.get(isList ? 0 : 1)
        def varName = 'v' + (level > 1 ? level : '')
        def itemFrom = [fromItemType.asField(varName)] + from.subList(1, from.size())
        def itemTransformation = makeTransformation(toItemType, itemFrom, level + 1, true)

        if (itemTransformation) {
            def mapper = isList ? findMapper(LIST_R, [LIST_T, FUN_T_R]) : findMapper(MAP_KR, [MAP_KT, FUN_T_R])
            def itemTransformationValue = varName != itemTransformation && !itemTransformation.contains('.') ? itemTransformation : "${varName} -> ${itemTransformation}"
            String methodCall = "${mapper.name}(${makeTransformationParams([from[0]])}, $itemTransformationValue)"
            if (mapper.static) {
                builder.addImport(mapper.owner)
                "${mapper.owner.className}.$methodCall"
            } else {
                def mapperField = builder.injectDependency(mapper.owner)
                "${mapperField.name}.$methodCall"
            }
        } else {
            null
        }
    }

    private boolean isCollectionTransformation(ClassCd toType, List<FieldCm> from) {
        (toType.isList() && from[0].type.isList()) ||
                (toType.isMap() && from[0].type.isMap())
    }

    private String makeEnumToStringTransformation(FieldCm from) {
        if (from.nonnull) {
            "${getter(from.name)}.getValue()"
        } else {
            builder.addImport('java.util.Optional')
            builder.addImport(from.type)
            "Optional.ofNullable(${getter(from.name)}).map(${from.type.className}::getValue).orElse(null)"
        }
    }

    private String makeTransformationParams(List<FieldCm> from) {
        from.collect { it.name ? getter(it.name) : it.value }.join(', ')
    }

    private String makeCall(String instance, String method, List<FieldCm> from, boolean allowMethodReference) {
        if (allowMethodReference && from.size() == 1) {
            "${instance}::${method}"
        } else {
            def call = "${method}(${makeTransformationParams(from)})"
            instance == 'this' ? call : "${instance}.${call}"
        }
    }

    private MethodCm findMapper(ClassCd resultType, List<ClassCd> params, boolean failIfNotFound = true) {
        def key = MapperEntry.Key.of(resultType, params)
        def result = classIndex.getMapper(key)
        if (result) {
            result.methodCm
        } else if (failIfNotFound) {
            throw new IllegalStateException("Can't find mapper $key")
        } else {
            null
        }
    }

    private ClassCd getClass(Object typeOfCode) {
        builder.classLocator(typeOfCode).model(model).get()
    }

}
