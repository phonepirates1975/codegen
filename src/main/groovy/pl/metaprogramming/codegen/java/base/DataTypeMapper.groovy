/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.model.data.DataType

import java.util.function.Function

class DataTypeMapper {
    private Map<DataType, ClassCd> map
    private Function<DataType, ClassCd> function
    private Map<Object, DataTypeMapper> mapperByContext = [:]

    DataTypeMapper(Map<DataType, String> map) {
        this.map = new HashMap<>()
        map.each {
            this.map.put(it.key, ClassCd.of(it.value))
        }
    }

    DataTypeMapper(Function<DataType, ClassCd> function) {
        this.function = function
    }

    void setMapping(DataType dataType, String className) {
        setMapping(dataType, ClassCd.of(className))
    }

    void setMapping(DataType dataType, ClassCd classCd) {
        map.put(dataType, classCd)
    }

    ClassCd map(DataType dateType, Object context) {
        mapperByContext.getOrDefault(context, this).map(dateType)
    }

    ClassCd map(DataType dateType) {
        map ? map.get(dateType) : function.apply(dateType)
    }

    def setMapper(DataTypeMapper mapper, Object... contexts) {
        contexts.each {
            mapperByContext.put(it, mapper)
        }
    }

    DataTypeMapper clone() {
        DataTypeMapper result = new DataTypeMapper(function)
        if (map) {
            result.map = map.collectEntries { [(it.key): it.value] } as Map<DataType, ClassCd>
        }
        mapperByContext = mapperByContext.collectEntries { [(it.key): it.value.clone()] } as Map<Object, DataTypeMapper>
        result
    }
}
