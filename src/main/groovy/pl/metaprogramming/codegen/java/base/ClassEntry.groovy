/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import groovy.transform.EqualsAndHashCode
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.base.ClassBuilder

class ClassEntry {
    ClassCd clazz
    Object model
    Object classType
    ClassBuilder builder

    private boolean declarationMade
    private boolean built

    @Lazy
    Key key = Key.of(classType, model)

    boolean isToMake() {
        !built && toGenerate
    }

    boolean isToGenerate() {
        builder != null ? builder.isToGenerate() : false
    }

    void markAsUsed() {
        clazz.markAsUsed()
    }

    void makeImplementation() {
        if (!built) {
            built = true
            builder.makeImplementation()
        }
    }

    void makeDeclaration() {
        if (!declarationMade) {
            declarationMade = true
            builder?.makeDeclaration()
        }
    }

    @EqualsAndHashCode
    static class Key {
        Object model
        Object classType

        static Key of(Object classType, Object model = null) {
            new Key(classType: classType, model: model)
        }

        String toString() {
            model == null ? classType
                    : "$classType:$model"
        }

    }
}