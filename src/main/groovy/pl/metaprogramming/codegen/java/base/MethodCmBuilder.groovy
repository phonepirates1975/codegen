/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.codegen.java.base.ClassCmBuilder

abstract class MethodCmBuilder<T> {

    ClassCmBuilder<?> builder
    T model
    MethodCm methodCm

    abstract void makeImplementation()

    abstract MethodCm makeDeclaration()

    MethodCm make() {
        makeDeclaration()
        makeImplementation()
        methodCm.markAsUsed()
        methodCm
    }

    MethodCm getMethodCm() {
        if (methodCm == null) {
            methodCm = makeDeclaration()
        }
        methodCm
    }

    boolean isGenerateIfUsed() {
        builder?.config?.generateIfUsed
    }

}
