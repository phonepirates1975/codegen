/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import groovy.transform.ToString
import groovy.transform.builder.Builder
import groovy.transform.builder.SimpleStrategy
import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.java.ClassCd

import java.util.function.Consumer
import java.util.function.Predicate

@ToString(includeNames = true)
@Builder(builderStrategy = SimpleStrategy)
class ClassBuilderConfigurator {

    ClassNameBuilder<?> classNameBuilder = (modelName, model) -> modelName.capitalize()

    String packageName
    String projectDir = ''
    String projectSubDir = 'src/main/java'
    Object classType // type of class, eg.: DTO, SERVICE

    /**
     * Lets you register as a different type of code in the class index.
     * For example, an ENUM type can be registered as DTO, which can simplify code model building implementations.
     * In particular, that the ENUM can be regarded as a special case of the DTO.
     */
    Object registerClassType

    // used only by ClassCmBuilder/JavaCodeFormatter
    List<ClassCmBuildStrategy<?>> strategies = []
    DataTypeMapper dataTypeMapper

    // used only by GspTemplateFormatter
    String gspTemplate
    List dependencies // list of single occurrence class types on which it depends

    Boolean generateIfUsed
    Predicate<ClassCmBuilder> isToGeneratePredicate
    List<Model> predefinedModels = []

    static class Model {
        Object model
        String name
    }

    void register(ClassIndex classIndex, CodegenParams params) {
        if (predefinedModels) {
            predefinedModels.each {
                register(classIndex, params, it.name, it.model)
            }
        } else {
            register(classIndex, params, null, null)
        }
    }

    ClassBuilder register(ClassIndex classIndex, CodegenParams params, String modelName, Object model) {
        ClassBuilder builder = gspTemplate
                ? new ClassGspBuilder(classCd: ClassCd.of(packageName, classNameBuilder.make(modelName, model)))
                : new ClassCmBuilder(modelName: modelName)
        builder.classIndex = classIndex
        builder.model = model
        builder.config = this
        builder.params = params
        if (registerClassType) {
            classIndex.put(builder.classCd, model, registerClassType, builder)
        } else {
            classIndex.put(builder)
        }
        builder
    }

    ClassBuilderConfigurator addStrategy(ClassCmBuildStrategy<?>... strategies) {
        strategies.each { this.strategies.add(it) }
        this
    }

    ClassBuilderConfigurator removeStrategy(Class<ClassCmBuildStrategy> strategyClass) {
        strategies.removeIf({ strategyClass.isInstance(it) })
        this
    }

    ClassBuilderConfigurator removeStrategy(ClassCmBuildStrategy strategy) {
        strategies.remove(strategy)
        this
    }

    ClassBuilderConfigurator replaceStrategy(Class<ClassCmBuildStrategy<?>> strategyClass, ClassCmBuildStrategy<?> strategy) {
        strategies.replaceAll({ strategyClass.isInstance(it) ? strategy : it })
        this
    }

    ClassBuilderConfigurator replaceStrategy(ClassCmBuildStrategy<?> oldStrategy, ClassCmBuildStrategy<?> newStrategy) {
        strategies.replaceAll({ it == oldStrategy ? newStrategy : it })
        this
    }

    ClassBuilderConfigurator onDeclaration(Consumer<ClassCmBuildStrategy<?>> declarationHandler) {
        addStrategy(new ClassCmBuildStrategy() {
            @Override
            void makeDeclaration() {
                declarationHandler.accept(this)
            }
        })
    }

    ClassBuilderConfigurator onImplementation(Consumer<ClassCmBuildStrategy<?>> implementationHandler) {
        addStrategy(new ClassCmBuildStrategy() {
            @Override
            void makeImplementation() {
                implementationHandler.accept(this)
            }
        })
    }

    ClassBuilderConfigurator onDecoration(Consumer<ClassCmBuildStrategy<?>> decorationHandler) {
        addStrategy(new ClassCmBuildStrategy() {
            @Override
            void makeDecoration() {
                decorationHandler.accept(this)
            }
        })
    }

    ClassBuilderConfigurator setFixedName(String fixedName) {
        classNameBuilder = (modelName, model) -> fixedName
        this
    }

    ClassBuilderConfigurator setNameSuffix(String suffix) {
        setNamePrefixAndSuffix('', suffix)
    }

    ClassBuilderConfigurator setNamePrefixAndSuffix(String prefix, String suffix) {
        classNameBuilder = (modelName, model) -> "$prefix${modelName.capitalize()}$suffix"
        this
    }

    String getBaseDir() {
        projectDir ? "$projectDir/$projectSubDir" : projectSubDir
    }
}
