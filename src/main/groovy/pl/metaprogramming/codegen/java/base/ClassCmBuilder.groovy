/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import groovy.text.SimpleTemplateEngine
import groovy.util.logging.Slf4j
import pl.metaprogramming.codegen.generator.CodeGenerationTask
import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.ClassCm
import pl.metaprogramming.codegen.java.FieldCm
import pl.metaprogramming.codegen.java.JavaModuleParams
import pl.metaprogramming.codegen.java.JavaNameMapper
import pl.metaprogramming.codegen.java.formatter.JavaCodeFormatter
import pl.metaprogramming.model.data.DataSchema

import java.util.function.Consumer

@Slf4j
class ClassCmBuilder<T> extends ClassBuilder<T> {

    boolean wasMadeImplementation = false

    String modelName
    String packageName

    ClassCm classCm

    ClassCmBuilder<T> make() {
        try {
            makeDeclaration()
            makeImplementation()
            this
        } catch (RuntimeException e) {
            throw new IllegalStateException("Can't build code model for $model", e)
        }
    }

    @Override
    Object getClassType() {
        config.classType
    }

    @Override
    ClassCm getClassCd() {
        if (classCm == null) {
            classCm = new ClassCm(makePackageName(), nameMapper.toClassName(config.classNameBuilder.make(modelName, model)))
            classCm.setUsed(!config.generateIfUsed)
        }
        classCm
    }

    @Override
    void makeDeclaration() {
        perform("makeDeclaration")
    }

    @Override
    void makeDecoration() {
        perform("makeDecoration")
    }

    @Override
    void makeImplementation() {
        perform("makeImplementation")
        wasMadeImplementation = true
    }

    @Override
    boolean isToGenerate() {
        super.isToGenerate() || calculateIsToGenerate()
    }

    private boolean calculateIsToGenerate() {
        if (config.isToGeneratePredicate?.test(this)) {
            classCm.markAsUsed()
            true
        } else {
            false
        }
    }

    private void perform(String method) {
        try {
            log.debug("start $method for $classCm ($config.classType - $modelName)")
            config.strategies.each {
                def strategy = it.getInstance(this)
                strategy.invokeMethod(method, null)
            }
            log.debug("finish $method for $classCm ($config.classType - $modelName)")
        } catch (Exception e) {
            panic(e)
        }
    }

    private void panic(Exception e) {
        throw new IllegalStateException("Can't build $classCm ($config.classType for $model)", e)
    }

    @Override
    CodeGenerationTask makeGenerationTask(String filePath) {
        new CodeGenerationTask(
                destFilePath: filePath,
                codeModel: classCm,
                formatter: new JavaCodeFormatter(classCm)
        )
    }

    private String makePackageName() {
        if (packageName) {
            return packageName
        }
        if (!config.packageName) {
            throw new IllegalStateException("Undefined package for class type ${classType}.")
        }
        if (config.packageName.contains('$')) {
            def modelMap = model.properties
            return new SimpleTemplateEngine()
                    .createTemplate(config.packageName)
                    .make(modelMap).toString()
        }
        config.packageName
    }

    void addAnnotation(AnnotationCm annotation) {
        classCm.annotations.add(annotation)
    }

    FieldCm injectDependency(ClassCd toInject) {
        if (classCm == toInject) {
            return toInject.asField('this')
        }
        FieldCm existField = classCm.fields.find { it.type == toInject }
        if (existField) {
            return existField
        }
        def fieldName = toInject.isInterface() && toInject.className ==~ /I[A-Z].*/
                ? toInject.className.substring(1).uncapitalize()
                : toInject.className.uncapitalize()
        addField(fieldName, toInject)
    }

    FieldCm addField(String name, ClassCd type, Consumer<FieldCm> fieldBuilder = null) {
        classCm.addField(name, type) { fieldBuilder?.accept(it) }
    }

    FieldCm addField(FieldCm fieldCm) {
        classCm.addField(fieldCm)
    }

    void addImport(String... classes) {
        classes.each {
            classCm.addImport(it)
        }
    }

    void addImport(ClassCd... classes) {
        classes.each { clazz ->
            if (clazz.packageName) {
                classCm.addImport(clazz.canonicalName)
                clazz.genericParams?.each {
                    addImport(it)
                }
            }
        }
    }

    void addImportStatic(ClassCd classCd) {
        classCm.addImport("static ${classCd.getCanonicalName()}.*".toString())
    }

    ClassCd getClass(def typeOfCode, def model = model) {
        classLocator(typeOfCode).model(model).get()
    }

    ClassLocator classLocator(def typeOfCode) {
        new ClassLocator(
                dataTypeMapper: config.dataTypeMapper,
                classIndex: classIndex,
                classType: typeOfCode,
                model: model
        )
    }

    public <T> T getParams(Class<T> clazz) {
        params.get(clazz)
    }

    JavaNameMapper getNameMapper() {
        getParams(JavaModuleParams).nameMapper
    }

    String getFieldName(DataSchema schema) {
        schema.getAdditive('JAVA_NAME', { nameMapper.toFieldName(schema.code) })
    }

    @Override
    String toString() {
        "Builder of $config.classType for $modelName - $classCm"
    }
}
