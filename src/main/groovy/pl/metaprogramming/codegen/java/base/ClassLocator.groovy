/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import groovy.transform.TupleConstructor
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.ClassCm
import pl.metaprogramming.model.data.ArrayType
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.data.MapType

@TupleConstructor
class ClassLocator {

    private ClassIndex classIndex
    private DataTypeMapper dataTypeMapper
    private Object classType
    private Object model
    private boolean forceDeclaration
    private boolean doNotMarkAsUsed

    ClassLocator model(Object model) {
        this.model = model
        this
    }

    ClassLocator doNotMarkAsUsed() {
        this.doNotMarkAsUsed = true
        this
    }

    boolean isUsed() {
        doNotMarkAsUsed().get().used
    }

    ClassCd get() {
        require(find(), "Can't find Class ($classType) for $model")
    }

    ClassCm getDeclared() {
        forceDeclaration = true
        ClassCm.cast(get())
    }

    Optional<ClassCd> getOptional() {
        Optional.ofNullable(find())
    }

    ClassCd getGeneric(List<?> genericParams) {
        get().withGeneric(genericParams.collect {
            classType = it
            get()
        })
    }

    ClassCd getGeneric(Object... genericParams) {
        getGeneric(Arrays.asList(genericParams))
    }

    private ClassCd find() {
        if (classType instanceof ClassCd) {
            classType
        } else if (classType instanceof DataType) {
            dataTypeMapper.map(classType)
        } else if (model instanceof DataType) {
            getClassForDataType(model)
        } else {
            getClass(model)
        }
    }

    ClassCd getClassForDataType(DataType dataType) {
        if (dataType && dataTypeMapper) {
            def result = dataTypeMapper.map(dataType, classType)
            if (result) {
                return result
            }
        }
        if (dataType instanceof ArrayType) {
            getClassForDataType(dataType.itemsSchema.dataType).asList()
        } else if (dataType instanceof MapType) {
            getClassForDataType(dataType.valuesSchema.dataType).asMapBy(ClassCd.stringType())
        } else {
            require(getClass(dataType),
                    "Can't find class for $dataType (for type of code: $classType)")
        }
    }

    private ClassCd getClass(Object model) {
        doNotMarkAsUsed
                ? classIndex.findClass(classType, model)
                : classIndex.getClass(classType, model, forceDeclaration)
    }

    protected ClassCd require(ClassCd value, String errorMessage) {
        if (value == null) {
            throw new IllegalStateException(errorMessage)
        }
        value
    }

}
