/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base


import pl.metaprogramming.codegen.java.formatter.ClassNameFormatter
import pl.metaprogramming.codegen.java.formatter.CodeBuffer
import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.ClassCm
import pl.metaprogramming.codegen.java.FieldCm
import pl.metaprogramming.codegen.java.JavaNameMapper

import java.util.function.Predicate

abstract class BaseMethodCmBuilder<T> extends MethodCmBuilder<T> {

    protected static ClassNameFormatter classNameFormatter = new ClassNameFormatter()

    protected CodeBuffer codeBuf = new CodeBuffer()

    abstract String makeImplBody()

    void makeImplementation() {
        methodCm.implBody = makeImplBody()
    }

    protected <T> T getParams(Class<T> clazz) {
        builder.params.get(clazz)
    }

    ClassCm getClassModel() {
        builder.classCm
    }

    ClassCd getSuperClass() {
        classModel.superClass
    }

    JavaNameMapper getNameMapper() {
        builder.nameMapper
    }

    ClassCd getClass(def typeOfCode, def model = model) {
        typeOfCode instanceof ClassCd ? typeOfCode : builder.getClass(typeOfCode, model)
    }

    ClassCm getClassCm(def typeOfCode, def model = model) {
        getClass(typeOfCode, model) as ClassCm
    }

    ClassLocator classLocator(def typeOfCode) {
        builder.classLocator(typeOfCode).model(model)
    }

    String componentRef(def classType, String fieldName = null) {
        def dependency = injectDependency(getClass(classType))
        if (fieldName) {
            dependency.name = fieldName
        }
        dependency.name
    }

    String getter(String... path) {
        MappingExpressionBuilder.getter(path.collect {
            nameMapper.toFieldName(it)
        } as List<String>)
    }

    FieldCm injectDependency(ClassCd toInject) {
        builder.injectDependency(toInject)
    }

    MappingExpressionBuilder mapping() {
        new MappingExpressionBuilder(builder, model)
    }

    void addImport(String... classes) {
        builder.addImport(classes)
    }

    void addImport(ClassCd... classes) {
        builder.addImport(classes)
    }

    void addImportStatic(ClassCd classCd) {
        builder.addImportStatic(classCd)
    }

    void addAnnotation(AnnotationCm annotation) {
        builder.addAnnotation(annotation)
    }

    List<FieldCm> fields(Predicate<FieldCm> predicate) {
        builder.classCm.fields.findAll { predicate.test(it) }
    }

    FieldCm addField(String name, ClassCd type) {
        builder.addField(name, type)
    }

    FieldCm addField(FieldCm fieldCm) {
        builder.addField(fieldCm)
    }

    String addVarDeclaration(FieldCm field) {
        addVarDeclaration(field.name, field.type, field.value.toString())
    }

    String addVarDeclaration(String varName, ClassCd varType, String varExp) {
        addImport(varType)
        String fixedFieldExp = varExp == 'new'
                ? "new $varType.className()" : varExp
        codeBuf.addLines("$varType.className $varName = $fixedFieldExp;")
        varName
    }

    String callComponent(def classType, String callExp) {
        injectDependency(getClass(classType)).name + '.' + callExp
    }

    String ofNullable(String expression) {
        addImport('java.util.Optional')
        "Optional.ofNullable($expression)"
    }
}