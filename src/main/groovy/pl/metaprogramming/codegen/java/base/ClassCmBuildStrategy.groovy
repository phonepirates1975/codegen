/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import pl.metaprogramming.codegen.java.*
import pl.metaprogramming.codegen.java.formatter.CodeBuffer
import pl.metaprogramming.model.data.DataSchema

import java.util.function.Consumer
import java.util.function.Function
import java.util.function.Predicate

/**
 * Build strategy class should implements at least one of methods:
 *
 * <ul>
 *     <li>makeDeclaration</li>
 *     <li>makeImplementation</li>
 *     <li>makeDecoration</li>
 * </ul>
 * @param < T >   class representing the model for which the code is generated
 */
class ClassCmBuildStrategy<T> {

    protected ClassCmBuilder<T> builder
    protected Function<?, T> modelMapper
    CodeBuffer codeBuf = new CodeBuffer()


    void makeDeclaration() {
        // not every build strategy needs to implement this method
    }

    void makeImplementation() {
        // not every build strategy needs to implement this method
    }

    void makeDecoration() {
        // not every build strategy needs to implement this method
    }

    ClassCmBuildStrategy<T> getInstance(ClassCmBuilder<T> builder) {
        this.builder = builder
        this
    }

    T getModel() {
        modelMapper ? modelMapper.apply(builder.model) : builder.model
    }

    ClassCm getClassModel() {
        ClassCm.cast(builder.classCd)
    }

    String getModelName() {
        builder.modelName
    }

    Object getClassType() {
        builder.classType
    }

    public <T> T getParams(Class<T> clazz) {
        builder.getParams(clazz)
    }

    JavaNameMapper getNameMapper() {
        builder.nameMapper
    }

    String getFieldName(DataSchema schema) {
        builder.getFieldName(schema)
    }

    void addImport(String... classes) {
        builder.addImport(classes)
    }

    void addImport(ClassCd... classes) {
        builder.addImport(classes)
    }

    void setKind(ClassKind kind) {
        classModel.setKind(kind)
    }

    void addInterfaces(Object... interfaces) {
        interfaces.each {
            classModel.addInterface(toClassCd(it))
        }
    }

    private ClassCd toClassCd(Object clazz) {
        if (clazz instanceof ClassCd) {
            clazz
        } else if (clazz instanceof String) {
            ClassCd.of(clazz)
        } else {
            throw new IllegalArgumentException("Can't determine Class by $clazz")
        }
    }

    List<ClassCd> getInterfaces() {
        classModel.interfaces
    }

    void addGenericParams(ClassCd... genericParams) {
        classModel.addGenericParams(genericParams)
    }

    ClassCd getSuperClass() {
        return classModel.getSuperClass()
    }

    void setSuperClass(ClassCd superClass) {
        check(classModel.getSuperClass() == null, "Can't set super class '$superClass', super class already set to '$classModel.superClass'")
        classModel.superClass = superClass
    }

    void setComment(String comment) {
        classModel.description = comment
    }

    void addAnnotation(AnnotationCm annotation) {
        builder.addAnnotation(annotation)
    }

    void addAnnotations(List<AnnotationCm> annotations) {
        classModel.annotations.addAll(annotations)
    }


    void addMethod(MethodCmBuilder... builders) {
        builders.each { bind(it).make() }
    }

    void addMethod(String methodName, Consumer<MethodCm> methodBuilder) {
        classModel.addMethod(methodName) { methodBuilder.accept(it) }
    }

    void addFluentApiMethod(String methodName, Consumer<MethodCm> methodBuilder) {
        classModel.addMethod(methodName) {
            methodBuilder.accept(it)
            it.resultType = classModel
            it.implBody = it.implBody + codeBuf.newLine + 'return this;'
        }
    }

    void addMapper(String methodName, Consumer<MethodCm> methodBuilder) {
        MethodCm methodCm = MethodCm.of(methodName)
        methodBuilder.accept(methodCm)
        addMapper(new FixedMethodCmBuilder(methodCm: methodCm))
    }

    void addMapper(MethodCmBuilder... builders) {
        builders.each {
            classIndex.putMapper(bind(it))
        }
    }

    void addInheritedMapper(String name, ClassCd to, List<ClassCd> from) {
        classIndex.putMapper(new FixedMethodCmBuilder(methodCm: MethodCm.of(name)
                .resultType(to)
                .addParams(from.collect { it.asField() })
                .ownerClass(classModel)
        ))
    }

    MethodCmBuilder bind(MethodCmBuilder<?> builder) {
        builder.builder = this.builder
        if (builder.model == null) {
            builder.model = model
        }
        builder.methodCm.mapper = true
        classModel.addMethod(builder.methodCm)
        if (!config.generateIfUsed) {
            builder.methodCm.markAsUsed()
        }
        builder
    }

    FieldCm addField(FieldCm fieldCm) {
        builder.addField(fieldCm)
    }

    FieldCm addField(String fieldName, ClassCd fieldType, Consumer<FieldCm> fieldBuilder = null) {
        builder.addField(fieldName, fieldType, fieldBuilder)
    }

    List<FieldCm> findFields(Predicate<FieldCm> predicate) {
        classModel.fields.findAll { predicate.test(it) }
    }

    ClassLocator classLocator(Object typeOfCode) {
        builder.classLocator(typeOfCode)
    }

    ClassCd getClass(Object classType, Object model = builder.model) {
        builder.getClass(classType, model)
    }

    ClassCm getClassCm(Object classType) {
        classLocator(classType).getDeclared()
    }

    MappingExpressionBuilder mapping(Object model = getModel()) {
        new MappingExpressionBuilder(builder, model)
    }

    private ClassIndex getClassIndex() {
        builder.classIndex
    }

    private ClassBuilderConfigurator getConfig() {
        builder.config
    }

    protected void check(boolean isOk, String errorMessage) {
        if (!isOk) {
            panic(errorMessage)
        }
    }

    protected void panic(String message, Exception cause = null) {
        classIndex.printIndex()
        throw new IllegalStateException("Can't build $classModel ($config.classType for $model)\n$message", cause)
    }

}