/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import pl.metaprogramming.codegen.generator.CodeGenerationTask
import pl.metaprogramming.codegen.java.formatter.GspTemplateFormatter
import pl.metaprogramming.codegen.java.ClassCd

class ClassGspBuilder extends ClassBuilder {
    ClassCd classCd
    Map codeModel
    boolean wasMadeImplementation

    @Override
    Object getClassType() {
        config.classType
    }

    @Override
    void makeDeclaration() {
        // nothing to do
    }

    @Override
    void makeDecoration() {
        // nothing to do
    }

    @Override
    void makeImplementation() {
        wasMadeImplementation = true
        codeModel = [:]
        codeModel['classCd'] = classCd
        config.dependencies?.each {
            codeModel.put(it.toString(), classIndex.useClass(it))
        }
        codeModel['params'] = params
    }

    @Override
    CodeGenerationTask makeGenerationTask(String filePath) {
        new CodeGenerationTask(
                destFilePath: filePath,
                codeModel: codeModel,
                formatter: new GspTemplateFormatter(config.gspTemplate)
        )
    }
}
