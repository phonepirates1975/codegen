/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import groovy.util.logging.Slf4j
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.MethodCm

import java.util.function.Predicate

@Slf4j
class ClassIndex {

    private static boolean constructionPhase

    List<ClassIndex> dependsOn = []

    private final String name
    private final Map<ClassEntry.Key, ClassEntry> index = new HashMap<>()
    private final Map<MapperEntry.Key, MapperEntry> mappersIndex = [:]
    private final Set<String> usedNames = []

    ClassIndex(String name) {
        this.name = name
    }

    Collection<ClassEntry> getCodesToGenerate() {
        index.values().findAll { it.toGenerate }
    }

    void makeCodeModels() {
        makeCodeDeclarations()
        constructionPhase = true
        markAsUsed()
        while (areCodeModelsToBuild()) {
            makeClasses()
            makeMappers()
        }
        constructionPhase = false
    }

    void makeCodeDeclarations() {
        index.values().each { it.makeDeclaration() }
    }

    private void markAsUsed() {
        // based on always generated classes mark other classes as used
        index.values().each {
            if (it.toMake) it.markAsUsed()
        }
        dependsOn.each { it.markAsUsed() }
    }

    boolean areCodeModelsToBuild() {
        index.values().any { it.toMake } ||
                mappersIndex.values().any { it.toMake } ||
                dependsOn.any { it.areCodeModelsToBuild() }
    }

    void makeClasses() {
        index.values().each {
            if (it.toMake) it.makeImplementation()
        }
        dependsOn.each { it.makeClasses() }
    }

    void makeMappers() {
        mappersIndex.values()
                .findAll { it.toMake }
                .each {
                    log.debug("Going to makeImplementation for $it.methodCm")
                    it.makeImplementation()
                }
        dependsOn.each {
            it.makeMappers()
        }
    }

    void put(ClassBuilder builder) {
        put(builder.classCd, builder.model, builder.config.classType, builder)
    }

    void put(ClassCd classCd, def model, def classType, ClassBuilder builder = null) {
        checkClassName(classCd.canonicalName, true)
        def entry = new ClassEntry(clazz: classCd, classType: classType, model: model, builder: builder)
        index.put(entry.key, entry)
    }

    ClassCd useClass(Object classType) {
        getClassByKey(ClassEntry.Key.of(classType), true, false)
    }

    ClassCd getClass(Object classType, Object model, boolean forceDeclaration) {
        def result = getClassByKey(ClassEntry.Key.of(classType, model), constructionPhase, forceDeclaration)
        result ?: model != null ? getClassByKey(ClassEntry.Key.of(classType), constructionPhase, forceDeclaration) : null
    }

    ClassCd findClass(Object classType, Object model) {
        def result = getClassByKey(ClassEntry.Key.of(classType, model), false, false)
        result ?: model != null ? getClassByKey(ClassEntry.Key.of(classType), false, false) : null
    }

    boolean isToGenerate(Predicate<ClassEntry> predicate) {
        index.values().any { it.isToGenerate() && predicate.test(it) }
    }

    private ClassCd getClassByKey(ClassEntry.Key key, boolean markAsUsed, boolean forceDeclaration) {
        def entry = index.get(key)
        if (entry != null) {
            if (forceDeclaration) {
                entry.makeDeclaration()
            }
            def result = entry.clazz
            if (markAsUsed) {
                result.markAsUsed()
            }
            result
        } else {
            dependsOn.findResult { it.getClassByKey(key, markAsUsed, forceDeclaration) }
        }
    }

    void putMappers(MethodCm... methods) {
        methods.each { addMapper(MapperEntry.of(it)) }
    }

    void putMapper(MethodCmBuilder mapperBuilder) {
        addMapper(MapperEntry.of(mapperBuilder))
    }

    void putMapper(ClassCd thisClass, String thisMethod, ClassCd toClass) {
        addMapper(MapperEntry.of(thisClass, thisMethod, toClass))
    }

    private void addMapper(MapperEntry mapper) {
        if (mapper.methodCm.resultType == null) {
            throw new IllegalArgumentException("Mapper should return value")
        }
        if (mappersIndex.containsKey(mapper.key)) {
            printIndex()
            throw new IllegalStateException("Mapper already defined $mapper.key")

        }
        mappersIndex.put(mapper.key, mapper)
    }

    MapperEntry getMapper(MapperEntry.Key key) {
        def result = mappersIndex.get(key)
        if (result) {
            result.markAsUsed()
            result
        } else {
            dependsOn.findResult { it.getMapper(key) }
        }
    }

    void checkClassName(String canonicalName, boolean markAsUsed) {
        if (usedNames.contains(canonicalName)) {
            throw new IllegalStateException("class $canonicalName already registred")
        }
        dependsOn.each { it.checkClassName(canonicalName, false) }
        if (markAsUsed) {
            usedNames.add(canonicalName)
        }
    }

    void printIndex() {
        printLine "Code index of $name"
        printLine()
        printItems collectClassesToGenerate()
        printItems collectMappers()
        dependsOn.each { it.printIndex() }
    }

    private List<String> collectClassesToGenerate() {
        index.findAll { it.value.toGenerate }
                .collect {
                    "$it.value.clazz.canonicalName ($it.key.classType for $it.key.model)".toString()
                }
                .sort()
    }

    private List<String> collectMappers() {
        mappersIndex.collect {
            "$it.key: $it.value".toString()
        }.sort()
    }

    private static void printItems(List items) {
        items.each { printLine "\t$it" }
        printLine()
    }

    private static void printLine(String message = null) {
        println message ?: '----------------------------------------------'
    }

}
