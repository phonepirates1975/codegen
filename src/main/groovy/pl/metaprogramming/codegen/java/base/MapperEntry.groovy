/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base


import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.MethodCm

class MapperEntry {

    final MethodCm methodCm
    final Key key
    private final MethodCmBuilder builder
    private boolean built

    MapperEntry(MethodCm methodCm, MethodCmBuilder builder, Key key) {
        this.methodCm = methodCm
        this.builder = builder
        this.key = key
    }

    static MapperEntry of(MethodCm methodCm) {
        new MapperEntry(methodCm, null, Key.of(methodCm))
    }

    static MapperEntry of(MethodCmBuilder builder) {
        new MapperEntry(builder.methodCm, builder, Key.of(builder.methodCm))
    }

    static MapperEntry of(ClassCd thisClass, String thisMethod, ClassCd toClass) {
        new MapperEntry(
                MethodCm.of(thisMethod).resultType(toClass).ownerClass(thisClass),
                null,
                Key.of(toClass, [thisClass]))
    }

    void markAsUsed() {
        methodCm.markAsUsed()
    }

    boolean isToMake() {
        !built && builder && (builder.methodCm.used || !builder.generateIfUsed)
    }

    void makeImplementation() {
        built = true
        builder.makeImplementation()
    }

    static class Key {
        final ClassCd to
        final List<ClassCd> from

        private Key(ClassCd to, List<ClassCd> from) {
            this.to = to
            this.from = from
        }

        static Key of(ClassCd to, List<ClassCd> from) {
            new Key(to, from)
        }

        static Key of(MethodCm methodCm) {
            of(methodCm.resultType, methodCm.params.collect { it.type })
        }

        @Override
        String toString() {
            "($from) -> $to"
        }

        @Override
        boolean equals(Object obj) {
            obj instanceof Key && toString() == obj.toString()
        }

        @Override
        int hashCode() {
            toString().hashCode()
        }
    }
}
