/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import groovy.transform.TupleConstructor
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode
import groovy.util.logging.Slf4j
import pl.metaprogramming.codegen.java.*
import pl.metaprogramming.codegen.java.base.BaseMethodCmBuilder
import pl.metaprogramming.model.data.ArrayType
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.ObjectType
import pl.metaprogramming.model.data.constraint.*
import pl.metaprogramming.model.oas.Parameter

import java.util.function.Predicate

import static pl.metaprogramming.codegen.java.ClassCd.stringType
import static pl.metaprogramming.codegen.java.validation.ValidationTypeOfCode.*
import static pl.metaprogramming.model.data.DataTypeCode.*

@Slf4j
class ValidationMethodBuilder extends BaseMethodCmBuilder<ObjectType> {

    static final String DESCRIPTION_FIELD_NAME = 'DESCRIPTION_FIELD_NAME'

    ClassCm dtoClass
    def validatorTypeOfCode
    boolean addDataTypeValidations
    def enumTypeOfCode

    @Override
    MethodCm makeDeclaration() {
        def genericParams = superClass ? superClass.genericParams : classModel.interfaces[0].genericParams
        MethodCm.of('check')
                .addParam('ctx', classLocator(VALIDATION_CONTEXT).getGeneric(genericParams))
    }

    @Override
    String makeImplBody() {
        prepareImplBody()
    }

    protected String prepareImplBody() {
        def constraints = new ConstraintCheckers(dtoClass, new ConstraintsSupport(model.additives).constraints)

        constraints.highPriorityCheckers.each {
            codeBuf.addLines("ctx.check(${it});")
        }
        model.additives['x-validation-beans']?.each {
            setValidationBean(it as Map<String, String>)
        }
        if (model.inherits) {
            codeBuf.addLines("${getObjectChecker(model.inherits[0])}.checkWithParent(ctx);")
        }
        model.fields.each {
            addFieldValidations(it)
        }
        constraints.lowPriorityCheckers.each {
            codeBuf.addLines("ctx.check(${it});")
        }
        codeBuf.take()
    }

    protected void setValidationBean(Map<String, String> spec) {
        assert spec['class']
        assert spec['factory']
        def beanClass = ClassCd.of(spec['class'])
        def factoryDesc = spec['factory'].split(':')
        assert factoryDesc.length == 2
        addImport(beanClass)
        def factoryField = injectDependency(ClassCd.of(factoryDesc[0]))
        codeBuf.newLine("ctx.setBean(${beanClass.className}.class, $factoryField.name::${factoryDesc[1]});")
    }

    String getDescriptionField(DataSchema schema) {
        schema.getAdditive(DESCRIPTION_FIELD_NAME)
    }

    String getObjectChecker(ObjectType objectType) {
        injectDependency(getClass(validatorTypeOfCode, objectType)).name
    }

    String toJavaExpr(ValidatorPointer validator) {
        if (VALIDATION_COMMON_CHECKERS.name() == validator.classCanonicalName) {
            return validator.staticField
        }

        ClassCd validatorClass = ClassCd.of(validator.classCanonicalName)
        addImport(validatorClass)

        if (validator.staticField) {
            return "${validatorClass.className}.${validator.staticField}"
        }

        String validatorObject = validator.diBean
                ? injectDependency(validatorClass).name
                : "ctx.getBean(${validatorClass.className}.class)"
        validator.method ? "${validatorObject}::${validator.method}" : validatorObject
    }

    void addFieldValidations(DataSchema schema) {
        def fieldCm = dtoClass.getField(schema)
        def checkers = new CheckersCollector(fieldCm, schema, fieldCm.type).collect()
        if (checkers) {
            addImportStatic(dtoClass)
            addImportStatic(getClass(VALIDATION_COMMON_CHECKERS))
            codeBuf.newLine("ctx.check(${getDescriptionField(schema)}, ${checkers.join(', ')});")
        }
    }

    @TypeChecked(TypeCheckingMode.SKIP)
    ValidationParams getValidationParams() {
        getParams(ValidationParams)
    }

    @TupleConstructor
    class CheckersCollector {
        FieldCm fieldCm
        DataSchema schema
        ClassCd type
        int level = 1
        List<String> checkers = []

        List<String> nestedCheckers(DataSchema schema, ClassCd type) {
            new CheckersCollector(fieldCm, schema, type, level + 1).collect()
        }

        List<String> collect() {
            def xValidations = new ConstraintCheckers(type, schema.constraints)
            checkers.addAll(xValidations.highPriorityCheckers)
            if (schema.isRequired) {
                checkers.add('required()')
            }
            if (schema.isArray()) {
                addArrayChecker()
                return checkers
            }
            if (schema.isMap()) {
                addMapChecker()
                return checkers
            }
            if (addDataTypeValidations && schema.isType(BOOLEAN, INT32, INT64, FLOAT, DOUBLE, BASE64)) {
                checkers.add(schema.dataType.typeCode.name())
            }
            addObjectChecker()
            addEnumChecker()
            addFormatChecker()
            addPatternChecker()
            addLengthChecker()
            checkers.addAll(xValidations.lowPriorityCheckers)
            // validation that requires data conversion should be perform at the end
            addMinMaxChecker()
            checkers
        }

        void addArrayChecker() {
            if (!schema.array) return
            def arrayType = schema.arrayType
            if (arrayType.uniqueItems) {
                checkers.add('unique()')
            }
            addListSizeChecker(arrayType)
            def itemCheckers = nestedCheckers(arrayType.itemsSchema, type.genericParams[0])
            if (!itemCheckers.isEmpty()) {
                checkers.add("list(${itemCheckers.join(', ')})".toString())
            }
        }

        void addListSizeChecker(ArrayType arrayType) {
            if (!arrayType.minItems && !arrayType.maxItems) return
            String checkerName = "${getDescription()}${level > 1 ? '_' + level : ''}_SIZE"
            addCheckerField(checkerName, "size(${arrayType.minItems}, ${arrayType.maxItems})", type)
            checkers.add(checkerName)
        }

        void addMapChecker() {
            if (!schema.isMap()) return
            def itemCheckers = nestedCheckers(schema.mapType.valuesSchema, type.genericParams[1])
            if (!itemCheckers.isEmpty()) {
                checkers.add("mapValues(${itemCheckers.join(', ')})".toString())
            }
        }

        void addObjectChecker() {
            if (!schema.isObject() || schema instanceof Parameter || !schema.objectType.fields) return
            checkers.add(getObjectChecker(schema.objectType))
        }

        void addEnumChecker() {
            if (!schema.isEnum() || !addDataTypeValidations) return
            def enumClass = getClass(enumTypeOfCode, schema.dataType)
            def checkerName = builder.nameMapper.toConstantName(enumClass.className)
            if (fields { it.name == checkerName }.isEmpty()) {
                addImport(enumClass)
                addCheckerField(checkerName, "allow(${enumClass.className}.values())")
            }
            checkers.add(checkerName)
        }

        void addPatternChecker() {
            if (!schema.pattern) return
            String checkerName = "${getDescription()}_PATTERN"
            addImport('java.util.regex.Pattern')
            String fixedPattern = schema.pattern.
                    replace('\\', '\\\\').
                    replace('"', '\\"')
            String errorCode = schema.invalidPatternCode
            addCheckerField(checkerName, "matches(Pattern.compile(\"$fixedPattern\")${errorCode ? (', "' + errorCode + '"') : ''})")
            checkers.add(checkerName)
        }

        void addMinMaxChecker() {
            if (!schema.minimum && !schema.maximum) return
            String checkerName = "${getDescription()}_MIN_MAX"
            def type = getClass(schema.dataType)
            addImport(type)
            if (addDataTypeValidations) {
                addCheckerField(checkerName, "range(${type.className}::new, ${ValueCm.escaped(schema.minimum)}, ${ValueCm.escaped(schema.maximum)})")
            } else {
                addCheckerField(checkerName, "range(${mapping().from(schema.minimum).to(type)}, ${mapping().from(schema.maximum).to(type)})", type)
            }
            checkers.add(checkerName)
        }

        void addLengthChecker() {
            if (!schema.minLength && !schema.maxLength) return
            String checkerName = "${getDescription()}_LENGTH"
            addCheckerField(checkerName, "length(${schema.minLength}, ${schema.maxLength})")
            checkers.add(checkerName)
        }

        void addFormatChecker() {
            def validator = validationParams.formatValidators.getValidator(schema.format)
            if (validator) {
                checkers.add(toJavaExpr(validator))
            }
        }

        String getDescription() {
            getDescriptionField(fieldCm.getModel() as DataSchema)
        }

        private void addCheckerField(String checkerName, String checkerValue, ClassCd testedType = stringType()) {
            addField(classLocator(VALIDATION_CHECKER).getGeneric(testedType)
                    .asField(checkerName)
                    .setFinal().setStatic()
                    .assign(checkerValue))
        }
    }

    @TupleConstructor
    class ConstraintCheckers {
        ClassCd dataType
        List<DataConstraint> directives = []

        List<String> getHighPriorityCheckers() {
            getCheckers { it.priority <= 0 }
        }

        List<String> getLowPriorityCheckers() {
            getCheckers { it.priority > 0 }
        }

        List<String> getCheckers(Predicate<DataConstraint> predicate) {
            directives
                    .findAll { predicate.test(it) }
                    .sort { it.priority }
                    .collect { addCustomInvalidCode(toJavaExpr(it), it) }
        }

        private String addCustomInvalidCode(String checkExpr, DataConstraint constraint) {
            if (constraint.errorCode == null) {
                checkExpr
            } else {
                def enumItem = makeEnumRef(VALIDATION_ERROR_CODES_ENUM, constraint.errorCode)
                getClassCm(VALIDATION_CHECKER).getMethod('withError').markAsUsed()
                "${checkExpr}.withError(${enumItem.name})"
            }
        }

        private String toJavaExpr(DataConstraint it) {
            if (it instanceof ValidationDirective) {
                toJavaExpr((it as ValidationDirective).pointer)
            } else if (it instanceof DictionaryConstraint) {
                makeDictionaryCheckExpr(it)
            } else if (it instanceof RuleConstraint) {
                makeRuleCheckExpr(it)
            } else if (it instanceof ConditionConstraint) {
                makeCompareCheckExpr(it.expression)
            } else if (it instanceof CheckerRef) {
                toJavaExpr(ValidatorPointer.of(it as CheckerRef))
            } else {
                throw new IllegalStateException("Unsupported directive: $it")
            }
        }

        private String makeRuleCheckExpr(RuleConstraint rule) {
            def validator = validationParams.ruleCheckers.getValidator(rule.ruleCode)
            if (validator != null) {
                return toJavaExpr(validator)
            }
            if (!validationParams.useNamedBeanAsRuleValidator) {
                throw new IllegalStateException("Unknown constraint rule: $rule.ruleCode" +
                        "\nSee https://metaprogramming.gitlab.io/codegen/docs/guides/validations to get know how to handle rule constraints.")
            }
            def checkerName = nameMapper.toFieldName(rule.ruleCode)
            if (!fields { it.name == checkerName }) {
                addField(checkerName, getClass(VALIDATION_CHECKER).withGeneric(dataType))
                        .addAnnotation(AnnotationCm.of("org.springframework.beans.factory.annotation.Qualifier",
                                ['value': ValueCm.escaped(rule.ruleCode)]))
            }
            checkerName
        }

        private String makeCompareCheckExpr(String compare) {
            def exp = CompareExpression.parse(compare, model as ObjectType)
            addDataTypeValidations
                    ? "${exp.operator.name()}(${getDescription(exp.field1)}, ${getDescription(exp.field2)}, ${getDeserializeMapper(exp.field1)})"
                    : "${exp.operator.name()}(${getDescription(exp.field1)}, ${getDescription(exp.field2)})"
        }

        private String getDescription(DataSchema schema) {
            getDescriptionField(schema)
        }

        private String makeDictionaryCheckExpr(DictionaryConstraint constraint) {
            mapping()
                    .to(getClass(VALIDATION_CHECKER).withGeneric(stringType()))
                    .from(makeEnumParam(VALIDATION_DICTIONARY_CODES_ENUM, constraint.dictionaryCode))
                    .make()
        }

        private String getDeserializeMapper(DataSchema schema) {
            mapping().to(schema.dataType).from(stringType()).make()
        }

        private FieldCm makeEnumParam(def enumType, String value) {
            def enumItem = makeEnumRef(enumType, value)
            enumItem.ownerClass.asExpression(enumItem.name)
        }

        private EnumItemCm makeEnumRef(def enumType, String value) {
            def enumClass = classLocator(enumType).getDeclared()
            addImportStatic(enumClass)
            def enumItem = enumClass.addEnumItem(nameMapper.toConstantName(value), true)
            if (enumType == VALIDATION_ERROR_CODES_ENUM) {
                enumItem.value = ValueCm.escaped(value)
            }
            enumItem
        }
    }

}
