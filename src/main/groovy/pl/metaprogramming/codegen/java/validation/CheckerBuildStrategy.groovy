/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.ClassKind
import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy

import static pl.metaprogramming.codegen.java.ClassCd.genericParamV
import static pl.metaprogramming.codegen.java.validation.ValidationTypeOfCode.VALIDATION_CONTEXT
import static pl.metaprogramming.codegen.java.validation.ValidationTypeOfCode.VALIDATION_ERROR_CODES_ENUM

class CheckerBuildStrategy extends ClassCmBuildStrategy<Object> {
    @Override
    void makeDeclaration() {
        setKind(ClassKind.INTERFACE)
        addGenericParams(genericParamV())

        addMethod("check") { it.addParam("context", getClass(VALIDATION_CONTEXT).withGeneric(genericParamV())) }
        addMethod("checkWithParent") {
            it.addParam("context", getClass(VALIDATION_CONTEXT).withGeneric(ClassCd.genericParamT().withSuper(genericParamV())))
            it.addAnnotation(AnnotationCm.suppressWarnings())
            it.implBody = "check((ValidationContext) context);"
        }
        addMethod("checkNull") {
            it.resultType = 'boolean'
            it.implBody = 'return false;'
        }
        addMapper("withError") {
            it.resultType = classModel
            it.addParam("errorCode", getClass(VALIDATION_ERROR_CODES_ENUM))
            it.implBody = codeBuf
                    .add("return context -> check(new ValidationContext<>(context, error -> ValidationError.builder()")
                    .indentInc(2).addLines(
                    ".code(errorCode.getValue())",
                    ".message(error.getMessage())",
                    ".field(error.getField())",
                    ".status(error.getStatus())",
                    ".stopValidation(error.isStopValidation())",
                    ".build()));"
            ).take()
        }
    }
}
