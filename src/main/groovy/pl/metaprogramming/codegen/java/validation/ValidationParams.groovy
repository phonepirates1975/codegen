/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import groovy.transform.builder.Builder
import groovy.transform.builder.SimpleStrategy
import pl.metaprogramming.model.data.constraint.CheckerRef

/**
 * Representation of a set of parameters allowing the parameterization
 * of the generated validation codes.
 */
@Builder(builderStrategy = SimpleStrategy)
class ValidationParams {

    final CheckerMap formatValidators = new CheckerMap()
    final CheckerMap ruleCheckers = new CheckerMap()

    ValidationParams setFormatValidator(String format, String validator) {
        formatValidators.set(format, validator)
        this
    }

    ValidationParams setFormat(String format, CheckerRef checkerRef) {
        formatValidators.set(format, checkerRef)
        this
    }

    ValidationParams setRule(String rule, String checkerClassName) {
        setRule(rule, CheckerRef.of(checkerClassName))
    }

    ValidationParams setRule(String rule, CheckerRef checkerRef) {
        ruleCheckers.set(rule, checkerRef)
        this
    }

    boolean useJakartaBeanValidation = false

    boolean stopAfterFirstError = false

    boolean throwExceptionIfValidationFailed = true

    boolean useNamedBeanAsRuleValidator = true

    String isRequiredErrorCode = "is_required"
    String isNotAllowedErrorCode = "cannot_be_set"
    String isInvalidDataErrorCode = "invalid_data"
    String isNotBase64ErrorCode = "is_not_base64"
    String isNotBooleanErrorCode = "is_not_boolean"
    String isNotLongErrorCode = "is_not_64bit_integer"
    String isNotIntErrorCode = "is_not_32bit_integer"
    String isNotFloatErrorCode = "is_not_float"
    String isNotDoubleErrorCode = "is_not_double"
    String isNotNumberErrorCode = "is_not_number"
    String isNotDateErrorCode = "is_not_date"
    String isNotDateTimeErrorCode = "is_not_date_time"
    String isNotAllowedValueErrorCode = "is_not_allowed_value"
    String isTooSmallErrorCode = "is_too_small"
    String isTooBigErrorCode = "is_too_big"
    String isTooShortErrorCode = "is_too_short"
    String isTooLongErrorCode = "is_too_long"
    String isNotEqualErrorCode = "is_not_equal"
    String isNotMatchPatternErrorCode = "is_not_match_pattern"
    String hasTooFewItemsErrorCode = "has_too_few_items"
    String hasTooManyItems = "has_too_many_items"
    String hasDuplicatedItems = "has_duplicated_items"
}
