/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import pl.metaprogramming.model.data.constraint.CheckerRef
import pl.metaprogramming.model.data.constraint.ValidatorPointer

class CheckerMap {

    private final Map<String, ValidatorPointer> map = [:]

    CheckerMap setIfNotPresent(String code, String validator) {
        if (!map.containsKey(code)) {
            set(code, validator)
        }
        this
    }

    CheckerMap set(String code, String validator) {
        map.put(code, ValidatorPointer.fromExpression(validator))
        this
    }

    CheckerMap set(String code, CheckerRef checkerRef) {
        map.put(code, ValidatorPointer.of(checkerRef))
        this
    }

    ValidatorPointer getValidator(String code) {
        map.get(code)
    }

    CheckerMap clone() {
        def result = new CheckerMap()
        map.forEach(result.map.&put)
        result
    }
}
