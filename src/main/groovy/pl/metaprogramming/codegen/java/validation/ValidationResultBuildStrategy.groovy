/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode
import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy

import static pl.metaprogramming.codegen.java.ClassCd.*

class ValidationResultBuildStrategy extends ClassCmBuildStrategy<Object> {

    @Override
    void makeImplementation() {
        addInterfaces('java.io.Serializable')
        addField('stopped', booleanPrimitiveType()) { it.addAnnotation(AnnotationCm.lombokGetter()) }
        def requestField = addField('request', objectType()) {
            it.addAnnotation(AnnotationCm.lombokGetter())
            it.setTransient()
        }
        addField('status', integerType()) { it.addAnnotation(AnnotationCm.lombokGetter()) }

        addImport('java.util.ArrayList')
        addField('errors', errorClass.asList()) {
            it.addAnnotation(AnnotationCm.lombokGetter())
            it.assign('new ArrayList<>()')
        }

        addMethod(classModel.className) {
            it.addParam(requestField.alias('request'))
            it.implBody = "this.request = request;"
        }
        addMethod('isValid') {
            it.resultType = booleanPrimitiveType()
            it.implBody = 'return status == null;'
        }
        addMethod('isFieldValid') {
            it.resultType = booleanPrimitiveType()
            it.addParam('field', stringType())
            it.implBody = codeBuf.block(
                    "for (ValidationError e : errors)",
                    "if (field != null && field.equals(e.getField())) return false;").endBlock()
                    .addLines('return true;').take()
        }
        addMethod('setStatus') {
            it.resultType = classModel
            it.addParam('status', integerPrimitiveType())
            it.implBody = codeBuf.addLines(
                    'this.status = status;',
                    'return this;').take()
        }
        addMethod('addError') {
            it.resultType = classModel
            it.addParam('error', errorClass)
            it.implBody = codeBuf.addLines(
                    'status = error.getStatus() == null ? 400 : error.getStatus();',
                    'errors.add(error);',
                    shouldStopAfterFirstError()
                            ? 'stopped = true;'
                            : 'stopped = error.isStopValidation();',
                    'return this;').take()
        }
        addMethod('getMessage') {
            it.resultType = stringType()
            it.implBody = 'return errors.get(0).getMessage();'
        }
        addMethod('setError') {
            it.resultType = classModel
            it.addParam('status', integerPrimitiveType())
            it.addParam('message', stringType())
            it.implBody = codeBuf.addLines(
                    'addError(ValidationError.builder().status(status).message(message).stopValidation(true).build());',
                    'return this;').take()
        }
    }

    ClassCd getErrorClass() {
        getClass(ValidationTypeOfCode.VALIDATION_ERROR)
    }

    @TypeChecked(TypeCheckingMode.SKIP)
    boolean shouldStopAfterFirstError() {
        getParams(ValidationParams).stopAfterFirstError
    }

}
