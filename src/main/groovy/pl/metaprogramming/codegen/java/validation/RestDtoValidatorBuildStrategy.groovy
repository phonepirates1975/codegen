/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy

import static pl.metaprogramming.codegen.java.validation.ValidationTypeOfCode.VALIDATION_CHECKER

class RestDtoValidatorBuildStrategy extends ClassCmBuildStrategy {

    def dtoTypeOfCode
    def validatorTypeOfCode
    boolean addDataTypeValidations
    def enumTypeOfCode

    @Override
    void makeImplementation() {
        def dtoClass = getClassCm(dtoTypeOfCode)
        addInterfaces(classLocator(VALIDATION_CHECKER).getGeneric(dtoClass))
        addMethod(new ValidationMethodBuilder(
                dtoClass: dtoClass,
                validatorTypeOfCode: validatorTypeOfCode,
                enumTypeOfCode: enumTypeOfCode,
                addDataTypeValidations: addDataTypeValidations))
    }

}
