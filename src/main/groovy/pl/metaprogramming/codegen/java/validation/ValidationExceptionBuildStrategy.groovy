/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassCd

class ValidationExceptionBuildStrategy extends ClassCmBuildStrategy<Object> {

    static final AnnotationCm ANNOT_LB_ALL_ARGS_CONTR = AnnotationCm.of('lombok.AllArgsConstructor')

    @Override
    void makeImplementation() {
        setSuperClass(ClassCd.of('java.lang.RuntimeException'))
        addAnnotations([ANNOT_LB_ALL_ARGS_CONTR, AnnotationCm.lombokGetter()])
        addField('result', getClass(ValidationTypeOfCode.VALIDATION_RESULT)) { it.setFinal()}
        addMethod('getMessage') {
            it.resultType = ClassCd.stringType()
            it.addAnnotation(AnnotationCm.override())
            it.addImplDependencies(ClassCd.of('java.util.stream.Collectors'))
            it.implBody = '''String errors = result.getErrors().stream()
        .map(err -> err.getField() == null ? err.getCode() : String.format("%s - %s", err.getField(), err.getCode()))
        .collect(Collectors.joining(", "));
return String.format("Failed validation (%d) with errors: %s", result.getStatus(), errors);'''
        }
    }
}
