/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode
import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy

import static pl.metaprogramming.codegen.java.ClassCd.genericParamV
import static pl.metaprogramming.codegen.java.validation.ValidationTypeOfCode.*

class BaseValidatorBuildStrategy extends ClassCmBuildStrategy<Object> {

    @Override
    void makeImplementation() {
        classModel.abstractModifier()
        addGenericParams(genericParamV())
        addInterfaces(classLocator(VALIDATION_CHECKER).getGeneric(genericParamV()))
        addMethod('validate') {
            it.resultType = getClass(VALIDATION_RESULT)
            it.addParam('input', genericParamV())
            it.implBody = makeValidateImplBody()
        }
    }

    private String makeValidateImplBody() {
        codeBuf.addLines(
                'ValidationContext<V> context = new ValidationContext<>(input);',
                'check(context);',
        )
        if (throwExceptionIfValidationFailed) {
            def vex = getClass(VALIDATION_EXCEPTION)
            addImport(vex)
            codeBuf.ifBlock('!context.isValid()', "throw new ${vex.className}(context.getResult());").endBlock()
        }
        codeBuf.addLines('return context.getResult();')
        codeBuf.take()
    }

    @TypeChecked(TypeCheckingMode.SKIP)
    private boolean isThrowExceptionIfValidationFailed() {
        getParams(ValidationParams).throwExceptionIfValidationFailed
    }

}
