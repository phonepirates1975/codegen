/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

enum ValidationTypeOfCode {
    VALIDATION_CONTEXT,
    VALIDATION_FIELD,
    VALIDATION_CHECKER,
    VALIDATION_COMMON_CHECKERS,
    VALIDATION_SIMPLE_CHECKER,
    VALIDATION_ERROR,
    VALIDATION_RESULT,
    VALIDATION_RESULT_MAPPER,           // VALIDATION_RESULT -> ResponseEntity
    REST_EXCEPTION_HANDLER,             // Exception (especially VALIDATION_EXCEPTION) -> ResponseEntity
    VALIDATION_EXCEPTION,
    VALIDATION_VALIDATOR,
    VALIDATION_DICTIONARY_CHECKER,
    VALIDATION_DICTIONARY_CODES_ENUM,
    VALIDATION_ERROR_CODES_ENUM,

    DTO_VALIDATOR,
    REQUEST_VALIDATOR
}