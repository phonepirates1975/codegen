/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.ObjectType

class CompareExpression {

    DataSchema field1
    DataSchema field2
    Operator operator

    static CompareExpression parse(String expression, ObjectType schema) {
        new Parser(expression: expression, schema: schema).parse()
    }

    static Map<String, Operator> OPERATOR_MAP = [
            '>' : Operator.gt,
            '>=': Operator.ge,
            '<' : Operator.lt,
            '<=': Operator.le,
            '=' : Operator.eq,
    ]

    static enum Operator {
        gt,
        ge,
        lt,
        le,
        eq
    }

    private static class Parser {
        String expression
        ObjectType schema

        CompareExpression parse() {
            String[] val = expression.split(' ')
            check(val.length == 3)
            check(OPERATOR_MAP.containsKey(val[1]))
            def field1 = getField(val[0])
            def field2 = getField(val[2])
            check(field1.dataType == field2.dataType, 'fields should be of the same type')
            new CompareExpression(field1: field1, field2: field2, operator: OPERATOR_MAP[val[1]])
        }

        private DataSchema getField(String fieldCode) {
            def field = schema.field(fieldCode)
            check(field != null, "$fieldCode doesn't exists")
            field
        }

        private check(boolean assertion, String details = '') {
            if (!assertion) {
                throw new RuntimeException("Invalid expression: $expression, should have syntax compare:field1 [>|>=|<|<=|=] filed2, $details")
            }
        }
    }
}
