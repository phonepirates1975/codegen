/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy

import static pl.metaprogramming.codegen.java.ClassCd.stringType
import static pl.metaprogramming.codegen.java.validation.ValidationTypeOfCode.VALIDATION_CHECKER
import static pl.metaprogramming.codegen.java.validation.ValidationTypeOfCode.VALIDATION_DICTIONARY_CODES_ENUM

class DictionaryCheckerBuildStrategy extends ClassCmBuildStrategy {

    @Override
    void makeDeclaration() {
        addMapper('check') {
            it.resultType = getClass(VALIDATION_CHECKER).withGeneric(stringType())
            it.addParam('code', getClass(VALIDATION_DICTIONARY_CODES_ENUM))
            it.implBody = 'return ctx -> { /* TODO implement me */ };'
        }
    }

}
