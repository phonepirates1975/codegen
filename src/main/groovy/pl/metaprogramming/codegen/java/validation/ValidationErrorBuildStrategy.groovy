/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.codegen.java.AnnotationCm

import static pl.metaprogramming.codegen.java.ClassCd.*

class ValidationErrorBuildStrategy extends ClassCmBuildStrategy<Object> {

    static final AnnotationCm ANNOT_LB_BUILDER = AnnotationCm.of('lombok.Builder')
    static final AnnotationCm ANNOT_LB_VALUE = AnnotationCm.of('lombok.Value')

    @Override
    void makeImplementation() {
        addAnnotations([ANNOT_LB_BUILDER, ANNOT_LB_VALUE])
        addInterfaces('java.io.Serializable')
        addField('field', stringType())
        addField('code', stringType())
        addField('message', stringType())
        addField('messageArgs', objectType().asArray())
        addField('stopValidation', booleanPrimitiveType())
        addField('status', integerType())
    }
}
