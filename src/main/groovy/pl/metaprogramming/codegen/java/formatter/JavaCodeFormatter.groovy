/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.formatter

import pl.metaprogramming.codegen.generator.CodeFormatter
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.ClassCm
import pl.metaprogramming.codegen.java.EnumItemCm
import pl.metaprogramming.codegen.java.FieldCm
import pl.metaprogramming.codegen.java.MethodCm

class JavaCodeFormatter extends BaseJavaCodeFormatter implements CodeFormatter<ClassCm> {

    static final String JAVA_LANG_PKG = 'java.lang'

    ClassCm classCm

    String format(ClassCm codeModel) {
        format()
    }

    JavaCodeFormatter(ClassCm classCm) {
        this.classCm = classCm
        initImports()
    }

    void initImports() {
        def excludePackages = [classCm.packageName, JAVA_LANG_PKG] +
                classCm.imports.
                        findAll { !it.startsWith('static ') && it.endsWith('.*') }.
                        collect { it.substring(0, it.length() - 2) }
        Set<ClassCd> classes = []
        classCm.annotations.each { it.collectDependencies(classes) }
        classCm.superClass?.collectDependencies(classes)
        classCm.interfaces.each { it.collectDependencies(classes) }
        classCm.fields.each { it.collectDependencies(classes) }
        classCm.enumItems.each { it.collectDependencies(classes) }
        classCm.methodsToGenerate.each { it.collectDependencies(classes) }
        List<String> result = []
        result.addAll(classCm.imports.findAll {
            it.endsWith('.*') || !matchPackage(it, excludePackages)
        })
        result.addAll(classes
                .findAll { it.packageName && !excludePackages.contains(it.packageName) && !it.genericParam }
                .collect { "${it.packageName}.${it.className}".toString() })
        imports = result.unique().sort()
    }

    private boolean matchPackage(String importExp, List<String> packages) {
        packages.contains(importExp.substring(0, importExp.lastIndexOf('.')))
    }

    String format() {
        init(classCm.packageName)

        buf.add("package $classCm.packageName;").newLine()
        buf.addLines(imports.collect { "import $it;" }).newLine()

        if (classCm.description) {
            buf.addLines(formatJavadoc(classCm.description))
        }
        buf.addLines(formatAnnotations(classCm.annotations)).newLine()

        buf.add(formatClassHeader()).add(' {')
        buf.indent(1).newLine()

        addEnumItems(buf)
        addFields(buf)

        buf.indent().newLine()
        if (classCm.fields && classCm.methodsToGenerate) buf.newLine()
        addMethods(buf)

        buf.indent().newLine('}').newLine()
        buf.take()
    }

    private void addEnumItems(CodeBuffer buf) {
        EnumItemCm prevItem
        classCm.enumItems.each {
            if (prevItem) {
                buf.add(',')
                if (prevItem.description) buf.add(" // $prevItem.description")
            }
            buf.addLines(formatAnnotations(it.annotations))
            buf.newLine("${it.name}")
            if (it.value) {
                buf.add("(${it.value})")
            }
            prevItem = it
        }
        if (prevItem) {
            buf.add(';')
            if (prevItem.description) buf.add(" // $prevItem.description")
            buf.newLine()
        }
    }

    private void addFields(CodeBuffer buf) {
        def valueFields = classCm.fields.findAll { it.isFinal() && it.value != null }
        if (valueFields) {
            valueFields.each {
                addField(it, buf)
            }
            buf.newLine()
        }
        (classCm.fields - valueFields).each {
            addField(it, buf)
        }
    }

    private void addField(FieldCm field, CodeBuffer buf) {
        if (field.description) {
            buf.newLine().addLines(formatJavadoc(field.description))
        }
        buf.newLine().add(formatField(field))
        buf.add(';')
    }

    String formatField(FieldCm field) {
        def tokens = [] as List<String>
        collectAnnotations(tokens, field.annotations)
        tokens.add(field.accessModifier.name().toLowerCase())
        if (field.isStatic()) tokens.add('static')
        if (field.isFinal()) tokens.add('final')
        if (field.isTransient()) tokens.add('transient')
        tokens.add(formatTypeName(field))
        if (field.value != null) {
            tokens.add('=')
            tokens.add(field.value.toString())
        }
        tokens.join(SPACE)
    }

    private void addMethods(CodeBuffer buf) {
        classCm.methodsToGenerate.each {
            buf.indent(1)
            if (it.description) {
                buf.addLines(formatJavadoc(it.description))
            }
            if (it.annotations) {
                buf.addLines(formatAnnotations(it.annotations))
            }
            if (classCm.interface && !it.implBody) {
                buf.newLine(formatSignature(it)).add(";").newLine()
            } else {
                buf.newLine("${formatSignature(it)} {")
                buf.indent(2).addLines(it.implBody)
                buf.indent(1).newLine("}").indent().newLine()
            }
        }
    }

    String formatClassHeader() {
        def buf = new StringBuffer()
        buf.append('public ')
        if (classCm.isAbstract()) {
            buf.append('abstract ')
        }
        buf.append(classCm.isInterface() ? 'interface' : classCm.isEnum() ? 'enum' : 'class')
        buf.append(' ').append(classCm.className)
        buf.append(formatGenericParams(classCm.genericParams))
        if (classCm.superClass) {
            buf.append(" extends ${formatUsage(classCm.superClass)}")
        }
        if (classCm.interfaces.size() > 0) {
            buf.append(classCm.isInterface() ? ' extends ' : ' implements ')
            buf.append(classCm.interfaces.collect { formatUsage(it) }.join(', '))
        }
        buf.toString()
    }


    String formatParam(FieldCm fieldCm) {
        "${formatAnnotations(fieldCm.annotations, SPACE)}${formatTypeName(fieldCm)}"
    }

    String formatTypeName(FieldCm fieldCm) {
        "${formatUsage(fieldCm.type)} ${fieldCm.name}"
    }

    static List<String> formatJavadoc(String comment) {
        def result = ['/**']
        comment.eachLine { result.add(' * ' + it) }
        result.add(' */')
        result
    }


    String formatSignature(MethodCm methodCm) {
        def builder = new ConcatenationBuilder()
        addModifiers(builder, methodCm)
        builder.append(genericParams(methodCm))
                .append(methodCm.constructor ? null : formatUsage(methodCm.resultType))
                .append("${methodCm.name}(${methodCm.params ? methodCm.params.collect { formatParam(it) }.join(', ') : ''})")
        if (methodCm.throwExceptions) {
            builder.append(buf.newLine)
            builder.append('throws')
            builder.append(methodCm.throwExceptions.collect { it.className }.join(', '))
        }
        builder.toString()
    }

    void addModifiers(ConcatenationBuilder builder, MethodCm methodCm) {
        if (methodCm.ownerClass.isInterface()) {
            if (methodCm.implBody && !methodCm.isStatic()) builder.append('default')
        } else {
            if (methodCm.isPublic()) builder.append('public')
            if (methodCm.isPrivate() && !(methodCm.isConstructor() && methodCm.ownerClass.isEnum())) {
                builder.append('private')
            }
        }
        if (methodCm.isStatic()) builder.append('static')
    }

    String genericParams(MethodCm methodCm) {
        Set<ClassCd> genericParams = []
        if (methodCm.resultType) {
            collectGenericParams(methodCm.resultType, genericParams)
        }
        methodCm.params?.each { collectGenericParams(it.type, genericParams) }
        if (!methodCm.static && methodCm.ownerClass.genericParams) {
            genericParams.removeAll(methodCm.ownerClass.genericParams)
        }
        formatGenericParams(genericParams)
    }

    static collectGenericParams(ClassCd cm, Set<ClassCd> result) {
        if (cm.isGenericParam()) {
            result.add(cm)
        } else {
            cm.genericParams?.each { collectGenericParams(it, result) }
        }
    }

    static String toUpperCase(String javaName) {
        StringBuilder buf = new StringBuilder()
        for (int i = 0; i < javaName.length(); i++) {
            char c = javaName.charAt(i)
            if (Character.isUpperCase(c) && i > 0) {
                buf.append('_')
            }
            buf.append(Character.toUpperCase(c))
        }
        buf.toString()
    }

    static class ConcatenationBuilder {
        private StringBuilder builder = new StringBuilder()

        ConcatenationBuilder append(String text) {
            if (text?.trim()) {
                if (builder.size() > 0) {
                    builder.append(' ')
                }
                builder.append(text)
            }
            this
        }

        String toString() {
            builder.toString()
        }
    }

}
