/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.formatter

import pl.metaprogramming.codegen.Codegen

class CodeBuffer {
    StringBuffer buf
    String indent
    String newLine
    String tab

    CodeBuffer() {
        this(Codegen.NEW_LINE, JavaCodeFormatter.TAB)
    }

    CodeBuffer(String newLine, String tab) {
        this.newLine = newLine
        this.tab = tab
        buf = new StringBuffer()
        indent = ''
    }

    CodeBuffer reset() {
        buf = new StringBuffer()
        indent = ''
        this
    }

    CodeBuffer indent(int tabs = 0) {
        indent = tab * tabs
        this
    }

    CodeBuffer indentInc(int tabsChange) {
        if (tabsChange >= 0) {
            indent = indent + tab * tabsChange
        } else {
            indent = indent - tab * -tabsChange
        }
        this
    }

    CodeBuffer block(String header, String... lines) {
        newLine("$header {")
        indentInc(1)
        addLines(lines)
        this
    }

    CodeBuffer ifBlock(String condition, String... lines) {
        block("if ($condition)", lines)
        this
    }

    CodeBuffer ifElseValue(String condition, String ifValue, String elseValue) {
        newLine(condition)
        indent = indent + tab + tab
        newLine('? ' + ifValue)
        newLine(': ' + elseValue)
        indent = indent - (tab + tab)
        this
    }

    CodeBuffer tryBlock(String... lines) {
        block('try', lines)
        this
    }

    CodeBuffer catchBlock(String exceptions, String... lines) {
        indentInc(-1)
        newLine("} catch ($exceptions) {")
        indentInc(1)
        addLines(lines)
        this
    }

    CodeBuffer endBlock() {
        indentInc(-1)
        newLine('}')
        this
    }

    CodeBuffer add(Serializable text) {
        buf.append(text)
        this
    }

    CodeBuffer newLine(String text = '') {
        if (buf.size() > 0 || !text) buf.append(newLine)
        if (indent) buf.append(indent)
        buf.append(text)
        this
    }

    CodeBuffer addLines(def lines) {
        if (lines instanceof List) {
            lines.each { newLine(it as String) }
        } else if (lines instanceof String || lines instanceof GString) {
            lines.eachLine { newLine(it) }
        }
        this
    }

    CodeBuffer addLines(String... lines) {
        lines.each { newLine(it) }
        this
    }

    String of(String... lines) {
        addLines(lines).take()
    }

    @Override
    String toString() {
        buf.toString()
    }

    String take() {
        def result = buf.toString()
        reset()
        result
    }
}
