/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.formatter

import groovy.text.SimpleTemplateEngine
import groovy.text.Template
import pl.metaprogramming.codegen.generator.CodeFormatter

class GspTemplateFormatter implements CodeFormatter {

    private static Map<String, Template> templates = [:]
    private String templateName
    private String charset
    private Template template

    GspTemplateFormatter(String templateName, String charset = 'UTF-8') {
        this.templateName = templateName
        this.charset = charset
    }

    String format(Object codeModel) {
        if (template == null) {
            template = getTemplate(templateName)
        }
        generate([d: codeModel])
    }

    String generate(Map binding) {
        try {
            def writable = template.make(binding)
            def sw = new StringWriter()
            writable.writeTo(sw)
            fixLineBreak(sw.toString())
        } catch (Exception e) {
            throw new RuntimeException("Can't generate from template: $templateName, binding: $binding ", e)
        }
    }


    private Template getTemplate(String templateName) {
        if (!templates.containsKey(templateName)) {
            templates.put(templateName, compileTemplate(templateName))
        }
        templates.get(templateName)
    }

    private Template compileTemplate(String templateName) {
        new SimpleTemplateEngine(getClass().getClassLoader()).createTemplate(getResourceContent(templateName))
    }

    private String getResourceContent(String resourceName) {
        def resource = getClass().getResource(resourceName)
        assert resource, "Can't find resource $resourceName"
        resource.getText(charset)
    }

    private static String fixLineBreak(String content) {
        new CodeBuffer().addLines(content).take()
    }
}
