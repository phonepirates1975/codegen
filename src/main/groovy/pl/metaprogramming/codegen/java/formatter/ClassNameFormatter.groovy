/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.formatter

import pl.metaprogramming.codegen.java.ClassCd

/*
Corner case to handle:
1. method (and classes) with generic params
    public <T extends Some> T call(Class<T> clazz)
2. expose nested generic params
    public Map<String, List<String>> call()
3. avoid expose nested generic params
    object.cast(List.class)
4. 'void' and 'Void' cases
    void callMe()
    ResponseEntity<Void> callMe()
5. sometimes the name should be with the package name and sometimes not
 */
class ClassNameFormatter {

    List<String> imports
    String packageName

    String classRef(ClassCd classCd) {
        getClassName(classCd) + arraySuffix(classCd) + '.class'
    }

    String genericParamsDeclaration(Collection<ClassCd> genericParams) {
        formatGenericParams(genericParams, true)
    }

    String format(ClassCd classCd, boolean nested = false, boolean withGenericParamExtends = false) {
        if (classCd == null || classCd.isVoid()) {
            return nested ? 'Void' : 'void'
        }
        def buf = new StringBuffer()
        buf.append(getClassName(classCd))
        buf.append(formatGenericParams(classCd.genericParams, withGenericParamExtends))
        if (withGenericParamExtends && classCd.genericParam && classCd.getSuperClass()) {
            buf.append(" extends ${format(classCd.getSuperClass(), true, withGenericParamExtends)}")
        }
        buf.append(arraySuffix(classCd))
        buf.toString()
    }

    private String formatGenericParams(Collection<ClassCd> genericParams, boolean withGenericParamExtends) {
        if (genericParams) {
            def result = genericParams.collect {
                format(it, true, withGenericParamExtends)
            }
            "<${result.join(',')}>"
        } else {
            ''
        }
    }

    private String getClassName(ClassCd classCd) {
        isImported(classCd) ? classCd.className : classCd.canonicalName
    }

    private String arraySuffix(ClassCd classCd) {
        classCd.array ? '[]' : ''
    }

    private boolean isImported(ClassCd classCd) {
        classCd.packageName == 'java.lang' ||
                classCd.packageName == packageName ||
                imports == null ||
                imports.contains(classCd.canonicalName) ||
                imports.contains(classCd.packageName + '.*')
    }

}
