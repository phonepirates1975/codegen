/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.formatter

import pl.metaprogramming.codegen.generator.CodeDecorator
import pl.metaprogramming.codegen.Codegen

class LicenceDecorator implements CodeDecorator {

    private String licenceHeader

    @Override
    String apply(String s) {
        licenceHeader + s
    }

    LicenceDecorator(String licence) {
        def buf = new StringBuilder()
        buf.append('/*').append(lineBreak())
        licence.eachLine {
            buf.append(it.trim() ? " * $it" : ' *').append(lineBreak())
        }
        buf.append(' */').append(lineBreak()).append(lineBreak())
        licenceHeader = buf.toString()
    }

    private static String lineBreak() {
        Codegen.NEW_LINE
    }

}
