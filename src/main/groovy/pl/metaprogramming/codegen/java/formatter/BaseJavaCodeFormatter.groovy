/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.formatter


import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.ValueCm

import static pl.metaprogramming.codegen.Codegen.NEW_LINE

class BaseJavaCodeFormatter {

    static String TAB = '    '
    static String SPACE = ' '

    List<String> imports = []
    String packageName
    CodeBuffer buf
    ClassNameFormatter classNameFormatter

    void init(String packageName) {
        this.packageName = packageName
        buf = new CodeBuffer(NEW_LINE, TAB)
        classNameFormatter = new ClassNameFormatter(packageName: packageName, imports: imports)
    }

    void collectAnnotations(List<String> result, List<AnnotationCm> annotations) {
        annotations?.each { result.add(formatAnnotation(it)) }
    }

    List<String> formatAnnotations(List<AnnotationCm> annotations) {
        annotations.collect { formatAnnotation(it) }
    }

    String formatAnnotations(List<AnnotationCm> annotations, String separator) {
        annotations ? annotations.collect { formatAnnotation(it) }.join(separator) + separator : ''
    }

    String formatAnnotation(AnnotationCm annotationCm) {
        StringBuilder buf = new StringBuilder()
        buf.append('@').append(formatUsage(annotationCm.annotationClass))
        if (annotationCm.params != null && annotationCm.params.size() > 0) {
            buf.append('(')
            if (annotationCm.params.every { it.key == 'value' }) {
                buf.append(formatValueCm(annotationCm.params.get('value')))
            } else {
                buf.append(annotationCm.params.collect {
                    it.key + " = ${formatValueCm(it.value)}"
                }.join(', '))
            }
            buf.append(')')
        }
        buf.toString()
    }

    static String formatValueCm(ValueCm valueCm) {
        if (valueCm.array) {
            def itemSeparatorFirst = valueCm.itemPerLine ? NEW_LINE + TAB : ''
            def itemSeparatorLast = valueCm.itemPerLine ? NEW_LINE : ''
            def itemSeparator = ', ' + itemSeparatorFirst
            '{' + itemSeparatorFirst +
                    valueCm.value.collect { escapedString(it, valueCm.escaped) }.join(itemSeparator) +
                    itemSeparatorLast + '}'
        } else {
            escapedString(valueCm.value[0], valueCm.escaped)
        }
    }

    static String escapedString(String value, boolean escape = true) {
        value != null ? (escape ? "\"${value}\"" : value) : 'null'
    }

    String formatUsage(ClassCd classCd) {
        classNameFormatter.format(classCd)
    }

    String formatGenericParams(Collection<ClassCd> genericParams) {
        classNameFormatter.genericParamsDeclaration(genericParams)
    }

}
