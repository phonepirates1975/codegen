/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.java.base.ClassBuilderConfigurator
import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.codegen.java.base.DataTypeMapper
import pl.metaprogramming.codegen.java.validation.ValidationParams
import pl.metaprogramming.codegen.java.builders.LombokDataBuildStrategy

import java.util.function.Consumer

class JavaModuleConfigurator<C extends JavaModuleConfigurator> {

    protected static final LombokDataBuildStrategy LOMBOK_DATA_STRATEGY = new LombokDataBuildStrategy()

    protected DataTypeMapper dataTypeMapper

    CodegenParams params
    protected boolean generateIfUsed = false
    Map<Object, ClassBuilderConfigurator> configs = [:]

    JavaModuleConfigurator(CodegenParams params = new CodegenParams()) {
        this.params = params.withIfNotSet(JavaModuleParams.DEFAULTS)
                .withIfNotSet(new ValidationParams())
        dataTypeMapper = params.get(JavaModuleParams).dataTypeMapper.clone()
    }

    protected ClassCmBuildStrategy getDiStrategy() {
        params.get(JavaModuleParams).diStrategy
    }

    protected ClassCmBuildStrategy getComponentStrategy() {
        params.get(JavaModuleParams).componentStrategy
    }

    protected ClassCmBuildStrategy getGeneratedStrategy() {
        params.get(JavaModuleParams).generatedStrategy
    }

    Map<Object, ClassBuilderConfigurator> getClassBuilderConfigs() {
        configs.values().each {
            it.addStrategy(generatedStrategy)
        }
        configs
    }

    C update(Object classType, Consumer<ClassBuilderConfigurator> modifier) {
        modifier.accept(configs[classType])
        (C) this
    }

    C setProjectDir(String projectDir, Object... classTypes) {
        updateConfigs({ it.projectDir = projectDir }, classTypes)
    }

    C setProjectSubDir(String projectSubDir, Object... classTypes) {
        updateConfigs({ it.projectSubDir = projectSubDir }, classTypes)
    }

    C setRootPackage(String rootPackage) {
        setPackage(rootPackage)
    }

    C setPackage(String packageName, Object... classTypes) {
        updateConfigs({ it.packageName = packageName }, classTypes)
    }

    C generateOnlyIfUsed(Object... classTypes) {
        updateConfigs({ it.generateIfUsed = true }, classTypes)
    }

    C generateAlways(Object... classTypes) {
        updateConfigs({ it.generateIfUsed = false }, classTypes)
    }

    C removeClass(def classType) {
        configs.remove(classType)
        (C) this
    }

    C addClass(ClassBuilderConfigurator config) {
        if (config.generateIfUsed == null) {
            config.generateIfUsed = generateIfUsed
        }
        config.dataTypeMapper = dataTypeMapper
        configs.put(config.classType, config)
        (C) this
    }

    C typeOfCode(def classType, Consumer<ClassBuilderConfigurator> setter) {
        setter.accept(typeOfCode(classType))
        (C) this
    }

    ClassBuilderConfigurator typeOfCode(def classType) {
        if (configs.containsKey(classType)) {
            configs.get(classType)
        } else {
            var config = new ClassBuilderConfigurator(classType: classType)
            addClass(config)
            config
        }
    }

    C addClass(def classType, String suffix, ClassCmBuildStrategy... strategies) {
        addClass(classType, suffix, strategies.toList())
    }

    C addClass(def classType, String suffix, List<ClassCmBuildStrategy> strategies) {
        addClass(makeClassConfig(classType, suffix, strategies))
    }

    C addFixedClass(def classType, String className, ClassCmBuildStrategy... strategies) {
        addClass(prepareFixedClass(classType, className, strategies))
    }

    C addFixedClass(def classType, String className, List dependencies = null) {
        typeOfCode(classType)
                .setFixedName(className)
                .setGspTemplate("/pl/metaprogramming/codegen/java/${className}.java.gsp")
                .setDependencies(dependencies)
        (C) this
    }

    C addLombokData(ClassBuilderConfigurator config) {
        config.strategies = config.strategies + LOMBOK_DATA_STRATEGY
        addClass(config)
    }

    C addLombokData(def classType, String suffix, ClassCmBuildStrategy... strategies) {
        addLombokData(makeClassConfig(classType, suffix, strategies.toList()))
    }

    C addComponent(ClassBuilderConfigurator config, Consumer<ClassBuilderConfigurator> setter = {}) {
        config.addStrategy(componentStrategy, diStrategy)
        setter.accept(config)
        addClass(config)
    }

    C addComponent(def classType, String suffix, ClassCmBuildStrategy... strategies) {
        addComponent(makeClassConfig(classType, suffix, strategies.toList()))
    }

    C updateConfigs(Consumer<ClassBuilderConfigurator> updater, Object... classTypes) {
        getClassesConfigs(classTypes).each { updater.accept(it) }
        (C) this
    }

    private Collection<ClassBuilderConfigurator> getClassesConfigs(Object... classTypes) {
        classTypes ? configs.findAll { classTypes.contains(it.key) }.collect { it.value } : configs.values()
    }

    protected static ClassBuilderConfigurator makeClassConfig(def classType, String suffix, List<ClassCmBuildStrategy> builders) {
        new ClassBuilderConfigurator(
                classType: classType,
                strategies: builders
        ).setNameSuffix(suffix)
    }

    protected static ClassBuilderConfigurator prepareFixedClass(def classType, String className, ClassCmBuildStrategy... builders) {
        new ClassBuilderConfigurator(classType: classType, strategies: builders.toList())
                .setFixedName(className)
    }
}
