/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.codegen.java.base.BaseMethodCmBuilder
import pl.metaprogramming.codegen.java.spring.rs.RestParamsBuilder
import pl.metaprogramming.model.oas.Operation

import static pl.metaprogramming.codegen.java.spring.base.SpringDefs.RESPONSE_ENTITY
import static pl.metaprogramming.codegen.java.spring.rs2t.SpringRs2tTypeOfCode.*
import static pl.metaprogramming.codegen.java.validation.ValidationTypeOfCode.VALIDATION_RESULT

class RestControllerMethodBuilder extends BaseMethodCmBuilder<Operation> {

    @Override
    MethodCm makeDeclaration() {
        new RestParamsBuilder(builder, REST_DTO).makeControllerMethod(model)
    }

    @Override
    String makeImplBody() {
        def request = mapping().to(REST_REQUEST_DTO).from(methodCm.params).makeField('request')
        def validationResult = mapping().to(VALIDATION_RESULT).from(request).makeField('validationResult')
        def serviceCall = mapping().to(RESPONSE_DTO).from(mapping().to(REQUEST_DTO).from(request)).makeField()
        addVarDeclaration(request)
        addVarDeclaration(validationResult)
        codeBuf.ifElseValue("return ${validationResult.name}.isValid()",
                mapping().to(RESPONSE_ENTITY).from(serviceCall).make(),
                "${mapping().to(RESPONSE_ENTITY).from(validationResult)};")
                .take()
    }
}
