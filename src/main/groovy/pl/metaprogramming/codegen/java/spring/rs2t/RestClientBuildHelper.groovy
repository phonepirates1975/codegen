/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.base.ClassCmBuilder
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.oas.HttpResponse
import pl.metaprogramming.model.oas.Operation

import static pl.metaprogramming.codegen.java.ClassCd.genericParamUnknown
import static pl.metaprogramming.codegen.java.ClassCd.stringType
import static pl.metaprogramming.codegen.java.spring.base.SpringDefs.*

class RestClientBuildHelper {
    ClassCmBuilder<?> builder
    Operation operation

    RestClientBuildHelper(ClassCmBuilder<?> builder, Operation operation) {
        this.builder = builder
        this.operation = operation
    }

    ClassCd getRequestEntity() {
        REQUEST_ENTITY.withGeneric(getHttpEntityBodyClass())
    }

    ClassCd getHttpEntityBodyClass() {
        if (operation.multipart) {
            MULTI_VALUE_MAP
        } else if (operation.requestBodySchema) {
            builder.getClass(SpringRs2tTypeOfCode.REST_DTO, operation.requestBodySchema.dataType)
        } else {
            ClassCd.voidType()
        }
    }

    HttpResponse getSuccessResponse() {
        if (operation.responses.size() == 1 && operation.responses.first().isDefault()) {
            operation.responses.first()
        } else {
            operation.successResponse
        }
    }

    boolean hasResponseBody() {
        successResponse?.schema?.dataType != null
    }

    DataType getResponseObject() {
        successResponse?.schema?.dataType
    }

    ClassCd getResponseClass() {
        hasResponseBody() ? builder.getClass(SpringRs2tTypeOfCode.REST_DTO, responseObject) : stringType()
    }

    ClassCd getResponseEntity() {
        RESPONSE_ENTITY.withGeneric(hasResponseBody() ? responseClass : genericParamUnknown())
    }
}
