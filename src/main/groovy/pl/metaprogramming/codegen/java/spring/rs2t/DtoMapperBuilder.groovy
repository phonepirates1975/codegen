/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import groovy.transform.builder.Builder
import groovy.transform.builder.SimpleStrategy
import pl.metaprogramming.codegen.java.base.BaseMethodCmBuilder
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.ClassCm
import pl.metaprogramming.codegen.java.FieldCm
import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.ObjectType

@Builder(builderStrategy = SimpleStrategy)
class DtoMapperBuilder extends BaseMethodCmBuilder {
    static final String PARAM_FIELD_NAME = 'value'

    ObjectType objectType
    ClassCd to
    ClassCd from
    List<FieldCm> fromFields
    boolean handleNullParam
    boolean isPrivate
    boolean resultAsParam
    boolean callMapperWithResultParamAsImpl
    boolean throwNotImplementedExceptionAsImpl

    @Override
    MethodCm makeDeclaration() {
        def result = MethodCm.of(makeName()).resultType(to).addParams(makeParams())
        if (isPrivate) result.privateModifier()
        result
    }

    private String makeName() {
        resultAsParam ? 'map' : "map2$to.className"
    }

    private List<FieldCm> makeParams() {
        if (fromFields != null) {
            fromFields
        } else {
            List<FieldCm> params = [from.asField(PARAM_FIELD_NAME)]
            if (resultAsParam) {
                params.add(0, to.asField( 'result'))
            }
            params
        }
    }

    @Override
    String makeImplBody() {
        try {
            if (throwNotImplementedExceptionAsImpl) {
                'throw new RuntimeException("Not implemented");'
            } else if (callMapperWithResultParamAsImpl) {
                "return $PARAM_FIELD_NAME == null ? null : ${mapping().to(to).from([to.asField("new ${to.className}()"), from.asField(PARAM_FIELD_NAME)])};"
            } else {
                makeMapperImplBody()
            }
        } catch (Exception e) {
            throw new IllegalStateException("Can't make implementation for ${builder.classCm.className}.${methodCm.name}", e)
        }
    }

    private String makeMapperImplBody() {
        if (resultAsParam) {
            handleSuperClass()
            codeBuf.add("return result")
        } else if (handleNullParam) {
            codeBuf.add("return value == null ? null : new ${to.className}()")
        } else {
            codeBuf.add("return new ${to.className}()")
        }
        codeBuf.indent(2)
        objectType.fields.each { addMapping(it) }
        codeBuf.newLine().add(';').take()
    }

    private void handleSuperClass() {
        if (to instanceof ClassCm && to.superClass) {
            codeBuf.add("${mapping().to(to.superClass).from(methodCm.params.collect { it.type.superClass.asField(it.name) })};")
                    .newLine()
        }
    }

    private FieldCm addMapping(DataSchema schema) {
        try {
            def toField = (to as ClassCm).getField(schema)
            def fromField = getFromField(schema)
            def transformation = mapping().to(toField.type).from(transformParam(fromField, schema))
            codeBuf.newLine().add(".set${toField.name.capitalize()}($transformation)")
            fromField
        } catch (Exception e) {
            throw new IllegalStateException("Can't generate field for $builder.modelName : $schema", e)
        }
    }

    private FieldCm transformParam(FieldCm fromField, DataSchema schema) {
        if (fromFields) {
            if (schema.defaultValue) {
                addImport('java.util.Optional')
                return fromField.type.asExpression("Optional.ofNullable($fromField.name).orElse(${mapping().from(schema.defaultValue).to(fromField.type)})")
            }
            fromField
        } else {
            fromField.type.asField("${PARAM_FIELD_NAME}.${fromField.name}")
        }
    }

    private FieldCm getFromField(Object schema) {
        if (fromFields) {
            fromFields.find { it.model == schema }
        } else {
            (from as ClassCm).getField(schema)
        }
    }

}
