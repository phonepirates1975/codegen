/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode
import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassKind
import pl.metaprogramming.codegen.java.FieldCm
import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.codegen.java.spring.SpringCommonsTypeOfCode
import pl.metaprogramming.codegen.java.spring.SpringRestParams
import pl.metaprogramming.model.oas.HttpResponse

import static pl.metaprogramming.codegen.java.ClassCd.*

class RestResponseStatusInterfaceBuildStrategy extends ClassCmBuildStrategy<Integer> {

    boolean withContent = true

    @Override
    void makeImplementation() {
        setKind(ClassKind.INTERFACE)
        addGenericParams(genericParamR())
        setSuperClass(getClass(SpringCommonsTypeOfCode.REST_RESPONSE_INTERFACE).withGeneric(genericParamR()))

        addMethod('is' + modelName) {
            it.resultType = 'boolean'
            it.implBody = (isDefaultResponse()
                    ? 'return !getDeclaredStatuses().contains(getStatus());'
                    : "return isStatus($model);") as String
        }
        if (withContent) {
            addGenericParams(genericParamT())
            addMethod('get' + modelName) {
                it.resultType = genericParamT()
                it.addAnnotation(AnnotationCm.suppressWarnings())
                it.implBody = codeBuf
                        .ifBlock("is${modelName}()",
                                "return (T) getBody();")
                        .endBlock()
                        .addLines(makeGetResponseException())
                        .take()
            }
        }
        if (!isStaticFactoryMethod()) {
            addAnnotation(AnnotationCm.javaxParametersAreNonnullByDefault())
            addMethod('set' + modelName) {
                it.resultType = genericParamR()
                it.addParams(makeSetResponseParams())
                it.implBody(makeSetResponseImplBody())
            }
        }
    }

    private String makeGetResponseException() {
        isDefaultResponse()
                ? "throw new IllegalStateException(String.format(\"Response other than %s is not set\", getDeclaredStatuses()));"
                : "throw new IllegalStateException(\"Response ${model} is not set\");"
    }

    private List<FieldCm> makeSetResponseParams() {
        def params = [] as List<FieldCm>
        if (isDefaultResponse()) {
            params.add(integerType().asField('status'))
        }
        if (withContent) {
            params.add(genericParamT().asField('body'))
        }
        params
    }

    private String makeSetResponseImplBody() {
        if (isDefaultResponse()) {
            codeBuf.ifBlock("getDeclaredStatuses().contains(status)",
                    'throw new IllegalArgumentException(String.format("Status %s is declared. Use dedicated setter for it.", status));'
            ).endBlock()
        }
        codeBuf.addLines("return set(${isDefaultResponse() ? 'status' : modelName}, ${withContent ? 'body' : 'null'});")
                .take()
    }

    private boolean isDefaultResponse() {
        model == HttpResponse.DEFAULT
    }

    @TypeChecked(TypeCheckingMode.SKIP)
    private boolean isStaticFactoryMethod() {
        getParams(SpringRestParams).staticFactoryMethodForRestResponse
    }

}
