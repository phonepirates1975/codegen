/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.codegen.java.validation.ValidationTypeOfCode
import pl.metaprogramming.codegen.java.spring.base.SpringDefs

class ValidationResultMapperBuildStrategy extends ClassCmBuildStrategy<Object> {

    @Override
    void makeDeclaration() {
        addMapper('map') {
            it.resultType = SpringDefs.RESPONSE_ENTITY
            it.addParam(getClass(ValidationTypeOfCode.VALIDATION_RESULT).asField('validationResult').addAnnotation(AnnotationCm.javaxNonnul()))
            it.implBody = prepareImplBody()
        }
    }


    private String prepareImplBody() {
        codeBuf
                .addLines('return ResponseEntity')
                .indent(2)
                .addLines(
                        '.status(validationResult.getStatus())',
                        '.body(validationResult.getMessage());   // FIXME and remove \'@Generated\' annotation')
                .take()
    }
}
