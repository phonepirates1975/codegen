/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.codegen.java.base.BaseMethodCmBuilder
import pl.metaprogramming.codegen.java.spring.SpringCommonsTypeOfCode
import pl.metaprogramming.model.oas.Operation
import pl.metaprogramming.model.oas.ParamLocation
import pl.metaprogramming.model.oas.Parameter

import static pl.metaprogramming.codegen.java.spring.base.SpringDefs.HTTP_HEADERS
import static pl.metaprogramming.codegen.java.spring.base.SpringDefs.MULTI_VALUE_MAP

class RestOutRequestMapperBuilder extends BaseMethodCmBuilder<Operation> {

    private static final String REQUEST_PARAM = 'request'
    private static final String RAW_REQUEST_VAR = 'rawRequest'
    private static final String URI_VAR = 'uri'

    @Lazy
    protected RestClientBuildHelper helper = new RestClientBuildHelper(builder, model)

    @Override
    MethodCm makeDeclaration() {
        MethodCm.of('map')
                .resultType(helper.requestEntity)
                .addParam(getClass(SpringRs2tTypeOfCode.REQUEST_DTO).asField(REQUEST_PARAM).addAnnotation(AnnotationCm.javaxNonnul()))
    }

    @Override
    String makeImplBody() {
        addImport('java.net.URI', 'org.springframework.web.util.UriComponentsBuilder')
        makeRawRequestVar()
        makeUriVar()
        def headersVar = makeHeadersVar()
        def bodyVar = makeBodyVar()
        codeBuf.addLines("return RequestEntity.${model.type.name().toLowerCase()}($URI_VAR)")
        codeBuf.indentInc(2)
        if (headersVar) {
            codeBuf.addLines(".headers($headersVar)")
        }
        if (hasRequestBody()) {
            if (!(model.consumeJson || model.multipart)) {
                codeBuf.addLines(".header(\"content-type\", \"${model.consumes.first()}\")")
            }
            codeBuf.addLines(".body($bodyVar);")
        } else {
            codeBuf.addLines('.build();')
        }
        codeBuf.indentInc(-2)
        codeBuf.take()
    }

    private void makeRawRequestVar() {
        addVarDeclaration(mapping()
                .to(SpringRs2tTypeOfCode.REST_REQUEST_DTO)
                .from(methodCm.params)
                .makeField(RAW_REQUEST_VAR))
    }

    private void makeUriVar() {
        def pathParams = makePathParamsVar()
        codeBuf
                .addLines("URI $URI_VAR = UriComponentsBuilder")
                .indentInc(2)
                .addLines(".fromUriString(${callComponent(SpringCommonsTypeOfCode.ENDPOINT_PROVIDER, 'getEndpoint("' + model.path + '")')})")
                .addLines(model.getParameters(ParamLocation.QUERY).collect {
                    ".queryParam${paramNameValue(it)}"
                })
        if (pathParams) {
            codeBuf.addLines(".build($pathParams);")
        } else {
            codeBuf.addLines('.build()', '.toUri();')
        }
        codeBuf.indentInc(-2)
    }

    private String makePathParamsVar() {
        def pathParams = model.getParameters(ParamLocation.PATH)
        if (pathParams.empty) {
            return ''
        }
        addImport('java.util.Map', 'java.util.HashMap')
        codeBuf.addLines("Map<String, String> pathParams = new HashMap<>();")
        codeBuf.addLines(pathParams.collect {
            "pathParams.put${paramNameValue(it)};"
        })
        "pathParams"
    }

    private String paramNameValue(Parameter parameter) {
        "(\"$parameter.name\", ${getter(RAW_REQUEST_VAR, parameter.name)})"
    }

    private String makeBodyVar() {
        model.multipart ? makeMultipartBodyVar()
                : model.requestBodySchema ? getter(RAW_REQUEST_VAR, model.requestBodySchema.code) : null
    }

    private String makeMultipartBodyVar() {
        addImport(MULTI_VALUE_MAP)
        addImport('org.springframework.util.LinkedMultiValueMap')
        String var = 'body'
        codeBuf.addLines("MultiValueMap<String, Object> $var = new LinkedMultiValueMap<>();")
        codeBuf.addLines(model.getParameters(ParamLocation.FORMDATA).collect {
            "${var}.add${paramNameValue(it)};"
        })
        codeBuf.addLines(model.multipartFormDataRequestBody?.fields?.collect {
            "${var}.add(\"$it.code\", ${getter(RAW_REQUEST_VAR, it.code)});"
        })
        var
    }

    private String makeHeadersVar() {
        def headers = model.getParameters(ParamLocation.HEADER)
        if (headers.empty) {
            return null
        }
        String var = 'headers'
        addVarDeclaration(var, HTTP_HEADERS, 'new')
        codeBuf.addLines(headers.collect {
            "${ofNullable(paramGetter(RAW_REQUEST_VAR, it))}.ifPresent(v -> ${var}.add(\"$it.name\", v));"
        })
        var
    }

    private String paramGetter(String varName, Parameter param) {
        getter(varName, builder.getFieldName(param))
    }

    private boolean hasRequestBody() {
        model.requestBodySchema || model.multipart
    }
}
