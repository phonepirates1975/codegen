/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs

import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode
import groovy.util.logging.Slf4j
import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.FieldCm
import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.codegen.java.builders.BaseDtoBuildStrategy
import pl.metaprogramming.codegen.java.validation.ValidationParams
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.oas.Parameter

import static pl.metaprogramming.codegen.java.spring.rs.SpringRsTypeOfCode.DTO

@Slf4j
class DtoBuildStrategy extends BaseDtoBuildStrategy {

    JakartaBeanValidationSupport jbvSupport = new JakartaBeanValidationSupport()
    boolean withDescriptiveFields = true
    boolean withJsonAnnotations = true

    @Override
    void addField(DataSchema schema) {
        FieldCm fieldCm = addField(getFieldName(schema), getClass(DTO, schema.dataType)) {
            it.model = schema
            it.description = schema.description
            it.annotations = makeFieldAnnotations(schema)
            if (schema instanceof Parameter) {
                String paramDesc = "$schema.name $schema.location parameter."
                it.description = it.description
                        ? paramDesc + Codegen.NEW_LINE + '<br/>' + it.description
                        : paramDesc
            }
        }
        if (shouldAddDescriptiveFields()) {
            addDescriptiveFields(schema, fieldCm)
        }
    }

    @Override
    void makeImplementation() {
        model.fields.each {
            if (it.defaultValue) {
                def field = classModel.getField(it)
                field.value = mapping().from(it.defaultValue).to(field.type).makeValue()
            }
        }
    }

    private List<AnnotationCm> makeFieldAnnotations(DataSchema schema) {
        def result = [] as List<AnnotationCm>
        if (schema.code != getFieldName(schema) && withJsonAnnotations) {
            result.add(AnnotationCm.of('com.fasterxml.jackson.annotation.JsonProperty',
                    [value: ValueCm.escaped(schema.code)]))
        }
        if (isJbvEnabled()) {
            jbvSupport.addAnnotations(result as List<AnnotationCm>, schema)
        } else {
            result.add(schema.isRequired ? AnnotationCm.javaxNonnul() : AnnotationCm.javaxNullable())
        }
        result
    }

    @TypeChecked(TypeCheckingMode.SKIP)
    private boolean isJbvEnabled() {
        getParams(ValidationParams).useJakartaBeanValidation
    }

    private boolean shouldAddDescriptiveFields() {
        withDescriptiveFields && !isJbvEnabled()
    }
}
