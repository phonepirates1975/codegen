/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.codegen.java.spring.base.SpringDefs
import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.MethodCm

import java.util.function.Consumer

import static pl.metaprogramming.codegen.java.ClassCd.*

class SerializationUtilsBuildStrategy extends ClassCmBuildStrategy {

    static final ClassCd T_COLLECTORS = of('java.util.stream.Collectors')
    static final ClassCd T_LOCAL_DATE = of('java.time.LocalDate')
    static final ClassCd T_LOCAL_DATE_TIME = of('java.time.LocalDateTime')
    static final ClassCd T_ZONED_DATE_TIME = of('java.time.ZonedDateTime')
    static final ClassCd T_ZONED_OFFSET = of('java.time.ZoneOffset')
    static final ClassCd T_DATA_TIME_FORMATTER = of('java.time.format.DateTimeFormatter')
    static final ClassCd T_BIG_DECIMAL = of('java.math.BigDecimal')

    @Override
    void makeDeclaration() {
        // inner methods
        addMethod('fromString') {
            it.resultType = genericParamT()
            it.addParam('value', stringType())
            it.addParam('transformer', 'java.util.function.Function<java.lang.String, T>')
            it.implBody = 'return value == null || value.isEmpty() ? null : transformer.apply(value);'
        }
        addMethod('toString') {
            it.resultType = stringType()
            it.addParam('value', genericParamT())
            it.addParam('transformer', 'java.util.function.Function<T, java.lang.String>')
            it.implBody = 'return value == null ? null : transformer.apply(value);'
        }

        // exposed methods
        def rawParam = stringType().asField('value')
        def formatParam = stringType().asField('format')

        // collections support
        addMapper('transformList') {
            it.resultType = 'java.util.List<R>'
            it.addParam('value', 'java.util.List<T>')
            it.addParam('transformer', 'java.util.function.Function<T, R>')
            it.implBody = 'return value == null ? null : value.stream().map(transformer).collect(Collectors.toList());'
            it.addImplDependencies(T_COLLECTORS)
        }
        addMapper('transformMap') {
            it.resultType = 'java.util.Map<K, R>'
            it.addParam('value', 'java.util.Map<K, T>')
            it.addParam('transformer','java.util.function.Function<T, R>')
            it.implBody = 'return value == null ? null : value.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> transformer.apply(e.getValue())));'
            it.addImplDependencies(T_COLLECTORS)
        }

        // long support
        addMapper('toLong') {
            it.resultType = longType()
            it.addParam(rawParam)
            it.implBody = 'return fromString(value, Long::valueOf);'
        }
        addMapper('toString') {
            it.resultType = stringType()
            it.addParam('value', longType())
            it.implBody = 'return toString(value, Object::toString);'
        }
        // integer support
        addMapper('toInteger') {
            it.resultType = integerType()
            it.addParam(rawParam)
            it.implBody 'return fromString(value, Integer::valueOf);'
        }
        addMapper('toString') {
            it.resultType = stringType()
            it.addParam('value', integerType())
            it.implBody = 'return toString(value, Object::toString);'
        }
        // float support
        addMapper('toFloat') {
            it.resultType = floatType()
            it.addParam(rawParam)
            it.implBody = 'return fromString(value, Float::valueOf);'
        }
        addMapper('toString') {
            it.resultType = stringType()
            it.addParam('value', floatType())
            it.implBody = 'return toString(value, Object::toString);'
        }
        // BigDecimal support
        addMapper('toBigDecimal') {
            it.resultType = T_BIG_DECIMAL
            it.addParam(rawParam)
            it.implBody = 'return fromString(value, BigDecimal::new);'
        }
        addMapper('toString') {
            it.resultType = stringType()
            it.addParam('value', bigDecimalType())
            it.implBody = 'return toString(value, Object::toString);'
        }
        addMapper('toBigDecimal') {
            it.resultType = bigDecimalType()
            it.addParams(rawParam, formatParam)
            it.implBody = 'return fromString(value, v -> new BigDecimal(v));'
        }
        addMapper('toString') {
            it.resultType = stringType()
            it.addParam('value', bigDecimalType()).addParam(formatParam)
            it.implBody = 'return toString(value, v -> v.toString());'
        }
        // LocalDate support
        addMapper('toLocalDate') {
            it.resultType = T_LOCAL_DATE
            it.addParam('value', stringType())
            it.implBody = 'return fromString(value, LocalDate::parse);'
        }
        addMapper('toString') {
            it.resultType = stringType()
            it.addParam('value', T_LOCAL_DATE)
            it.implBody = 'return toString(value, DateTimeFormatter.ISO_LOCAL_DATE::format);'
            it.addImplDependencies(T_DATA_TIME_FORMATTER)
        }
        // LocalDateTime support
        addMapper('toLocalDateTime') {
            it.resultType = T_LOCAL_DATE_TIME
            it.addParam('value', stringType())
            it.implBody = 'return fromString(value, v -> ZonedDateTime.parse(v, DateTimeFormatter.ISO_OFFSET_DATE_TIME).withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime());'
            it.addImplDependencies(T_ZONED_DATE_TIME, T_ZONED_OFFSET, T_DATA_TIME_FORMATTER)
        }
        addMapper('toString') {
            it.resultType = stringType()
            it.addParam('value', T_LOCAL_DATE_TIME)
            it.implBody = 'return toString(value, v -> DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(value.atZone(ZoneOffset.UTC)));'
            it.addImplDependencies(T_ZONED_OFFSET, T_DATA_TIME_FORMATTER)
        }
        addMapper('toString') {
            it.resultType = stringType()
            it.addParam('value', T_ZONED_DATE_TIME)
            it.implBody = 'return toString(value, v -> DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(value));'
            it.addImplDependencies(T_DATA_TIME_FORMATTER)
        }
        // Boolean support
        addMapper('toBoolean') {
            it.resultType = booleanType()
            it.addParam(rawParam)
            it.implBody = 'return fromString(value, Boolean::valueOf);'
        }
        addMapper('toString') {
            it.resultType = stringType()
            it.addParam('value', booleanType())
            it.implBody = 'return toString(value, Object::toString);'
        }
        // Double support
        addMapper('toDouble') {
            it.resultType = doubleType()
            it.implBody = 'return fromString(value, Double::valueOf);'
            it.addParam(rawParam)
        }
        addMapper('toString') {
            it.resultType = stringType()
            it.addParam('value', doubleType())
            it.implBody = 'return toString(value, Object::toString);'
        }
        // binary support
        addMapper('toBytes') {
            it.resultType = byteArrayType()
            it.addParam(rawParam)
            it.implBody = 'return value != null ? Base64.getDecoder().decode(value) : null;'
            it.addImplDependencies(of('java.util.Base64'))
        }
        addMapper('toString') {
            it.resultType = stringType()
            it.addParam('value', byteArrayType())
            it.implBody = 'return value != null ? Base64.getEncoder().encodeToString(value) : null;'
            it.addImplDependencies(of('java.util.Base64'))
        }
        addMapper('toBytes') {
            it.resultType = byteArrayType()
            it.addParam('value', of('javax.servlet.http.HttpServletRequest'))
            it.addAnnotation(AnnotationCm.of('lombok.SneakyThrows'))
            it.implBody = 'return value != null ? IOUtils.toByteArray(value.getInputStream()) : null;'
            it.addImplDependencies(of('org.apache.commons.io.IOUtils'))
        }
        addMapper('toBytes') {
            it.resultType = byteArrayType()
            it.addParam('value', SpringDefs.RESOURCE)
            it.addAnnotation(AnnotationCm.of('lombok.SneakyThrows'))
            it.implBody = 'return value != null ? IOUtils.toByteArray(value.getInputStream()) : null;'
            it.addImplDependencies(of('org.apache.commons.io.IOUtils'))
        }
        addMapper('toBytes') {
            it.resultType = byteArrayType()
            it.addParam(SpringDefs.MULTIPART_FILE.asField('file'))
            it.addAnnotation(AnnotationCm.of('lombok.SneakyThrows'))
            it.implBody = 'return file != null ? file.getBytes() : null;'
        }
    }

    @Override
    void addMethod(String methodName, Consumer<MethodCm> methodBuilder) {
        super.addMethod(methodName, methodBuilder.andThen { it.staticModifier() })
    }

    @Override
    void addMapper(String methodName, Consumer<MethodCm> methodBuilder) {
        super.addMapper(methodName, methodBuilder.andThen { it.staticModifier() })
    }

}
