/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode
import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.java.JavaModuleConfigurator
import pl.metaprogramming.codegen.java.JavaModuleGenerator
import pl.metaprogramming.codegen.java.JavaModuleParams
import pl.metaprogramming.codegen.java.base.ClassCmBuilder
import pl.metaprogramming.codegen.java.jaxb.JaxbTypeOfCode
import pl.metaprogramming.codegen.java.jaxb.XmlDtoBuildStrategy
import pl.metaprogramming.codegen.java.jaxb.XmlEnumBuildStrategy
import pl.metaprogramming.codegen.java.jaxb.XmlPackageInfoBuilder
import pl.metaprogramming.codegen.java.spring.soap.SoapClientBuildStrategy
import pl.metaprogramming.codegen.java.spring.soap.SoapClientConfigurationBuildStrategy
import pl.metaprogramming.codegen.java.spring.soap.SpringSoapTypeOfCode
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.wsdl.WsdlApi

import java.util.function.Consumer

class SpringSoapClientGenerator extends JavaModuleGenerator<WsdlApi> {

    public static final TOC_CLIENT = SpringSoapTypeOfCode.CLIENT
    public static final TOC_CLIENT_CONFIGURATION = SpringSoapTypeOfCode.CLIENT_CONFIGURATION
    public static final TOC_DTO = JaxbTypeOfCode.DTO
    public static final TOC_ENUM = JaxbTypeOfCode.ENUM

    static SpringSoapClientGenerator of(
            WsdlApi model,
            CodegenParams params,
            Consumer<Configurator> config) {
        def configurator = new Configurator(params)
        config.accept(configurator)
        new SpringSoapClientGenerator(model, configurator)
    }

    static SpringSoapClientGenerator of(
            WsdlApi model,
            Consumer<Configurator> config) {
        of(model, new CodegenParams(), config)
    }

    SpringSoapClientGenerator(WsdlApi model, JavaModuleConfigurator config) {
        super(model, config.classBuilderConfigs, config.params)
    }

    @Override
    void generate() {
        addSchemaCodes(model)
        addPackageInfo(model)
        addOperationCodes(model)
        makeCodeModels()
    }

    void addPackageInfo(WsdlApi model) {
        def baseDir = getConfig(TOC_DTO).baseDir
        assert baseDir == getConfig(TOC_ENUM).baseDir
        def namespaces = model.schemas.values().collect { it.namespace }.unique()
        packageInfoList = namespaces.collect { namespace ->
            XmlPackageInfoBuilder.make(
                    baseDir,
                    namespace,
                    getPackage(namespace),
                    model.namespaceElementFormDefault[namespace],
                    params.get(JavaModuleParams).generatedAnnotation)
        }
    }

    void addOperationCodes(WsdlApi model) {
        String clientName = model.name.capitalize()
        addClass(TOC_CLIENT, model, clientName)
        addClass(TOC_CLIENT_CONFIGURATION, model, clientName)
    }

    void addSchemaCodes(WsdlApi model) {
        model.schemas.values().each { schema ->
            addClass(schema)
            model.schemaPackages.add(getPackage(schema.namespace))
        }
    }

    ClassCmBuilder addClass(DataSchema schema) {
        addClass(
                schema.namespace,
                schema.enum ? TOC_ENUM : TOC_DTO,
                schema.code,
                schema.dataType
        )
    }

    ClassCmBuilder addClass(String namespace, def classType, String className, def model) {
        addClass(new ClassCmBuilder<>(
                model: model,
                modelName: className,
                packageName: getPackage(namespace)
        ), classType)
    }

    @TypeChecked(TypeCheckingMode.SKIP)
    String getPackage(String namespace) {
        def packageName = getParams(SpringSoapParams).namespace2Package[namespace]
        assert packageName, "Package not defined for namespace: $namespace"
        packageName
    }

    static class Configurator extends JavaModuleConfigurator<Configurator> {

        Configurator(CodegenParams params) {
            super(params.withIfNotSet(new SpringSoapParams()))
            init()
        }

        private init() {
            generateIfUsed = true
            addClass(TOC_ENUM, '', new XmlEnumBuildStrategy())
            addLombokData(TOC_DTO, '', new XmlDtoBuildStrategy())
            addClass(TOC_CLIENT, 'Client', new SoapClientBuildStrategy())
            addClass(TOC_CLIENT_CONFIGURATION, 'ClientConfiguration', new SoapClientConfigurationBuildStrategy(), diStrategy)
            generateAlways(TOC_CLIENT, TOC_CLIENT_CONFIGURATION)
            this
        }

        Configurator setNamespacePackage(String namespace, String packageName) {
            params.get(SpringSoapParams).namespace2Package[namespace] = packageName
            this
        }
    }

}
