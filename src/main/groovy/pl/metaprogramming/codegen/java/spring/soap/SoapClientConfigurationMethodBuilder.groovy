/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.soap

import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.codegen.java.base.BaseMethodCmBuilder
import pl.metaprogramming.codegen.java.spring.SpringCommonsTypeOfCode
import pl.metaprogramming.codegen.java.spring.base.SpringDefs
import pl.metaprogramming.model.wsdl.WsdlApi

import static pl.metaprogramming.codegen.java.jaxb.JaxbTypeOfCode.DTO
import static pl.metaprogramming.codegen.java.spring.soap.SpringSoapTypeOfCode.CLIENT

class SoapClientConfigurationMethodBuilder extends BaseMethodCmBuilder<WsdlApi> {

    @Override
    MethodCm makeDeclaration() {
        def clientClass = getClass(CLIENT)
        MethodCm.of("create${clientClass.className}")
                .resultType(clientClass)
                .addAnnotation(SpringDefs.ANNOT_BEAN)
    }

    @Override
    String makeImplBody() {
        def resultClass = getClass(CLIENT).className

        addImport('org.springframework.oxm.jaxb.Jaxb2Marshaller')
        codeBuf
                .newLine('Jaxb2Marshaller marshaller = new Jaxb2Marshaller();')
                .newLine("marshaller.setPackagesToScan($contextPaths);")
                .newLine("$resultClass client = new $resultClass();")
                .newLine("client.setDefaultUri(${getEndpointUri()});")
                .newLine("client.setMarshaller(marshaller);")
                .newLine("client.setUnmarshaller(marshaller);")
                .newLine("return client;")
                .take()
    }

    String getContextPaths() {
        model.schemaPackages
                .findAll { pkg ->
                    builder.classIndex.isToGenerate {
                        it.clazz.packageName == pkg && it.classType == DTO
                    }
                }
                .collect { "\"$it\"" }
                .join(", ")
    }

    String getEndpointUri() {
        classLocator(SpringCommonsTypeOfCode.ENDPOINT_PROVIDER).getOptional()
                .map({
                    def endpointProviderField = injectDependency(it)
                    "${endpointProviderField.name}.getEndpoint(\"${model.uri.toURL().path}\")"
                }).orElseGet({ "\"${model.uri}\"" })
    }
}
