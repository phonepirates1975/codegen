/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode
import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.java.JavaModuleConfigurator
import pl.metaprogramming.codegen.java.JavaModuleGenerator
import pl.metaprogramming.codegen.java.builders.EnumBuildStrategy
import pl.metaprogramming.codegen.java.builders.InterfaceBuildStrategy
import pl.metaprogramming.codegen.java.spring.rs2t.*
import pl.metaprogramming.codegen.java.validation.RestDtoValidatorBuildStrategy
import pl.metaprogramming.codegen.java.validation.RestRequestValidatorBuildStrategy
import pl.metaprogramming.codegen.java.validation.ValidationParams
import pl.metaprogramming.codegen.java.validation.ValidationTypeOfCode
import pl.metaprogramming.model.oas.Parameter
import pl.metaprogramming.model.oas.RestApi

import java.util.function.Consumer

import static pl.metaprogramming.codegen.java.ClassCd.stringType
import static pl.metaprogramming.codegen.java.spring.base.SpringDefs.*

class SpringRestService2tGenerator extends JavaModuleGenerator<RestApi> {

    public static final TOC_REST_CONTROLLER = SpringRs2tTypeOfCode.REST_CONTROLLER
    public static final TOC_REST_CONTROLLER_MO = SpringRs2tTypeOfCode.REST_CONTROLLER_MO
    public static final TOC_FACADE = SpringRs2tTypeOfCode.FACADE
    public static final TOC_FACADE_IMPL = SpringRs2tTypeOfCode.FACADE_IMPL
    public static final TOC_REST_DTO_VALIDATOR = ValidationTypeOfCode.DTO_VALIDATOR
    public static final TOC_REST_REQUEST_VALIDATOR = ValidationTypeOfCode.REQUEST_VALIDATOR
    public static final TOC_DTO = SpringRs2tTypeOfCode.DTO
    public static final TOC_REST_DTO = SpringRs2tTypeOfCode.REST_DTO
    public static final TOC_ENUM = SpringRs2tTypeOfCode.ENUM
    public static final TOC_REST_REQUEST_DTO = SpringRs2tTypeOfCode.REST_REQUEST_DTO
    public static final TOC_REQUEST_DTO = SpringRs2tTypeOfCode.REQUEST_DTO
    public static final TOC_RESPONSE_DTO = SpringRs2tTypeOfCode.RESPONSE_DTO
    public static final TOC_REST_MAPPER = SpringRs2tTypeOfCode.REST_MAPPER
    public static final TOC_REQUEST_MAPPER = SpringRs2tTypeOfCode.REQUEST_MAPPER
    public static final TOC_RESPONSE_MAPPER = SpringRs2tTypeOfCode.RESPONSE_MAPPER

    static SpringRestService2tGenerator of(
            RestApi model,
            CodegenParams params,
            Consumer<Configurator> configUpdater) {
        def configurator = new Configurator(params)
        configUpdater.accept(configurator)
        def result = new SpringRestService2tGenerator(model, configurator)
        result.typeOfCodesForOperation = configurator.typeOfCodesForOperation
        result
    }

    static SpringRestService2tGenerator of(
            RestApi model,
            Consumer<Configurator> config) {
        of(model, new CodegenParams(), config)
    }

    SpringRestService2tGenerator(RestApi model, JavaModuleConfigurator config) {
        super(model, config.classBuilderConfigs, config.params)
    }

    List typeOfCodesForOperation

    @Override
    void generate() {
        classIndex.putMapper(MULTIPART_FILE, 'getResource', RESOURCE)
        addObjectParamsCodes(model.parameters)
        addSchemaCodes(model)
        addOperationCodes(model)
        makeCodeModels()
    }

    void addObjectParamsCodes(List<Parameter> parameters) {
        parameters.findAll { it.isObject() }.each { paramDef ->
            classIndex.put(stringType(), paramDef.dataType, TOC_REST_DTO)
            addClass(TOC_DTO, paramDef.dataType, paramDef.objectType.code)
            addClass(TOC_REST_MAPPER, paramDef.dataType, paramDef.objectType.code)
        }
    }

    void addSchemaCodes(RestApi model) {
        model.schemas.each { schema ->
            if (schema.isEnum()) {
                addClass(TOC_ENUM, schema)
            }
            if (schema.isObject()) {
                addClass(TOC_DTO, schema)
                addClass(TOC_REST_DTO, schema)
                addClass(TOC_REST_MAPPER, schema)
                addClass(TOC_REST_DTO_VALIDATOR, schema)
            }
        }
    }

    void addOperationCodes(RestApi model) {
        model.groupedOperations.each { group, operations ->
            operations.each { operation ->
                String dataName = operation.code.capitalize()
                operationClassTypes().each { classType ->
                    addClass(classType, operation, dataName)
                }
            }
            resourceClassTypes().each { classType ->
                addClass(classType, operations, group)
            }
        }
    }

    @TypeChecked(TypeCheckingMode.SKIP)
    private boolean isMultiOperationControllerStrategy() {
        !getParams(SpringRestParams).controllerPerOperation
    }

    private List operationClassTypes() {
        typeOfCodesForOperation + (isMultiOperationControllerStrategy() ? [] : [TOC_REST_CONTROLLER])
    }

    private List resourceClassTypes() {
        def result = [TOC_FACADE, TOC_FACADE_IMPL]
        if (isMultiOperationControllerStrategy()) {
            result.add(TOC_REST_CONTROLLER_MO)
        }
        result
    }

    static class Configurator extends JavaModuleConfigurator<Configurator> {

        List<Object> typeOfCodesForOperation = [
                TOC_REST_REQUEST_DTO,
                TOC_REQUEST_DTO,
                TOC_RESPONSE_DTO,
                TOC_REQUEST_MAPPER,
                TOC_REST_REQUEST_VALIDATOR,
                TOC_RESPONSE_MAPPER] as List<Object>

        Configurator(CodegenParams params) {
            super(params.withIfNotSet(SpringRestParams.DEFAULTS))
            dataTypeMapper.setMapper(REST_DATA_TYPE_MAPPER, TOC_REST_DTO, TOC_REST_REQUEST_DTO, TOC_REST_CONTROLLER_MO)
            params.get(ValidationParams).formatValidators
                    .setIfNotPresent('date', 'field:VALIDATION_COMMON_CHECKERS.ISO_DATE')
                    .setIfNotPresent('date-time', 'field:VALIDATION_COMMON_CHECKERS.ISO_DATE_TIME')
            init()
        }

        private void init() {
            generateIfUsed = true
            addComponent(TOC_REST_REQUEST_VALIDATOR, 'Validator', new RestRequestValidatorBuildStrategy(
                    dtoTypeOfCode: TOC_REST_REQUEST_DTO,
                    validatorTypeOfCode: TOC_REST_DTO_VALIDATOR,
                    enumTypeOfCode: TOC_ENUM,
                    addDataTypeValidations: true
            ))
            addComponent(TOC_REST_DTO_VALIDATOR, 'Validator', new RestDtoValidatorBuildStrategy(
                    dtoTypeOfCode: TOC_REST_DTO,
                    validatorTypeOfCode: TOC_REST_DTO_VALIDATOR,
                    enumTypeOfCode: TOC_ENUM,
                    addDataTypeValidations: true
            ))

            addClass(TOC_ENUM, 'Enum', new EnumBuildStrategy())
            addClass(TOC_RESPONSE_DTO, 'Response', new RestResponseBuildStrategy())
            typeOfCode(TOC_RESPONSE_MAPPER)
                    .setNameSuffix('ResponseMapper')
                    .addStrategy(componentStrategy, diStrategy)
                    .onDeclaration { it.addMapper(new RestInResponseMapperBuilder()) }

            addLombokData(TOC_DTO, 'Dto', new DtoBuildStrategy())
            addLombokData(TOC_REST_DTO, 'Rdto', new JsonDtoBuildStrategy())
            addComponent(TOC_REST_MAPPER, 'Mapper', new RestDtoMapperBuildStrategy())

            addLombokData(TOC_REQUEST_DTO, 'Request', new DtoBuildStrategy(modelMapper: OPERATION_REQUEST_SCHEMA))
            addLombokData(TOC_REST_REQUEST_DTO, 'Rrequest', new PayloadFieldAppenderBuildStrategy(), new RawDtoBuildStrategy(modelMapper: OPERATION_REQUEST_SCHEMA))
            addComponent(TOC_REQUEST_MAPPER, 'RequestMapper', new RestInRequestMapperBuildStrategy())

            typeOfCode(TOC_REST_CONTROLLER)
                    .setNameSuffix('Controller')
                    .onImplementation { it.addMethod(new RestControllerMethodBuilder()) }
                    .onDecoration { it.addAnnotation(ANNOT_REST_CONTROLLER) }
                    .addStrategy(diStrategy)
            addClass(TOC_REST_CONTROLLER_MO, 'Controller', new RestControllerMoBuildStrategy(), diStrategy)
            addClass(TOC_FACADE, 'Facade', new InterfaceBuildStrategy(TOC_FACADE_IMPL))
            addComponent(TOC_FACADE_IMPL, 'FacadeImpl', new FacadeBuildStrategy())

            generateAlways(TOC_FACADE, TOC_FACADE_IMPL, TOC_REST_CONTROLLER, TOC_REST_CONTROLLER_MO)
        }

        Configurator setRootPackage(String rootPackage) {
            super.setRootPackage(rootPackage)
            setPackage(rootPackage + '.application', TOC_FACADE_IMPL)
            setPackage(rootPackage + '.ports.in.rest.dtos', TOC_DTO, TOC_ENUM, TOC_REQUEST_DTO, TOC_RESPONSE_DTO)
            setPackage(rootPackage + '.ports.in.rest', TOC_FACADE)
            setPackage(rootPackage + '.adapters.in.rest.dtos', TOC_REST_DTO, TOC_REST_REQUEST_DTO)
            setPackage(rootPackage + '.adapters.in.rest.mappers', TOC_REST_MAPPER, TOC_REQUEST_MAPPER)
            setPackage(rootPackage + '.adapters.in.rest.validators', TOC_REST_DTO_VALIDATOR)
            if (params.get(SpringRestParams).controllerPerOperation) {
                setPackage(rootPackage + '.adapters.in.rest.${group}', TOC_REST_REQUEST_VALIDATOR, TOC_REST_CONTROLLER, TOC_RESPONSE_MAPPER)
            } else {
                setPackage(rootPackage + '.adapters.in.rest.validators', TOC_REST_REQUEST_VALIDATOR)
                setPackage(rootPackage + '.adapters.in.rest.mappers', TOC_RESPONSE_MAPPER)
                setPackage(rootPackage + '.adapters.in.rest', TOC_REST_CONTROLLER_MO)
            }
            this
        }

    }

}
