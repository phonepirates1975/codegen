/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs


import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.model.oas.Operation

import static pl.metaprogramming.codegen.java.spring.base.SpringDefs.ANNOT_SERVICE

class FacadeBuildStrategy extends ClassCmBuildStrategy<List<Operation>> {

    @Override
    void makeDeclaration() {
        addAnnotation(ANNOT_SERVICE)
        model.each { operation ->
            def responseClass = ResponseClassResolver.resolve(builder, operation)
            addMapper(nameMapper.toMethodName(operation.code)) {
                it.resultType = responseClass
                it.addParam(getClass(SpringRsTypeOfCode.REQUEST_DTO, operation).asField("request").addAnnotation(AnnotationCm.javaxNonnul()))
                it.implBody = responseClass.isVoid() ? '' : 'return null;'
                it.description = operation.description
            }
        }
    }
}
