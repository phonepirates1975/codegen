/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs

import pl.metaprogramming.codegen.java.AnnotationCm

class EnumBuildStrategy extends pl.metaprogramming.codegen.java.builders.EnumBuildStrategy {
    static final AnnotationCm JSON_VALUE = AnnotationCm.of('com.fasterxml.jackson.annotation.JsonValue')

    @Override
    void makeImplementation() {
        super.makeImplementation()
        classModel.fields[0].annotations.add(JSON_VALUE)
    }
}
