/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.model.oas.HttpResponse

import static pl.metaprogramming.codegen.java.spring.base.SpringDefs.HTTP_STATUS_CODE_EXCEPTION

class RestOutResponseFailMapperBuilder extends RestOutResponseMapperBuilder {

    static final AnnotationCm ANNOT_SLF4J = AnnotationCm.of('lombok.extern.slf4j.Slf4j')

    @Override
    MethodCm makeDeclaration() {
        MethodCm.of('map')
                .resultType(responseClass)
                .addParam('e', HTTP_STATUS_CODE_EXCEPTION)
    }

    @Override
    String makeImplBody() {
        def anyResponseWithContent = model.responses.any { !it.successResponse && it.schema }
        if (anyResponseWithContent) {
            addAnnotation(ANNOT_SLF4J)
            addImport('com.fasterxml.jackson.core.JsonProcessingException')
            codeBuf.tryBlock()
        }
        def nonDefaultFailResponses = model.responses.findAll { !it.default && !it.successResponse }
        nonDefaultFailResponses.each {
            codeBuf.ifBlock("e.getRawStatusCode() == $it.status", handle(it))
                    .endBlock()
        }
        if (model.defaultResponse) {
            codeBuf.addLines(handle(model.defaultResponse))
        } else {
            codeBuf.addLines("throw e;")
        }
        if (anyResponseWithContent) {
            codeBuf
                    .catchBlock("JsonProcessingException e2",
                            "log.error(\"Can't deserialize data\", e2);",
                            "throw e;")
                    .endBlock()
        }
        codeBuf.take()
    }

    private String handle(HttpResponse response) {
        String responseSetter = 'set' + (response.default ? 'Other' : response.status)
        def args = []
        if (response.default) {
            args.add('e.getRawStatusCode()')
        }
        if (response.schema) {
            def responseObject = response.schema.dataType
            def restDtoClass = getClass(SpringRs2tTypeOfCode.REST_DTO, responseObject)
            addImport('com.fasterxml.jackson.databind.ObjectMapper')
            addImport(restDtoClass)
            String restDtoExpField = addVarDeclaration("restValue", restDtoClass, "new ObjectMapper().readValue(e.getResponseBodyAsString(), ${restDtoClass.className}.class)")
            args.add(mapping()
                    .to(getClass(SpringRs2tTypeOfCode.DTO, responseObject))
                    .from(restDtoClass, restDtoExpField)
                    .make())
        }
        "return ${responseObjectCreatePrefix}.${responseSetter}(${args.join(', ')});"
    }
}
