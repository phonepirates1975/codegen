/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.codegen.java.base.BaseMethodCmBuilder
import pl.metaprogramming.codegen.java.spring.SpringCommonsTypeOfCode
import pl.metaprogramming.codegen.java.spring.rs.RestParamsBuilder
import pl.metaprogramming.model.oas.Operation

import static pl.metaprogramming.codegen.java.spring.base.SpringDefs.RESPONSE_ENTITY
import static pl.metaprogramming.codegen.java.spring.rs2t.SpringRs2tTypeOfCode.*
import static pl.metaprogramming.codegen.java.validation.ValidationTypeOfCode.VALIDATION_RESULT

class RestControllerWithHandlerMethodBuilder extends BaseMethodCmBuilder<Operation> {

    RestControllerWithHandlerMethodBuilder(Operation operation) {
        model = operation
    }

    @Override
    MethodCm makeDeclaration() {
        new RestParamsBuilder(builder, REST_DTO).makeControllerMethod(model)
    }

    @Override
    String makeImplBody() {
        def restRequestHandler = getClass(SpringCommonsTypeOfCode.REST_REQUEST_HANDLER_TEMPLATE)
        methodCm.addImplDependencies(restRequestHandler)
        codeBuf
                .addLines("return ${restRequestHandler.className}.handle(")
                .indent(2)
                .addLines("${mapping().to(REST_REQUEST_DTO).from(methodCm.params)},",
                        "${mapping().to(VALIDATION_RESULT).from(REST_REQUEST_DTO)},",
                        "${mapping().to(RESPONSE_ENTITY).from(VALIDATION_RESULT)},",
                        "${mapping().to(REQUEST_DTO).from(REST_REQUEST_DTO)},",
                        "${mapping().to(RESPONSE_DTO).from(REQUEST_DTO)},",
                        "${mapping().to(RESPONSE_ENTITY).from(RESPONSE_DTO)});")
                .take()
    }
}
