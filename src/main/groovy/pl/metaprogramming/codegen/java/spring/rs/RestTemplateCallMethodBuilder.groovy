/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs

import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.ClassCm
import pl.metaprogramming.codegen.java.FieldCm
import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.codegen.java.base.BaseMethodCmBuilder
import pl.metaprogramming.codegen.java.libs.Jackson
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.model.oas.Operation
import pl.metaprogramming.model.oas.ParamLocation

import static pl.metaprogramming.codegen.java.ClassCd.stringType
import static pl.metaprogramming.codegen.java.spring.base.SpringDefs.RESPONSE_ENTITY
import static pl.metaprogramming.codegen.java.spring.rs.SpringRsTypeOfCode.*
import static pl.metaprogramming.model.oas.ParamLocation.*

class RestTemplateCallMethodBuilder extends BaseMethodCmBuilder<Operation> {

    static final String VAR_REQUEST = 'request'

    ClassCm requestClass

    RestTemplateCallMethodBuilder(Operation operation) {
        model = operation
    }

    @Override
    MethodCm makeDeclaration() {
        requestClass = classLocator(REQUEST_DTO).getDeclared()
        MethodCm.of(nameMapper.toMethodName(model.code))
                .description(makeDescription())
                .resultType(ResponseClassResolver.resolve(builder, model))
                .addParam(VAR_REQUEST, requestClass)
    }

    String makeDescription() {
        String head = "[$model.type] $model.path"
        model.description ? "$head<br/>$Codegen.NEW_LINE$model.description" : head
    }

    @Override
    String makeImplBody() {
        addImport(Spring.httpMethod())

        def restClientProvider = componentRef(getClass(REST_CLIENT_PROVIDER, model.api), 'clientProvider')
        def callPrefix = model.successResponseSchema ? 'return ' : ''
        codeBuf.addLines("${callPrefix}${restClientProvider}.of(${Spring.httpMethod(model.type)}, \"$model.path\")")
        codeBuf.indentInc(2)
        appendParams(PATH)
        appendParams(QUERY)
        appendParams(HEADER)
        appendOnError()
        appendBody()
        codeBuf.addLines(".exchange(${responseBodyClassValue})")
        if (model.successResponseSchema && methodCm.resultType != RESPONSE_ENTITY) {
            codeBuf.addLines('.getBody()')
        }
        codeBuf.add(';').take()
    }

    String getTypeRef(ClassCd classCd) {
        if (classCd.genericParams) {
            def jacksonTypeRef = Jackson.typeReference(classCd)
            addImport(jacksonTypeRef)
            "new ${classNameFormatter.format(jacksonTypeRef)}(){}"
        } else {
            classNameFormatter.classRef(classCd)
        }
    }

    Map<Integer, ClassCd> getErrorClasses() {
        Map<Integer, ClassCd> result = [:]
        def defaultErrorResponse = model.api.defaultErrorResponse
        model.errorResponses.each {
            def errorDataType = it.schema?.dataType
            if (errorDataType != null && errorDataType != defaultErrorResponse) {
                result.put(it.status, getClass(DTO, errorDataType))
            }
        }
        result
    }

    private void appendParams(ParamLocation paramLocation) {
        def addMethod = "${paramLocation.name().toLowerCase()}Param"
        model.getParameters(paramLocation).each {
            def field = requestClass.getField(it)
            def optional = it.isRequired || field.type.isList() ? '' : 'Optional'
            def paramExp = [HEADER, QUERY].contains(paramLocation)
                    ? toStringExp(field)
                    : getter(VAR_REQUEST, field.name)
            def paramName = paramLocation == HEADER ? Spring.httpHeaders(it.name) : ValueCm.escaped(it.name)
            if (!paramName.escaped) addImport(Spring.httpHeaders())
            codeBuf.addLines(".${addMethod}${optional}($paramName, $paramExp)")
        }
    }

    private String toStringExp(FieldCm field) {
        def toType = field.type.isList() ? stringType().asList() : stringType()
        mapping().to(toType).from(field.alias("${VAR_REQUEST}.${field.name}"))
    }

    private void appendOnError() {
        codeBuf.addLines(errorClasses.collect {
            addImport(it.value)
            def typeRef = getTypeRef(it.value)
            it.key ? ".onError($it.key, $typeRef)" : ".onError($typeRef)"
        })
    }

    private void appendBody() {
        if (!model.consumeJson && model.consumes) {
            codeBuf.addLines(".headerParam(\"content-type\", \"${model.consumes.first()}\")")
        }
        if (model.multipart) {
            codeBuf.addLines(model.getParameters(FORMDATA).collect {
                ".formDataParam(\"$it.name\", ${getter(VAR_REQUEST, it.name)})"
            })
            codeBuf.addLines(model.multipartFormDataRequestBody?.fields?.collect {
                ".formDataParam(\"$it.code\", ${getter(VAR_REQUEST, it.code)})"
            })
        } else if (model.requestBodySchema) {
            codeBuf.addLines(".body(${getter(VAR_REQUEST, model.requestBodySchema.code)})")
        }
    }

    private String getResponseBodyClassValue() {
        model.successResponseSchema
        def classCd = methodCm.resultType == RESPONSE_ENTITY
                ? methodCm.resultType.genericParams[0]
                : methodCm.resultType
        if (classCd.genericParams) {
            def responseClassType = classNameFormatter.format(classCd)
            addImport('org.springframework.core.ParameterizedTypeReference')
            "new ParameterizedTypeReference<$responseClassType>() {}"
        } else {
            classNameFormatter.classRef(classCd)
        }
    }

}
