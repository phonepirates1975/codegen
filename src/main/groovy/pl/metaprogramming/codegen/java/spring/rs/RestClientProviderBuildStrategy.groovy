package pl.metaprogramming.codegen.java.spring.rs

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.codegen.java.base.BaseMethodCmBuilder
import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.codegen.java.libs.Jackson
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.codegen.java.spring.SpringCommonsTypeOfCode
import pl.metaprogramming.codegen.java.spring.base.SpringDefs
import pl.metaprogramming.model.oas.RestApi

import static pl.metaprogramming.codegen.java.ClassCd.stringType

class RestClientProviderBuildStrategy extends ClassCmBuildStrategy<RestApi> {

    @Override
    void makeImplementation() {
        def endpointProperty = "${nameMapper.toConstantName(model.name)}_BASE_URL"

        addField("baseUrl", stringType()) {
            it.addAnnotation(AnnotationCm.of(SpringDefs.ANNOT_VALUE, [value: ValueCm.escaped("\${$endpointProperty:http://localhost:8080}")]))
        }

        addMethod(new RestClientFactoryMethod())
    }

    static class RestClientFactoryMethod extends BaseMethodCmBuilder<RestApi> {
        @Override
        MethodCm makeDeclaration() {
            MethodCm.of('of') {
                it.resultType = getClass(SpringCommonsTypeOfCode.REST_CLIENT)
                it.addParam('method', Spring.httpMethod())
                it.addParam('path', stringType())
            }
        }

        @Override
        String makeImplBody() {
            def defaultErrorResponse = model.getDefaultErrorResponse()
            def restTemplate = componentRef(SpringDefs.REST_TEMPLATE)
            def objectMapper = componentRef(Jackson.objectMapper())
            def impl = "return new ${methodCm.resultType.className}(method, baseUrl + path, $restTemplate, $objectMapper)"
            if (defaultErrorResponse) {
                def defaultErrorResponseClass = getClass(SpringRsTypeOfCode.DTO, defaultErrorResponse)
                methodCm.implDependencies.add(defaultErrorResponseClass)
                "${impl}.onError(${defaultErrorResponseClass.className}.class);"
            } else {
                "$impl;"
            }
        }
    }
}
