/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

enum SpringRs2tTypeOfCode {

    REST_CLIENT,
    REST_CLIENT_IMPL,

    REST_CONTROLLER_MO,                 // all operation from group in one controller class
    REST_CONTROLLER,                    // controller class for one operation

    FACADE,
    FACADE_IMPL,
    ENUM,
    DTO,
    REQUEST_DTO,
    RESPONSE_DTO,

    REST_MAPPER,                        // REST_DTO <-> REST_REQUEST_DTO
    REQUEST_MAPPER,                     // REQUEST_REST_DTO -> REQUEST_DTO, REQUEST_DTO -> HttpEntity
    RESPONSE_MAPPER,                    // RESPONSE_DTO -> ResponseEntity, ResponseEntity|HttpStatusCodeException -> RESPONSE_DTO

    REST_REQUEST_DTO,
    REST_DTO,
    REST_REQUEST_VALIDATOR,
    REST_DTO_VALIDATOR,

}
