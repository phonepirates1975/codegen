/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.FieldCm
import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.codegen.java.base.BaseMethodCmBuilder
import pl.metaprogramming.model.oas.Operation

import static pl.metaprogramming.codegen.java.spring.base.SpringDefs.HTTP_STATUS_CODE_EXCEPTION
import static pl.metaprogramming.codegen.java.spring.base.SpringDefs.REST_TEMPLATE

class RestOutInvokeMethodBuilder extends BaseMethodCmBuilder<Operation> {

    private static final String REQUEST_PARAM = 'request'
    private static final FieldCm REST_TEMPLATE_FIELD = REST_TEMPLATE.asField('restTemplate')

    @Lazy
    private RestClientBuildHelper helper = new RestClientBuildHelper(builder, model)

    RestOutInvokeMethodBuilder(Operation operation) {
        model = operation
    }

    @Override
    MethodCm makeDeclaration() {
        MethodCm.of(nameMapper.toMethodName(model.code))
                .resultType(getClass(SpringRs2tTypeOfCode.RESPONSE_DTO))
                .addParam(getClass(SpringRs2tTypeOfCode.REQUEST_DTO)
                        .asField(REQUEST_PARAM)
                        .addAnnotation(AnnotationCm.javaxNonnul()))
                .description(model.description)
    }

    @Override
    String makeImplBody() {
        setup()
        codeBuf
                .reset()
                .tryBlock("return ${getSuccessResponseMapper()};")
                .catchBlock("HttpStatusCodeException e",
                        "return $failResponseMapper;"
                )
                .endBlock()
                .take()
    }

    private void setup() {
        if (fields { it == REST_TEMPLATE_FIELD }.empty) {
            addField(REST_TEMPLATE_FIELD)
        }
        addImport('org.springframework.web.client.HttpStatusCodeException')
    }

    private FieldCm getRestCall() {
        def responseClass = helper.responseClass
        addImport(responseClass)
        def exchangeArgs = [
                mapping().to(helper.requestEntity).from(methodCm.params).make(),
                getResponseClassType(responseClass)]
        helper.responseEntity.asExpression("restTemplate.exchange(${exchangeArgs.join(', ')})")
    }

    private String getResponseClassType(ClassCd responseClass) {
        if (responseClass.genericParams) {
            def responseClassType = classNameFormatter.format(responseClass)
            addImport('org.springframework.core.ParameterizedTypeReference')
            "new ParameterizedTypeReference<$responseClassType>() {}"
        } else {
            classNameFormatter.classRef(responseClass)
        }
    }

    private String getSuccessResponseMapper() {
        mapping().to(SpringRs2tTypeOfCode.RESPONSE_DTO).from(restCall).make()
    }

    private String getFailResponseMapper() {
        mapping().to(SpringRs2tTypeOfCode.RESPONSE_DTO).from(HTTP_STATUS_CODE_EXCEPTION, 'e').make()
    }
}
