/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.codegen.java.spring.SpringRestParams
import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassKind

import static pl.metaprogramming.codegen.java.ClassCd.*

class RestResponseInterfaceBuildStrategy extends ClassCmBuildStrategy {

    @Override
    void makeImplementation() {
        setKind(ClassKind.INTERFACE)
        addGenericParams(genericParamR())
        addAnnotation(AnnotationCm.javaxParametersAreNonnullByDefault())
        addImport('java.util.Objects')
        addMethod('getBody') { it.resultType = objectType() }
        addMethod('getStatus') { it.resultType = integerType() }
        addMethod('self') { it.resultType = genericParamR() }
        addMethod('getDeclaredStatuses') { it.resultType = integerType().asCollection() }
        addMethod('getHeaders') { it.resultType = stringType().asMapBy(stringType()) }
        addMethod('setHeader') {
            it.resultType(genericParamR())
            it.addParam('name', stringType())
            it.addParam('value', stringType())
            it.implBody(codeBuf.addLines('getHeaders().put(name, value);', 'return self();').take())
        }
        addMethod('isStatus') {
            it.resultType = 'boolean'
            it.addParam('status', integerType())
            it.implBody('return Objects.equals(status, getStatus());')
        }

        if (!getParams(SpringRestParams).staticFactoryMethodForRestResponse) {
            addMethod('set') {
                it.resultType(genericParamR())
                it.addParam('status', integerType())
                it.addParam('body', objectType())
            }
        }
    }
}
