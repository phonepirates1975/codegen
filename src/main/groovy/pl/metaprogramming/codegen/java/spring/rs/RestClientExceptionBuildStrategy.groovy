package pl.metaprogramming.codegen.java.spring.rs

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.codegen.java.spring.base.SpringDefs

class RestClientExceptionBuildStrategy extends ClassCmBuildStrategy {

    @Override
    void makeImplementation() {
        setSuperClass(ClassCd.of('java.lang.RuntimeException'))
        addAnnotation(AnnotationCm.lombokRequiredArgsConstructor())
        addAnnotation(AnnotationCm.lombokGetter())
        addField('httpStatusCode', ClassCd.integerType()) {
            it.setFinal()
            it.addAnnotation(AnnotationCm.javaxNonnul())
        }
        addField('headers', SpringDefs.HTTP_HEADERS) {
            it.setFinal()
            it.addAnnotation(AnnotationCm.javaxNonnul())
        }
        addField('response', ClassCd.objectType()) {
            it.setFinal()
            it.setTransient()
            it.addAnnotation(AnnotationCm.javaxNonnul())
        }

        addMethod('getResponse') {
            it.resultType = ClassCd.genericParamT()
            it.addParam('clazz', ClassCd.claasType(ClassCd.genericParamT()))
            it.implBody = 'return clazz.cast(response);'
        }
    }
}
