package pl.metaprogramming.codegen.java.spring.rs

import pl.metaprogramming.codegen.java.*
import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.codegen.java.spring.base.SpringDefs
import pl.metaprogramming.codegen.java.libs.Jackson
import pl.metaprogramming.codegen.java.libs.Spring

import static pl.metaprogramming.codegen.java.spring.SpringCommonsTypeOfCode.REST_CLIENT_EXCEPTION
import static pl.metaprogramming.codegen.java.ClassCd.*

class RestClientBuildStrategy extends ClassCmBuildStrategy {

    @Override
    void makeImplementation() {
        addAnnotation(AnnotationCm.lombokRequiredArgsConstructor())

        addFields()
        addParamsMethods()
        addOnErrorMethods()
        addExchangeMethods()
    }

    private void addFields() {
        addImport('java.util.HashMap')
        addImport(Spring.multiValueMap(), Spring.linkedMultiValueMap())
        addField('method', Spring.httpMethod()) { it.setFinal() }
        addField('path', stringType()) { it.setFinal() }
        addField('restTemplate', SpringDefs.REST_TEMPLATE) { it.setFinal() }
        addField('objectMapper', Jackson.objectMapper()) { it.setFinal() }

        addField('defaultErrorType', objectType())
        addMapField('errorClasses', integerType(), objectType())
        addField('body', objectType())

        addMapField('pathParams', stringType(), objectType())
        addMultiValueMapField('headerParams', stringType())
        addMultiValueMapField('formDataParams', objectType())
        addMultiValueMapField('queryParams', stringType())
    }

    private void addOnErrorMethods() {
        def jacksonTypeReference = Jackson.typeReference(genericParamT())

        addFluentApiMethod('onError') {
            it.addParam('defaultErrorType', jacksonTypeReference)
            it.implBody = 'this.defaultErrorType = defaultErrorType;'
        }
        addFluentApiMethod('onError') {
            it.addParam('defaultErrorClass', claasType(genericParamT()))
            it.implBody = 'this.defaultErrorType = defaultErrorClass;'
        }
        addFluentApiMethod('onError') {
            it.addParam('httpStatus', integerType())
            it.addParam('errorType', jacksonTypeReference)
            it.implBody = 'errorClasses.put(httpStatus, errorType);'
        }
        addFluentApiMethod('onError') {
            it.addParam('httpStatus', integerType())
            it.addParam('errorClass', claasType(genericParamT()))
            it.implBody = 'errorClasses.put(httpStatus, errorClass);'
        }
    }

    private void addExchangeMethods() {
        def genericParam = genericParamT()
        def restExceptionClass = getClass(REST_CLIENT_EXCEPTION)
        addMethod('exchange') {
            it.resultType = SpringDefs.RESPONSE_ENTITY.withGeneric(genericParam)
            it.addParam('responseType', of('org.springframework.core.ParameterizedTypeReference').withGeneric(genericParam))
            it.implBody = "return execute(() -> restTemplate.exchange(makeRequest(), responseType));"
        }
        addMethod('exchange') {
            it.resultType = SpringDefs.RESPONSE_ENTITY.withGeneric(genericParam)
            it.addParam('responseType', claasType(genericParam))
            it.implBody = "return execute(() -> restTemplate.exchange(makeRequest(), responseType));"
        }

        //makeRequestEntity
        addMethod('makeRequest') {
            it.privateModifier()
            it.resultType = SpringDefs.REQUEST_ENTITY.withGeneric(genericParamUnknown())
            it.implBody = makeRequestEntityImplBody()
        }

        //execute
        addImport('org.springframework.web.client.RestClientResponseException',
                'org.springframework.http.HttpHeaders',
                'org.springframework.http.MediaType',
        )
        addMethod('execute') {
            it.privateModifier()
            it.resultType = genericParam
            it.addParam('executor', of('java.util.function.Supplier').withGeneric(genericParam))
            it.implBody = codeBuf
                    .tryBlock('return executor.get();')
                    .catchBlock('RestClientResponseException e', 'throw map(e);')
                    .endBlock()
                    .take()
        }
        addMethod('map') {
            it.privateModifier()
            it.resultType = of('java.lang.RuntimeException')
            it.addParam('e', of('org.springframework.web.client.RestClientResponseException'))
            it.implBody = codeBuf
                    .tryBlock('HttpHeaders headers = e.getResponseHeaders();',
                            'if (headers == null || !MediaType.APPLICATION_JSON.equals(headers.getContentType())) return e;',
                            'Object dto = readValue(e.getResponseBodyAsString(), getErrorBodyType(e.getRawStatusCode()));',
                            "return new ${restExceptionClass.className}(e.getRawStatusCode(), headers, dto);")
                    .catchBlock('Exception ex', 'return e;')
                    .endBlock()
                    .take()
        }
        addMethod('getErrorBodyType') {
            it.privateModifier()
            it.resultType = objectType()
            it.addParam('httpStatus', integerPrimitiveType())
            it.implBody = 'return errorClasses.containsKey(httpStatus) ? errorClasses.get(httpStatus) : defaultErrorType;'
        }
        addMethod('readValue') {
            it.privateModifier()
            it.resultType = objectType()
            it.addThrow('com.fasterxml.jackson.core.JacksonException')
            it.addParam('body', stringType())
            it.addParam('type', objectType())
            it.implBody = codeBuf
                    .ifBlock('type instanceof Class', 'return objectMapper.readValue(body, (Class<?>) type);').endBlock()
                    .ifBlock('type instanceof TypeReference', 'return objectMapper.readValue(body, (TypeReference<?>) type);').endBlock()
                    .addLines('throw new IllegalArgumentException("Unsupported type reference: " + type);')
                    .take()
        }
    }

    private void addParamsMethods() {
        addImport('java.util.Objects')
        addFluentApiMethod('body') {
            it.addRequiredParam('body', objectType())
            it.implBody = 'this.body = body;'
        }
        addParamMethod('header', stringType())
        addOptionalParamMethod('header', stringType())
        addParamMethod('query', stringType())
        addOptionalParamMethod('query', stringType())
        addListParamMethod('query')
        addParamMethod('path', objectType())
        addParamMethod('formData', objectType())
        addMethod('required') {
            it.privateModifier()
            it.resultType = genericParamT()
            it.addParam('value', it.resultType)
            it.addParam('name', stringType())
            it.implBody = "return Objects.requireNonNull(value, name + \" is required\");"
        }
    }

    private void addOptionalParamMethod(String paramType, ClassCd valueType) {
        addFluentApiMethod("${paramType}ParamOptional") {
            it.addRequiredParam("name", stringType())
            it.addParam("value", valueType)
            it.implBody = "if (value != null) ${paramType}Param(name, value);"
        }
    }

    private void addParamMethod(String paramType, ClassCd valueType) {
        def addMethod = paramType == 'path' ? 'put' : 'add'
        addFluentApiMethod("${paramType}Param") {
            it.addRequiredParam('name', stringType())
            it.addRequiredParam('value', valueType)
            it.implBody = "${paramType}Params.${addMethod}(name, required(value, name));"
        }
    }

    private void addListParamMethod(String paramType) {
        addImport('java.util.Collections')
        addFluentApiMethod("${paramType}Param") {
            it.addRequiredParam('name', stringType())
            it.addParam('value', stringType().asList())
            it.implBody = "${paramType}Params.addAll(name, value != null ? value : Collections.emptyList());"
        }
    }

    private void addMapField(String name, ClassCd keyType, ClassCd valueType) {
        addField(name, valueType.asMapBy(keyType)) {
            it.setFinal()
            it.value = ValueCm.value('new HashMap<>()')
        }
    }

    private void addMultiValueMapField(String name, ClassCd valueType) {
        addField(name, Spring.multiValueMap(stringType(), valueType)) {
            it.setFinal()
            it.value = Spring.linkedMultiValueMap().constructExp()
        }
    }

    private String makeRequestEntityImplBody() {
        addImport('java.net.URI', 'org.springframework.web.util.UriComponentsBuilder')
        codeBuf.addLines('URI uri = UriComponentsBuilder.fromUriString(path)')
                .indentInc(2)
                .addLines('.queryParams(queryParams)',
                        '.build(pathParams);')
                .indentInc(-2)
                .addLines(
                        'Object requestBody = formDataParams.isEmpty() ? this.body : formDataParams;',
                        'return new RequestEntity<>(requestBody, headerParams, method, uri);'
                ).take()
    }

}
