/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.codegen.java.base.BaseMethodCmBuilder
import pl.metaprogramming.codegen.java.spring.base.SpringDefs
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.oas.HttpResponse
import pl.metaprogramming.model.oas.Operation

class RestInResponseMapperBuilder extends BaseMethodCmBuilder<Operation> {

    @Override
    MethodCm makeDeclaration() {
        MethodCm.of('map')
                .resultType(SpringDefs.RESPONSE_ENTITY)
                .addParam(getClass(SpringRs2tTypeOfCode.RESPONSE_DTO).asField('response').addAnnotation(AnnotationCm.javaxNonnul()))
    }

    @Override
    String makeImplBody() {
        codeBuf.addLines(
                'ResponseEntity.BodyBuilder responseBuilder = ResponseEntity.status(response.getStatus());',
                'response.getHeaders().forEach(responseBuilder::header);'
        )
        if (model.responses.size() == 1) {
            codeBuf.addLines("return ${prepareResponseMap(model.responses[0])};")
        } else {
            def responses = model.responses.toList().sort { -it.status }
            responses.take(responses.size() - 1).each {
                codeBuf
                        .ifBlock("response.is${it.status}()",
                                "return ${prepareResponseMap(it)};")
                        .endBlock()
            }
            codeBuf.addLines("return ${prepareResponseMap(responses.last())};")
        }
        codeBuf.take()
    }

    private String prepareResponseMap(HttpResponse response) {
        if (response.schema == null) {
            'responseBuilder.build()'
        } else {
            String responseGetter = "response.get${response.status ?: 'Other'}()"
            "responseBuilder.body(${makeTransformation(response.schema, responseGetter)})"
        }
    }

    private String makeTransformation(DataSchema schema, String fieldName) {
        if (schema.dataType.isBinary()) {
            return fieldName
        }
        mapping().withModel(schema.dataType)
                .to(SpringRs2tTypeOfCode.REST_DTO)
                .from(SpringRs2tTypeOfCode.DTO, fieldName)
                .make()
    }
}
