/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

enum SpringCommonsTypeOfCode {
    ENDPOINT_PROVIDER,

    ENUM_VALUE_INTERFACE,
    VALUE_HOLDER_TEMPLATE,
    REST_RESPONSE_ABSTRACT,
    REST_RESPONSE_INTERFACE,
    REST_RESPONSE_STATUS_INTERFACE,
    REST_RESPONSE_STATUS_NO_CONTENT_INTERFACE,
    REST_REQUEST_HANDLER_TEMPLATE,
    MAP_RAW_VALUE_SERIALIZER,
    SERIALIZATION_UTILS,
    REST_CLIENT,
    REST_CLIENT_EXCEPTION,
}
