/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs

enum SpringRsTypeOfCode {
    ENUM,
    DTO,
    REQUEST_DTO,

    // for REST services
    OPERATION_EXECUTOR,
    OPERATION_EXECUTOR_IMPL,
    FACADE,
    FACADE_IMPL,
    REST_CONTROLLER,

    // for REST clients
    CLIENT,
    CLIENT_IMPL,
    REST_CLIENT_PROVIDER,
}