/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.base

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.base.DataTypeMapper
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.data.ObjectType
import pl.metaprogramming.model.oas.Operation

import java.util.function.Function

import static pl.metaprogramming.codegen.java.ClassCd.objectType
import static pl.metaprogramming.codegen.java.ClassCd.stringType

interface SpringDefs {

    Function<Operation, ObjectType> OPERATION_REQUEST_SCHEMA = { Operation o -> o.requestSchema } as Function<Operation, ObjectType>
    DataTypeMapper REST_DATA_TYPE_MAPPER = new DataTypeMapper({ DataType it ->
        it.isBinary() ? SpringDefs.RESOURCE : !it.isComplex() ? stringType() : null
    } as Function<DataType, ClassCd>)

    AnnotationCm ANNOT_REST_CONTROLLER = AnnotationCm.of('org.springframework.web.bind.annotation.RestController')
    AnnotationCm ANNOT_AUTOWIRED = AnnotationCm.of('org.springframework.beans.factory.annotation.Autowired')
    AnnotationCm ANNOT_BEAN = AnnotationCm.of('org.springframework.context.annotation.Bean')
    AnnotationCm ANNOT_CONFIGURATION = AnnotationCm.of('org.springframework.context.annotation.Configuration')
    AnnotationCm ANNOT_SERVICE = AnnotationCm.of('org.springframework.stereotype.Service')
    AnnotationCm ANNOT_COMPONENT = AnnotationCm.of('org.springframework.stereotype.Component')

    String ANNOT_VALUE = "org.springframework.beans.factory.annotation.Value"

    ClassCd REST_TEMPLATE = ClassCd.of('org.springframework.web.client.RestTemplate')
    ClassCd REQUEST_ENTITY = ClassCd.of('org.springframework.http.RequestEntity')
    ClassCd RESPONSE_ENTITY = ClassCd.of('org.springframework.http.ResponseEntity')
    ClassCd HTTP_HEADERS = ClassCd.of('org.springframework.http.HttpHeaders')
    ClassCd HTTP_STATUS_CODE_EXCEPTION = ClassCd.of('org.springframework.web.client.HttpStatusCodeException')

    ClassCd MULTIPART_FILE = ClassCd.of('org.springframework.web.multipart.MultipartFile')
    ClassCd RESOURCE = ClassCd.of('org.springframework.core.io.Resource')
    ClassCd MULTI_VALUE_MAP = ClassCd.of('org.springframework.util.MultiValueMap', [stringType(), objectType()])

}
