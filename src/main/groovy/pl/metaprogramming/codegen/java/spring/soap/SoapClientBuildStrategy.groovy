/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.soap

import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.model.wsdl.WsdlApi

import static pl.metaprogramming.codegen.java.jaxb.JaxbTypeOfCode.DTO

class SoapClientBuildStrategy extends ClassCmBuildStrategy<WsdlApi> {
    static final ClassCd SUPERCLASS = ClassCd.of('org.springframework.ws.client.core.support.WebServiceGatewaySupport')

    @Override
    void makeDeclaration() {
        setSuperClass(SUPERCLASS)
        model.operations.each {operation ->
            def resultType = getClass(DTO, operation.output.dataType)
            addMethod(operation.name) {
                it.resultType = resultType
                it.addParam('request', getClass(DTO, operation.input.dataType))
                it.implBody = "return ($resultType.className) getWebServiceTemplate().marshalSendAndReceive(request);"
            }
        }
    }
}
