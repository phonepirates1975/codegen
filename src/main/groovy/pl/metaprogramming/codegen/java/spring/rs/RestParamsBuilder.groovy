/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs

import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode
import groovy.transform.builder.Builder
import groovy.transform.builder.SimpleStrategy
import pl.metaprogramming.codegen.java.*
import pl.metaprogramming.codegen.java.base.ClassCmBuilder
import pl.metaprogramming.codegen.java.spring.SpringRestParams
import pl.metaprogramming.codegen.java.spring.base.SpringDefs
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.DataTypeCode
import pl.metaprogramming.model.oas.Operation
import pl.metaprogramming.model.oas.ParamLocation
import pl.metaprogramming.model.oas.Parameter

import static pl.metaprogramming.codegen.java.ClassCd.stringType

@Builder(builderStrategy = SimpleStrategy)
class RestParamsBuilder {

    static final AnnotationCm REQUEST_BODY_ANNOTATIONS = AnnotationCm.of('org.springframework.web.bind.annotation.RequestBody')

    static final Map<ParamLocation, String> PARAM_ANNOTATIONS = [
            (ParamLocation.PATH)  : 'PathVariable',
            (ParamLocation.QUERY) : 'RequestParam',
            (ParamLocation.HEADER): 'RequestHeader',
            (ParamLocation.COOKIE): 'CookieValue',
    ]

    RestParamsBuilder(ClassCmBuilder<?> builder, Object bodyTypeOfCode) {
        this.builder = builder
        this.bodyTypeOfCode = bodyTypeOfCode
    }

    ClassCmBuilder<?> builder
    Operation operation
    boolean addControllerAnnotations
    boolean useJakartaBeanValidation
    boolean useDataAutoconversion
    Object bodyTypeOfCode

    MethodCm makeControllerMethod(Operation operation) {
        this.operation = operation
        addControllerAnnotations = true
        MethodCm.of(builder.nameMapper.toMethodName(operation.code))
                .resultType(SpringDefs.RESPONSE_ENTITY)
                .addParams(make(operation))
                .addAnnotation(makeMethodAnnotation())
                .description(operation.description)
    }

    List<FieldCm> make(Operation operation) {
        this.operation = operation
        List<FieldCm> params = []
        if (payloadField && operation.requestBodySchema != null) {
            params.add(stringType().asField(payloadField) {
                it.addAnnotation(REQUEST_BODY_ANNOTATIONS)
            })
        }

        operation.requestSchema.fields.each { schema ->
            String javaName = builder.nameMapper.toFieldName(schema.code)
            if (schema instanceof Parameter) {
                def param = schema as Parameter
                params.add(getParamType(schema).asField(javaName) {
                    it.model = schema
                    it.annotations = makeParamAnnotations(param, javaName)
                })
            } else {
                params.add(FieldCm.of(builder.nameMapper.toFieldName(schema.code), operation.isOctetStream()
                        ? SpringDefs.RESOURCE
                        : builder.getClass(bodyTypeOfCode, operation.requestBodySchema.dataType)) {
                    it.model = schema
                    it.annotations = makeBodyParamAnnotations()
                })
            }
        }
        params
    }

    @TypeChecked(TypeCheckingMode.SKIP)
    private String getPayloadField() {
        builder.getParams(SpringRestParams).payloadField
    }

    private AnnotationCm makeMethodAnnotation() {
        Map<String, ValueCm> params = [
                value   : ValueCm.escaped(operation.path),
                produces: ValueCm.escapedArray(operation.produces),
        ]
        if (operation.type.hasBody()) {
            params.consumes = ValueCm.escapedArray(operation.consumes)
        }
        String canonicalName = "org.springframework.web.bind.annotation.${operation.type.name().toLowerCase().capitalize()}Mapping"
        AnnotationCm.of(canonicalName, params)
    }

    protected ClassCd getParamType(DataSchema schema) {
        if (schema.isType(DataTypeCode.BINARY)) {
            SpringDefs.MULTIPART_FILE
        } else if (schema.isEnum()) {
            stringType()
        } else {
            builder.getClass(bodyTypeOfCode, schema.dataType)
        }
    }

    protected List<AnnotationCm> makeBodyParamAnnotations() {
        if (!addControllerAnnotations) {
            return []
        }
        def result = [REQUEST_BODY_ANNOTATIONS]
        if (useJakartaBeanValidation) {
            result.add(JakartaBeanValidationSupport.VALID)
        }
        result
    }

    protected List<AnnotationCm> makeParamAnnotations(Parameter param, String javaName) {
        if (!addControllerAnnotations) {
            return []
        }
        Map<String, ValueCm> params = [:]
        if (param.name != javaName) params.put('value', ValueCm.escaped(param.name))
        if (!(useDataAutoconversion && param.isRequired)) {
            params.put('required', ValueCm.value('false'))
        }
        String annotation = getParamAnnotation(param.location)
        def result = [] as List<AnnotationCm>
        result.add(AnnotationCm.of("org.springframework.web.bind.annotation.$annotation" as String, params))
        if (useDataAutoconversion) {
            if (param.isTypeOrItemType(DataTypeCode.DATE_TIME)) {
                result.add(makeDateTimeFormatAnnotation('DATE_TIME'))
            } else if (param.isTypeOrItemType(DataTypeCode.DATE)) {
                result.add(makeDateTimeFormatAnnotation('DATE'))
            }
        }
        result
    }

    private AnnotationCm makeDateTimeFormatAnnotation(String isoFormat) {
        AnnotationCm.of(
                'org.springframework.format.annotation.DateTimeFormat',
                ['iso': ValueCm.value('DateTimeFormat.ISO.' + isoFormat)]
        )
    }

    protected String getParamAnnotation(ParamLocation paramLocation) {
        if (paramLocation == ParamLocation.FORMDATA) {
            return 'RequestPart'
        }
        String annotation = PARAM_ANNOTATIONS.get(paramLocation)
        if (!annotation) {
            throw new RuntimeException("[$operation.code] Can't handle parameter location $paramLocation")
        }
        annotation
    }

}
