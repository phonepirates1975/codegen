/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.builders.BaseDtoBuildStrategy
import pl.metaprogramming.model.data.DataSchema

class DtoBuildStrategy extends BaseDtoBuildStrategy {

    @Override
    void makeImplementation() {
        super.makeImplementation()
        setComment(model.description)
    }

    void addField(DataSchema schema) {
        def typeOfCode = schema.enumOrItemEnum ? SpringRs2tTypeOfCode.ENUM : SpringRs2tTypeOfCode.DTO
        addField(getFieldName(schema), getClass(typeOfCode, schema.dataType)) {
            it.model = schema
            it.description = schema.description
            it.addAnnotation(schema.isRequired ? AnnotationCm.javaxNonnul() : AnnotationCm.javaxNullable())
        }
    }
}
