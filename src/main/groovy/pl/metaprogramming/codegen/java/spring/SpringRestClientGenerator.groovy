/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.java.JavaModuleConfigurator
import pl.metaprogramming.codegen.java.JavaModuleGenerator
import pl.metaprogramming.codegen.java.builders.InterfaceBuildStrategy
import pl.metaprogramming.codegen.java.spring.base.SpringDefs
import pl.metaprogramming.codegen.java.spring.rs.*
import pl.metaprogramming.codegen.java.validation.ValidationParams
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.oas.RestApi

import java.util.function.Consumer

class SpringRestClientGenerator extends JavaModuleGenerator<RestApi> {

    public static final TOC_CLIENT = SpringRsTypeOfCode.CLIENT
    public static final TOC_CLIENT_IMPL = SpringRsTypeOfCode.CLIENT_IMPL
    public static final TOC_DTO = SpringRsTypeOfCode.DTO
    public static final TOC_REQUEST_DTO = SpringRsTypeOfCode.REQUEST_DTO
    public static final TOC_ENUM = SpringRsTypeOfCode.ENUM
    public static final TOC_REST_CLIENT_PROVIDER = SpringRsTypeOfCode.REST_CLIENT_PROVIDER

    static SpringRestClientGenerator of(
            RestApi model,
            CodegenParams params,
            Consumer<Configurator> config) {
        def configurator = new Configurator(params)
        config.accept(configurator)
        new SpringRestClientGenerator(model, configurator)
    }

    static SpringRestClientGenerator of(
            RestApi model,
            Consumer<Configurator> config) {
        of(model, new CodegenParams(), config)
    }

    SpringRestClientGenerator(RestApi model, JavaModuleConfigurator config) {
        super(model, config.classBuilderConfigs, config.params)
    }

    @Override
    void generate() {
        addClass(TOC_REST_CLIENT_PROVIDER, model, model.name)
        addSchemaCodes(model)
        addOperationCodes(model)
        makeCodeModels()
    }

    void addSchemaCodes(RestApi model) {
        model.schemas.each { schema ->
            if (schema.isEnum()) {
                addClass(TOC_ENUM, schema)
            }
            if (schema.isObject()) {
                addClass(TOC_DTO, schema)
            }
        }
    }

    void addOperationCodes(RestApi model) {
        model.groupedOperations.each { group, operations ->
            operations.each { operation ->
                String dataName = operation.code.capitalize()
                addClass(TOC_REQUEST_DTO, operation, dataName)
            }
            [TOC_CLIENT_IMPL, TOC_CLIENT].each { classType ->
                addClass(classType, operations, group)
            }
        }
    }

    static class Configurator extends JavaModuleConfigurator<Configurator> {

        Configurator(CodegenParams params) {
            super(params.withIfNotSet(new ValidationParams()).withIfNotSet(new SpringRestParams()))
            dataTypeMapper.setMapping(DataType.DATE_TIME, 'java.time.ZonedDateTime')
            dataTypeMapper.setMapping(DataType.BINARY, SpringDefs.RESOURCE)
            generateIfUsed = true
            init()
        }

        private void init() {
            typeOfCode(TOC_ENUM)
                    .addStrategy(new EnumBuildStrategy())
                    .setRegisterClassType(TOC_DTO)
            typeOfCode(TOC_DTO)
                    .setNameSuffix('Dto')
                    .addStrategy(new DtoBuildStrategy(withDescriptiveFields: false), LOMBOK_DATA_STRATEGY)
            typeOfCode(TOC_REQUEST_DTO)
                    .setNameSuffix('Request')
                    .addStrategy(LOMBOK_DATA_STRATEGY, new DtoBuildStrategy(
                            modelMapper: SpringDefs.OPERATION_REQUEST_SCHEMA,
                            withDescriptiveFields: false,
                            withJsonAnnotations: false
                    ))
            typeOfCode(TOC_CLIENT_IMPL)
                    .setNameSuffix('ClientImpl')
                    .addStrategy(new ClientBuildStrategy(), componentStrategy, diStrategy)
            typeOfCode(TOC_CLIENT)
                    .setNameSuffix('Client')
                    .addStrategy(new InterfaceBuildStrategy(TOC_CLIENT_IMPL))

            typeOfCode(TOC_REST_CLIENT_PROVIDER)
                    .setNameSuffix('RestClientProvider')
                    .addStrategy(new RestClientProviderBuildStrategy(), componentStrategy, diStrategy)

            generateAlways(TOC_CLIENT_IMPL, TOC_CLIENT)
        }


        @Override
        Configurator setRootPackage(String rootPackage) {
            setPackages(rootPackage, 'rest')
        }


        Configurator setPackages(String rootPackage, String adapterName) {
            super.setRootPackage(rootPackage)
            setPackage("${rootPackage}.ports.out.${adapterName}.dtos", TOC_DTO, TOC_ENUM, TOC_REQUEST_DTO)
            setPackage("${rootPackage}.adapters.out.${adapterName}", TOC_CLIENT, TOC_CLIENT_IMPL, TOC_REST_CLIENT_PROVIDER)
            this
        }

    }

}
