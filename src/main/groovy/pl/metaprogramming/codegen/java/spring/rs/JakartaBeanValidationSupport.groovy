/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.model.data.DataSchema

import static pl.metaprogramming.model.data.DataTypeCode.*

class JakartaBeanValidationSupport {

    static final AnnotationCm VALID = AnnotationCm.of('javax.validation.Valid')
    static final AnnotationCm NOT_NULL = AnnotationCm.of('javax.validation.constraints.NotNull')

    List<AnnotationCm> addAnnotations(List<AnnotationCm> result, DataSchema schema) {
        if (schema.isRequired) {
            result.add(NOT_NULL)
        }
        if (schema.minimum) {
            result.add(minMax('Min', schema.minimum, schema))
        }
        if (schema.maximum) {
            result.add(minMax('Max', schema.maximum, schema))
        }
        if (schema.object) {
            result.add(VALID)
        }
        result
    }

    private static AnnotationCm minMax(String restriction, String value, DataSchema schema) {
        boolean isInt = schema.isType(INT16, INT32, INT64)
        AnnotationCm.of(
                "javax.validation.constraints.${isInt ? '' : 'Decimal'}$restriction",
                [value: isInt ? ValueCm.value(value) : ValueCm.escaped(value)])
    }
}
