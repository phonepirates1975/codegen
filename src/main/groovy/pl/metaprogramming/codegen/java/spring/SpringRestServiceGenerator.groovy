/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode
import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.java.base.ClassNameBuilder
import pl.metaprogramming.codegen.java.JavaModuleConfigurator
import pl.metaprogramming.codegen.java.JavaModuleGenerator
import pl.metaprogramming.codegen.java.spring.base.SpringDefs
import pl.metaprogramming.codegen.java.spring.rs.AdditionalFieldsBuildStrategy
import pl.metaprogramming.codegen.java.spring.rs.ControllerBuildStrategy
import pl.metaprogramming.codegen.java.spring.rs.DtoBuildStrategy
import pl.metaprogramming.codegen.java.spring.rs.EnumBuildStrategy
import pl.metaprogramming.codegen.java.spring.rs.FacadeBuildStrategy
import pl.metaprogramming.codegen.java.spring.rs.OperationExecutorBuildStrategy
import pl.metaprogramming.codegen.java.builders.InterfaceBuildStrategy
import pl.metaprogramming.codegen.java.spring.rs.SpringRsTypeOfCode
import pl.metaprogramming.codegen.java.validation.ValidationParams
import pl.metaprogramming.codegen.java.validation.RestDtoValidatorBuildStrategy
import pl.metaprogramming.codegen.java.validation.RestRequestValidatorBuildStrategy
import pl.metaprogramming.codegen.java.validation.ValidationTypeOfCode
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.oas.Operation
import pl.metaprogramming.model.oas.OperationType
import pl.metaprogramming.model.oas.RestApi

import java.util.function.Consumer

import static pl.metaprogramming.codegen.java.validation.ValidationTypeOfCode.REST_EXCEPTION_HANDLER

class SpringRestServiceGenerator extends JavaModuleGenerator<RestApi> {

    /**
     * Interface for operation executor (*Query/*Command).
     * Generated when SpringRestParams.delegateToFacade == false.
     */
    public static final TOC_OPERATION_EXECUTOR = SpringRsTypeOfCode.OPERATION_EXECUTOR

    /**
     * Operation executor (*Query/*Command).
     * Generated when SpringRestParams.delegateToFacade == false.
     */
    public static final TOC_OPERATION_EXECUTOR_IMPL = SpringRsTypeOfCode.OPERATION_EXECUTOR_IMPL

    /**
     * Facade for REST operations (interface).
     * Generated when SpringRestParams.delegateToFacade == true.
     */
    public static final TOC_FACADE = SpringRsTypeOfCode.FACADE

    /**
     * Facade for REST operations (implementation).
     * Generated when SpringRestParams.delegateToFacade == true.
     */
    public static final TOC_FACADE_IMPL = SpringRsTypeOfCode.FACADE_IMPL
    public static final TOC_REST_CONTROLLER = SpringRsTypeOfCode.REST_CONTROLLER
    public static final TOC_REQUEST_DTO = SpringRsTypeOfCode.REQUEST_DTO
    public static final TOC_REQUEST_VALIDATOR = ValidationTypeOfCode.REQUEST_VALIDATOR
    public static final TOC_DTO = SpringRsTypeOfCode.DTO
    public static final TOC_DTO_VALIDATOR = ValidationTypeOfCode.DTO_VALIDATOR
    public static final TOC_ENUM = SpringRsTypeOfCode.ENUM

    static SpringRestServiceGenerator of(
            RestApi model,
            CodegenParams params,
            Consumer<Configurator> config) {
        def configurator = new Configurator(params)
        config.accept(configurator)
        new SpringRestServiceGenerator(model, configurator)
    }

    static SpringRestServiceGenerator of(
            RestApi model,
            Consumer<Configurator> config) {
        of(model, new CodegenParams(), config)
    }

    SpringRestServiceGenerator(RestApi model, JavaModuleConfigurator config) {
        super(model, config.classBuilderConfigs, config.params)
        if (generateValidator()) {
            operationClassTypes.add(TOC_REQUEST_VALIDATOR)
        }
        if (!delegateToFacade()) {
            operationClassTypes.addAll(TOC_OPERATION_EXECUTOR, TOC_OPERATION_EXECUTOR_IMPL)
        }
    }

    @Override
    void generate() {
        addSchemaCodes(model)
        addOperationCodes(model)
        if (generateValidator()) {
            // TODO should be set as generate always
            classIndex.useClass(REST_EXCEPTION_HANDLER)
        }
        makeCodeModels()
    }

    void addSchemaCodes(RestApi model) {
        model.schemas.each { schema ->
            if (schema.isEnum()) {
                addClass(TOC_ENUM, schema)
            }
            if (schema.isObject()) {
                addClass(TOC_DTO, schema)
                if (generateValidator()) {
                    addClass(TOC_DTO_VALIDATOR, schema)
                }
            }
        }
    }

    void addOperationCodes(RestApi model) {
        model.groupedOperations.each { group, operations ->
            operations.each { operation ->
                String dataName = operation.code.capitalize()
                operationClassTypes.each { classType ->
                    addClass(classType, operation, dataName)
                }
            }
            if (delegateToFacade()) {
                [TOC_FACADE_IMPL, TOC_FACADE].each { classType ->
                    addClass(classType, operations, group)
                }
            }
        }
    }

    @TypeChecked(TypeCheckingMode.SKIP)
    private boolean generateValidator() {
        !getParams(ValidationParams).useJakartaBeanValidation
    }

    @TypeChecked(TypeCheckingMode.SKIP)
    private boolean delegateToFacade() {
        getParams(SpringRestParams).delegateToFacade
    }

    private final List<Object> operationClassTypes = [
            TOC_REQUEST_DTO,
            TOC_REST_CONTROLLER]


    static class Configurator extends JavaModuleConfigurator<Configurator> {

        Configurator(CodegenParams params) {
            super(params.withIfNotSet(new ValidationParams()).withIfNotSet(new SpringRestParams()))
            dataTypeMapper.setMapping(DataType.DATE_TIME, 'java.time.ZonedDateTime')
            generateIfUsed = true
            init()
        }

        private void init() {
            params.get(ValidationParams).throwExceptionIfValidationFailed = true
            def additionalBeansForRequestBuildStrategy = new AdditionalFieldsBuildStrategy(params.get(SpringRestParams).injectBeansIntoRequest)
            typeOfCode(TOC_REQUEST_VALIDATOR)
                    .setNameSuffix('Validator')
                    .addStrategy(componentStrategy, diStrategy, new RestRequestValidatorBuildStrategy(
                            dtoTypeOfCode: TOC_REQUEST_DTO, validatorTypeOfCode: TOC_DTO_VALIDATOR, enumTypeOfCode: TOC_DTO
                    ))
            typeOfCode(TOC_DTO_VALIDATOR)
                    .setNameSuffix('Validator')
                    .addStrategy(componentStrategy, diStrategy, new RestDtoValidatorBuildStrategy(
                            dtoTypeOfCode: TOC_DTO, validatorTypeOfCode: TOC_DTO_VALIDATOR, enumTypeOfCode: TOC_DTO
                    ))
            typeOfCode(TOC_ENUM)
                    .addStrategy(new EnumBuildStrategy())
                    .setRegisterClassType(TOC_DTO)
            typeOfCode(TOC_DTO)
                    .setNameSuffix('Dto')
                    .addStrategy(new DtoBuildStrategy(), LOMBOK_DATA_STRATEGY)
            typeOfCode(TOC_REQUEST_DTO)
                    .setNameSuffix('Request')
                    .addStrategy(additionalBeansForRequestBuildStrategy, new DtoBuildStrategy(modelMapper: SpringDefs.OPERATION_REQUEST_SCHEMA), LOMBOK_DATA_STRATEGY)
            typeOfCode(TOC_REST_CONTROLLER)
                    .setNameSuffix('Controller')
                    .addStrategy(new ControllerBuildStrategy(), additionalBeansForRequestBuildStrategy, diStrategy)
            typeOfCode(TOC_OPERATION_EXECUTOR_IMPL)
                    .setClassNameBuilder(this::operationExecutorClassNameBuilder as ClassNameBuilder<Operation>)
                    .addStrategy(new OperationExecutorBuildStrategy())
            typeOfCode(TOC_OPERATION_EXECUTOR)
                    .setClassNameBuilder(this::operationExecutorInterfaceNameBuilder as ClassNameBuilder<Operation>)
                    .addStrategy(new InterfaceBuildStrategy(TOC_OPERATION_EXECUTOR_IMPL))
            typeOfCode(TOC_FACADE_IMPL)
                    .setNameSuffix('FacadeImpl')
                    .addStrategy(new FacadeBuildStrategy())
            typeOfCode(TOC_FACADE)
                    .setNameSuffix('Facade')
                    .addStrategy(new InterfaceBuildStrategy(TOC_FACADE_IMPL))

            generateAlways(TOC_FACADE, TOC_FACADE_IMPL, TOC_OPERATION_EXECUTOR, TOC_OPERATION_EXECUTOR_IMPL, TOC_REST_CONTROLLER)
        }

        @Override
        Configurator setRootPackage(String rootPackage) {
            setPackages(rootPackage, 'rest')
        }

        Configurator setPackages(String rootPackage, String adapterName) {
            super.setRootPackage(rootPackage)
            setPackage("${rootPackage}.process", TOC_OPERATION_EXECUTOR_IMPL)
            setPackage("${rootPackage}.ports.in.${adapterName}", TOC_FACADE, TOC_OPERATION_EXECUTOR)
            setPackage("${rootPackage}.ports.in.${adapterName}.dtos", TOC_DTO, TOC_ENUM, TOC_REQUEST_DTO)
            setPackage("${rootPackage}.adapters.in.${adapterName}.controllers", TOC_REST_CONTROLLER)
            setPackage("${rootPackage}.adapters.in.${adapterName}.validators", TOC_REQUEST_VALIDATOR, TOC_DTO_VALIDATOR)
            this
        }

        String operationExecutorInterfaceNameBuilder(String modelName, Operation operation) {
            'I' + operationExecutorClassNameBuilder(modelName, operation)
        }

        String operationExecutorClassNameBuilder(String modelName, Operation operation) {
            modelName.capitalize() +
                    (OperationType.GET == operation.type ? 'Query' : 'Command')
        }
    }

}
