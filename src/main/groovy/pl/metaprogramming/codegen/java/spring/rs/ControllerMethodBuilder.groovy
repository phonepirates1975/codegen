/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs

import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode
import pl.metaprogramming.codegen.java.ClassCm
import pl.metaprogramming.codegen.java.FieldCm
import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.codegen.java.base.BaseMethodCmBuilder
import pl.metaprogramming.codegen.java.spring.SpringRestParams
import pl.metaprogramming.codegen.java.validation.ValidationParams
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.oas.Operation

import static pl.metaprogramming.codegen.java.spring.base.SpringDefs.RESPONSE_ENTITY
import static pl.metaprogramming.codegen.java.spring.rs.SpringRsTypeOfCode.DTO
import static pl.metaprogramming.codegen.java.spring.rs.SpringRsTypeOfCode.REQUEST_DTO
import static pl.metaprogramming.codegen.java.validation.ValidationTypeOfCode.VALIDATION_RESULT

class ControllerMethodBuilder extends BaseMethodCmBuilder<Operation> {

    @Override
    MethodCm makeDeclaration() {
        def methodCm = new RestParamsBuilder(builder, DTO)
                .setUseJakartaBeanValidation(isJbvEnabled())
                .setUseDataAutoconversion(true)
                .makeControllerMethod(model)
        methodCm.resultType = methodCm.resultType.withGeneric(ResponseClassResolver.successClass(builder, model))
        methodCm
    }

    @Override
    String makeImplBody() {
        def responseClass = ResponseClassResolver.resolve(builder, model)
        def request = makeRequestField(methodCm.params + additionalFields)
        if (!isJbvEnabled()) {
            codeBuf.addLines("${mapping().from(request).to(VALIDATION_RESULT)};")
        }
        def serviceCall = mapping().to(responseClass).from(request).make()
        if (responseClass.isVoid()) {
            methodCm.implBody = codeBuf.addLines(
                    "$serviceCall;",
                    "return ${emptyResponse};"
            ).take()
        } else if (responseClass == RESPONSE_ENTITY) {
            methodCm.implBody = codeBuf.addLines(
                    "return ${serviceCall};"
            ).take()
        } else {
            methodCm.implBody = codeBuf.addLines(
                    "return ${getResponse(serviceCall)};"
            ).take()
        }
    }

    private FieldCm makeRequestField(List<FieldCm> fields) {
        def requestClass = getClass(REQUEST_DTO) as ClassCm
        addImport(requestClass)
        codeBuf.newLine("$requestClass.className request = new $requestClass.className()")
        codeBuf.indent(2)
        fields.each {
            def toField = requestClass.getField(it.model)
            codeBuf.newLine(".set${it.name.capitalize()}(${getParamValue(it, toField)})")
        }
        codeBuf.add(';').indent()
        requestClass.asField('request')
    }

    private String getParamValue(FieldCm param, FieldCm toField) {
        def schema = (param.model instanceof DataSchema ? param.model : null) as DataSchema
        def paramValue = param.type == toField.type ? param.name : mapping().from(param).to(toField.type).make()
        if (schema?.defaultValue) {
            "${ofNullable(paramValue)}.orElse(${mapping().from(schema.defaultValue).to(toField.type)})"
        } else {
            paramValue
        }
    }

    private String getEmptyResponse() {
        def statusMethod = model.successResponse
                ? STATUS_2_METHOD.get(model.successResponse.status)
                : 'noContent'
        def responseBuildExp = statusMethod ? "${statusMethod}()" : "status(${model.successResponse.status})"
        "ResponseEntity.${responseBuildExp}.build()"
    }

    private String getResponse(String serviceCallExp) {
        def statusMethod = STATUS_2_METHOD.get(model.successResponse.status)
        def responseBuildExp = statusMethod ? "${statusMethod}($serviceCallExp)"
                : "status(${model.successResponse.status}).body($serviceCallExp)"
        "ResponseEntity.${responseBuildExp}"
    }

    @TypeChecked(TypeCheckingMode.SKIP)
    private boolean isJbvEnabled() {
        getParams(ValidationParams).useJakartaBeanValidation
    }

    @TypeChecked(TypeCheckingMode.SKIP)
    private List<FieldCm> getAdditionalFields() {
        getParams(SpringRestParams).injectBeansIntoRequest.collect {
            it.withModel(model)
        }
    }

    private static Map<Integer, String> STATUS_2_METHOD = [
            200: 'ok',
            204: 'noContent',
    ]
}
