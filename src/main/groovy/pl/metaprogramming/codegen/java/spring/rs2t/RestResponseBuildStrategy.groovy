/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode
import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.FieldCm
import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.codegen.java.spring.SpringCommonsTypeOfCode
import pl.metaprogramming.codegen.java.spring.SpringRestParams
import pl.metaprogramming.model.oas.HttpResponse
import pl.metaprogramming.model.oas.Operation

import static pl.metaprogramming.codegen.java.ClassCd.integerType
import static pl.metaprogramming.codegen.java.ClassCd.objectType

class RestResponseBuildStrategy extends ClassCmBuildStrategy<Operation> {
    static final String COLLECTIONS = 'java.util.Collections'

    @Override
    void makeImplementation() {
        setSuperClass(getClass(SpringCommonsTypeOfCode.REST_RESPONSE_ABSTRACT).withGeneric(classModel))
        if (isStaticFactoryMethod()) {
            addAnnotation(AnnotationCm.javaxParametersAreNonnullByDefault())
            addConstructor()
        }
        addGetDeclaredStatuses()
        addMethod('self') {
            it.resultType = classModel
            it.implBody = 'return this;'
        }

        model.responses.each {
            def responseType = it.schema != null
                    ? SpringCommonsTypeOfCode.REST_RESPONSE_STATUS_INTERFACE
                    : SpringCommonsTypeOfCode.REST_RESPONSE_STATUS_NO_CONTENT_INTERFACE
            def genericParams = [classModel] as List<ClassCd>
            if (it.schema) {
                genericParams.add(getResponseClass(it))
            }
            addInterfaces(getClass(responseType, it.status).withGeneric(genericParams))
            addFactoryMethod(it)
        }
    }

    private void addConstructor() {
        addMethod(classModel.className) {
            it.privateModifier()
            it.resultType = classModel
            it.addParam('status', integerType())
            it.addParam('body', objectType())
            it.implBody = 'super(status, body);'
        }
    }

    private void addGetDeclaredStatuses() {
        def field = addField('DECLARED_STATUSES', integerType().asCollection()) {
            it.setStatic().setFinal()
            it.assign(getDeclaredResponsesValue())
        }
        addMethod('getDeclaredStatuses') {
            it.resultType = field.type
            it.implBody('return DECLARED_STATUSES;')
        }
    }

    private String getDeclaredResponsesValue() {
        def statuses = model.responses.findAll { !it.default }.collect { it.status }
        if (statuses.empty) {
            addImport(COLLECTIONS)
            return 'Collections.emptyList()'
        }
        if (statuses.size() == 1) {
            addImport(COLLECTIONS)
            return "Collections.singletonList(${statuses[0]})"
        }
        addImport('java.util.Arrays')
        return "Arrays.asList(${statuses.join(', ')})"
    }

    private void addFactoryMethod(HttpResponse response) {
        if (!isStaticFactoryMethod()) {
            return
        }
        addMethod('set' + (response.isDefault() ? 'Other' : response.status)) {
            it.staticModifier()
            it.resultType = classModel
            it.addParams(getFactoryMethodParams(response))
            it.implBody = getFactoryMethodImpl(response)
        }
    }

    private List<FieldCm> getFactoryMethodParams(HttpResponse response) {
        List<FieldCm> params = []
        if (response.isDefault()) {
            params.add(integerType().asField('status'))
        }
        if (response.schema != null) {
            params.add(getResponseClass(response).asField('body'))
        }
        params
    }

    private String getFactoryMethodImpl(HttpResponse response) {
        String status = response.isDefault() ? 'status' : '' + response.status
        String body = response.schema != null ? 'body' : 'null'
        if (response.isDefault()) {
            codeBuf.ifBlock("DECLARED_STATUSES.contains(status)",
                    'throw new IllegalArgumentException(String.format("Status %s is declared. Use dedicated factory method for it.", status));'
            ).endBlock()

        }
        codeBuf.addLines("return new ${classModel.className}($status, $body);")
                .take()
    }

    protected ClassCd getResponseClass(HttpResponse model) {
        if (model.schema) {
            def typeOfCode = model.schema.enumOrItemEnum ? SpringRs2tTypeOfCode.ENUM : SpringRs2tTypeOfCode.DTO
            getClass(typeOfCode, model.schema.dataType)
        } else {
            null
        }
    }

    @TypeChecked(TypeCheckingMode.SKIP)
    private boolean isStaticFactoryMethod() {
        getParams(SpringRestParams).staticFactoryMethodForRestResponse
    }
}
