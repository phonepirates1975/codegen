/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.java.JavaModuleConfigurator
import pl.metaprogramming.codegen.java.JavaModuleGenerator
import pl.metaprogramming.codegen.java.builders.EnumBuildStrategy
import pl.metaprogramming.codegen.java.builders.InterfaceBuildStrategy
import pl.metaprogramming.codegen.java.spring.rs2t.*
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.oas.Parameter
import pl.metaprogramming.model.oas.RestApi

import java.util.function.Consumer

import static pl.metaprogramming.codegen.java.ClassCd.stringType
import static pl.metaprogramming.codegen.java.spring.base.SpringDefs.*

class SpringRestClient2tGenerator extends JavaModuleGenerator<RestApi> {

    public static final TOC_REST_CLIENT = SpringRs2tTypeOfCode.REST_CLIENT
    public static final TOC_REST_CLIENT_IMPL = SpringRs2tTypeOfCode.REST_CLIENT_IMPL
    public static final TOC_DTO = SpringRs2tTypeOfCode.DTO
    public static final TOC_REST_DTO = SpringRs2tTypeOfCode.REST_DTO
    public static final TOC_ENUM = SpringRs2tTypeOfCode.ENUM
    public static final TOC_REST_REQUEST_DTO = SpringRs2tTypeOfCode.REST_REQUEST_DTO
    public static final TOC_REQUEST_DTO = SpringRs2tTypeOfCode.REQUEST_DTO
    public static final TOC_RESPONSE_DTO = SpringRs2tTypeOfCode.RESPONSE_DTO
    public static final TOC_REST_MAPPER = SpringRs2tTypeOfCode.REST_MAPPER
    public static final TOC_REQUEST_MAPPER = SpringRs2tTypeOfCode.REQUEST_MAPPER
    public static final TOC_RESPONSE_MAPPER = SpringRs2tTypeOfCode.RESPONSE_MAPPER

    static SpringRestClient2tGenerator of(
            RestApi model,
            CodegenParams params,
            Consumer<Configurator> config) {
        def configurator = new Configurator(params)
        config.accept(configurator)
        new SpringRestClient2tGenerator(model, configurator)
    }

    static SpringRestClient2tGenerator of(
            RestApi model,
            Consumer<Configurator> config) {
        of(model, new CodegenParams(), config)
    }

    private static final List OPERATION_CLASSES = [TOC_REST_REQUEST_DTO, TOC_REQUEST_DTO, TOC_RESPONSE_DTO, TOC_REQUEST_MAPPER, TOC_RESPONSE_MAPPER]

    SpringRestClient2tGenerator(RestApi model, JavaModuleConfigurator config) {
        super(model, config.classBuilderConfigs, config.params)
    }

    @Override
    void generate() {
        addObjectParamsCodes(model.parameters)
        addSchemaCodes(model)
        addOperationCodes(model)
        makeCodeModels()
    }

    void addObjectParamsCodes(List<Parameter> parameters) {
        parameters.findAll { it.isObject() }.each { paramDef ->
            classIndex.put(stringType(), paramDef.dataType, TOC_REST_DTO)
            addClass(TOC_DTO, paramDef.dataType, paramDef.objectType.code)
            addClass(TOC_REST_MAPPER, paramDef.dataType, paramDef.objectType.code)
        }
    }

    void addSchemaCodes(RestApi model) {
        model.schemas.each { schema ->
            if (schema.isEnum()) {
                addClass(TOC_ENUM, schema)
            }
            if (schema.isObject()) {
                addClass(TOC_DTO, schema)
                addClass(TOC_REST_DTO, schema)
                addClass(TOC_REST_MAPPER, schema)
            }
        }
    }

    void addOperationCodes(RestApi model) {
        model.groupedOperations.each { group, operations ->
            operations.each { operation ->
                String dataName = operation.code.capitalize()
                OPERATION_CLASSES.each { classType ->
                    addClass(classType, operation, dataName)
                }
            }
            addClass(TOC_REST_CLIENT, operations, group)
            addClass(TOC_REST_CLIENT_IMPL, operations, group)
        }
    }

    static class Configurator extends JavaModuleConfigurator<Configurator> {

        Configurator(CodegenParams params) {
            super(params.withIfNotSet(SpringRestParams.DEFAULTS))
            dataTypeMapper.setMapping(DataType.BINARY, RESOURCE)
            init()
        }

        private void init() {
            generateIfUsed = true
            addClass(TOC_ENUM, 'Enum', new EnumBuildStrategy())
            addClass(TOC_RESPONSE_DTO, 'Response', new RestResponseBuildStrategy())
            addComponent(TOC_RESPONSE_MAPPER, 'ResponseMapper', new RestOutResponseMapperBuildStrategy())

            addLombokData(TOC_DTO, 'Dto', new DtoBuildStrategy())
            addLombokData(TOC_REST_DTO, 'Rdto', new JsonDtoBuildStrategy(withDescriptiveFields: false))
            addComponent(TOC_REST_MAPPER, 'Mapper', new RestDtoMapperBuildStrategy())

            addLombokData(TOC_REQUEST_DTO, 'Request', new DtoBuildStrategy(modelMapper: OPERATION_REQUEST_SCHEMA))
            addLombokData(TOC_REST_REQUEST_DTO, 'Rrequest', new PayloadFieldAppenderBuildStrategy(), new RawDtoBuildStrategy(withDescriptiveFields: false, modelMapper: OPERATION_REQUEST_SCHEMA))
            addComponent(TOC_REQUEST_MAPPER, 'RequestMapper', new RestOutRequestMapperBuildStrategy())

            addClass(TOC_REST_CLIENT, 'Client', new InterfaceBuildStrategy(TOC_REST_CLIENT_IMPL))
            addComponent(TOC_REST_CLIENT_IMPL, 'ClientImpl', new RestClientImplBuildStrategy())

            dataTypeMapper.setMapper(REST_DATA_TYPE_MAPPER, TOC_REST_DTO, TOC_REST_REQUEST_DTO)
            generateAlways(TOC_REST_CLIENT, TOC_REST_CLIENT_IMPL)
        }

        Configurator setRootPackage(String rootPackage) {
            setPackages(rootPackage, 'rest')
        }

        Configurator setPackages(String rootPackage, String adapterName) {
            super.setRootPackage(rootPackage)
            setPackage("${rootPackage}.ports.out.${adapterName}.dtos", TOC_DTO, TOC_ENUM, TOC_REQUEST_DTO, TOC_RESPONSE_DTO)
            setPackage("${rootPackage}.ports.out.${adapterName}", TOC_REST_CLIENT)
            setPackage("${rootPackage}.adapters.out.${adapterName}.dtos", TOC_REST_DTO, TOC_REST_REQUEST_DTO)
            setPackage("${rootPackage}.adapters.out.${adapterName}.mappers", TOC_REST_MAPPER, TOC_REQUEST_MAPPER, TOC_RESPONSE_MAPPER)
            setPackage("${rootPackage}.adapters.out.${adapterName}", TOC_REST_CLIENT_IMPL)
            this
        }
    }

}
