/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs


import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.model.oas.Operation

import static pl.metaprogramming.codegen.java.spring.base.SpringDefs.ANNOT_COMPONENT

class OperationExecutorBuildStrategy extends ClassCmBuildStrategy<Operation> {

    static final ClassCd SCOPED_PROXY_MODE = ClassCd.of('org.springframework.context.annotation.ScopedProxyMode')
    static final AnnotationCm ANNOT_SCOPE_PROTOTYPE = AnnotationCm.of('org.springframework.context.annotation.Scope', [
            value    : ValueCm.escaped('prototype'),
            proxyMode: ValueCm.value("${SCOPED_PROXY_MODE.className}.TARGET_CLASS")
    ])

    @Override
    void makeDeclaration() {
        addImport(SCOPED_PROXY_MODE)
        addAnnotations([ANNOT_COMPONENT, ANNOT_SCOPE_PROTOTYPE])
        addMapper('execute') {
            it.resultType = ResponseClassResolver.resolve(builder, model)
            it.addParam(getClass(SpringRsTypeOfCode.REQUEST_DTO, model).asField("request").addAnnotation(AnnotationCm.javaxNonnul()))
            it.implBody = makeImplBody(!it.resultType.isVoid())
            it.description = model.description
        }
    }

    private String makeImplBody(boolean returnNull) {
        codeBuf.addLines('// implement me and remove @Generated annotation')
        if (returnNull) codeBuf.addLines('return null;')
        codeBuf.take()
    }
}
