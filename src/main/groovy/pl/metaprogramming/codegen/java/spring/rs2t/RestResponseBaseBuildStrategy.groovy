/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.codegen.java.spring.SpringCommonsTypeOfCode
import pl.metaprogramming.codegen.java.spring.SpringRestParams

import static pl.metaprogramming.codegen.java.ClassCd.*

class RestResponseBaseBuildStrategy extends ClassCmBuildStrategy {

    @Override
    void makeImplementation() {
        def staticFactoryMethodForRestResponse = getParams(SpringRestParams).staticFactoryMethodForRestResponse
        addGenericParams(genericParamR())
        classModel.abstractModifier()
        addInterfaces(getClass(SpringCommonsTypeOfCode.REST_RESPONSE_INTERFACE).withGeneric(genericParamR()))
        addField('status', integerType()) {
            it.addAnnotation(AnnotationCm.lombokGetter())
            it.setFinal(staticFactoryMethodForRestResponse)
        }
        addField('body', objectType()) {
            it.addAnnotation(AnnotationCm.lombokGetter())
            it.setFinal(staticFactoryMethodForRestResponse)
        }
        addField('headers', stringType().asMapTo(stringType())) {
            it.addAnnotation(AnnotationCm.lombokGetter())
            it.setFinal()
            it.assign('new java.util.TreeMap<>()')
        }

        if (!getParams(SpringRestParams).staticFactoryMethodForRestResponse) {
            addMethod('set') {
                it.resultType = genericParamR()
                it.addParam(integerType().asField('status').addAnnotation(AnnotationCm.javaxNonnul()))
                it.addParam('body', objectType())
                it.addAnnotation(AnnotationCm.override())
                it.implBody = codeBuf.ifBlock(
                        'this.status != null',
                        'throw new IllegalStateException(String.format("Response already initialized with %s - %s", this.status, this.body));')
                        .endBlock()
                        .addLines(
                                "this.status = status;",
                                "this.body = body;",
                                "return self();").take()
            }
        } else {
            addAnnotation(AnnotationCm.lombokRequiredArgsConstructor())
        }
    }
}
