/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import groovy.transform.builder.Builder
import groovy.transform.builder.SimpleStrategy

/**
 * Representation of a set of parameters allowing the parameterization
 * (in selected aspects) of the WS client code generator
 * for the spring framework.
 */
@Builder(builderStrategy = SimpleStrategy)
class SpringSoapParams {

    /**
     * A mapping to specify a java package for classes
     * reflecting XML elements from specified namespaces.
     */
    Map<String, String> namespace2Package = [:]

    SpringSoapParams setNamespacePackage(String namespace, String packageName) {
        namespace2Package.put(namespace, packageName)
        this
    }
}
