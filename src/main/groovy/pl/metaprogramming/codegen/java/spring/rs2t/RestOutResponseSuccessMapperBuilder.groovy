/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.MethodCm

class RestOutResponseSuccessMapperBuilder extends RestOutResponseMapperBuilder {

    @Override
    MethodCm makeDeclaration() {
        MethodCm.of('map')
                .resultType(responseClass)
                .addParam(helper.responseEntity.asField(RESPONSE_PARAM).addAnnotation(AnnotationCm.javaxNonnul()))
    }

    @Override
    String makeImplBody() {
        codeBuf
                .addLines(helper.successResponse.isDefault()
                        ? "return ${responseObjectCreatePrefix}.setOther(responseEntity.getStatusCodeValue(), $successDataResolver);"
                        : "return ${responseObjectCreatePrefix}.set${helper.successResponse.status}($successDataResolver);")
                .take()
    }

    private String getSuccessDataResolver() {
        if (helper.hasResponseBody()) {
            String restDtoExp = "${RESPONSE_PARAM}.getBody()"
            def result = mapping().withModel(helper.responseObject)
                    .to(SpringRs2tTypeOfCode.DTO)
                    .from(SpringRs2tTypeOfCode.REST_DTO, restDtoExp)
                    .make()
            if (result.toString() == restDtoExp) {
                addImport('java.util.Objects')
                "Objects.requireNonNull($restDtoExp)"
            } else {
                result
            }
        } else {
            ''
        }
    }
}
