/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.Model
import pl.metaprogramming.codegen.java.JavaModuleConfigurator
import pl.metaprogramming.codegen.java.JavaModuleGenerator
import pl.metaprogramming.codegen.java.base.ClassBuilderConfigurator
import pl.metaprogramming.codegen.java.builders.BaseEnumBuildStrategy
import pl.metaprogramming.codegen.java.builders.EnumValueInterfaceBuilder
import pl.metaprogramming.codegen.java.builders.PlainEnumBuildStrategy
import pl.metaprogramming.codegen.java.builders.PrivateConstructorBuildStrategy
import pl.metaprogramming.codegen.java.jaxb.JaxbTypeOfCode
import pl.metaprogramming.codegen.java.spring.base.EndpointProviderBuildStrategy
import pl.metaprogramming.codegen.java.spring.rs.RestClientBuildStrategy
import pl.metaprogramming.codegen.java.spring.rs.RestClientExceptionBuildStrategy
import pl.metaprogramming.codegen.java.spring.rs2t.*
import pl.metaprogramming.codegen.java.validation.*
import pl.metaprogramming.model.oas.HttpResponse

import java.util.function.Consumer

class SpringCommonsGenerator extends JavaModuleGenerator<EmptyModel> {

    public static final TOC_JAXB_LOCAL_DATE_ADAPTER = JaxbTypeOfCode.LOCAL_DATE_ADAPTER
    public static final TOC_JAXB_LOCAL_DATE_TIME_ADAPTER = JaxbTypeOfCode.LOCAL_DATE_TIME_ADAPTER
    public static final TOC_VALIDATION_CONTEXT = ValidationTypeOfCode.VALIDATION_CONTEXT
    public static final TOC_VALIDATION_FIELD = ValidationTypeOfCode.VALIDATION_FIELD
    public static final TOC_VALIDATION_CHECKER = ValidationTypeOfCode.VALIDATION_CHECKER
    public static final TOC_VALIDATION_COMMON_CHECKERS = ValidationTypeOfCode.VALIDATION_COMMON_CHECKERS
    public static final TOC_VALIDATION_SIMPLE_CHECKER = ValidationTypeOfCode.VALIDATION_SIMPLE_CHECKER
    public static final TOC_VALIDATION_ERROR = ValidationTypeOfCode.VALIDATION_ERROR
    public static final TOC_VALIDATION_RESULT = ValidationTypeOfCode.VALIDATION_RESULT
    public static final TOC_VALIDATION_RESULT_MAPPER = ValidationTypeOfCode.VALIDATION_RESULT_MAPPER
    public static final TOC_REST_EXCEPTION_HANDLER = ValidationTypeOfCode.REST_EXCEPTION_HANDLER
    public static final TOC_VALIDATION_EXCEPTION = ValidationTypeOfCode.VALIDATION_EXCEPTION
    public static final TOC_VALIDATION_VALIDATOR = ValidationTypeOfCode.VALIDATION_VALIDATOR
    public static final TOC_VALIDATION_DICTIONARY_CHECKER = ValidationTypeOfCode.VALIDATION_DICTIONARY_CHECKER
    public static final TOC_VALIDATION_DICTIONARY_CODES_ENUM = ValidationTypeOfCode.VALIDATION_DICTIONARY_CODES_ENUM
    public static final TOC_VALIDATION_ERROR_CODES_ENUM = ValidationTypeOfCode.VALIDATION_ERROR_CODES_ENUM

    public static final TOC_ENDPOINT_PROVIDER = SpringCommonsTypeOfCode.ENDPOINT_PROVIDER
    public static final TOC_ENUM_VALUE_INTERFACE = SpringCommonsTypeOfCode.ENUM_VALUE_INTERFACE
    public static final TOC_VALUE_HOLDER_TEMPLATE = SpringCommonsTypeOfCode.VALUE_HOLDER_TEMPLATE
    public static final TOC_REST_RESPONSE_ABSTRACT = SpringCommonsTypeOfCode.REST_RESPONSE_ABSTRACT
    public static final TOC_REST_RESPONSE_INTERFACE = SpringCommonsTypeOfCode.REST_RESPONSE_INTERFACE
    public static final TOC_REST_RESPONSE_STATUS_INTERFACE = SpringCommonsTypeOfCode.REST_RESPONSE_STATUS_INTERFACE
    public static final TOC_REST_RESPONSE_STATUS_NO_CONTENT_INTERFACE = SpringCommonsTypeOfCode.REST_RESPONSE_STATUS_NO_CONTENT_INTERFACE
    public static final TOC_REST_REQUEST_HANDLER_TEMPLATE = SpringCommonsTypeOfCode.REST_REQUEST_HANDLER_TEMPLATE
    public static final TOC_MAP_RAW_VALUE_SERIALIZER = SpringCommonsTypeOfCode.MAP_RAW_VALUE_SERIALIZER
    public static final TOC_SERIALIZATION_UTILS = SpringCommonsTypeOfCode.SERIALIZATION_UTILS

    public static final TOC_REST_CLIENT = SpringCommonsTypeOfCode.REST_CLIENT
    public static final TOC_REST_CLIENT_EXCEPTION = SpringCommonsTypeOfCode.REST_CLIENT_EXCEPTION


    static SpringCommonsGenerator of(CodegenParams params, Consumer<Configurator> config) {
        def configurator = new Configurator(params)
        config.accept(configurator)
        new SpringCommonsGenerator(configurator)
    }

    static SpringCommonsGenerator of(Consumer<Configurator> config) {
        of(new CodegenParams(), config)
    }

    SpringCommonsGenerator(JavaModuleConfigurator config) {
        super(EmptyModel.of('Commons'), config.classBuilderConfigs, config.params)
    }

    @Override
    void generate() {
        classBuilderConfigs.values().each {
            try {
                it.register(classIndex, params)
            } catch (Exception e) {
                throw new IllegalStateException("Can't register class: " + it.classType, e)
            }
        }
        makeCodeModels()
    }

    static class EmptyModel implements Model {
        String name
        static EmptyModel of(String name) {
            new EmptyModel(name: name)
        }
    }

    static class Configurator extends JavaModuleConfigurator<Configurator> {

        static List<Integer> HTTP_STATUSES = [HttpResponse.DEFAULT] + (100..102) +
                (200..208) + [226] +
                (300..308) +
                (400..418) + (422..426) + (449..451) + [420, 428, 429, 431, 444, 499] +
                (500..511) + [598, 599]

        Configurator(CodegenParams params) {
            super(params
                    .withIfNotSet(SpringRestParams.DEFAULTS)
                    .withIfNotSet(new ValidationParams()))
            generateIfUsed = true
            init()
        }

        private void init() {
            initRestCommon()

            addFixedClass(TOC_VALIDATION_CONTEXT, 'ValidationContext')
            addFixedClass(TOC_VALIDATION_CHECKER, 'Checker', new CheckerBuildStrategy())
            addFixedClass(TOC_VALIDATION_SIMPLE_CHECKER, 'SimpleChecker', [TOC_VALIDATION_CHECKER])
            addFixedClass(TOC_VALIDATION_FIELD, 'Field')
            addFixedClass(TOC_VALIDATION_ERROR, 'ValidationError', new ValidationErrorBuildStrategy())
            addFixedClass(TOC_VALIDATION_RESULT, 'ValidationResult', new ValidationResultBuildStrategy())
            addFixedClass(TOC_VALIDATION_EXCEPTION, 'ValidationException', new ValidationExceptionBuildStrategy())
            addFixedClass(TOC_VALIDATION_VALIDATOR, 'Validator', new BaseValidatorBuildStrategy())
            addFixedClass(TOC_VALIDATION_COMMON_CHECKERS, 'CommonCheckers', [TOC_ENUM_VALUE_INTERFACE, TOC_VALIDATION_SIMPLE_CHECKER])

            addComponent(prepareFixedClass(TOC_VALIDATION_DICTIONARY_CHECKER, 'DictionaryChecker', new DictionaryCheckerBuildStrategy()))
            addFixedClass(TOC_VALIDATION_DICTIONARY_CODES_ENUM, 'DictionaryCodes', new PlainEnumBuildStrategy())
            addFixedClass(TOC_VALIDATION_ERROR_CODES_ENUM, 'ErrorCodes', new BaseEnumBuildStrategy())

            addFixedClass(TOC_REST_REQUEST_HANDLER_TEMPLATE, 'RestRequestHandler', [TOC_VALUE_HOLDER_TEMPLATE, TOC_REST_RESPONSE_INTERFACE, TOC_VALIDATION_RESULT])

            addComponent(prepareFixedClass(TOC_VALIDATION_RESULT_MAPPER, 'ValidationResultMapper', new ValidationResultMapperBuildStrategy()))
            addFixedClass(TOC_REST_EXCEPTION_HANDLER, 'RestExceptionHandler', [TOC_VALIDATION_EXCEPTION, TOC_VALIDATION_RESULT, TOC_VALIDATION_ERROR, TOC_VALIDATION_COMMON_CHECKERS, TOC_ENUM_VALUE_INTERFACE])

            addComponent(prepareFixedClass(TOC_ENDPOINT_PROVIDER, 'EndpointProvider', new EndpointProviderBuildStrategy()))

            addFixedClass(TOC_JAXB_LOCAL_DATE_ADAPTER, 'LocalDateAdapter')
            addFixedClass(TOC_JAXB_LOCAL_DATE_TIME_ADAPTER, 'LocalDateTimeAdapter')
        }

        private void initRestCommon() {
            typeOfCode(TOC_SERIALIZATION_UTILS) {
                it.fixedName = 'SerializationUtils'
                it.addStrategy(new PrivateConstructorBuildStrategy(),
                        new SerializationUtilsBuildStrategy()
                )
            }
            addFixedClass(TOC_ENUM_VALUE_INTERFACE, 'EnumValue', new EnumValueInterfaceBuilder())
            addFixedClass(TOC_VALUE_HOLDER_TEMPLATE, 'ValueHolder')
            addFixedClass(TOC_MAP_RAW_VALUE_SERIALIZER, 'MapRawValueSerializer')
            addFixedClass(TOC_REST_RESPONSE_ABSTRACT, 'RestResponseBase', new RestResponseBaseBuildStrategy())
            addFixedClass(TOC_REST_RESPONSE_INTERFACE, 'RestResponse', new RestResponseInterfaceBuildStrategy())
            addRestResponseStatusInterfaces()

            // REST client
            addFixedClass(TOC_REST_CLIENT, 'RestClient', new RestClientBuildStrategy())
            addFixedClass(TOC_REST_CLIENT_EXCEPTION, 'RestClientException', new RestClientExceptionBuildStrategy())
        }

        private void addRestResponseStatusInterfaces() {
            def statusesModels = HTTP_STATUSES.collect {
                new ClassBuilderConfigurator.Model(model: it, name: '' + (it == HttpResponse.DEFAULT ? 'Other' : it))
            } as List<ClassBuilderConfigurator.Model>
            typeOfCode(TOC_REST_RESPONSE_STATUS_INTERFACE)
                    .setNamePrefixAndSuffix('RestResponse', '')
                    .addStrategy(new RestResponseStatusInterfaceBuildStrategy())
                    .setPredefinedModels(statusesModels)
                    .setGenerateIfUsed(true)
            typeOfCode(TOC_REST_RESPONSE_STATUS_NO_CONTENT_INTERFACE)
                    .setNamePrefixAndSuffix('RestResponse', 'NoContent')
                    .addStrategy(new RestResponseStatusInterfaceBuildStrategy(withContent: false))
                    .setPredefinedModels(statusesModels)
                    .setGenerateIfUsed(true)
        }

        Configurator setRootPackage(String rootPackage) {
            super.setRootPackage(rootPackage)
            setPackage(rootPackage + '.validator',
                    TOC_VALIDATION_CONTEXT,
                    TOC_VALIDATION_CHECKER,
                    TOC_VALIDATION_SIMPLE_CHECKER,
                    TOC_VALIDATION_FIELD,
                    TOC_VALIDATION_ERROR,
                    TOC_VALIDATION_RESULT,
                    TOC_VALIDATION_RESULT_MAPPER,
                    TOC_VALIDATION_EXCEPTION,
                    TOC_VALIDATION_VALIDATOR,
                    TOC_VALIDATION_DICTIONARY_CHECKER,
                    TOC_VALIDATION_DICTIONARY_CODES_ENUM,
                    TOC_VALIDATION_ERROR_CODES_ENUM,
                    TOC_VALIDATION_COMMON_CHECKERS)
            this
        }
    }

}
