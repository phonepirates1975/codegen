/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.builders

import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.FieldCm
import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.codegen.java.formatter.JavaCodeFormatter
import pl.metaprogramming.codegen.java.validation.ValidationMethodBuilder
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.data.ObjectType
import pl.metaprogramming.model.oas.Parameter

import static pl.metaprogramming.codegen.java.ClassCd.objectType
import static pl.metaprogramming.codegen.java.ClassCd.stringType
import static pl.metaprogramming.codegen.java.validation.ValidationTypeOfCode.VALIDATION_FIELD

abstract class BaseDtoBuildStrategy extends ClassCmBuildStrategy<ObjectType> {

    @Override
    void makeDeclaration() {
        if (model.inherits) {
            assert model.inherits.size() == 1
            setSuperClass(getClass(classType, model.inherits[0]))
        }
        addFields()
    }

    void addFields() {
        model.fields.each { field ->
            try {
                addField(field)
            } catch (Exception e) {
                throw new IllegalStateException("Can't handle field '$field.code' for model '$modelName'", e)
            }
        }
    }

    abstract void addField(DataSchema schema)

    ClassCd getClass(Object classType, DataType dataType) {
        if (isUnspecifiedObject(dataType)) {
            objectType().asMapBy(stringType())
        } else {
            super.getClass(classType, dataType)
        }
    }

    private boolean isUnspecifiedObject(DataType dataType) {
        if (dataType instanceof ObjectType) {
            dataType.code == null && dataType.fields.empty && dataType.inherits.empty
        } else {
            false
        }
    }

    protected void addDescriptiveFields(DataSchema schema, FieldCm fieldCm) {
        def originalName = schema instanceof Parameter ? "$schema.name ($schema.location parameter)" : schema.code
        def descriptionFieldName =  schema.getAdditive(ValidationMethodBuilder.DESCRIPTION_FIELD_NAME) {
            'FIELD_' + JavaCodeFormatter.toUpperCase(fieldCm.name)
        }
        def fieldClass = getClass(VALIDATION_FIELD)
        String value = "new ${fieldClass.className}<>(" +
                "\"$originalName\", " +
                "${classModel.className}::get${fieldCm.name.capitalize()})"

        addField(descriptionFieldName, fieldClass.withGeneric(classModel, fieldCm.type)) {
            it.setPublic().setStatic().setFinal()
            it.assign(value)
        }
    }
}
