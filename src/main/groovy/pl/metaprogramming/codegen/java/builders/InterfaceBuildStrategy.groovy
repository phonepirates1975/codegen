/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.builders

import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassCm
import pl.metaprogramming.codegen.java.ClassKind

class InterfaceBuildStrategy extends ClassCmBuildStrategy {

    private Object implClassType

    InterfaceBuildStrategy(Object implClassType) {
        this.implClassType = implClassType
    }

    @Override
    void makeDeclaration() {
        setKind(ClassKind.INTERFACE)
        def implClass = classLocator(implClassType).getDeclared()
        implClass.interfaces.add(classModel)
        implClass.methods.findAll {
            it.public && !it.constructor
        }.each {method ->
            addMethod(method.name) {
                it.resultType(method.resultType)
                it.addParams(method.params)
                it.description(method.description)
            }
            method.description = null
            method.addAnnotation(AnnotationCm.override())
            method.ownerInterface = classModel as ClassCm
        }
    }
}
