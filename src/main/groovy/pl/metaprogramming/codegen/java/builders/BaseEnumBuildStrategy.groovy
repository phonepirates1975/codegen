/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.builders

import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassKind
import pl.metaprogramming.codegen.java.EnumItemCm
import pl.metaprogramming.model.data.EnumType

import static pl.metaprogramming.codegen.java.ClassCd.stringType

class BaseEnumBuildStrategy extends ClassCmBuildStrategy<EnumType> {

    @Override
    void makeDeclaration() {
        setKind(ClassKind.ENUM)
        if (model) {
            model.allowedValues.each {
                addEnumItem(it)
            }
        }
        addMethod(classModel.className) {
            it.addParam('value', stringType())
            it.privateModifier()
            it.implBody('this.value = value;')
        }
    }

    @Override
    void makeImplementation() {
        addField('value', stringType()) {
            it.addAnnotation(AnnotationCm.lombokGetter())
            it.setFinal()
        }
    }

    EnumItemCm addEnumItem(String value) {
        classModel.addEnumItem(nameMapper.toConstantName(value))
                .value(value)
                .description(model.getDescription(value))
    }
}
