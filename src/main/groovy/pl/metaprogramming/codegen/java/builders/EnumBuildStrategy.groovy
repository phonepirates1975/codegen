/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.builders

import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.JavaModuleParams
import pl.metaprogramming.codegen.java.spring.SpringCommonsTypeOfCode

import static pl.metaprogramming.codegen.java.ClassCd.stringType

class EnumBuildStrategy extends BaseEnumBuildStrategy {

    @Override
    void makeDeclaration() {
        super.makeDeclaration()
        ClassCd enumInterface = getClass(SpringCommonsTypeOfCode.ENUM_VALUE_INTERFACE)
        addInterfaces(enumInterface)
        addMapper('fromValue') {
            it.used(fromValueMethodShouldBeAlwaysGenerated)
            it.resultType = classModel
            it.staticModifier()
            it.addParam('value', stringType())
            it.implBody = "return ${enumInterface.className}.fromValue(value, ${classModel.className}.class);"
        }
    }

    @TypeChecked(TypeCheckingMode.SKIP)
    boolean getFromValueMethodShouldBeAlwaysGenerated() {
        getParams(JavaModuleParams).alwaysGenerateEnumFromValueMethod
    }

}
