/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.builders

import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.ClassKind

import static pl.metaprogramming.codegen.java.ClassCd.genericParamT
import static pl.metaprogramming.codegen.java.ClassCd.stringType

class EnumValueInterfaceBuilder extends ClassCmBuildStrategy {

    @Override
    void makeImplementation() {
        ClassCd genericType = genericParamT().withSuper(classModel)
        setKind(ClassKind.INTERFACE)
        addMethod('getValue') { it.resultType = stringType() }
        addMethod('equalsValue') {
            it.resultType = 'boolean'
            it.addParam('value', stringType())
            it.implBody = "return getValue().equals(value);"
        }
        addMethod('fromValue') {
            it.resultType = genericType
            it.addParam('value', stringType())
            it.addParam('enumClass', 'java.lang.Class<T>')
            it.staticModifier()
            it.implBody = fromValueImplBody()
        }
    }

    private String fromValueImplBody() {
        codeBuf.ifBlock("value == null", "return null;")
                .endBlock()
                .block("for (T e : enumClass.getEnumConstants())",
                        'if (e.equalsValue(value)) return e;')
                .endBlock()
                .addLines("throw new IllegalArgumentException(String.format(\"Unknown value '%s' of enum '%s'\", value, enumClass.getCanonicalName()));")
                .take()
    }
}
