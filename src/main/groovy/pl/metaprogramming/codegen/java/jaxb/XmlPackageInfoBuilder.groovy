/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.jaxb


import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.base.PackageInfoCm
import pl.metaprogramming.codegen.java.ValueCm

class XmlPackageInfoBuilder {

    static PackageInfoCm make(String baseDir, String namespace, String packageName, String elementFormDefault, AnnotationCm generatedAnnotation) {
        new PackageInfoCm(
                baseDir: baseDir,
                packageName: packageName,
                annotations: [generatedAnnotation, prepareAnnotations(namespace, elementFormDefault)]
        )
    }

    private static AnnotationCm prepareAnnotations(String namespace, String elementFormDefault) {
        def xmlSchemaAnnotation = AnnotationCm.of('javax.xml.bind.annotation.XmlSchema', [
                namespace: ValueCm.escaped(namespace)])
        if (elementFormDefault == 'qualified') {
            xmlSchemaAnnotation.params.put('elementFormDefault', ValueCm.value('javax.xml.bind.annotation.XmlNsForm.QUALIFIED'))
        }
        xmlSchemaAnnotation
    }
}
