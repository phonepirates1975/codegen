/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.jaxb

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.codegen.java.builders.BaseDtoBuildStrategy
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.ObjectType

import static pl.metaprogramming.codegen.java.jaxb.JaxbTypeOfCode.*

class XmlDtoBuildStrategy extends BaseDtoBuildStrategy {

    static AnnotationCm XML_ACCESSOR_TYPE_ANNOTATION = AnnotationCm.of('javax.xml.bind.annotation.XmlAccessorType', [value: ValueCm.value("XmlAccessType.FIELD")])

    @Override
    void makeImplementation() {
        super.makeImplementation()
        addImport("javax.xml.bind.annotation.XmlAccessType")
        addAnnotation(XML_ACCESSOR_TYPE_ANNOTATION)
        addAnnotation(prepareXmlTypeAnnotation(model))
        if (model.isRootElement) {
            addAnnotation(prepareXmlRootElementAnnotation(model))
        }
    }

    @Override
    void addField(DataSchema schema) {
        def classCd = getClass(schema.enumOrItemEnum ? ENUM : DTO, schema.dataType)
        addField(getFieldName(schema), classCd) {
            it.description = schema.description
            it.annotations = getFieldAnnotations(schema, classCd)
        }
    }

    private List<AnnotationCm> getFieldAnnotations(DataSchema schema, ClassCd classCd) {
        def result = [] as List<AnnotationCm>
        result.add(schema.isRequired && !schema.isNillable ? AnnotationCm.javaxNonnul() : AnnotationCm.javaxNullable())
        addXmlElementAnnotation(result, schema)
        if (classCd.canonicalName == 'java.time.LocalDate') {
            addXmlJavaTypeAdapterAnnotation(result, LOCAL_DATE_ADAPTER)
        }
        if (classCd.canonicalName == 'java.time.LocalDateTime') {
            addXmlJavaTypeAdapterAnnotation(result, LOCAL_DATE_TIME_ADAPTER)
        }
        result
    }

    void addXmlElementAnnotation(List<AnnotationCm> result, DataSchema schema) {
        Map<String, ValueCm> xmlElementAttributes = [:]
        if (schema.isRequired) {
            xmlElementAttributes.put('required', ValueCm.value('true'))
        }
        if (schema.isNillable) {
            xmlElementAttributes.put('nillable', ValueCm.value('true'))
        }
        if (getFieldName(schema) != schema.code) {
            xmlElementAttributes.put('name', ValueCm.escaped(schema.code))
        }
        if (xmlElementAttributes) {
            result.add(AnnotationCm.of('javax.xml.bind.annotation.XmlElement', xmlElementAttributes))
        }
    }

    private void addXmlJavaTypeAdapterAnnotation(List<AnnotationCm> result, def adapterClassType) {
        def adapterClass = getClass(adapterClassType)
        addImport(adapterClass)
        result.add(AnnotationCm.of(
                'javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter',
                [value: ValueCm.value("${adapterClass.className}.class")])
                .addDependency(adapterClass))
    }

    AnnotationCm prepareXmlTypeAnnotation(ObjectType model) {
        AnnotationCm.of('javax.xml.bind.annotation.XmlType', [
                name     : ValueCm.escaped(model.isRootElement ? '' : model.code),
                propOrder: ValueCm.escapedArray(
                        model.fields.collect { getFieldName(it) },
                        true)
        ])
    }

    static AnnotationCm prepareXmlRootElementAnnotation(ObjectType model) {
        AnnotationCm.of('javax.xml.bind.annotation.XmlRootElement', [
                name: ValueCm.escaped(model.code)])
    }
}
