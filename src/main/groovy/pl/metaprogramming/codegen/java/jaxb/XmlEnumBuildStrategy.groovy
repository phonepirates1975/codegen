/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.jaxb


import pl.metaprogramming.codegen.java.builders.EnumBuildStrategy
import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.EnumItemCm
import pl.metaprogramming.codegen.java.ValueCm

class XmlEnumBuildStrategy extends EnumBuildStrategy {
    @Override
    void makeImplementation() {
        super.makeImplementation()
        addAnnotation(AnnotationCm.of('javax.xml.bind.annotation.XmlEnum'))
        addAnnotation(AnnotationCm.of('javax.xml.bind.annotation.XmlType', [name: ValueCm.escaped(model.code)]))
    }

    @Override
    EnumItemCm addEnumItem(String value) {
        def item = super.addEnumItem(value)
        if (value != item.name) {
            item.annotations.add(AnnotationCm.of(
                    'javax.xml.bind.annotation.XmlEnumValue',
                    [value: ValueCm.escaped(value)]
            ))
        }
        item
    }
}
