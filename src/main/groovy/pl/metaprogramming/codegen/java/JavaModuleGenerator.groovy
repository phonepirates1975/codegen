/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.Model
import pl.metaprogramming.codegen.ModuleGenerator
import pl.metaprogramming.codegen.generator.CodeGenerationTask
import pl.metaprogramming.codegen.java.base.ClassBuilderConfigurator
import pl.metaprogramming.codegen.java.base.ClassCmBuilder
import pl.metaprogramming.codegen.java.base.PackageInfoCm
import pl.metaprogramming.codegen.java.formatter.PackageInfoFormatter
import pl.metaprogramming.codegen.java.base.ClassEntry
import pl.metaprogramming.codegen.java.base.ClassIndex
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.NamedDataType

abstract class JavaModuleGenerator<T extends Model> implements ModuleGenerator<T> {

    final ClassIndex classIndex
    final T model
    final Map<Object, ClassBuilderConfigurator> classBuilderConfigs
    final CodegenParams params

    List<PackageInfoCm> packageInfoList = []
    Set<String> usedPackages = []

    JavaModuleGenerator(T model, Map<Object, ClassBuilderConfigurator> classBuilderConfigs, CodegenParams params) {
        this.model = model
        this.params = params
        this.classBuilderConfigs = classBuilderConfigs
        this.classIndex = new ClassIndex(model.name)
    }

    void setDependencies(List<ModuleGenerator> modules) {
        classIndex.dependsOn = modules
                .findAll { it instanceof JavaModuleGenerator }
                .collect { (it as JavaModuleGenerator).classIndex }
    }

    @Override
    List<CodeGenerationTask> getCodesToGenerate() {
        if (params.skipGeneration) {
            return []
        }
        makeClassGenerationTasksFromClassIndex() + makePackageInfoGenerationsTasks()
    }

    protected ModuleGenerator makeCodeModels() {
        classIndex.makeCodeModels()
        this
    }

    protected List<CodeGenerationTask> makeClassGenerationTasksFromClassIndex() {
        classIndex.codesToGenerate.collect {
            it.builder.makeDecoration()
            makeCodeGenerationTask(it)
        }
    }

    protected CodeGenerationTask makeCodeGenerationTask(ClassEntry entry) {
        if (!entry.clazz.packageName) {
            throw new IllegalStateException("Generated classes should have specified package: ${entry.clazz}")
        }
        usedPackages.add(entry.clazz.packageName)
        try {
            def result = entry.builder.makeGenerationTask(getClassFilePath(entry.classType, entry.clazz))
            result.codeDecorators = getParams(JavaModuleParams).codeDecorators
            result
        } catch (Exception e) {
            throw new IllegalStateException("Can't make CodeGenerationTask for: ${entry.classType}", e)
        }
    }

    protected List<CodeGenerationTask> makePackageInfoGenerationsTasks() {
        packageInfoList.findAll { usedPackages.contains(it.packageName) }.collect {
            new CodeGenerationTask(
                    destFilePath: getJavaFilePath(it.baseDir, it.packageName, 'package-info'),
                    codeModel: it,
                    formatter: new PackageInfoFormatter()
            )
        }
    }

    protected String getClassFilePath(Object classType, ClassCd classCd) {
        def baseDir = getConfig(classType).baseDir
        assert baseDir, "No base dir for class type: $classType"
        getJavaFilePath(baseDir, classCd.packageName, classCd.className)
    }

    static protected String getJavaFilePath(String baseDir, String packageName, String className) {
        "$baseDir/${pkgToDir(packageName)}/${className}.java"
    }

    protected ClassCmBuilder addClass(def classType, def model, String modelName) {
        getConfig(classType).register(classIndex, params, modelName, model) as ClassCmBuilder
    }

    protected ClassCmBuilder addClass(Object classType, DataSchema schema) {
        def dataType = schema.dataType as NamedDataType
        addClass(classType, dataType, dataType.code)
    }

    protected ClassCmBuilder addClass(ClassCmBuilder builder, Object classType) {
        builder.config = getConfig(classType)
        builder.params = params
        builder.classIndex = classIndex
        classIndex.put(builder)
        builder
    }

    protected ClassBuilderConfigurator getConfig(def classType) {
        assert classBuilderConfigs.containsKey(classType), "No generation configuration for class type: $classType"
        classBuilderConfigs.get(classType)
    }

    protected <V> V getParams(Class<V> clazz) {
        params.get(clazz)
    }

    static String pkgToDir(String pkg) {
        pkg.replaceAll('\\.', '/')
    }

    @Override
    String toString() {
        "${getClass().name} for ${model.name}"
    }
}