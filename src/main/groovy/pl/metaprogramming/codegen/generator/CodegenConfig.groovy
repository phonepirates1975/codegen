/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.generator

import pl.metaprogramming.codegen.ModuleGenerator

class CodegenConfig {
    boolean verbose
    boolean storeAnyKindOfStatusesInIndexFile
    boolean forceMode = true
    boolean addLastGenerationTag = false
    boolean addLastUpdateTag = true
    String charset = 'UTF-8'
    File baseDir = new File('.')
    File indexFile
    List<ModuleGenerator> modules = []

    File getIndexFile() {
        indexFile ?: new File(baseDir, 'generatedFiles.yaml')
    }
}
