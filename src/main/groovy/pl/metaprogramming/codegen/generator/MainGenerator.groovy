/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.generator

import groovy.transform.ToString
import groovy.util.logging.Slf4j
import org.yaml.snakeyaml.DumperOptions
import org.yaml.snakeyaml.DumperOptions.LineBreak
import org.yaml.snakeyaml.Yaml
import pl.metaprogramming.codegen.Codegen

import java.nio.file.Files
import java.util.function.Predicate

@Slf4j
class MainGenerator {
    static String DATETIME_FORMAT = 'yyyy-MM-dd HH:mm:ss'

    CodegenConfig cfg
    Map<String, CodeFile> fileIndex = [:]
    String prevGenerationDate
    String generationDate

    private Map<File, Set<String>> dirFiles = new HashMap<>()
    private Boolean mngGitRepo

    static MainGenerator run(CodegenConfig cfg) {
        new MainGenerator(cfg).runAll()
    }

    static MainGenerator cleanup(CodegenConfig cfg, boolean total) {
        new MainGenerator(cfg).cleanup(total)
    }

    MainGenerator(CodegenConfig cfg) {
        this.cfg = cfg
    }

    MainGenerator runAll() {
        loadIndex(FileStatus.ABANDONED)
        generationDate = new Date().format(DATETIME_FORMAT)
        cfg.modules.each { it.generate() }
        cfg.modules.each {
            it.codesToGenerate.each {
                updateIndex(it)
            }
        }
        removeFiles('Delete abandoned files') {
            it.status == FileStatus.ABANDONED
        }
        writeFiles()
        saveIndex()
        this
    }

    MainGenerator cleanup(boolean total) {
        mngGitRepo = false
        loadIndex(FileStatus.UNCHANGED)
        def removed = removeFiles("Delete generated files [${total ? 'include' : 'skip'} manually modified codes]") {
            total || it.status != FileStatus.DETACHED
        }
        fileIndex.removeAll { removed.contains(it.value) }
        Files.walk(cfg.baseDir.toPath())
                .sorted(Comparator.reverseOrder())
                .map({ it.toFile() })
                .filter({ it.isDirectory() })
                .forEach({ it.delete() })
        saveIndex()
        this
    }

    private void updateIndex(CodeGenerationTask<?> task) {
        task.md5 = HashBuilder.build(task.content)
        def fileMetadata = fileIndex.get(task.destFilePath)
        if (fileMetadata == null) {
            addToIndex(task)
        } else {
            checkFileStatus(fileMetadata)
            fileMetadata.task = task
            if (fileMetadata.state.exists && !fileMetadata.state.annotatedByGenerated) {
                fileMetadata.status = FileStatus.DETACHED
            } else {
                fileMetadata.status = fileMetadata.state.md5 == task.md5
                        ? FileStatus.UNCHANGED
                        : FileStatus.UPDATED
            }
            log.debug("$task.destFilePath ... ${fileMetadata.status}")
        }
    }

    private void addToIndex(CodeGenerationTask<?> task) {
        def state = getFileState(task.destFilePath)
        if (state.exists) {
            if (!state.annotatedByGenerated) {
                if (existsIndexFile()) {
                    log.info("$task.destFilePath already exists, and does not have Generated annotaion")
                }
                addToIndex(task, FileStatus.DETACHED)
            } else if (state.md5 == task.md5) {
                if (existsIndexFile()) {
                    log.info("$task.destFilePath already exists, and has the same content like generated one")
                }
                addToIndex(task, FileStatus.UNCHANGED)
            } else {
                log.warn("$task.destFilePath already exists, and has other content than generated one.")
                addToIndex(task, FileStatus.UPDATED)
            }
        } else {
            addToIndex(task, FileStatus.CREATED)
        }
    }

    private void addToIndex(CodeGenerationTask<?> task, FileStatus status) {
        log.debug("$task.destFilePath ... $status")
        fileIndex.put(task.destFilePath, new CodeFile(
                task: task,
                path: task.destFilePath,
                status: status,
                state: new FileState(exists: false)
        ))
    }

    private void checkFileStatus(CodeFile fileMetadata) {
        if (!fileMetadata.state.exists) {
            if (cfg.forceMode) {
                log.warn("${fileMetadata.path} was manualy removed and will be restored.")
            } else {
                throw new RuntimeException("The file ${fileMetadata.path} has been manually deleted. Set foreceMode to proceed.")
            }
        }
        if (fileMetadata.status == FileStatus.MALFORMED) {
            if (cfg.forceMode) {
                log.warn("${fileMetadata.path} has been manually modified and will be overwritten.")
            } else {
                throw new RuntimeException("The file ${fileMetadata.path} has been manually modified. Set foreceMode to proceed.")
            }
        }
    }

    private Collection<CodeFile> removeFiles(String label, Predicate<CodeFile> filter) {
        def filtered = fileIndex.values().findAll { filter.test(it) }
        if (filtered && label) {
            log.debug(label)
        }
        filtered.each {
            log.debug("$it.path ... $it.status")
            def file = getFile(it.path)
            if (file.exists() && !file.delete()) {
                throw new RuntimeException("Can't remove file $it")
            }
            removeInGit(file)
        }
        filtered
    }

    private void removeInGit(File file) {
        if (mngGitRepo == null) {
            try {
                def gitStatus = gitCmd("git status", file.getParentFile())
                mngGitRepo = gitStatus == 0
            } catch (Exception ignored) {
                mngGitRepo = false
            }
        }
        if (mngGitRepo) {
            gitCmd("git rm ${file.getName()}", file.getParentFile())
        }
    }

    private int gitCmd(String cmd, File workingDir) {
        try {
            def process = cmd.execute([], workingDir)
            process.waitForOrKill(500)
            return process.exitValue()
        } catch(Exception e) {
            return -1
        }
    }

    private void writeFiles() {
        def writeStatuses = EnumSet.of(FileStatus.CREATED, FileStatus.UPDATED)
        fileIndex.values().each {
            if (writeStatuses.contains(it.status)) {
                overwrite(it.task.destFilePath, it.task.content)
            }
        }
    }

    private void saveIndex() {
        Yaml yaml = new Yaml(new DumperOptions(
                defaultFlowStyle: DumperOptions.FlowStyle.BLOCK,
                lineBreak: lineBreakByCodeFormatter()
        ))
        def indexValue = [:]
        if (cfg.addLastGenerationTag) {
            indexValue.put('lastGeneration', generationDate)
        }
        indexValue.put('files', fileIndex.values()
                .findAll { it.status != FileStatus.ABANDONED }
                .sort { it.path }
                .collect {
                    [
                            path      : it.path,
                            status    : cfg.storeAnyKindOfStatusesInIndexFile || [FileStatus.DETACHED].contains(it.status) ? it.status.name() : null,
                            lastUpdate: !cfg.addLastUpdateTag ? null : [FileStatus.CREATED, FileStatus.UPDATED].contains(it.status) ? generationDate : it.lastUpdate ?: 'unknown',
                            md5       : cfg.forceMode ? null : it.task?.md5,
                    ].findAll { it.value != null }
                })
        overwrite(cfg.indexFile, yaml.dump(indexValue))
    }

    private LineBreak lineBreakByCodeFormatter() {
        for (def lb : LineBreak.values()) {
            if (Codegen.NEW_LINE == lb.getString()) {
                return lb
            }
        }
        LineBreak.getPlatformLineBreak()
    }

    private void loadIndex(FileStatus defaultStatus) {
        if (!existsIndexFile()) {
            log.warn("Index file ($cfg.indexFile.path) does not exists")
            return
        }
        Map index = new Yaml().load(cfg.indexFile.text)
        prevGenerationDate = index.lastGeneration
        index.files.each { Map<String, String> info ->
            def state = getFileState(info.path)
            def oldMd5 = cfg.forceMode ? state.md5 : info.md5
            // path auto-fix, codegen version before 0.6.1 had a bug and sometimes the relative path started with '/'
            // TODO [2021-07-19] this fix should be removed in some time
            def path = info.path.startsWith('/') ? info.path.substring(1) : info.path
            fileIndex.put(path, new CodeFile(
                    path: path,
                    lastUpdate: info.lastUpdate,
                    state: state,
                    status: !state.annotatedByGenerated ? FileStatus.DETACHED
                            : state.md5 != oldMd5 ? FileStatus.MALFORMED
                            : defaultStatus,
            ))
        }
    }

    private boolean existsIndexFile() {
        cfg.indexFile.exists()
    }

    private FileState getFileState(String relativeFilePath) {
        def file = getFile(relativeFilePath)
        boolean isJavaOrGroovy = file.name.endsWith('.java') || file.name.endsWith('.groovy')
        HashBuilder hashBuilder = new HashBuilder()
        FileState result = new FileState()
        result.exists = isFileExists(file)
        if (result.exists) {
            file.eachLine {
                if (isJavaOrGroovy && !result.annotatedByGenerated) {
                    def trimmed = it.trim()
                    if (trimmed.startsWith('@Generated')
                            || trimmed.startsWith('@javax.annotation.processing.Generated')
                            || trimmed.startsWith('@javax.annotation.Generated')) {
                        result.annotatedByGenerated = true
                    }
                }
                hashBuilder.addLine(it)
            }
        }
        result.md5 = hashBuilder.hash
        result
    }

    private boolean isFileExists(File file) {
        file.exists() && dirFiles.computeIfAbsent(file.getParentFile(), { it.list() as Set<String> })
                .contains(file.getName())
    }

    private File getFile(String relativeFilePath) {
        new File(cfg.baseDir, relativeFilePath)
    }

    private File overwrite(String relativeFilePath, String content) {
        overwrite(new File(cfg.baseDir, relativeFilePath), content)
    }

    private File overwrite(File file, String content) {
        file.delete()
        file.parentFile.mkdirs()
        file.createNewFile()
        file.write(content, cfg.charset)
        file
    }

    @ToString(includes = ['path', 'status'], includePackage = false)
    static class CodeFile {
        String path
        String lastUpdate
        FileState state
        FileStatus status
        CodeGenerationTask<?> task
    }

    @ToString
    static class FileState {
        boolean exists
        boolean annotatedByGenerated
        String md5
    }

    static enum FileStatus {
        CREATED,    // plik został utworzony przez generator
        UPDATED,    // plik został nadpisany przez generator
        UNCHANGED,  // plik nie uległ zmianie w wyniku uruchomienia generatora
        DETACHED,   // plik nie będzie nadpisywany przez generator (w java usunięcie adnotacji @Generated)
        ABANDONED,  // plik był, ale już nie jest generowany - do usunięcia
        MALFORMED,
        // suma kontrolna istniejącego pliku nie zgadza się z sumą kontrolną zapisaną w indeksie, przy czym plik nie został oznaczony jako DETACHED
    }

}
