/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package pl.metaprogramming.codegen.java.libs

import pl.metaprogramming.codegen.java.ClassCd

class Jackson {
    companion object {
        private val TYPE_REFERENCE =
            ClassCd.of("com.fasterxml.jackson.core.type.TypeReference").withUnknownGeneric()
        private val OBJECT_MAPPER = ClassCd.of("com.fasterxml.jackson.databind.ObjectMapper")

        @JvmStatic
        fun typeReference(classCd: ClassCd) = TYPE_REFERENCE.withGeneric(classCd)

        @JvmStatic
        fun objectMapper() = OBJECT_MAPPER
    }
}
