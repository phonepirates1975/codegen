/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package pl.metaprogramming.codegen.java.libs

import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.GENERIC_PARAM_UNKNOWN
import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.model.oas.OperationType

class Spring {
    companion object {
        private val MULTI_VALUE_MAP =
            ClassCd.of("org.springframework.util.MultiValueMap")
        private val LINKED_MULTI_VALUE_MAP =
            ClassCd.of("org.springframework.util.LinkedMultiValueMap")
                .withGeneric(GENERIC_PARAM_UNKNOWN, GENERIC_PARAM_UNKNOWN)
        private val HTTP_METHOD = ClassCd.of("org.springframework.http.HttpMethod")
        private val HTTP_HEADERS = ClassCd.of("org.springframework.http.HttpHeaders")

        private val KNOWN_HEADERS = mapOf(
            "authorization" to "AUTHORIZATION",
            "set-cookie" to "SET_COOKIE",
            "date" to "DATE",
            "etag" to "ETAG",
            "expires" to "EXPIRES",
            "last-modified" to "LAST_MODIFIED",
        )

        @JvmStatic
        fun linkedMultiValueMap() = LINKED_MULTI_VALUE_MAP

        @JvmStatic
        fun multiValueMap() = MULTI_VALUE_MAP

        @JvmStatic
        fun multiValueMap(keyType: ClassCd, valueType: ClassCd) = MULTI_VALUE_MAP.withGeneric(keyType, valueType)

        @JvmStatic
        fun httpMethod() = HTTP_METHOD

        @JvmStatic
        fun httpMethod(method: OperationType) = HTTP_METHOD.staticFieldExp(method.name)

        @JvmStatic
        fun httpHeaders(): ClassCd = HTTP_HEADERS

        @JvmStatic
        fun httpHeaders(name: String) =
            KNOWN_HEADERS[name.lowercase()]?.let { HTTP_HEADERS.staticFieldExp(it) } ?: ValueCm.escaped(name)
    }
}
