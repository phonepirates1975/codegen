/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

import java.util.*

/**
 * genericClass i genericParams są ze sobą powiązane.
 * genericClass to reprezentacja klasy szablonowej, a genericParams to jej parametry.
 * Wskazanie genericClass jest potrzebne tylko wtedy gdy jest to klasa generowana i
 * należy ją generować tylko wtedy gdy jest używana.
 */
internal class ClassCdImpl internal constructor(
    name: ClassName,
    override val kind: ClassKind = ClassKind.CLASS,
    genericClass: ClassCd? = null,
    override val genericParams: List<ClassCd>? = null,
    override val superClass: ClassCd? = null,
) : ClassDefBase(name, genericClass), ClassCd

open class ClassCm private constructor(
    name: ClassName,
    override var kind: ClassKind = ClassKind.CLASS,
    override var genericParams: MutableList<ClassCd>? = null,
    genericClass: ClassCd? = null,
    superClass: ClassCd? = null,
    val imports: MutableSet<String> = mutableSetOf(),
    val interfaces: MutableList<ClassCd> = mutableListOf(),
    val fields: MutableList<FieldCm> = mutableListOf(),
    val methods: MutableList<MethodCm> = mutableListOf(),
    val enumItems: MutableList<EnumItemCm> = mutableListOf(),
    val annotations: MutableList<AnnotationCm> = mutableListOf(),
    var description: String? = null,
    private val modifiers: MutableSet<ClassModifier> = EnumSet.noneOf(ClassModifier::class.java),
) : ClassDefBase(name, genericClass), ClassCd {

    constructor(packageName: String?, className: String) : this(ClassName(className, packageName))

    override var superClass: ClassCd? = superClass
        set(value) {
            field = value
            if (used) field?.markAsUsed()
        }

    override fun markAsUsed() {
        if (!used) {
            super.markAsUsed()
            interfaces.forEach { it.markAsUsed() }
            methods.forEach {
                if (!it.mapper) {
                    it.markAsUsed()
                }
            }
            fields.forEach { it.markAsUsed() }
        }
    }

    fun getMethodsToGenerate() = methods.filter { it.used }

    @JvmOverloads
    fun abstractModifier(set: Boolean = true) = modifiers.switch(ClassModifier.ABSTRACT, set)

    fun isAbstract() = modifiers.contains(ClassModifier.ABSTRACT)

    fun addGenericParams(vararg genericParams: ClassCd) = apply {
        if (this.genericParams != null) this.genericParams?.addAll(genericParams)
        else this.genericParams = genericParams.toMutableList()
    }

    fun addImport(importEntry: String) {
        if (importEntry.indexOf('.') <= 0) {
            throw IllegalArgumentException("Illegal import entry '$importEntry'")
        }
        imports.add(importEntry)
    }

    fun removeImport(importEntry: String) {
        imports.remove(importEntry)
    }

    fun addInterface(implementedInterface: ClassCd) {
        interfaces.add(implementedInterface)
        if (used) implementedInterface.markAsUsed()
    }

    fun addAnnotation(annotation: AnnotationCm) = apply {
        annotations.add(annotation)
    }

    fun addField(fieldCm: FieldCm): FieldCm {
        fields.find { it.name == fieldCm.name }?.let {
            throw IllegalStateException("field ${fieldCm.name} already exists")
        }
        fields.add(fieldCm)
        if (used) fieldCm.markAsUsed()
        return fieldCm
    }

    @JvmOverloads
    fun addField(name: String, type: ClassCd, builder: (FieldCm.() -> Unit) = {}) =
        addField(type.asField(name, builder))

    fun addMethod(methodCm: MethodCm) {
        methods.find { it.matchNameAndParams(methodCm) }?.let {
            throw IllegalStateException("Can't add method: $methodCm; is in conflict with $it")
        }
        methodCm.ownerClass = this
        methods.add(methodCm)
        if (used && !methodCm.mapper) methodCm.markAsUsed()
    }

    fun addMethod(name: String, builder: MethodCm.() -> Unit) = addMethod(MethodCm.of(name, builder))

    @JvmOverloads
    fun addEnumItem(item: String, sort: Boolean = false): EnumItemCm {
        if (!isEnum) {
            throw IllegalStateException("Class $className is not an enum")
        }
        val exists = enumItems.find { it.name == item }
        if (exists != null) {
            return exists
        }
        val itemCm = EnumItemCm(this, item)
        enumItems.add(itemCm)
        if (sort) enumItems.sortWith(compareBy { it.name })
        return itemCm
    }

    fun getField(schema: Any) = fields.find { it.model == schema }
        ?: throw IllegalStateException("No field found for: $schema, in class: $this")

    fun getMethod(name: String) = methods.find { it.name == name }
        ?: throw IllegalStateException("No $name method found in class: $this")
}

class EnumItemCm(
    var ownerClass: ClassCd,
    var name: String,
    var description: String? = null,
    var value: ValueCm? = null,
    var annotations: MutableList<AnnotationCm> = mutableListOf()
) {
    constructor(ownerClass: ClassCd, name: String) : this(ownerClass, name, null)

    fun value(value: String) = apply { this.value = ValueCm.escaped(value) }
    fun description(description: String?) = apply { this.description = description }

    fun collectDependencies(dependencies: MutableCollection<ClassCd>) {
        annotations.forEach { it.collectDependencies(dependencies) }
    }
}

class FieldCm internal constructor(
    var type: ClassCd,
    var name: String? = null,
    var value: ValueCm? = null,
    var annotations: MutableList<AnnotationCm>? = null,
    var model: Any? = null,
    var description: String? = null,
    private var accessModifier: AccessModifier = AccessModifier.PRIVATE,
    private val modifiers: MutableSet<FieldModifier> = EnumSet.noneOf(FieldModifier::class.java),
    private var nonnull: Boolean = false
) {
    companion object {
        @JvmStatic
        @JvmOverloads
        fun of(name: String, type: ClassCd, builder: FieldCm.() -> Unit = {}) = name.asField(type).apply(builder)

        @JvmStatic
        fun stringValue(value: String) = FieldCm(ClassCd.stringType(), null, ValueCm.escaped(value))
    }

    fun withModel(model: Any) = FieldCm(
        type,
        name,
        value,
        annotations?.toMutableList(),
        model,
        description,
        accessModifier,
        EnumSet.copyOf(modifiers),
        nonnull
    )

    fun alias(name: String) = FieldCm(type, name)

    fun collectDependencies(dependencies: MutableCollection<ClassCd>) {
        type.collectDependencies(dependencies)
        annotations?.forEach { it.collectDependencies(dependencies) }
    }

    fun markAsUsed() {
        type.markAsUsed()
        annotations?.forEach { it.markAsUsed() }
    }

    fun addAnnotation(annotation: AnnotationCm) = apply {
        if (annotations != null) annotations?.add(annotation)
        else annotations = mutableListOf(annotation)
    }

    fun hasAnnotation(annotationClass: String) =
        annotations?.find { it.annotationClass.canonicalName == annotationClass } != null

    fun assign(value: String) = apply { this.value = ValueCm.value(value) }

    fun assign(value: ValueCm) = apply { this.value = value }

    @JvmOverloads
    fun setNonnull(nonnull: Boolean = true) = apply { this.nonnull = nonnull }
    fun isNonnull() = nonnull

    fun setPublic() = apply { accessModifier = AccessModifier.PUBLIC }
    fun isPrivate() = accessModifier == AccessModifier.PRIVATE
    fun getAccessModifier() = accessModifier

    @JvmOverloads
    fun setFinal(set: Boolean = true) = apply { modifiers.switch(FieldModifier.FINAL, set) }
    fun isFinal() = modifiers.contains(FieldModifier.FINAL)

    @JvmOverloads
    fun setStatic(set: Boolean = true) = apply { modifiers.switch(FieldModifier.STATIC, set) }
    fun isStatic() = modifiers.contains(FieldModifier.STATIC)

    @JvmOverloads
    fun setTransient(set: Boolean = true) = apply { modifiers.switch(FieldModifier.TRANSIENT, set) }
    fun isTransient() = modifiers.contains(FieldModifier.TRANSIENT)

    override fun toString(): String {
        return "${name}: $type"
    }
}

class MethodCm(
    var ownerClass: ClassCd? = null,
    var ownerInterface: ClassCd? = null,
    val name: String,
    var resultType: ClassCd? = null,
    val params: MutableList<FieldCm> = mutableListOf(),
    var annotations: MutableList<AnnotationCm>? = null,
    var implBody: String? = null,
    val implDependencies: MutableList<ClassCd> = mutableListOf(),
    var description: String? = null,
    var used: Boolean = false,
    var mapper: Boolean = false,
    var accessModifier: AccessModifier = AccessModifier.PUBLIC,
    var throwExceptions: MutableList<ClassCd>? = null,
    private val modifiers: MutableSet<MethodModifier> = EnumSet.noneOf(MethodModifier::class.java),
) {
    companion object {
        @JvmStatic
        @JvmOverloads
        fun of(name: String, builder: MethodCm.() -> Unit = {}) = MethodCm(name = name).apply(builder)
    }

    fun isConstructor() = name == ownerClass?.className
    fun isPublic() = accessModifier == AccessModifier.PUBLIC
    fun isPrivate() = accessModifier == AccessModifier.PRIVATE

    @JvmOverloads
    fun staticModifier(set: Boolean = true) = apply { modifiers.switch(MethodModifier.STATIC, set) }
    fun isStatic() = modifiers.contains(MethodModifier.STATIC)

    fun privateModifier() = apply { accessModifier = AccessModifier.PRIVATE }

    fun matchNameAndParams(methodCm: MethodCm) = name == methodCm.name && paramTypes == methodCm.paramTypes

    val paramTypes: List<ClassCd> get() = params.map { it.type }

    fun setResultType(canonicalName: String) {
        resultType = ClassCd.of(canonicalName)
    }

    @JvmOverloads
    fun addParam(name: String, type: ClassCd, builder: FieldCm.() -> Unit = {}) =
        apply { params.add(FieldCm.of(name, type, builder)) }

    fun addParam(name: String, type: String) = apply { params.add(name.asField(ClassCd.of(type))) }
    fun addParam(param: FieldCm) = apply { params.add(param) }
    fun addParams(vararg params: FieldCm) = apply { this.params.addAll(params) }
    fun addParams(params: List<FieldCm>) = apply { this.params.addAll(params) }

    fun addRequiredParam(name: String, type: ClassCd) =
        apply { params.add(FieldCm.of(name, type) { addAnnotation(AnnotationCm.javaxNonnul()) }) }

    fun ownerClass(ownerClass: ClassCd) = apply { this.ownerClass = ownerClass }
    fun getOwner() = ownerInterface ?: ownerClass

    fun addThrow(exception: ClassCd) = apply {
        if (throwExceptions == null) {
            throwExceptions = mutableListOf()
        }
        throwExceptions!!.add(exception)
    }

    fun addThrow(exception: String) = addThrow(ClassCd.of(exception))

    fun markAsUsed() {
        if (!used) {
            used = true
            ownerClass?.markAsUsed()
            ownerInterface?.markAsUsed()
            resultType?.markAsUsed()
            params.forEach { it.markAsUsed() }
        }
    }

    fun annotations(annotations: MutableList<AnnotationCm>?) = apply { this.annotations = annotations }
    fun addAnnotation(annotation: AnnotationCm) = apply {
        if (annotations != null) annotations?.add(annotation)
        else annotations = mutableListOf(annotation)
    }

    fun description(description: String?) = apply { this.description = description }
    fun resultType(resultType: ClassCd?) = apply { this.resultType = resultType }
    fun implBody(implBody: String?) = apply { this.implBody = implBody }
    fun used(used: Boolean) = apply { this.used = used }

    fun addImplDependencies(vararg dependencies: ClassCd) = apply { implDependencies.addAll(dependencies) }
    fun collectDependencies(dependencies: MutableCollection<ClassCd>) {
        resultType?.collectDependencies(dependencies)
        params.forEach { it.collectDependencies(dependencies) }
        annotations?.forEach { it.collectDependencies(dependencies) }
        implDependencies.forEach { it.collectDependencies(dependencies) }
        throwExceptions?.forEach { it.collectDependencies(dependencies) }
    }
}


fun String.asField(type: ClassCd) = FieldCm(type, this)

/**
 * Represents literals or field/params references
 */
class ValueCm private constructor(
    val value: List<String?>,
    val escaped: Boolean,
    val array: Boolean = false,
    val itemPerLine: Boolean = false
) {

    companion object {
        @JvmStatic
        fun value(value: String?) = ValueCm(listOf(value), escaped = false)

        @JvmStatic
        fun escaped(value: String?) = ValueCm(listOf(value), escaped = true)

        @JvmStatic
        @JvmOverloads
        fun escapedArray(value: List<String>, itemPerLine: Boolean = false) =
            ValueCm(value, array = true, escaped = true, itemPerLine = itemPerLine)
    }

    override fun toString(): String =
        if (array) "{${value.joinToString { toString(it) }}}"
        else toString(value[0])

    fun toString(v: String?): String =
        if (v == null) "null"
        else if (escaped) "\"$v\""
        else v
}

fun <T> MutableSet<T>.switch(value: T, set: Boolean) {
    if (set) this.add(value) else this.remove(value)
}

enum class AccessModifier {
    PUBLIC,
    PRIVATE,
}

enum class FieldModifier {
    STATIC,
    FINAL,
    TRANSIENT
}

enum class MethodModifier {
    STATIC,
}

enum class ClassModifier {
    ABSTRACT
}

enum class ClassKind {
    CLASS,
    INTERFACE,
    ENUM,
    ARRAY,
    GENERIC_PARAM,
    UNKNOWN_GENERIC_PARAM,
}

internal class ClassName(
    val name: String,
    val classPackage: String? = null
) {
    companion object {
        @JvmStatic
        fun of(canonicalName: String): ClassName {
            val lastDot = canonicalName.lastIndexOf('.')
            return if (lastDot > 0) ClassName(
                canonicalName.substring(lastDot + 1, canonicalName.length),
                canonicalName.substring(0, lastDot)
            ) else ClassName(canonicalName)
        }
    }

    val canonicalName: String
        get() = if (classPackage == null) name else "${classPackage}.${name}"

}

sealed class ClassDefBase(
    private val name: ClassName,
    private val genericClass: ClassCd? = null,
    override var used: Boolean = false,
) : ClassCd {

    override val canonicalName: String by name::canonicalName
    override val packageName: String? by name::classPackage
    override val className: String by name::name

    override fun with(genericParams: List<ClassCd>?, superClass: ClassCd?, kind: ClassKind?): ClassCd = ClassCdImpl(
        name,
        kind ?: this.kind,
        if (genericParams != null) this else genericClass,
        genericParams ?: this.genericParams,
        superClass ?: this.superClass
    )

    override fun markAsUsed() {
        if (!used) {
            used = true
            superClass?.markAsUsed()
            genericClass?.markAsUsed()
            genericParams?.forEach { it.markAsUsed() }
        }
    }

    override fun equals(other: Any?): Boolean {
        return other is ClassCd && other.canonicalName == this.canonicalName
    }

    override fun hashCode(): Int {
        return canonicalName.hashCode()
    }

    override fun toString(): String {
        return if (isVoid) "void"
        else if (isArray) "$canonicalName[]"
        else if (genericParams?.isNotEmpty() == true) "$canonicalName<${genericParams?.joinToString(", ")}>"
        else canonicalName
    }
}
