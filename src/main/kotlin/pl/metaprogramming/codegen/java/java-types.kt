/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

val VOID = ClassCd.of("java.lang.Void")
val OBJECT = ClassCd.of("java.lang.Object")
val CLASS = ClassCd.of("java.lang.Class")
val GENERIC_PARAM_T = ClassCd.genericParam("T")
val GENERIC_PARAM_R = ClassCd.genericParam("R")
val GENERIC_PARAM_V = ClassCd.genericParam("V")
val GENERIC_PARAM_UNKNOWN: ClassCd = ClassCdImpl(ClassName("?"), kind = ClassKind.UNKNOWN_GENERIC_PARAM)
val STRING = ClassCd.of("java.lang.String")

val INTEGER = ClassCd.of("java.lang.Integer")
val INTEGER_PRIMITIVE = ClassCd.of("int")
val LONG = ClassCd.of("java.lang.Long")
val FLOAT = ClassCd.of("java.lang.Float")
val DOUBLE = ClassCd.of("java.lang.Double")
val BOOLEAN = ClassCd.of("java.lang.Boolean")
val BOOLEAN_PRIMITIVE = ClassCd.of("boolean")
val BIG_DECIMAL = ClassCd.of("java.math.BigDecimal")

val BYTE_ARRAY = ClassCd.of("byte").asArray()

val COLLECTION = ClassCd.of("java.util.Collection")
val LIST = ClassCd.of("java.util.List")
val MAP = ClassCd.of("java.util.Map")

interface ClassCd {
    val canonicalName: String
    val packageName: String?
    val className: String
    val genericParams: List<ClassCd>?
    val kind: ClassKind
    val superClass: ClassCd?
    val used: Boolean

    fun markAsUsed()

    fun collectDependencies(dependencies: MutableCollection<ClassCd>) {
        dependencies.add(this)
        genericParams?.forEach { it.collectDependencies(dependencies) }
    }

    fun isClass(canonicalName: String) = this.canonicalName == canonicalName
    val isVoid: Boolean get() = this == VOID
    val isArray: Boolean get() = kind == ClassKind.ARRAY
    val isList: Boolean get() = this == LIST
    val isMap: Boolean get() = this == MAP
    val isEnum: Boolean get() = kind == ClassKind.ENUM
    val isInterface: Boolean get() = kind == ClassKind.INTERFACE
    val isGenericParam: Boolean get() = kind == ClassKind.GENERIC_PARAM

    fun asArray() = with(kind = ClassKind.ARRAY)
    fun asCollection() = COLLECTION.withGeneric(this)
    fun asList() = LIST.withGeneric(this)
    fun asMapBy(keyType: ClassCd) = MAP.withGeneric(keyType, this)
    fun asMapTo(valueType: ClassCd) = MAP.withGeneric(this, valueType)
    fun asField(name: String?) = FieldCm(this, name)
    fun asField(name: String, builder: FieldCm.() -> Unit) = FieldCm(this, name).apply(builder)
    fun asField() = FieldCm(this)
    fun asExpression(expression: String) = FieldCm(this, "").assign(expression)

    fun withGeneric(genericParams: List<ClassCd>) = with(genericParams = genericParams)
    fun withGeneric(vararg genericParams: ClassCd) = with(genericParams = genericParams.toList())
    fun withUnknownGeneric() = withGeneric(GENERIC_PARAM_UNKNOWN)
    fun withSuper(superClass: ClassCd) = with(superClass = superClass)

    fun with(genericParams: List<ClassCd>? = null, superClass: ClassCd? = null, kind: ClassKind? = null): ClassCd

    fun constructExp() = ValueCm.value("new ${className}${if (genericParams == null) "" else "<>" }()")
    fun staticFieldExp(fieldName: String) = ValueCm.value("${className}.${fieldName}")

    companion object {
        @JvmStatic
        fun of(canonicalName: String): ClassCd {
            return if (canonicalName.endsWith("[]")) {
                ClassCdImpl(
                    ClassName.of(canonicalName.substring(0, canonicalName.length - 2)),
                    kind = ClassKind.ARRAY
                )
            } else if (canonicalName.endsWith('>')) {
                val idx1 = canonicalName.indexOf('<')
                val genericParams = canonicalName.substring(idx1 + 1, canonicalName.length - 1)
                    .split(",").map { of(it.trim()) }
                ClassCdImpl(ClassName.of(canonicalName.substring(0, idx1)), genericParams = genericParams)
            } else if (canonicalName.length == 1 && canonicalName[0].isUpperCase())
                genericParam(canonicalName)
            else
                ClassCdImpl(ClassName.of(canonicalName))
        }

        @JvmStatic
        fun of(canonicalName: String, genericParams: List<ClassCd>): ClassCd =
            ClassCdImpl(ClassName.of(canonicalName), genericParams = genericParams)


        @JvmStatic
        fun of(packageName: String, className: String): ClassCd =
            ClassCdImpl(ClassName(className, packageName))

        @JvmStatic
        fun genericParam(symbol: String): ClassCd = ClassCdImpl(ClassName(symbol), kind = ClassKind.GENERIC_PARAM)

        @JvmStatic
        fun genericParamT() = GENERIC_PARAM_T

        @JvmStatic
        fun genericParamR() = GENERIC_PARAM_R

        @JvmStatic
        fun genericParamV() = GENERIC_PARAM_V

        @JvmStatic
        fun genericParamUnknown() = GENERIC_PARAM_UNKNOWN

        @JvmStatic
        fun voidType() = VOID

        @JvmStatic
        fun objectType() = OBJECT

        @JvmStatic
        fun claasType(type: ClassCd) = CLASS.withGeneric(type)

        @JvmStatic
        fun stringType() = STRING

        @JvmStatic
        fun integerType() = INTEGER

        @JvmStatic
        fun integerPrimitiveType() = INTEGER_PRIMITIVE

        @JvmStatic
        fun longType() = LONG

        @JvmStatic
        fun floatType() = FLOAT

        @JvmStatic
        fun doubleType() = DOUBLE

        @JvmStatic
        fun booleanType() = BOOLEAN

        @JvmStatic
        fun booleanPrimitiveType() = BOOLEAN_PRIMITIVE

        @JvmStatic
        fun byteArrayType() = BYTE_ARRAY

        @JvmStatic
        fun bigDecimalType() = BIG_DECIMAL
    }
}
