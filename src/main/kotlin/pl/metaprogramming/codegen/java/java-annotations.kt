/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

val OVERRIDE = AnnotationCm.of("java.lang.Override")
val JAVAX_NONNULL = AnnotationCm.of("javax.annotation.Nonnull")
val JAVAX_NULLABLE = AnnotationCm.of("javax.annotation.Nullable")
val JAVAX_PARAMETERS_ARE_NONNULL_BY_DEFAULT = AnnotationCm.of("javax.annotation.ParametersAreNonnullByDefault")
val SUPPRESS_WARNINGS = AnnotationCm.of("java.lang.SuppressWarnings", mapOf("value" to ValueCm.escaped("unchecked")))

val LOMBOK_GETTER = AnnotationCm.of("lombok.Getter")
val LOMBOK_REQUIRED_ARGS_CONSTRUCTOR = AnnotationCm.of("lombok.RequiredArgsConstructor")

class AnnotationCm(
    val annotationClass: ClassCd,
    val params: Map<String, ValueCm>? = null,
    var dependencies: MutableList<ClassCd>? = null
) {

    companion object {
        @JvmStatic
        @JvmOverloads
        fun of(canonicalName: String, params: Map<String, ValueCm>? = null) =
            AnnotationCm(ClassCd.of(canonicalName), params)

        @JvmStatic
        fun override() = OVERRIDE

        @JvmStatic
        fun javaxNonnul() = JAVAX_NONNULL

        @JvmStatic
        fun javaxNullable() = JAVAX_NULLABLE

        @JvmStatic
        fun javaxParametersAreNonnullByDefault() = JAVAX_PARAMETERS_ARE_NONNULL_BY_DEFAULT

        @JvmStatic
        fun suppressWarnings() = SUPPRESS_WARNINGS

        @JvmStatic
        fun lombokGetter() = LOMBOK_GETTER

        @JvmStatic
        fun lombokRequiredArgsConstructor() = LOMBOK_REQUIRED_ARGS_CONSTRUCTOR

    }

    fun addDependency(classCd: ClassCd) = apply {
        if (dependencies == null) dependencies = mutableListOf(classCd)
        else dependencies?.add(classCd)
    }

    fun collectDependencies(dependencies: MutableCollection<ClassCd>) {
        annotationClass.collectDependencies(dependencies)
        dependencies.forEach { it.collectDependencies(dependencies) }
    }

    fun markAsUsed() {
        annotationClass.markAsUsed()
        dependencies?.forEach { it.markAsUsed() }
    }

}
