/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

interface JavaNameMapper {
    fun toClassName(text: String): String
    fun toConstantName(text: String): String
    fun toMethodName(text: String) = toClassName(text).replaceFirstChar { it.lowercaseChar() }
    fun toFieldName(text: String) = toMethodName(text)
}

open class DefaultJavaNameMapper(private val javaNameNumberPrefix: String = "v") : JavaNameMapper {
    protected fun splitWords(text: String): MutableList<String> {
        val result = mutableListOf<String>()
        val buf = StringBuilder()
        var newWord = true
        var prevChar: Char? = null
        var nextChar: Char?
        for (i in text.indices) {
            val c = text[i]
            nextChar = if (text.length > (i + 1)) text[i + 1] else null
            newWord = !c.isLetterOrDigit()
                    || (c.isUpperCase() && nextChar?.isLowerCase() == true)
                    || (c.isUpperCase() && !newWord && !(prevChar!!).isUpperCase())
            if (newWord && buf.isNotEmpty()) {
                result.add(buf.toString())
                buf.clear()
            }
            if (c.isLetterOrDigit()) buf.append(c)
            prevChar = c
        }
        if (buf.isNotEmpty()) result.add(buf.toString())
        return result
    }

    protected open fun toWords(text: String): MutableList<String> {
        val result = splitWords(text)
        if (result[0][0].isDigit()) {
            result.add(0, javaNameNumberPrefix)
        }
        return result
    }

    override fun toClassName(text: String): String =
        toWords(text).joinToString("") { word ->
            word.lowercase().replaceFirstChar { it.uppercaseChar() }
        }

    override fun toConstantName(text: String): String =
        toWords(text).joinToString("_") { word -> word.uppercase() }
}
