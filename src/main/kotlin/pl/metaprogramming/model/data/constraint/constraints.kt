/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.data.constraint

import org.slf4j.LoggerFactory

const val X_CONSTRAINTS = "x-constraints"
const val X_VALIDATIONS = "x-validations"
const val SPEC_RULE = "rule"
const val SPEC_DICT = "dictionary"
const val SPEC_CONDITION = "condition"
const val SPEC_ERROR_CODE = "error-code"
const val SPEC_PRIORITY = "priority"
const val SPEC_INVALID_PATTERN_CODE = "invalid-pattern-code"
const val SPEC_CHECKER_CLASS = "checker-class"
const val SPEC_CHECKER_STATIC_FIELD = "checker-static-field"
const val SPEC_CHECKER_METHOD_REF = "checker-method-ref"

const val FIELD_PTR = "field:"
const val DI_BEAN_PTR = "di-bean:"
const val VALIDATION_BEAN_PTR = "bean:"

class Constraints {
    companion object {
        @JvmStatic
        fun rule(ruleCode: String): RuleConstraint = RuleConstraint(ruleCode)

        @JvmStatic
        fun dictionary(dictCode: String): DictionaryConstraint = DictionaryConstraint(dictCode)

        @JvmStatic
        fun condition(expression: String): ConditionConstraint = ConditionConstraint(expression)
    }
}

abstract class DataConstraint(
    var priority: Int? = null,
    var errorCode: String? = null
) {
    fun setPriority(priority: Int?): DataConstraint {
        this.priority = priority
        return this
    }

    fun setErrorCode(errorCode: String?): DataConstraint {
        this.errorCode = errorCode
        return this
    }
}

class RuleConstraint(val ruleCode: String) : DataConstraint()
class DictionaryConstraint(val dictionaryCode: String) : DataConstraint()
class ConditionConstraint(val expression: String) : DataConstraint()

class CheckerRef internal constructor(
    val className: String,
    val methodRef: String? = null,
    val staticField: String? = null,
) : DataConstraint() {
    companion object {
        @JvmStatic
        fun of(className: String): CheckerRef = CheckerRef(className)

        @JvmStatic
        fun ofMethodRef(className: String, method: String): CheckerRef = CheckerRef(className, methodRef = method)

        @JvmStatic
        fun ofStaticField(className: String, field: String): CheckerRef = CheckerRef(className, staticField = field)
    }
}

class ValidationDirective(val pointer: ValidatorPointer) : DataConstraint()

class ConstraintsSupport(val additives: MutableMap<Any, Any>) {

    private val log = LoggerFactory.getLogger(ConstraintsSupport::class.java)

    var invalidPatternCode: String?
        get() {
            for (it in getAdditives(X_CONSTRAINTS)) {
                if (it is Map<*, *> && it.containsKey(SPEC_INVALID_PATTERN_CODE))
                    return it[SPEC_INVALID_PATTERN_CODE] as String
            }
            return null
        }
        set(value) {
            getAdditives(X_CONSTRAINTS).add(mutableMapOf(SPEC_INVALID_PATTERN_CODE to value))
        }

    fun add(constraint: DataConstraint) {
        val spec = mutableMapOf<String, Any?>()
        if (constraint is RuleConstraint) spec[SPEC_RULE] = constraint.ruleCode
        if (constraint is DictionaryConstraint) spec[SPEC_DICT] = constraint.dictionaryCode
        if (constraint is ConditionConstraint) spec[SPEC_CONDITION] = constraint.expression
        if (constraint is CheckerRef) {
            spec[SPEC_CHECKER_CLASS] = constraint.className
            spec.add(constraint.staticField, SPEC_CHECKER_STATIC_FIELD)
            spec.add(constraint.methodRef, SPEC_CHECKER_METHOD_REF)
        }
        spec.add(constraint.errorCode, SPEC_ERROR_CODE)
        spec.add(constraint.priority, SPEC_PRIORITY)
        getAdditives(X_CONSTRAINTS).add(spec)
    }

    private fun MutableMap<String, Any?>.add(value: Any?, key: String) {
        if (value != null) this[key] = value
    }

    fun getConstraints(): List<DataConstraint> = Collector().collect()

    private fun getAdditives(element: String): MutableList<Any> {
        if (additives[element] == null) {
            additives[element] = mutableListOf<Any>()
        }
        @Suppress("UNCHECKED_CAST")
        return additives[element] as MutableList<Any>
    }

    private inner class Collector {
        val result = mutableListOf<DataConstraint>()

        fun collect(): List<DataConstraint> {
            getAdditives(X_VALIDATIONS).forEach(this::addValidation)
            getAdditives(X_CONSTRAINTS).forEach(this::addConstraint)
            var priority = 100
            result.forEach {
                if (it.priority == null) it.priority = ++priority
            }
            return result
        }

        private fun addValidation(spec: Any) {
            if (spec is String) {
                if (spec.startsWith("compare:")) {
                    result.add(ConditionConstraint(spec.substring("compare:".length)))
                } else {
                    result.add(ValidationDirective(ValidatorPointer.fromExpression(spec)))
                }
            } else {
                log.error("Can't handle '$X_VALIDATIONS': $spec")
            }
        }

        private fun addConstraint(spec: Any) {
            val constraint = makeConstraint(spec)
            if (constraint != null && spec is Map<*, *>) {
                constraint.errorCode = spec[SPEC_ERROR_CODE] as String?
                constraint.priority = spec[SPEC_PRIORITY] as Int?
                result.add(constraint)
            } else {
                log.error("Can't handle '$X_CONSTRAINTS': $spec")
            }
        }

        private fun makeConstraint(spec: Any): DataConstraint? {
            if (spec is Map<*, *>) {
                if (spec.containsKey(SPEC_RULE)) {
                    return Constraints.rule(spec[SPEC_RULE] as String)
                } else if (spec.containsKey(SPEC_DICT)) {
                    return Constraints.dictionary(spec[SPEC_DICT] as String)
                } else if (spec.containsKey(SPEC_CONDITION)) {
                    return Constraints.condition(spec[SPEC_CONDITION] as String)
                } else if (spec.containsKey(SPEC_CHECKER_CLASS)) {
                    return CheckerRef(
                        className = spec[SPEC_CHECKER_CLASS] as String,
                        staticField = spec[SPEC_CHECKER_STATIC_FIELD] as String?,
                        methodRef = spec[SPEC_CHECKER_METHOD_REF] as String?,
                    )
                }
            }
            return null
        }
    }
}

class ValidatorPointer private constructor(
    val classCanonicalName: String,
    val staticField: String? = null,
    val method: String? = null,
    val isDiBean: Boolean = false,
) {
    companion object {
        @JvmStatic
        fun of(checkerRef: CheckerRef): ValidatorPointer = ValidatorPointer(
            classCanonicalName = checkerRef.className,
            method = checkerRef.methodRef,
            staticField = checkerRef.staticField,
            isDiBean = checkerRef.staticField == null,
        )

        @JvmStatic
        fun fromExpression(expression: String): ValidatorPointer {
            if (expression.startsWith(FIELD_PTR)) {
                val value = expression.substring(FIELD_PTR.length)
                val idx = value.lastIndexOf('.')
                return ValidatorPointer(
                    classCanonicalName = value.substring(0, idx),
                    staticField = value.substring(idx + 1)
                )
            } else if (expression.startsWith(DI_BEAN_PTR)) {
                val value = expression.substring((DI_BEAN_PTR.length)).split(':')
                return ValidatorPointer(
                    classCanonicalName = value[0],
                    method = if (value.size > 1) value[1] else null,
                    isDiBean = true
                )
            } else if (expression.startsWith(VALIDATION_BEAN_PTR)) {
                val value = expression.substring((VALIDATION_BEAN_PTR.length)).split(':')
                return ValidatorPointer(
                    classCanonicalName = value[0],
                    method = if (value.size > 1) value[1] else null,
                )
            }
            throw IllegalArgumentException("Invalid expression: $expression")
        }
    }
}