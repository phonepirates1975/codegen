/*
 * Copyright (c) 2020 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen

import groovy.util.logging.Slf4j

import java.time.Duration
import java.time.LocalDateTime
import java.util.concurrent.TimeUnit

import static java.util.Arrays.asList

@Slf4j
class BuildRunner {

    private static final def DEBUG = false

    static boolean run(ProcessBuilder processBuilder, long timeout = 540) {
        def task = "${processBuilder.command().join(' ')} (${processBuilder.directory().path})"
        def startTime = LocalDateTime.now()
        log.info "[$task] Start"
        def process = processBuilder.start()

        if (DEBUG) {
            process.waitForProcessOutput(System.out, System.err)
            def duration = Duration.between(startTime, LocalDateTime.now())
            log.info "[$task][$duration] Completed: ${process.exitValue()}"
            return process.exitValue() == 0
        }

        boolean exited = process.waitFor(timeout, TimeUnit.SECONDS)
        def duration = Duration.between(startTime, LocalDateTime.now())
        if (!exited || process.exitValue() != 0) {
            def status = exited ? "exitValue: ${process.exitValue()}" : 'TIMEOUT'
            log.info "[$task][$duration] Failed [$status]\n$process.text\n=================="
            false
        } else {
            log.info "[$task][$duration] Completed"
            true
        }
    }

    static ProcessBuilder gradle(File workingDir, String... tasks) {
        processBuilder(workingDir, [isWindows() ? 'gradlew.bat' : './gradlew'] + asList(tasks))
    }

    static ProcessBuilder mvn(File workingDir, String... tasks) {
        if (isWindows()) {
            processBuilder(workingDir, ['mvn.cmd'] + asList(tasks))
        } else {
            processBuilder(workingDir, ['mvn'] + asList(tasks))
        }
    }

    static ProcessBuilder npm(File workingDir, String task) {
        processBuilder(workingDir, [isWindows() ? 'npm.cmd' : 'npm', task])
    }

    static ProcessBuilder processBuilder(File workingDir, List<String> cmd) {
        new ProcessBuilder()
                .command(cmd)
                .directory(workingDir)
    }

    private static boolean isWindows() {
        System.getProperty('os.name').toLowerCase().contains('windows')
    }

}
