/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.testcontainers

class MavenContainer extends BuildContainer {

    MavenContainer(List<String> projects) {
        super(projects, 'maven:3-jdk-8', 'maven', 'maven-cache:/root/.m2/repository/')
    }

    boolean exec(String project, String subproject, String... tasks) {
        List<String> commands = ['mvn', '-f', "/projects/$project-maven/$subproject".toString()]
        tasks.each { commands.add(it) }
        exec(commands.toArray(new String[0]))
    }
}
