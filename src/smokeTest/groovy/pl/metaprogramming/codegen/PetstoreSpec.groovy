/*
 * Copyright (c) 2020 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen

import org.apache.commons.io.FileUtils
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator
import pl.metaprogramming.codegen.java.spring.SpringRestServiceGenerator
import pl.metaprogramming.model.oas.OpenapiParser
import spock.lang.Specification

import static pl.metaprogramming.codegen.BuildRunner.gradle
import static pl.metaprogramming.codegen.BuildRunner.run

class PetstoreSpec extends Specification {

    static final URL PETSTORE_API_URL = new URL('https://petstore.swagger.io/v2/swagger.yaml')
    static final File PETSTORE_API_FILE = new File('src/testFixtures/resources/swagger/petstore.yaml')
    static final File PROJECT_DIR = new File('build/petstore')

    def setupSpec() {
        FileUtils.copyURLToFile(PETSTORE_API_URL, PETSTORE_API_FILE)
        fixEndLineCharacter(PETSTORE_API_FILE)
        setupProject()
    }

    def "generate and compile"() {
        given:
        Codegen codegen = new Codegen()
                .setBaseDir(PROJECT_DIR)
                .addCommonModule(SpringCommonsGenerator.of {
                        it.setRootPackage('commons')
                })
                .addModule(SpringRestServiceGenerator.of(OpenapiParser.parse(PETSTORE_API_FILE.absolutePath)) {
                    it.setRootPackage('petstore')
                })
        when:
        def generator = codegen.generate()
        def files = generator.fileIndex.keySet()

        then:
        files.size() > 100
        isGenerated(files, 'Checker')
        isGenerated(files, 'CommonCheckers')
        !isGenerated(files, 'ErrorCodes')
        !isGenerated(files, 'DictionaryCodes')
        !isGenerated(files, 'DictionaryChecker')
        PROJECT_DIR.exists()
        PROJECT_DIR.isDirectory()
        run(gradle(PROJECT_DIR, 'classes'))
    }

    private boolean isGenerated(Set<String> files, String className) {
        files.find { it.endsWith("${className}.java") }
    }

    private void setupProject() {
        PROJECT_DIR.deleteDir()
        PROJECT_DIR.mkdir()
        setContent('build.gradle', BUILD_GRADLE_BODY)
        setContent('settings.gradle', "rootProject.name = 'petstore'")
        copy('gradle', 'gradlew', 'gradlew.bat')
    }

    private void setContent(String filename, String content) {
        def file = new File(PROJECT_DIR, filename)
        file.parentFile.mkdirs()
        file.write(content)
    }

    private void copy(String... files) {
        files.each {
            def src = new File(it)
            def dest = new File(PROJECT_DIR, it)
            if (src.isDirectory()) {
                FileUtils.copyDirectory(src, dest)
            } else {
                FileUtils.copyFile(src, dest)
            }
        }
    }

    private static final BUILD_GRADLE_BODY = '''
plugins {
    id 'org.springframework.boot' version '2.5.6'
    id 'io.spring.dependency-management' version '1.0.11.RELEASE'
    id 'java'
}

repositories {
    mavenCentral()
}

dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-web'
    implementation 'com.google.code.findbugs:findbugs:3.0.1'
    implementation 'commons-io:commons-io:2.11.0'
    compileOnly 'org.projectlombok:lombok:1.18.12'
    annotationProcessor 'org.projectlombok:lombok:1.18.12'
}
'''

    private void fixEndLineCharacter(File file) {
        def lines = file.readLines()
        file.withWriter { writer ->
            lines.each { line ->
                writer.println(line)
            }
        }
    }
}
