/*
 * Copyright (c) 2019 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen


import pl.metaprogramming.fixtures.TestProjectCodegenProvider
import spock.lang.Specification
import spock.lang.Timeout
import spock.lang.Unroll

import java.util.function.Function

import static pl.metaprogramming.codegen.BuildRunner.gradle
import static pl.metaprogramming.codegen.BuildRunner.run
import static pl.metaprogramming.fixtures.CodegenParamsProvider.*

@Timeout(600)
class TestProjectsSpec extends Specification {

    @Unroll
    def "generate and build: #testProject"() {
        given:
        def provider = new TestProjectCodegenProvider(testProject: testProject, params: params)
        when:
        def codegen = (factoryMethod as Function<TestProjectCodegenProvider, Codegen>).apply(provider)
        def genResult = codegen.generate()
        then:
        genResult != null
        run(gradle(projectDir(testProject), 'clean', task))

        where:
        testProject               | params                           | factoryMethod                  | task
        'rs-server-spring-1t'     | makeSpringRestServicesParams()   | { it.forRsServer() }           | 'build'
        'rs-server-spring-1t-jbv' | makeSpring1tJbvParams()          | { it.forRsServer() }           | 'build'
        'rs-server-spring-2t'     | makeSpringRestServices2tParams() | { it.forRsServerLegacyOas3() } | 'build'
        'rs-server-spring-legacy' | makeSpringRsLegacyParams()       | { it.forRsServerLegacyOas2() } | 'build'
        'rs-client-spring-1t'     | makeDefaultJavaParams(false)     | { it.forRsClient() }           | 'assemble'
        'rs-client-spring-2t'     | makeDefaultJavaParams()          | { it.forRsClientLegacy() }     | 'assemble'
        'ws-client-spring'        | makeWsClientParams()             | { it.forWsClient() }           | 'build'
    }

    def "REST clients should interact with #serverProject"() {
        given:
        def testTimeout = 60
        when:
        def server = ServerRunner.of(gradle(projectDir(serverProject), ':bootRun'))
        server.start()

        then:
        server.ready

        when:
        def rsClientSpring1tSuccess = run(gradle(projectDir('rs-client-spring-1t'), 'cleanTest', 'test'), testTimeout)
        def rsClientSpring2tSuccess = run(gradle(projectDir('rs-client-spring-2t'), 'cleanTest', 'test'), testTimeout)

        then:
        rsClientSpring1tSuccess && rsClientSpring2tSuccess

        cleanup:
        server.stop()

        where:
        serverProject << [
                'rs-server-spring-1t',
                'rs-server-spring-2t',
        ]
    }

    private File projectDir(String testProject) {
        new File('test-projects', testProject)
    }

}
