/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.fixtures

import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator
import pl.metaprogramming.codegen.java.spring.SpringRestClientGenerator
import pl.metaprogramming.codegen.java.spring.SpringRestServiceGenerator
import pl.metaprogramming.codegen.java.spring.SpringRestService2tGenerator
import pl.metaprogramming.codegen.java.spring.SpringRestClient2tGenerator
import pl.metaprogramming.codegen.java.spring.SpringSoapClientGenerator
import pl.metaprogramming.codegen.java.spring.rs.SpringRsTypeOfCode
import pl.metaprogramming.codegen.java.base.ClassCmBuildStrategy
import pl.metaprogramming.model.oas.Operation
import pl.metaprogramming.model.wsdl.WsdlParser
import pl.metaprogramming.model.wsdl.WsdlParserConfig

import static pl.metaprogramming.codegen.java.validation.ValidationTypeOfCode.*

class TestProjectCodegenProvider {
    def apiDepsModel = ExampleApiModels.loadExampleApiDepsModel()
    def apiModel = ExampleApiModels.loadExampleApiModel(apiDepsModel)
    String testProject
    CodegenParams params

    Codegen forRsServer() {
        makeCodegen()
                .addCommonModule(SpringCommonsGenerator.of(params) {
                    it.setRootPackage('example.commons')
                    it.setPackage('example.adapters.in.rest', REST_EXCEPTION_HANDLER)
                    it.setPackage('example.adapters.in.rest.validators.custom',
                            VALIDATION_DICTIONARY_CHECKER,
                            VALIDATION_DICTIONARY_CODES_ENUM,
                            VALIDATION_ERROR_CODES_ENUM)
                })
                .addModule(SpringRestServiceGenerator.of(apiDepsModel, params) {
                    it.setRootPackage('example')
                })
                .addModule(SpringRestServiceGenerator.of(apiModel, params) {
                    it.typeOfCode(SpringRsTypeOfCode.REQUEST_DTO) {
                        it.onDecoration { ClassCmBuildStrategy<Operation> builder ->
                            if (builder.model.requestSchema.field('authorizationParam')) {
                                builder.addInterfaces('example.process.IRequest')
                            }
                        }
                    }
                    it.setRootPackage('example')
                })
    }

    Codegen forRsClient() {
        makeCodegen()
                .addCommonModule(SpringCommonsGenerator.of(params) {
                    it.setRootPackage('example.commons')
                })
                .addModule(SpringRestClientGenerator.of(apiDepsModel, params) {
                    it.setRootPackage('example')
                })
                .addModule(SpringRestClientGenerator.of(apiModel, params) {
                    it.setRootPackage('example')
                })
    }

    Codegen forRsClientLegacy() {
        makeCodegen()
                .addCommonModule(SpringCommonsGenerator.of(params) {
                    it.setRootPackage('example.commons')
                })
                .addModule(SpringRestClient2tGenerator.of(apiDepsModel, params) {
                    it.setRootPackage('example.commons')
                })
                .addModule(SpringRestClient2tGenerator.of(apiModel, params) {
                    it.setRootPackage('example')
                })
    }

    Codegen forRsServerLegacyOas2() {
        forRsServerLegacy(ExampleApiModels.OpenapiVer.V2)
    }

    Codegen forRsServerLegacyOas3() {
        forRsServerLegacy(ExampleApiModels.OpenapiVer.V3)
    }

    Codegen forRsServerLegacy(def oasVer) {
        def apiDepsModel = ExampleApiModels.loadExampleApiDepsModel(oasVer)
        def apiModel = ExampleApiModels.loadExampleApiModel(apiDepsModel, oasVer)
        makeCodegen()
                .addCommonModule(SpringCommonsGenerator.of(params) {
                    it.setRootPackage('example.commons')
                    it.setPackage('example.adapters.in.rest', REST_EXCEPTION_HANDLER)
                })
                .addModule(SpringRestService2tGenerator.of(apiDepsModel, params) {
                    it.setRootPackage('example.commons')
                })
                .addModule(SpringRestService2tGenerator.of(apiModel, params) {
                    it.setRootPackage('example')
                })
    }

    Codegen forWsClient() {
        def wsMetaModel = WsdlParser.parse(
                'src/test/resources/wsdl/echo.wsdl',
                new WsdlParserConfig(serviceNameMapper: { it -> it.replace('PortService', '') })
        ).removeOperations { it.name == 'echoExtendedSkipped' }
        makeCodegen()
                .addCommonModule(SpringCommonsGenerator.of(params) {
                    it.setRootPackage('example.commons')
                })
                .addModule(SpringSoapClientGenerator.of(wsMetaModel, params) {
                    it.setRootPackage('example')
                    it.setNamespacePackage('http://example.com/echo', 'example.ws.ns1')
                    it.setNamespacePackage('http://example.com/echo1', 'example.ws.ns2')
                    it.setNamespacePackage('http://example.com/echo2', 'example.ws.ns3')
                })
    }

    private Codegen makeCodegen() {
        def baseDir = new File('test-projects/' + testProject)
        new Codegen()
                .addLastUpdateTag(false)
                .setBaseDir(baseDir)
                .setIndexFile(new File(baseDir, 'generatedFiles.yaml'))
    }


}
