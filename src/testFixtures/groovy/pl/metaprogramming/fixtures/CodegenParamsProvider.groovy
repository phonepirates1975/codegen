/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.fixtures

import org.jboss.forge.roaster.Roaster
import pl.metaprogramming.codegen.generator.CodeDecorator
import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.java.JavaModuleParams
import pl.metaprogramming.codegen.java.spring.SpringRestParams
import pl.metaprogramming.codegen.java.validation.ValidationParams
import pl.metaprogramming.codegen.java.spring.base.DiAutowiredBuildStrategy
import pl.metaprogramming.model.data.constraint.CheckerRef

class CodegenParamsProvider {

    static CodegenParams makeDefaultJavaParams(boolean formatCode = true) {
        new CodegenParams()
                .with(new JavaModuleParams()
                        .setCodeDecorators(formatCode ? [makeCodeFormatter()] : [])
                        .setGeneratedAnnotationClass('javax.annotation.Generated')
                )
                .with(makeValidationParams().setStopAfterFirstError(false))
    }

    static ValidationParams makeValidationParams() {
        new ValidationParams()
                .setFormat('amount', CheckerRef.of('example.commons.validator.AmountChecker'))
                .setRule('AUTHORIZATION_CONSTRAINT', 'example.adapters.in.rest.validators.AuthorizationChecker')
                .setRule('USER_AMOUNT', CheckerRef.ofMethodRef('example.adapters.in.rest.validators.UserDataValidationBean', 'checkAmountByUser'))
                .setRule('AMOUNT_SCALE', CheckerRef.ofStaticField('example.commons.validator.Checkers', 'AMOUNT_SCALE_CHECKER'))
                .setRule('EXTENDED_OBJECT_CONSTRAINT', 'example.adapters.in.rest.validators.ExtendedObjectChecker')
                .setRule('EXTENDED_OBJECT_ENUM_CONSTRAINT', 'example.adapters.in.rest.validators.ExtendedObjectEnumChecker')
    }

    static CodegenParams makeWsClientParams() {
        makeDefaultJavaParams()
                .update(JavaModuleParams, {
                    it.alwaysGenerateEnumFromValueMethod = true
                })
    }

    static CodegenParams makeSpringRestServices2tParams() {
        makeDefaultJavaParams(true)
                .update(ValidationParams) {
                    it.throwExceptionIfValidationFailed = false
                }
    }

    static CodegenParams makeSpringRsLegacyParams() {
        makeDefaultJavaParams()
                .with(new SpringRestParams(
                        controllerPerOperation: false,
                        staticFactoryMethodForRestResponse: false))
                .update(JavaModuleParams) {
                    it.diStrategy = new DiAutowiredBuildStrategy()
                }
                .update(ValidationParams) {
                    it.throwExceptionIfValidationFailed = false
                }
    }

    static CodegenParams makeSpringRestServicesParams() {
        makeDefaultJavaParams(false)
                .with(new SpringRestParams().injectBeanIntoRequest('context', 'example.process.Context'))
    }

    static CodegenParams makeSpring1tJbvParams() {
        makeDefaultJavaParams()
                .with(new ValidationParams().setUseJakartaBeanValidation(true))
                .with(new SpringRestParams().setDelegateToFacade(true))
    }

    static CodeDecorator makeCodeFormatter() {
        new CodeDecorator() {
            String apply(String code) {
                def props = new Properties()
                props.put('org.eclipse.jdt.core.formatter.tabulation.char', 'space')
                props.put('org.eclipse.jdt.core.formatter.lineSplit', '120')
                Roaster.format(props, code)
            }
        }
    }

}
